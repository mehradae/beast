/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.util.IntArray;
import edu.ucr.cs.bdlab.util.OperationUtil;
import edu.ucr.cs.bdlab.util.WritableExternalizable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.StringUtils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * An interface for spatially partitioning data into partitions.
 * @author Ahmed Eldawy
 *
 */
public interface SpatialPartitioner extends WritableExternalizable {
  Log LOG = LogFactory.getLog(SpatialPartitioner.class);

  @Target(ElementType.TYPE)
  @Retention(RetentionPolicy.RUNTIME)
  @interface Metadata {
    /**Whether this global indexer supports disjoint partitions or not*/
    boolean disjointSupported();

    /**The extension used with the global index. Also used for choosing this partitioner from command line.*/
    String extension();

    /**A short description for this partitioner*/
    String description();
  }

  /**
   * Annotates the required parameters of the {@link #construct(Summary, double[][], AbstractHistogram)} function in
   * implementation of this interface.
   */
  @Target(ElementType.PARAMETER)
  @Retention(RetentionPolicy.RUNTIME)
  @interface Required {}

  /**
   * Annotates the parameters of the {@link #construct(Summary, double[][], AbstractHistogram)} function that can be
   * used but are not required.
   */
  @Target(ElementType.PARAMETER)
  @Retention(RetentionPolicy.RUNTIME)
  @interface Preferred {}
  /**
   * The criterion used to decide the number of partitions
   */
  enum PartitionCriterion {
    /**A fixed number of partitions that are desired to be created*/
    FIXED,

    /**A desired size in bytes for each partition*/
    SIZE,

    /**A desired number of features in each partition regardless of their size in bytes*/
    COUNT,
  }

  /**
   * Initializes the partitioner before it is first constructed.
   * @param conf the environment configuration
   * @param disjoint set to true to produce disjoint partitions with probable replication of some features
   * @param pc the criterion that determines the number of partitions to create
   * @param value the value associated with the partitioning criteration
   */
  default void setup(Configuration conf, boolean disjoint, PartitionCriterion pc, long value) {}


  /**
   * Constructs the histogram using the given parameters. By default, only the summary parameter is provided
   * and the other two parameters {@code sample} and {@code histogram} are {@code null}. If any of these are needed,
   * the parameter should be annotated with {@link Required}. If any of them can be used to improve the results but
   * are not necessarily required, they can be annotated with {@link Preferred}.
   * @param summary the summary of the input
   * @param sample a sample points from the input
   * @param histogram a histogram of the input
   */
  void construct(Summary summary, double[][] sample, AbstractHistogram histogram);

  /**
   * Find all the overlapping partitions in an envelope (MBR).
   * @param mbr the MBR to find overlapping partitions
   * @param matchedPartitions an array to use for matched partitions
   */
  void overlapPartitions(Envelope mbr, IntArray matchedPartitions);
  
  /**
   * Returns the single partition that contains the center of the given envelope.
   * This method should return a value in the range [0, {@link #getPartitionCount()}[
   *
   * @param mbr the minimum bounding rectangle to find a partition
   * @return the ID of the best partition that the given MBR should be assigned to.
   */
  int overlapPartition(Envelope mbr);
  
  /**
   * Returns the minimum bounding rectangle (MBR) of the given partition.
   * @param partitionID the ID of the partition to get its MBR.
   * @param mbr output parameter for the MBR to avoid allocation of objects
   */
  void getPartitionMBR(int partitionID, Envelope mbr);
  
  /**
   * Returns total number of partitions
   * @return the total number of partitions
   */
  int getPartitionCount();

  /**Whether this partitioner is configured to produce disjoint partitions*/
  boolean isDisjoint();

  /**
   * Number of dimensions for this partitioner
   * @return the dimensions of the partitioner
   */
  int getCoordinateDimension();

  default Metadata getMetadata() {
    return this.getClass().getAnnotation(Metadata.class);
  }

  /**
   * Reads the most recent master file from the given path.
   * @param fileSystem the file system that contains the index
   * @param indexPath the path to the index (not the master file)
   * @return an array of partitions from the file. If no master file is found, a {@code null} is returned.
   * @throws IOException if an error happened while reading the file
   */
  static PartitionInfo[] readMasterFile(FileSystem fileSystem, Path indexPath) throws IOException {
    Path masterFilePath = SpatialInputFormat.getMasterFilePath(fileSystem, indexPath);
    List<PartitionInfo> partitions = new ArrayList<>();
    MasterFileReader reader = new MasterFileReader();
    try {
      Configuration conf = new Configuration();
      conf.setBoolean(SpatialInputFormat.ImmutableObjects, true);
      reader.initialize(masterFilePath, conf);
      while (reader.nextKeyValue()) {
        partitions.add(reader.getCurrentValue());
      }
      return partitions.toArray(new PartitionInfo[partitions.size()]);
    } finally {
      reader.close();
    }
  }

  /**
   * Returns the MBR of the underlying dataset on which the partitioner was created.
   * @return the envelope of the entire partitioner
   */
  Envelope getEnvelope();
}
