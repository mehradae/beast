/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.io.FeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;

/**
 * Reads features from an R-tree-indexed file.
 */
@FeatureReader.Metadata(
    description = "An R-tree locally indexed file for efficient range retrieval",
    shortName = "rtree",
    extension = ".rtree"
)
public class RTreeFeatureReader extends FeatureReader {

  /**A mutable key for all records*/
  protected Envelope key;

  /**A single mutable feature to scan over the input*/
  protected Feature value;

  /**Hadoop environment configuration*/
  private Configuration conf;

  protected Iterator<Feature> results;

  /**A flag that indicates that we should create a new object for each record*/
  protected boolean immutableObjects;

  @Override
  public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
    this.conf = context.getConfiguration();
    this.immutableObjects = conf.getBoolean(SpatialInputFormat.ImmutableObjects, false);
    FileSplit fsplit = (FileSplit) split;

    // Open the input file and read the header of the stored features
    FileSystem fs = fsplit.getPath().getFileSystem(conf);
    FSDataInputStream in = fs.open(fsplit.getPath());
    in.seek(fsplit.getStart());
    value = new Feature();
    value.readHeader(in);

    // Now, either read the entire file, or filter based on the MBR
    String filterMBRStr = conf.get(SpatialInputFormat.FilterMBR);
    RTreeGuttman.Deserializer<Feature> featureDeserializer = input -> {
      if (immutableObjects) {
        Feature value2 = new Feature();
        value2.copyAttributeMetadata(value);
        value = value2;
      }
      value.readValue(input);
      return value;
    };
    // Get the tree length by subtracting the Feature header size
    long treeLength = (fsplit.getStart() + fsplit.getLength()) - in.getPos();
    if (filterMBRStr != null) {
      // Filter based on the MBR
      String[] parts = filterMBRStr.split(",");
      assert parts.length % 1 == 0; // It has to be an even number
      int numDimensions = parts.length / 2;
      double[] minCoord = new double[numDimensions];
      double[] maxCoord = new double[numDimensions];
      for (int d$ = 0; d$ < numDimensions; d$++) {
        minCoord[d$] = Double.parseDouble(parts[d$]);
        maxCoord[d$] = Double.parseDouble(parts[numDimensions + d$]);
      }
      results = RTreeGuttman.search(in, treeLength, minCoord, maxCoord, featureDeserializer).iterator();
    } else {
      results = RTreeGuttman.readAll(in, treeLength, featureDeserializer).iterator();
    }
  }

  @Override
  public boolean nextKeyValue() {
    if (results.hasNext()) {
      value = results.next();
      if (key == null)
        key = new Envelope(value.getGeometry().getCoordinateDimension());
      value.getGeometry().envelope(key);
      return true;
    }
    return false;
  }

  @Override
  public Envelope getCurrentKey() {
    return key;
  }

  @Override
  public Feature getCurrentValue() {
    return value;
  }

  @Override
  public float getProgress() throws IOException {
    return results instanceof RTreeGuttman.DiskSearchIterator?
        ((RTreeGuttman.DiskSearchIterator<Feature>) results).getProgress() : 0.1f;
  }

  @Override
  public void close() throws IOException {
    if (results != null)
      ((Closeable)results).close();
  }
}
