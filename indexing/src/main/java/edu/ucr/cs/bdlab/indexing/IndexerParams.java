package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.OperationUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class IndexerParams {
  /**The type of the global index (partitioner)*/
  @OperationParam(
      description = "The type of the global index",
      required = true
  )
  public static final String GlobalIndex = "gindex";

  /**Whether to build a disjoint index (with no overlapping partitions)*/
  @OperationParam(
      description = "Build a disjoint index with no overlaps between partitions",
      defaultValue = "false"
  )
  public static final String DisjointIndex = "disjoint";

  /**The size of the synopsis used to summarize the input before building the index*/
  @OperationParam(
      description = "The size of the synopsis used to summarize the input, e.g., 1024, 10m, 1g",
      defaultValue = "10m"
  )
  public static final String SynopsisSize = "synopsissize";

  /**A flag to increase the load balancing by using the histogram with the sample, if possible*/
  @OperationParam(
      description = "Set this option to combine the sample with a histogram for accurate load balancing",
      defaultValue = "false"
  )
  public static final String BalancedPartitioning = "balanced";



  /**The criterion used to calculate the number of partitions*/
  @OperationParam(
      description = "The criterion used to compute the number of partitions. It can be one of:\n" +
          "\tFixed(n): Create a fixed number of partitions (n partitions)\n" +
          "\tSize(s): Create n partitions such that each partition contains around s bytes\n" +
          "\tCount(c): Create n partitions such that each partition contains around c records",
      defaultValue = "Size(128m)"
  )
  public static final String PartitionCriterionThreshold = "pcriterion";
  /**Configuration names to store the partitioner into the distributed cache of Hadoop*/
  public static final String PartitionerClass = "Partitioner.Class";
  public static final String PartitionerValue = "Partitioner.Value";

  /**A table of all the partitioners available*/
  public static final Map<String, Class<? extends SpatialPartitioner>> partitioners = loadPartitioners();

  /**
   * Returns a map of all partitioners supported by the system.
   * @return a map from a short partitioner name to the partitioner class.
   */
  public static Map<String, Class<? extends SpatialPartitioner>> getPartitioners() {
    return partitioners;
  }

  public static void printUsage(PrintStream out) {
    out.println("The available indexes are:");
    for (String indexerName : partitioners.keySet()) {
      SpatialPartitioner.Metadata indexerMetadata = partitioners.get(indexerName).getAnnotation(SpatialPartitioner.Metadata.class);
      out.printf("- %s: %s\n", indexerName, indexerMetadata.description());
    }
  }

  /**
   * Compute number of partitions for a partitioner given the partitioning criterion and the summary of
   * the dataset.
   * @param pCriterion the criterion that determines the number of partitions
   * @param pValue the value associated with the partitioning criterion
   * @param summary the summary of the dataset
   * @return the preferred number of partitions
   */
  public static int computeNumberOfPartitions(SpatialPartitioner.PartitionCriterion pCriterion, long pValue, Summary summary) {
    int numPartitions;
    switch (pCriterion) {
      case FIXED:
        numPartitions = (int) pValue;
        break;
      case COUNT:
        numPartitions = (int) Math.ceil((double) summary.numFeatures / pValue);
        break;
      case SIZE:
        numPartitions = (int) Math.ceil((double)summary.size / pValue);
        break;
      default:
        throw new RuntimeException("Unsupported criterion: " + pCriterion);
    }
    return numPartitions;
  }

  /**
   * Stores the given partitioner to the distributed cache of Hadoop. This should be used when writing the index to
   * the output to give {@link IndexOutputFormat} access to the partitioner.
   * @param hadoopConf the hadoop configuration to write the partitioner in
   * @param partitioner the partitioner instance
   * @throws IOException if an error happens while writing the file
   */
  public static void savePartitionerToHadoopConfiguration(Configuration hadoopConf, SpatialPartitioner partitioner) throws IOException {
    Path tempFile;
    FileSystem fs = FileSystem.get(hadoopConf);
    do {
      tempFile = new Path("spatialPartitioner_"+(int)(Math.random()*1000000));
    } while (fs.exists(tempFile));
    DataOutputStream out = fs.create(tempFile);
    partitioner.write(out);
    out.close();

    fs.deleteOnExit(tempFile);

    hadoopConf.setClass(PartitionerClass, partitioner.getClass(), SpatialPartitioner.class);
    hadoopConf.set(PartitionerValue, tempFile.toString());
  }

  /**
   * Retrieves the value of a partitioner for a given job.
   * @param hadoopConf the hadoop configuration to read the partitioner from
   * @return an instance of the partitioner
   */
  public static SpatialPartitioner readPartitionerFromHadoopConfiguration(Configuration hadoopConf) {
    Class<? extends SpatialPartitioner> klass = hadoopConf.getClass(PartitionerClass, SpatialPartitioner.class, SpatialPartitioner.class);
    if (klass == null)
      throw new RuntimeException("PartitionerClass is not set in Hadoop configuration");
    try {
      SpatialPartitioner partitioner = klass.newInstance();

      Path partitionerFile = new Path(hadoopConf.get(PartitionerValue));
      DataInputStream in = partitionerFile.getFileSystem(hadoopConf).open(partitionerFile);
      partitioner.readFields(in);
      in.close();
      return partitioner;
    } catch (InstantiationException e) {
      throw new RuntimeException("Error instantiating partitioner", e);
    } catch (IllegalAccessException e) {
      throw new RuntimeException("Error instantiating partitioner", e);
    } catch (IOException e) {
      throw new RuntimeException("Error retrieving partitioner value", e);
    }
  }

  /**
   * Load all the partitioners defined in the configuration file.
   * @return a map from short partitioner name to its class.
   */
  public static Map<String, Class<? extends SpatialPartitioner>> loadPartitioners() {
    // We use tree map to have them sorted alphabetically. This helps when using this map to print usage
    Map<String, Class<? extends SpatialPartitioner>> partitioners = new TreeMap<>();
    List<String> partitionerClasses = OperationUtil.readConfigurationXML("beast.xml").get("SpatialPartitioners");
    for (String partitionerClassName : partitionerClasses) {
      try {
        Class<? extends SpatialPartitioner> partitionerClass =
            Class.forName(partitionerClassName).asSubclass(SpatialPartitioner.class);
        SpatialPartitioner.Metadata metadata = partitionerClass.getAnnotation(SpatialPartitioner.Metadata.class);
        if (metadata == null) {
          SpatialPartitioner.LOG.warn(String.format("Skipping partitioner '%s' without a valid Metadata annotation", partitionerClass.getName()));
        } else {
          partitioners.put(metadata.extension(), partitionerClass);
        }
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return partitioners;
  }
}
