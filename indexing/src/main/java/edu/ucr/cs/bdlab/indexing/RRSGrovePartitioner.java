package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.util.OperationParam;

/**
 * A spatial partitioner that uses the RR*-Grove partitioning algorithm. Simply, it applies the RR*-tree node splitting
 * method iteratively until each partition contains betweem [m, M] records.
 */
@SpatialPartitioner.Metadata(
    disjointSupported = true,
    extension = "rrsgrove",
    description = "A partitioner that uses the RR*-tree node splitting algorithm on a sample of points to partition the space"
)
public class RRSGrovePartitioner extends RSGrovePartitioner {
  @OperationParam(
      description = "The desired ratio between the minimum and maximum partitions sizes ]0,1[",
      defaultValue = "0.95",
      required = false
  )
  public static final String MMRatio = "mmratio";

  protected Envelope[] partitionPoints(double[][] coords, int max, int min) {
    return RRStarTree.partitionPoints(coords, min, max, true, fractionMinSplitSize, aux);
  }
}
