/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.io.CSVFeature;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.CounterOutputStream;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;


public class RTreeFeatureWriterTest extends SparkTest {

  public void testWrite() throws IOException, InterruptedException {
    IFeature features[] = {
        new CSVFeature(new Point(1.0, 2.4), new String[] {"abc", "def"}, ',', null),
        new CSVFeature(new Point(3.0, 4.4), new String[] {"abcc", "deff"}, ',', null),
        new CSVFeature(new Point(5.0, 6.4), new String[] {"abbc", "deef"}, ',', null),
        new CSVFeature(new Point(7.0, 8.4), new String[] {"aabc", "ddef"}, ',', null),
    };
    RTreeFeatureWriter writer = new RTreeFeatureWriter();
    Path rtreePath = new Path(scratchPath, "test.rtree");
    Configuration conf = new Configuration();
    writer.initialize(rtreePath, conf);
    for (IFeature f : features) {
      writer.write(null, f);
    }
    writer.close(null);

    // Now, read the data back
    FileSystem fs = rtreePath.getFileSystem(conf);
    long fileLength = fs.getFileStatus(rtreePath).getLen();
    FSDataInputStream in = fs.open(rtreePath);
    Feature f = new Feature();
    f.readHeader(in);
    int rtreeSize = (int) (fileLength - in.getPos());

    RTreeGuttman rtree = new RTreeGuttman(4, 8);
    rtree.readFields(in, rtreeSize, input -> null);
    Iterable<RTreeGuttman.Entry> results = rtree.search(new double[]{0, 0}, new double[]{4, 5});
    int resultCount = 0;
    for (Object o : results)
      resultCount++;
    assertEquals(2, resultCount);
    rtree.close();
  }

  public void testEstimateSize() throws IOException {
    IFeature features[] = {
        new CSVFeature(new Point(1.0, 2.4), new String[] {"abc", "def"}, ',', null),
        new CSVFeature(new Point(3.0, 4.4), new String[] {"abcc", "deff"}, ',', null),
        new CSVFeature(new Point(5.0, 6.4), new String[] {"abbc", "deef"}, ',', null),
        new CSVFeature(new Point(7.0, 8.4), new String[] {"aabc", "ddef"}, ',', null),
    };
    RTreeFeatureWriter writer = new RTreeFeatureWriter();
    Configuration conf = new Configuration();
    writer.initialize(new CounterOutputStream(), conf);
    long size = 0;
    for (IFeature f : features) {
      size += writer.estimateSize(f);
    }
    writer.close(null);
    // No actual assert is needed. Just make sure that it did not fail
    assertTrue(size > 0);
  }
}