/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;

public class SpatialPartitionerTest extends SparkTest {

  public void testComputeNumberOfPartitions() {
    Summary s = new Summary();
    s.set(new double[]{-1.0, -2.0}, new double[] {2.0, 4.0});
    s.size = 10L * 1024 * 1024 * 1024; // 10 GB
    s.numFeatures = 10000000; // 10M records ~> 1 KB / record
    assertEquals(10, IndexerParams.computeNumberOfPartitions(
        SpatialPartitioner.PartitionCriterion.FIXED, 10, s));
    assertEquals(80, IndexerParams.computeNumberOfPartitions(
        SpatialPartitioner.PartitionCriterion.SIZE, 128L*1024*1024, s));
    assertEquals(100, IndexerParams.computeNumberOfPartitions(
        SpatialPartitioner.PartitionCriterion.COUNT, 100000, s));
  }

  public void testReadMasterFile() throws IOException {
    Path masterFilePath = new Path(scratchPath, "test/_master.grid");
    copyResource("/test_index/_master.grid", new File(masterFilePath.toString()));
    FileSystem fileSystem = masterFilePath.getFileSystem(new Configuration());
    PartitionInfo[] partitions = SpatialPartitioner.readMasterFile(fileSystem, masterFilePath.getParent());
    assertEquals(2, partitions.length);
    assertEquals(2, partitions[0].getRecordCount());
    assertEquals(2, partitions[0].getRecordCount());
    assertEquals(2, partitions[1].partitionId);
    assertEquals(new Envelope(2, 0.0, 0.0, 2.0, 2.0), partitions[0]);
    assertEquals("part-11.csv", partitions[1].filename);
  }
}