package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.stsynopses.Summary;
import org.apache.hadoop.io.Writable;

public class WritableSummary extends Summary implements Writable {

  public WritableSummary() {}

  public WritableSummary(Summary s) {
    super(s);
  }

}
