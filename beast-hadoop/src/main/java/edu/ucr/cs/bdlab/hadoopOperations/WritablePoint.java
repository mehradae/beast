package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.geolite.Point;
import org.apache.hadoop.io.Writable;

public class WritablePoint extends Point implements Writable {
  public WritablePoint(int coordinateDimension) {
    super(coordinateDimension);
  }

  public WritablePoint() {}
}
