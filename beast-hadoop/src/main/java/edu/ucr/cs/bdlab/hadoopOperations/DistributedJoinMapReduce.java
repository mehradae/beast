/*
 * Copyright 2019 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.cg.SpatialJoinAlgorithms;
import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.BinaryInputFormat;
import edu.ucr.cs.bdlab.io.BlockFilter;
import edu.ucr.cs.bdlab.io.JoinFilter;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.LocalJobRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * MapReduce implementation of the distribution join algorithm. This algorithm uses partition boundaries to find pairs
 * of overlapping partitions. Each pair is assigned to a mapper that finds the overlapping records in this partition
 * and writes them directly to the output. No reduce stage is needed.
 */
public class DistributedJoinMapReduce {
  private static final Log LOG = LogFactory.getLog(DistributedJoinMapReduce.class);

  @OperationParam(description = "Simplification threshold. Maximum number of points per geometry",
      defaultValue = "100", showInUsage = true)
  public static final String SimplificationThreshold = "threshold";

  /**
   * The map function takes a list of IFeature iterables and output a list of IFeatures. The input list contains
   * one iterable per input and the output contains one array of features for each output record. For spatial join,
   * both input and output arrays will always be of size two. This map function assumes that the input features are
   * immutable; i.e., objects are not reused. This allows this function to store all input records in a list and uses
   * this list to compute the spatial join in memory before wilting the result to the output. This can be ensured
   * by setting the parameter {@link edu.ucr.cs.bdlab.io.SpatialInputFormat#ImmutableObjects} to {@code true}.
   * This function also needs access to the MBR of each partition boundary to apply the reference point duplicate
   * avoidance technique. This is done by locating the input split from the map context.
   */
  public static class DJMap extends Mapper<Envelope[], Iterable<? extends IFeature>[], Object, IFeature> {

    protected int threshold;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      threshold = context.getConfiguration().getInt(SimplificationThreshold, 100);
    }

    @Override
    protected void map(Envelope[] mbrs, Iterable<? extends IFeature>[] value, Context context) {
      assert value.length == 2 : String.format("Spatial join can only work with two inputs not %d inputs", value.length);
      int numInputs = value.length;
      List<IFeature>[] inputs = new List[numInputs];

      // Store all features
      for (int iInput = 0; iInput < numInputs; iInput++) {
        inputs[iInput] = new ArrayList<>();
        for (IFeature feature : value[iInput])
          inputs[iInput].add(feature);
      }

      final Envelope refPointMBR = new Envelope(mbrs[0]);
      refPointMBR.shrink(mbrs[1]);

      Feature result = new Feature(EmptyGeometry.instance);
      long t1 = System.nanoTime();
      SpatialJoinAlgorithms.SJResult refineStep;
      SpatialJoinAlgorithms.ESJPredicate predicate = UserOptions.getEnumIgnoreCase(context.getConfiguration(),
          SpatialJoinMapReduce.SpatialJoinPredicate, SpatialJoinAlgorithms.ESJPredicate.Intersects);
      switch (predicate) {
        case Intersects:
          refineStep = (i, j, refPoint) -> {
            if (refPointMBR.containsPoint(refPoint)) {
              IFeature f1 = inputs[0].get(i);
              IFeature f2 = inputs[1].get(j);
              if (f1.getGeometry().intersects(f2.getGeometry())) {
                result.clearAttributes();
                result.setGeometry(new GeometryCollection(f1.getGeometry(), f2.getGeometry()));
                for (int iAttr = 0; iAttr < f1.getNumAttributes(); iAttr++)
                  result.appendAttribute(f1.getAttributeName(iAttr), f1.getAttributeValue(iAttr));
                for (int iAttr = 0; iAttr < f2.getNumAttributes(); iAttr++)
                  result.appendAttribute(f2.getAttributeName(iAttr), f2.getAttributeValue(iAttr));
                try {
                  context.write(NullWritable.get(), result);
                } catch (IOException | InterruptedException e) {
                  throw new RuntimeException("Error writing the output", e);
                }
              }
            } else {
              // Signal the progress to avoid timeout exceptions
              context.progress();
            }
          };
          break;
        case MBRIntersects:
          refineStep = (i, j, refPoint) -> {
            if (refPointMBR.containsPoint(refPoint)) {
              IFeature f1 = inputs[0].get(i);
              IFeature f2 = inputs[1].get(j);
              result.clearAttributes();
              result.setGeometry(new GeometryCollection(f1.getGeometry(), f2.getGeometry()));
              for (int iAttr = 0; iAttr < f1.getNumAttributes(); iAttr++)
                result.appendAttribute(f1.getAttributeName(iAttr), f1.getAttributeValue(iAttr));
              for (int iAttr = 0; iAttr < f2.getNumAttributes(); iAttr++)
                result.appendAttribute(f2.getAttributeName(iAttr), f2.getAttributeValue(iAttr));
              try {
                context.write(NullWritable.get(), result);
              } catch (IOException | InterruptedException e) {
                throw new RuntimeException("Error writing the output", e);
              }
            } else {
              // Signal the progress to avoid timeout exceptions
              context.progress();
            }
          };
          break;
        case Contains:
          refineStep = (i, j, refPoint) -> {
            // Apply duplicate avoidance
            if (refPointMBR.containsPoint(refPoint)) {
              IFeature f1 = inputs[0].get(i);
              IFeature f2 = inputs[1].get(j);
              if (f1.getGeometry().contains(f2.getGeometry())) {
                result.clearAttributes();
                result.setGeometry(f2.getGeometry());
                for (int iAttr = 0; iAttr < 1 && iAttr < f1.getNumAttributes(); iAttr++)
                  result.appendAttribute(f1.getAttributeName(iAttr), f1.getAttributeValue(iAttr));
                for (int iAttr = 0; iAttr < 1 && iAttr < f2.getNumAttributes(); iAttr++)
                  result.appendAttribute(f2.getAttributeName(iAttr), f2.getAttributeValue(iAttr));
                try {
                  context.write(NullWritable.get(), result);
                } catch (IOException | InterruptedException e) {
                  throw new RuntimeException("Error writing the output", e);
                }
              }
            } else {
              // Signal the progress to avoid timeout exceptions
              context.progress();
            }
          };
          break;
        default:
          throw new RuntimeException(String.format("Unsupported predicate '%s'", predicate));
      }

      int resultCount = SpatialJoinAlgorithms.spatialJoinIntersectsPlaneSweepFeatures(inputs[0], inputs[1], refineStep);
      long t2 = System.nanoTime();
      LOG.info(String.format("Finished plane-sweep in %f seconds and found %d results (before refinement)",
          (t2-t1)*1E-9, resultCount));
    }
  }

  /**
   * Computes the spatial join between the given two inputs and stores the result in the given output path.
   * @param in1
   * @param in2
   * @param output
   * @param opts
   */
  public static void DJMapReduce(Path in1, Path in2, Path output, UserOptions opts)
      throws IOException, ClassNotFoundException, InterruptedException {
    Configuration conf = opts.loadIntoHadoopConfiguration(null);
    conf.setBoolean(SpatialInputFormat.ImmutableObjects, true);
    conf.setClass(SpatialInputFormat.BlockFilterClass, JoinFilter.class, BlockFilter.class);
    conf.setInt(LocalJobRunner.LOCAL_MAX_MAPS, Runtime.getRuntime().availableProcessors());

    Job job = Job.getInstance(conf, "DJ");
    job.setJarByClass(DistributedJoinMapReduce.class);

    // Set input and output options
    job.setInputFormatClass(BinaryInputFormat.class);
    BinaryInputFormat.setInputPaths(job, in1, in2);
    job.setOutputFormatClass(TextOutputFormat.class);
    TextOutputFormat.setOutputPath(job, output);

    // Set mapper class for the map-only job
    job.setMapperClass(DJMap.class);
    job.setNumReduceTasks(0);

    job.waitForCompletion(true);
    if (!job.isSuccessful())
      throw new RuntimeException(String.format("Job %s finished", job.getJobID()));
  }
}
