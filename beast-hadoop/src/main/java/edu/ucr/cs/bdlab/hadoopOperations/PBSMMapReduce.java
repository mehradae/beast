/*
 * Copyright 2019 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.cg.SpatialJoinAlgorithms;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.indexing.GridPartitioner;
import edu.ucr.cs.bdlab.indexing.IndexerParams;
import edu.ucr.cs.bdlab.indexing.SpatialPartitioner;
import edu.ucr.cs.bdlab.io.SpatialFileSplit;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.util.IntArray;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.Parallel;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.LocalJobRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * MapReduce implementation of the Partition-based Spatial Merge join, a.k.a. SJMR.
 * Rather than using only a uniform grid, this class supports using any disjoint partitioner.
 */
public class PBSMMapReduce {
  private static final Log LOG = LogFactory.getLog(PBSMMapReduce.class);

  /**This configuration can be used to override the default grid dimension*/
  @OperationParam(description = "The grid dimension used with PBSM", defaultValue = "number of reduce tasks",
    showInUsage = false)
  public static final String PBSMGridDimension = "PBSM.GridDimension";

  @OperationParam(description = "Number of reduce tasks", defaultValue = "1", showInUsage = false)
  public static final String NumReduceTasks = "NumReducers";

  @OperationParam(description = "Number of cores to use on the machine", defaultValue = "Runtime.getRuntime().availableProcessors()", showInUsage = false)
  public static final String NumCores = "NumCores";

  @OperationParam(description = "Simplification threshold. Maximum number of points per geometry",
      defaultValue = "100", showInUsage = true)
  public static final String SimplificationThreshold = "threshold";

  /**
   * A map function that maps each feature to a single or multiple overlapping partitions.
   */
  static class SpatialPartitionerMap extends Mapper<Object, IFeature, IntWritable, IFeature> {
    /**The spatial partitioner created and stored*/
    protected SpatialPartitioner partitioner;

    /**The output partition ID*/
    protected IntWritable partitionId;

    /**A temporary array to hold the list of aoverlapping partitions to an object*/
    protected IntArray tempPartitions;

    /**The MBR of a geometry*/
    protected Envelope geometryMBR;

    /**A feature used to write the output*/
    protected Feature outFeature;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      partitioner = IndexerParams.readPartitionerFromHadoopConfiguration(context.getConfiguration());
      assert partitioner.isDisjoint();
      partitionId = new IntWritable();
      tempPartitions = new IntArray();
    }

    @Override
    public void run(Context context) throws IOException, InterruptedException {
      setup(context);
      outFeature = new WritableFeature();
      int inputIndex = ((SpatialFileSplit)context.getInputSplit()).getIndex();
      outFeature.appendAttribute("inputIndex", inputIndex);
      try {
        while (context.nextKeyValue()) {
          map(context.getCurrentKey(), context.getCurrentValue(), context);
        }
      } finally {
        cleanup(context);
      }
    }

    @Override
    protected void map(Object dummy, IFeature feature, Context context) throws IOException, InterruptedException {
      if (geometryMBR == null)
        geometryMBR = new Envelope(feature.getGeometry().getCoordinateDimension());
      feature.getGeometry().envelope(geometryMBR);
      // To create disjoint partitions, we have to replicate to all overlapping partitions
      partitioner.overlapPartitions(geometryMBR, tempPartitions);
      outFeature.setGeometry(feature.getGeometry());
      for (int i : tempPartitions) {
        partitionId.set(i);
        context.write(partitionId, outFeature);
      }
    }
  }

  /**
   * The reduce function takes all records in one partition and runs the plane-sweep join algorithm in it.
   */
  static class PBSMReduce extends Reducer<IntWritable, IFeature, NullWritable, IFeature> {

    /**The spatial partitioner created and stored*/
    protected SpatialPartitioner partitioner;

    protected int threshold;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      Configuration conf = context.getConfiguration();
      partitioner = IndexerParams.readPartitionerFromHadoopConfiguration(conf);
      threshold = conf.getInt(SimplificationThreshold, 100);
    }

    @Override
    protected void reduce(IntWritable key, Iterable<IFeature> values, Context context) throws IOException {
      List<IFeature>[] inputs = new ArrayList[] {new ArrayList<IFeature>(), new ArrayList<IFeature>()};

      // Create a dummy output stream for duplicating geometries
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      DataOutputStream dos = new DataOutputStream(baos);
      for (IFeature v : values) {
        int inputIndex = (Integer) v.getAttributeValue(0);
        baos.reset();
        v.getGeometry().write(dos);
        dos.flush();
        IGeometry newGeom = v.getGeometry().getType().createInstance();
        try (DataInputStream din = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()))) {
          newGeom.readFields(din);
          inputs[inputIndex].add(new Feature(newGeom));
        }
      }

      int numInputs = inputs.length;

      // Retrieve the MBRs of all the partitions to apply the duplicate avoidance technique
      Envelope refPointMBR = new Envelope(partitioner.getCoordinateDimension());
      partitioner.getPartitionMBR(key.get(), refPointMBR);
      final Feature result = new Feature();

      // Prepare the refine step based on the predicate
      long t1 = System.nanoTime();
      SpatialJoinAlgorithms.SJResult refineStep;

      SpatialJoinAlgorithms.ESJPredicate predicate = UserOptions.getEnumIgnoreCase(context.getConfiguration(),
          SpatialJoinMapReduce.SpatialJoinPredicate, SpatialJoinAlgorithms.ESJPredicate.Intersects);
      switch (predicate) {
        case Intersects:
          refineStep = (i, j, refPoint) -> {
            if (refPointMBR.containsPoint(refPoint)) {
              IFeature f1 = inputs[0].get(i);
              IFeature f2 = inputs[1].get(j);
              if (f1.getGeometry().intersects(f2.getGeometry())) {
                result.clearAttributes();
                result.setGeometry(f2.getGeometry());
                for (int iAttr = 0; iAttr < 1 && iAttr < f1.getNumAttributes(); iAttr++)
                  result.appendAttribute(f1.getAttributeName(iAttr), f1.getAttributeValue(iAttr));
                for (int iAttr = 0; iAttr < 1 && iAttr < f2.getNumAttributes(); iAttr++)
                  result.appendAttribute(f2.getAttributeName(iAttr), f2.getAttributeValue(iAttr));
                try {
                  context.write(NullWritable.get(), result);
                } catch (IOException | InterruptedException e) {
                  throw new RuntimeException("Error writing the output", e);
                }
              }
            } else {
              // Signal the progress to avoid timeout exceptions
              context.progress();
            }
          };
          break;
        case MBRIntersects:
          refineStep = (i, j, refPoint) -> {
            if (refPointMBR.containsPoint(refPoint)) {
              IFeature f1 = inputs[0].get(i);
              IFeature f2 = inputs[1].get(j);
              result.clearAttributes();
              result.setGeometry(new GeometryCollection(f1.getGeometry(), f2.getGeometry()));
              for (int iAttr = 0; iAttr < f1.getNumAttributes(); iAttr++)
                result.appendAttribute(f1.getAttributeName(iAttr), f1.getAttributeValue(iAttr));
              for (int iAttr = 0; iAttr < f2.getNumAttributes(); iAttr++)
                result.appendAttribute(f2.getAttributeName(iAttr), f2.getAttributeValue(iAttr));
              try {
                context.write(NullWritable.get(), result);
              } catch (IOException | InterruptedException e) {
                throw new RuntimeException("Error writing the output", e);
              }
            } else {
              // Signal the progress to avoid timeout exceptions
              context.progress();
            }
          };
          break;
        case Contains:
          refineStep = (i, j, refPoint) -> {
            if (refPointMBR.containsPoint(refPoint)) {
              IFeature f1 = inputs[0].get(i);
              IFeature f2 = inputs[1].get(j);
              if (f1.getGeometry().contains(f2.getGeometry())) {
                result.clearAttributes();
                result.setGeometry(f2.getGeometry());
                for (int iAttr = 0; iAttr < f1.getNumAttributes(); iAttr++)
                  result.appendAttribute(f1.getAttributeName(iAttr), f1.getAttributeValue(iAttr));
                for (int iAttr = 0; iAttr < f2.getNumAttributes(); iAttr++)
                  result.appendAttribute(f2.getAttributeName(iAttr), f2.getAttributeValue(iAttr));
                try {
                  context.write(NullWritable.get(), result);
                } catch (IOException | InterruptedException e) {
                  throw new RuntimeException("Error writing the output", e);
                }
              }
            } else {
              // Signal the progress to avoid timeout exceptions
              context.progress();
            }
          };
          break;
        default:
          throw new RuntimeException(String.format("Unsupported predicate '%s'", predicate));
      }
      int resultCount = SpatialJoinAlgorithms.spatialJoinIntersectsIndexFeatures(inputs[0], inputs[1], refineStep);
      long t2 = System.nanoTime();
      LOG.info(String.format("Finished R-tree join in cell #%d in %f seconds and found %d results (before refinement)",
          key.get(), (t2-t1)*1E-9, resultCount));
    }
  }


  /**
   * Computes the spatial join between the given two inputs and stores the result in the given output path.
   * @param in1
   * @param in2
   * @param output
   * @param opts
   */
  public static void PBSMMapReduce(Path in1, Path in2, Path output, UserOptions opts)
      throws IOException, ClassNotFoundException, InterruptedException {
    int numCores = opts.getInt(NumCores, Runtime.getRuntime().availableProcessors());
    LOG.info(String.format("Using %d cores", numCores));
    // Compute the MBRs of the input
    Path[] ins = new Path[]{in1, in2};
    final Summary[] inputMBRs = new Summary[ins.length];
    Parallel.forEach(ins.length, (i1, i2) -> {
      for (int i = i1; i < i2; i++) {
        try {
          inputMBRs[i] = SummaryMapReduce.computeMapReduce(ins[i], opts);
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
      return null;
    });
    Summary intersectionMBR = inputMBRs[0];
    for (int i = 1; i < inputMBRs.length; i++) {
      intersectionMBR.shrink(inputMBRs[i]);
      intersectionMBR.size += inputMBRs[i].size;
      intersectionMBR.numFeatures += inputMBRs[i].numFeatures;
    }

    // Prepare the mapreduce job for the PBSM
    int numReduceTasks = opts.getInt(NumReduceTasks,
        Math.max(new JobClient(new JobConf()).getClusterStatus().getMaxReduceTasks() - 1, 1));
    Configuration conf = opts.loadIntoHadoopConfiguration(null);
    int gridDimension = conf.getInt(PBSMGridDimension, numReduceTasks);
    LOG.info(String.format("Using a grid of dimensions %dx%d", gridDimension, gridDimension));
    SpatialPartitioner gridPartitioner = new GridPartitioner(intersectionMBR, new int[] {gridDimension, gridDimension});
    IndexerParams.savePartitionerToHadoopConfiguration(conf, gridPartitioner);

    conf.setInt(LocalJobRunner.LOCAL_MAX_MAPS, numCores);
    conf.setInt(LocalJobRunner.LOCAL_MAX_REDUCES, numCores);

    Job job = Job.getInstance(conf, "PBSM");
    job.setJarByClass(PBSMMapReduce.class);

    // Set input and output options
    job.setInputFormatClass(SpatialInputFormat.class);
    SpatialInputFormat.setInputPaths(job, in1, in2);
    job.setOutputFormatClass(TextOutputFormat.class);
    TextOutputFormat.setOutputPath(job, output);

    // Set mapper and reducer classes for the map-reduce job
    job.setMapperClass(SpatialPartitionerMap.class);
    job.setMapOutputKeyClass(IntWritable.class);
    job.setMapOutputValueClass(WritableFeature.class);
    job.setReducerClass(PBSMReduce.class);
    job.setNumReduceTasks(numReduceTasks * numReduceTasks);

    job.waitForCompletion(true);
    if (!job.isSuccessful())
      throw new RuntimeException(String.format("Job %s finished", job.getJobID()));
  }
}
