/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.indexing.IndexOutputFormat;
import edu.ucr.cs.bdlab.io.CSVFeature;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.util.OperationMetadata;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.LocalJobRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

@OperationMetadata(
    shortName =  "rq",
    description = "Runs a range query that finds all records within a region",
    inputArity = "1",
    outputArity = "1",
    inheritParams = {SpatialInputFormat.class, SpatialOutputFormat.class})
public class RangeQueryMapReduce implements CLIOperation {
  /**Logger for this class*/
  private static final Log LOG = LogFactory.getLog(RangeQueryMapReduce.class);

  /**The type of the global index (partitioner)*/
  @OperationParam(
      description = "The envelope (rectangle) range to search, e.g., 'x1,y1,x2,y2' for a 2D envelope",
      required = true
  )
  public static final String QueryRange = "range";

  /**
   * A map function that maps each feature to a single or multiple overlapping partitions.
   */
  static class RangeFilterMap extends Mapper<Object, IFeature, NullWritable, IFeature> {
    /**The MBR to search*/
    protected Envelope searchMBR;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      Configuration conf = context.getConfiguration();
      String[] strCoords = conf.get(QueryRange).split(",");
      double[] coords = new double[strCoords.length];
      for (int i = 0; i < strCoords.length; i++)
        coords[i] = Double.parseDouble(strCoords[i]);
      searchMBR = new Envelope(coords.length/2, coords);
    }

    @Override
    protected void map(Object dummy, IFeature feature, Context context) throws IOException, InterruptedException {
      if (searchMBR.intersects(feature.getGeometry()))
        context.write(NullWritable.get(), feature);
    }
  }

  /**
   * Searches all the ojects that overlap a given rectangular query range
   * @param in
   * @param out
   * @param conf
   */
  public static Job rangeQueryMapReduce(Path in, Path out, Configuration conf) throws IOException, InterruptedException, ClassNotFoundException {
    // Create the job
    conf.setInt(LocalJobRunner.LOCAL_MAX_MAPS, Runtime.getRuntime().availableProcessors());
    Job job = Job.getInstance(conf, "RangeQuery");
    job.setJarByClass(RangeQueryMapReduce.class);

    // Set input details
    job.setInputFormatClass(SpatialInputFormat.class);
    SpatialInputFormat.addInputPath(job, in);
    job.getConfiguration().set(SpatialInputFormat.FilterMBR, conf.get(QueryRange));

    // Set mapper details
    job.setMapperClass(RangeFilterMap.class);
    job.setMapOutputKeyClass(NullWritable.class);
    // TODO infer the output value class from the input format, i.e., CSVFeature for csv files and DBFFeature for shapefiles
    job.setMapOutputValueClass(CSVFeature.class);

    // This is a map-only job
    job.setNumReduceTasks(0);

    // Set output details
    job.setOutputFormatClass(SpatialOutputFormat.class);
    IndexOutputFormat.setOutputPath(job, out);

    // Start the job and wait for completion
    job.waitForCompletion(true);

    return job;
  }

  @Override
  public void run(UserOptions clo) throws IOException, InterruptedException {
    try {
      rangeQueryMapReduce(new Path(clo.getInput()), new Path(clo.getOutput()), clo);
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
}
