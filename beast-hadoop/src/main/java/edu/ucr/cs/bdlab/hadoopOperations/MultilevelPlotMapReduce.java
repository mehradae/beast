/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.davinci.Canvas;
import edu.ucr.cs.bdlab.davinci.CommonVisualizationHelper;
import edu.ucr.cs.bdlab.davinci.FlattenFeatures;
import edu.ucr.cs.bdlab.davinci.MultilevelPyramidPlotHelper;
import edu.ucr.cs.bdlab.davinci.Plotter;
import edu.ucr.cs.bdlab.davinci.PyramidOutputFormat;
import edu.ucr.cs.bdlab.davinci.PyramidPartitioner;
import edu.ucr.cs.bdlab.davinci.SubPyramid;
import edu.ucr.cs.bdlab.davinci.TileIndex;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.CSVEnvelopeDecoder;
import edu.ucr.cs.bdlab.io.CSVEnvelopeEncoder;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.util.FileUtil;
import edu.ucr.cs.bdlab.util.LongArray;
import edu.ucr.cs.bdlab.util.OperationMetadata;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.ClusterStatus;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.LocalJobRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Create a pyramid plot for an input file
 */
@OperationMetadata(
    shortName =  "mplot",
    description = "Plots the input file as a multilevel pyramid image",
    inputArity = "1",
    outputArity = "1",
    inheritParams = {SpatialInputFormat.class,
        CommonVisualizationHelper.class, MultilevelPyramidPlotHelper.class}
)
public class MultilevelPlotMapReduce implements CLIOperation {
  private static final Log LOG = LogFactory.getLog(MultilevelPlotMapReduce.class);

  /**The path to the path that contains the histogram*/
  public static final String HistogramPath = "Pyramid.HistogramPath";

  /**The range of sizes in which image tiles should be generated. Format is min...max = [min, max[*/
  public static final String ImageTileSizeRange = "Pyramid.ImageTileRange";

  @OperationParam(
      description = "Overwrite the output if it already exists {true, false}.",
      defaultValue = "false"
  )
  public static final String OverwriteOutput = "overwrite";

  /**The deepest level on which we can use flat partitioning*/
  public static final String MaxLevelForFlatPartitioning = "MaxLevelFlatPartitioning";

  public static class FlatPartitioningMap extends Mapper<Object, IFeature, LongWritable, Canvas> {
    @Override
    public void run(Context context) throws IOException, InterruptedException {
      setup(context);
      Configuration conf = context.getConfiguration();
      Plotter plotter = Plotter.createAndConfigurePlotter(conf);
      Envelope featuresMBR = CSVEnvelopeDecoder.instance.apply(conf.get("mbr"), null);
      int tileWidth = conf.getInt(MultilevelPyramidPlotHelper.TileWidth, 256);
      int tileHeight = conf.getInt(MultilevelPyramidPlotHelper.TileHeight, 256);
      Map<Long, Canvas> partialTiles = new HashMap<>();
      MultilevelPyramidPlotHelper.Range levels = MultilevelPyramidPlotHelper.getRange(conf,
          MultilevelPyramidPlotHelper.NumLevels, "7");
      // Create a pyramid partitioner that covers the region of interest and only creates image tiles
      String imageTileSizeRangeStr = conf.get(ImageTileSizeRange);
      AbstractHistogram histogram;
      PyramidPartitioner pp;
      if (imageTileSizeRangeStr == null) {
        // No need for a histogram. Create a full pyramid partitioner
        SubPyramid regionOfInterest = new SubPyramid(featuresMBR, levels.min, levels.max);
        pp = new PyramidPartitioner(regionOfInterest);
      } else {
        String[] parts = imageTileSizeRangeStr.split("\\.\\.");
        long minSizeImageTile = Long.parseLong(parts[0]);
        long maxSizeImageTile = Long.parseLong(parts[1]);
        Path histogramPath = new Path(conf.get(HistogramPath));
        FileSystem histogramFileSystem = histogramPath.getFileSystem(conf);
        histogram = HistogramMapReduce.readHistogram(histogramFileSystem, histogramPath);
        SubPyramid regionOfInterest = new SubPyramid(featuresMBR, levels.min, levels.max);
        pp = new PyramidPartitioner(regionOfInterest, histogram, minSizeImageTile, maxSizeImageTile);
      }

      try {
        Iterable<? extends IFeature> features = new ValuesIterable<>(context);
        if (conf.getBoolean(CommonVisualizationHelper.FlattenFeatures, false))
          features = new FlattenFeatures(features);
        MultilevelPyramidPlotHelper.createTiles(features, featuresMBR, pp, tileWidth, tileHeight, plotter, partialTiles);
        LongWritable key = new LongWritable();
        // Write all the tiles to the output
        for (Map.Entry<Long, Canvas> partialTile : partialTiles.entrySet()) {
          key.set(partialTile.getKey());
          context.write(key, partialTile.getValue());
        }
      } finally {
        cleanup(context);
      }
    }
  }

  /**
   * Flat partitioning reduce. Merges partial canvases into one and write it to the output.
   */
  public static class FlatPartitioningReduce extends Reducer<LongWritable, Canvas, Long, Canvas> {
    /**The configured plotter to plot image tiles*/
    Plotter plotter;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      plotter = Plotter.createAndConfigurePlotter(context.getConfiguration());
    }

    @Override
    protected void reduce(LongWritable tileID, Iterable<Canvas> partialTiles, Context context) throws IOException, InterruptedException {
      Canvas finalCanvas = null;
      for (Canvas partialCanvas : partialTiles) {
        if (finalCanvas == null)
          finalCanvas = plotter.createCanvas(partialCanvas.getWidth(), partialCanvas.getHeight(), partialCanvas.getInputMBR());
        plotter.merge(finalCanvas, partialCanvas);
      }
      context.write(tileID.get(), finalCanvas);
    }
  }

  /**
   * Pyramid partitioning map. Partitions the features to tiles based on the configuration.
   */
  public static class PyramidPartitioningMap extends Mapper<Envelope, IFeature, LongWritable, IFeature> {
    /**Pyramid partitioner for image tiles, based on size*/
    PyramidPartitioner imageTilePP;

    /**A temporary LongArray to find the overlapping tiles for each feature*/
    final LongArray tiles = new LongArray();

    /**A temporary Envelope to compute the MBR of each feature*/
    final Envelope featureMBR = new Envelope(2);

    /**A temporary LongWritable to write the keys*/
    final LongWritable tileID = new LongWritable();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      // Create the pyramid partitioner based on the configuration
      Configuration conf = context.getConfiguration();
      Envelope inputMBR = CSVEnvelopeDecoder.instance.apply(conf.get("mbr"), null);
      MultilevelPyramidPlotHelper.Range levels = MultilevelPyramidPlotHelper.getRange(conf,
          MultilevelPyramidPlotHelper.NumLevels, "7");
      String imageTileSizeRangeStr = conf.get(ImageTileSizeRange);
      AbstractHistogram histogram = null;
      if (conf.get(HistogramPath) != null) {
        Path histogramPath = new Path(conf.get(HistogramPath));
        FileSystem histogramFileSystem = histogramPath.getFileSystem(conf);
        histogram = HistogramMapReduce.readHistogram(histogramFileSystem, histogramPath);
      }

      int granularity = conf.getInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);

      SubPyramid pyramid = new SubPyramid(inputMBR, levels.min, levels.max);
      if (imageTileSizeRangeStr == null) {
        // Full partitioning. Consider all sizes
        imageTilePP = new PyramidPartitioner(pyramid);
      } else {
        // Limit the size to the given range
        String[] parts = imageTileSizeRangeStr.split("\\.\\.");
        long minSizeImageTile = Long.parseLong(parts[0]);
        long maxSizeImageTile = Long.parseLong(parts[1]);
        imageTilePP = new PyramidPartitioner(pyramid, histogram, minSizeImageTile, maxSizeImageTile);
      }
      imageTilePP.setGranularity(granularity);
    }

    @Override
    public void run(Context context) throws IOException, InterruptedException {
      setup(context);
      try {
        Iterable<? extends IFeature> features = new ValuesIterable<>(context);
        if (context.getConfiguration().getBoolean(CommonVisualizationHelper.FlattenFeatures, false))
          features = new FlattenFeatures(features);
        for (IFeature feature : features) {
          feature.getGeometry().envelope(featureMBR);
          imageTilePP.overlapPartitions(featureMBR, tiles);
          for (Long tid : tiles) {
            tileID.set(tid);
            context.write(tileID, new WritableFeature(feature));
          }
        }
      } finally {
        cleanup(context);
      }
    }
  }

  /**
   * Reduce function for the pyramid partitioning algorithm. It takes a tile and a set of all features in this tile.
   * If the tile is image tile, it plots it. If the tile is data tile, it writes all the features to the output.
   */
  public static class FineGrainedPyramidPartitioningReduce extends Reducer<LongWritable, IFeature, Long, Canvas> {
    Plotter plotter;

    /**The input MBR to compute the MBR of each tile*/
    Envelope featuresMBR;

    /**Tile width and height to plot the tiles*/
    int tileWidth, tileHeight;

    /**The histogram is used to classify tiles*/
    AbstractHistogram histogram;

    /**Data tile threshold to classify tiles*/
    long dataTileThreshold;

    /**A temporary tileIndex to decode each tile ID*/
    final TileIndex tileIndex = new TileIndex();

    /**A temporary variable to compute the MBR of each tile*/
    final Envelope tileMBR = new Envelope(2);

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      Configuration conf = context.getConfiguration();
      plotter = Plotter.createAndConfigurePlotter(conf);
      featuresMBR = CSVEnvelopeDecoder.instance.apply(conf.get("mbr"), null);
      tileWidth = conf.getInt(MultilevelPyramidPlotHelper.TileWidth, 256);
      tileHeight = conf.getInt(MultilevelPyramidPlotHelper.TileHeight, 256);
      dataTileThreshold = conf.getLongBytes(MultilevelPyramidPlotHelper.DataTileThreshold, 1024 * 1024);
      if (dataTileThreshold > 0) {
        Path histogramPath = new Path(conf.get(HistogramPath));
        FileSystem histogramFileSystem = histogramPath.getFileSystem(conf);
        histogram = HistogramMapReduce.readHistogram(histogramFileSystem, histogramPath);
      }
    }

    @Override
    protected void reduce(LongWritable tileID, Iterable<IFeature> iFeatures, Context context) throws IOException, InterruptedException {
      TileIndex.decode(tileID.get(), tileIndex);
      MultilevelPyramidPlotHelper.TileClass tileClass = dataTileThreshold == 0 ? MultilevelPyramidPlotHelper.TileClass.ImageTile :
          MultilevelPyramidPlotHelper.classifyTile(histogram, dataTileThreshold, tileIndex.z, tileIndex.x, tileIndex.y);
      switch (tileClass) {
        case ImageTile:
          // Image tile. Create a final tile image and write it to the output
          TileIndex.getMBR(featuresMBR, tileIndex.z, tileIndex.x, tileIndex.y, tileMBR);
          Canvas canvas = plotter.createCanvas(tileWidth, tileHeight, tileMBR);
          boolean modified = plotter.plot(canvas, plotter.smooth(iFeatures));
          if (modified)
            context.write(tileID.get(), canvas);
          break;
      }
    }
  }

  public static class CoarseGrainedPyramidPartitioningReduce extends FineGrainedPyramidPartitioningReduce {

    /**Pyramid partitioner for image tiles, based on size*/
    PyramidPartitioner imageTilePP;

    /**Range of levels to generate. Needed to create the subpyramid under each tile*/
    MultilevelPyramidPlotHelper.Range levels;

    /**Granularity of the partitioning*/
    int granularity;

    /**
     * Sub pyramid
     */
    final Map<Long, Canvas> canvases = new HashMap<>();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      Configuration conf = context.getConfiguration();
      granularity = conf.getInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
      levels = MultilevelPyramidPlotHelper.getRange(conf, MultilevelPyramidPlotHelper.NumLevels, "7");

      // Retrieve all partitioning information. Similar to the mapper.
      String imageTileSizeRangeStr = conf.get(ImageTileSizeRange);
      AbstractHistogram histogram = null;
      if (conf.get(HistogramPath) != null) {
        Path histogramPath = new Path(conf.get(HistogramPath));
        FileSystem histogramFileSystem = histogramPath.getFileSystem(conf);
        histogram = HistogramMapReduce.readHistogram(histogramFileSystem, histogramPath);
      }

      Envelope inputMBR = CSVEnvelopeDecoder.instance.apply(conf.get("mbr"), null);
      SubPyramid pyramid = new SubPyramid(inputMBR, levels.min, levels.max);
      if (imageTileSizeRangeStr != null) {
        String[] parts = imageTileSizeRangeStr.split("\\.\\.");
        long minSizeImageTile = Long.parseLong(parts[0]);
        long maxSizeImageTile = Long.parseLong(parts[1]);
        imageTilePP = new PyramidPartitioner(pyramid, histogram, minSizeImageTile, maxSizeImageTile);
        imageTilePP.setGranularity(granularity);
      }
    }

    @Override
    protected void reduce(LongWritable tileID, Iterable<IFeature> iFeatures, Context context) throws IOException, InterruptedException {
      TileIndex.decode(tileID.get(), tileIndex);
      // Form a subpyramid under the given tile
      int numLevelsToGenerate = (levels.max - tileIndex.z) % granularity + 1;
      SubPyramid pyramid = new SubPyramid(featuresMBR, tileIndex.z, tileIndex.z + numLevelsToGenerate - 1,
          tileIndex.x << (numLevelsToGenerate - 1), tileIndex.y << (numLevelsToGenerate - 1),
          (tileIndex.x + 1) << (numLevelsToGenerate - 1), (tileIndex.y + 1) << (numLevelsToGenerate - 1));

      PyramidPartitioner subPartitioner = new PyramidPartitioner(imageTilePP, pyramid);
      canvases.clear();
      MultilevelPyramidPlotHelper.createTiles(plotter.smooth(iFeatures), featuresMBR, subPartitioner,
          tileWidth, tileHeight, plotter, canvases);
      // Write all the tiles to the output
      for (Map.Entry<Long, Canvas> partialTile : canvases.entrySet()) {
        context.write(partialTile.getKey(), partialTile.getValue());
      }
    }
  }

  /**
   * Creates a multilevel plot for the given input and stores it in the output.
   * @param in
   * @param output
   * @param opts
   */
  public static void plotMapReduce(Path in, Path output, UserOptions opts) throws InterruptedException, IOException, ClassNotFoundException {

    if (opts.getBoolean(CommonVisualizationHelper.UseMercatorProjection, false)) {
      Envelope mbr = new Envelope(2, 0, 0, 256, 256);
      opts.setBoolean(CommonVisualizationHelper.VerticalFlip, false);
      opts.setBoolean(SpatialInputFormat.Mercator, true);
      opts.set("mbr", CSVEnvelopeEncoder.defaultEncoder.apply(mbr));
    } else  if (opts.get("mbr") == null) {
      // -------------------------------------------------------------------------------------------------------------
      // 1 - Compute the MBR if not computed
      Envelope mbr = SummaryMapReduce.computeMapReduce(in, opts);
      opts.set("mbr", CSVEnvelopeEncoder.defaultEncoder.apply(mbr));
      // ------- End of MBR computation ------------------------------------------------------------------------------
    }
    Configuration conf = opts.loadIntoHadoopConfiguration(null);
    conf.setInt(LocalJobRunner.LOCAL_MAX_MAPS, Runtime.getRuntime().availableProcessors());
    FileSystem outFS = output.getFileSystem(conf);
    long minSizeFlatPartitioning = conf.getLongBytes(MultilevelPyramidPlotHelper.MinSizeFlatPartitioning, 25 * 1024 * 1024);
    long dataTileThreshold = conf.getLongBytes(MultilevelPyramidPlotHelper.DataTileThreshold, 1024 * 1024);
    MultilevelPyramidPlotHelper.Range levels = MultilevelPyramidPlotHelper.getRange(conf, MultilevelPyramidPlotHelper.NumLevels, "7");

    // Compute the histogram if needed
    if (Math.max(dataTileThreshold, minSizeFlatPartitioning) > 0) {
      // -------------------------------------------------------------------------------------------------------------
      // 2 - Compute the histogram
      // If there is a decision to be made based on tile size, then we need to compute the histogram
      long maxHistogramSize = conf.getLongBytes(MultilevelPyramidPlotHelper.MaximumHistogramSize, 128 * 1024 * 1024);
      int binSize;
      String histogramType = conf.get(MultilevelPyramidPlotHelper.HistogramType, "simple");
      switch (histogramType) {
        case "simple": binSize = 8; break;
        case "euler": binSize = 32; break;
        default: throw new RuntimeException(String.format("Unknown histogram type '%s'", histogramType));
      }
      int gridDimension = MultilevelPyramidPlotHelper.computeHistogramDimension(maxHistogramSize, levels.max, binSize);
      UserOptions histogramOpts = new UserOptions(conf);
      histogramOpts.set(HistogramMapReduce.HistogramDimensions, String.format("%d,%d", gridDimension, gridDimension));
      histogramOpts.set(HistogramMapReduce.HistogramValue, "size");
      histogramOpts.setBoolean(HistogramMapReduce.ComputePrefixSum, true);
      // Generate a path to store the histogram
      Path histogramPath;
      do {
        histogramPath = new Path(output.getParent(), String.format("histogram-%06d-%s",
            (int)(Math.random() * 1000000), output.getName()));
      } while (outFS.exists(histogramPath));
      outFS.deleteOnExit(histogramPath);
      HistogramMapReduce.computeAndWrite(new Path[] {in}, histogramPath, histogramOpts);
      conf.set(HistogramPath, histogramPath.toString());
      AbstractHistogram histogram = HistogramMapReduce.readHistogram(outFS, histogramPath);
      // Use the histogram to limit the number of levels based on the estimated size
      levels.max = Math.min(levels.max,
          MultilevelPyramidPlotHelper.findDeepestTileWithSize(histogram, dataTileThreshold + 1));
      // ----- End of histogram computation --------------------------------------------------------------------------
    }

    Plotter plotter = Plotter.createAndConfigurePlotter(conf);
    Class<? extends Canvas> canvasClass = plotter.getCanvasClass();
    // Create an empty output directory where the output of the jobs will go into subdirectories under it
    outFS.mkdirs(output);

    ClusterStatus clusterStatus = new JobClient(new JobConf()).getClusterStatus();

    if (conf.getBoolean(OverwriteOutput, false)) {
      // Delete the output directory if it exists
      if (outFS.exists(output))
        outFS.delete(output, true);
    }

    // ---------------------------------------------------------------------------------------------------------------
    // 3- Create image tiles using flat partitioning for all tiles that are larger than MinSizeFlatPartitioning
    int maxLevelFlatPartitioning = Math.min(conf.getInt(MaxLevelForFlatPartitioning, 5), levels.max);
    if (maxLevelFlatPartitioning >= levels.min) {
      Configuration flatConf = new Configuration(conf);
      if (dataTileThreshold > 0)
        flatConf.set(ImageTileSizeRange, String.format("%d..%d", dataTileThreshold + 1, Long.MAX_VALUE));
      flatConf.set(MultilevelPyramidPlotHelper.NumLevels, String.format("%d..%d", levels.min, maxLevelFlatPartitioning));

      Job flatJob = Job.getInstance(flatConf, "mplot/flat");
      flatJob.setJarByClass(MultilevelPlotMapReduce.class);

      // Set mapper and reducer information
      flatJob.setMapperClass(FlatPartitioningMap.class);
      flatJob.setMapOutputKeyClass(LongWritable.class);
      flatJob.setMapOutputValueClass(canvasClass);
      flatJob.setReducerClass(FlatPartitioningReduce.class);
      flatJob.setNumReduceTasks(Math.max(1, clusterStatus.getMaxReduceTasks() * 9 / 10));

      // Set input and output
      flatJob.setInputFormatClass(SpatialInputFormat.class);
      SpatialInputFormat.setInputPaths(flatJob, in);
      flatJob.setOutputFormatClass(PyramidOutputFormat.class);
      PyramidOutputFormat.setOutputPath(flatJob, new Path(output, "flatimages"));
      flatJob.setOutputKeyClass(Long.class);
      flatJob.setOutputValueClass(canvasClass);

      // Run the job
      flatJob.waitForCompletion(true);
    }
    // -----End of FlatMapReduce job ---------------------------------------------------------------------------------

    if (maxLevelFlatPartitioning < levels.max) {
      // ---------------------------------------------------------------------------------------------------------------
      // 4- Create image tiles using pyramid partitioning for small image tiles
      Configuration pyramidConf = new Configuration(conf);
      if (dataTileThreshold > 0)
        pyramidConf.set(ImageTileSizeRange, String.format("%d..%d", dataTileThreshold + 1, Long.MAX_VALUE));
      pyramidConf.set(MultilevelPyramidPlotHelper.NumLevels, String.format("%d..%d",
          maxLevelFlatPartitioning + 1, levels.max));

      Job pyramidJob = Job.getInstance(pyramidConf, "mplot/pyramid");
      pyramidJob.setJarByClass(MultilevelPlotMapReduce.class);

      // Set mapper and reducer information
      pyramidJob.setMapperClass(PyramidPartitioningMap.class);
      pyramidJob.setMapOutputKeyClass(LongWritable.class);
      pyramidJob.setMapOutputValueClass(WritableFeature.class);
      if (conf.getInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1) == 1)
        pyramidJob.setReducerClass(FineGrainedPyramidPartitioningReduce.class);
      else
        pyramidJob.setReducerClass(CoarseGrainedPyramidPartitioningReduce.class);
      pyramidJob.setNumReduceTasks(Math.max(1, clusterStatus.getMaxReduceTasks() * 9 / 10));

      // Set input and output
      pyramidJob.setInputFormatClass(SpatialInputFormat.class);
      SpatialInputFormat.setInputPaths(pyramidJob, in);
      pyramidJob.setOutputFormatClass(PyramidOutputFormat.class);
      PyramidOutputFormat.setOutputPath(pyramidJob, new Path(output, "pyramid"));
      pyramidJob.setOutputKeyClass(Long.class);
      pyramidJob.setOutputValueClass(canvasClass);

      // Run the job
      pyramidJob.waitForCompletion(true);
      // -----End of pyramid partitioning job --------------------------------------------------------------------------
    }
    // Clean up the histogram
    if (conf.get(HistogramPath) != null)
      outFS.delete(new Path(conf.get(HistogramPath)), true);

    // Combine all subdirectories into one directory
    FileUtil.flattenDirectory(outFS, output);
    // Write the additional add-on files to allow the generated image to be easily visualized
    opts.setClass(CommonVisualizationHelper.PlotterClassName, plotter.getClass(), Plotter.class);
    opts.set("data", FileUtil.relativize(in, output).toString());
    MultilevelPyramidPlotHelper.writeVisualizationAddOnFiles(outFS, output, opts);
  }

  @Override
  public void run(UserOptions opts) throws IOException, InterruptedException, ClassNotFoundException {
    Path input = opts.getInputPaths()[0];
    Path output = new Path(opts.getOutput());
    plotMapReduce(input, output, opts);
  }
}
