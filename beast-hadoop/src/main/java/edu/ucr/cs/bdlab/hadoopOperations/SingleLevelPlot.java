/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.davinci.Canvas;
import edu.ucr.cs.bdlab.davinci.CommonVisualizationHelper;
import edu.ucr.cs.bdlab.davinci.ImageOutputFormat;
import edu.ucr.cs.bdlab.davinci.Plotter;
import edu.ucr.cs.bdlab.davinci.SingleLevelPlotHelper;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.CSVEnvelopeDecoder;
import edu.ucr.cs.bdlab.io.CSVEnvelopeEncoder;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.util.OperationMetadata;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.ClusterStatus;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.LocalJobRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.io.PrintStream;

@OperationMetadata(
    shortName =  "plot",
    description = "Plots an image for an input file",
    inputArity = "1",
    outputArity = "1",
    inheritParams = {SpatialInputFormat.class, SpatialOutputFormat.class,
        CommonVisualizationHelper.class, SingleLevelPlotHelper.class})
public class SingleLevelPlot implements CLIOperation {
  /**Logger for this class*/
  private static final Log LOG = LogFactory.getLog(SingleLevelPlot.class);

  public static void printUsage(PrintStream out) {
    out.println("The available plotters are:");
    for (String plotterName : Plotter.plotters.keySet()) {
      Plotter.Metadata plotterMetadata = Plotter.plotters.get(plotterName).getAnnotation(Plotter.Metadata.class);
      out.printf("- %s: %s\n", plotterName, plotterMetadata.description());
    }
  }

  /**
   * A map function that maps each feature to a single or multiple overlapping partitions. Each partition is visualized
   * into a canvas and all canvases are then sent to one reducer to be combined.
   */
  static class PlotterMap extends Mapper<Object, IFeature, NullWritable, Canvas> {
    /**The plotter created and stored*/
    protected Plotter plotter;

    /**Dimensions of the desired image*/
    protected int imageWidth, imageHeight;

    /**The MBR of the input*/
    protected Envelope inputMBR;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      Configuration conf = context.getConfiguration();
      plotter = SingleLevelPlotHelper.getConfiguredPlotter(conf);
      this.imageWidth = conf.getInt(SingleLevelPlotHelper.ImageWidth, 1000);
      this.imageHeight = conf.getInt(SingleLevelPlotHelper.ImageHeight, 1000);
      this.inputMBR = CSVEnvelopeDecoder.instance.apply(conf.get("mbr"), null);
    }

    @Override
    public void run(Context context) throws IOException, InterruptedException {
      this.setup(context);
      try {
        Canvas canvas = plotter.createCanvas(imageWidth, imageHeight, inputMBR);
        while(context.nextKeyValue())
          plotter.plot(canvas, context.getCurrentValue());
        context.write(NullWritable.get(), canvas);
      } finally {
        this.cleanup(context);
      }
    }
  }

  /**
   * A reduce function that merges all canvases into one.
   */
  static class CanvasMerger extends Reducer<NullWritable, Canvas, NullWritable, Writable> {

    /**The plotter created and stored*/
    protected Plotter plotter;

    /**Dimensions of the desired image*/
    protected int imageWidth, imageHeight;

    /**The MBR of the input*/
    protected Envelope inputMBR;

    /**Flip the final image vertically*/
    private boolean vflip;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      Configuration conf = context.getConfiguration();
      plotter = SingleLevelPlotHelper.getConfiguredPlotter(conf);
      this.imageWidth = conf.getInt(SingleLevelPlotHelper.ImageWidth, 1000);
      this.imageHeight = conf.getInt(SingleLevelPlotHelper.ImageHeight, 1000);
      this.inputMBR = CSVEnvelopeDecoder.instance.apply(conf.get("mbr"), null);
      this.vflip = conf.getBoolean(CommonVisualizationHelper.VerticalFlip, true);
    }

    @Override
    protected void reduce(NullWritable key, Iterable<Canvas> values, Context context) throws IOException, InterruptedException {
      Canvas finalCanvas = plotter.createCanvas(imageWidth, imageHeight, inputMBR);
      for (Canvas $partialCanvas : values) {
        plotter.merge(finalCanvas, $partialCanvas);
      }
      context.write(NullWritable.get(), finalCanvas);
    }
  }

  /**
   * Plots an input file using a MapReduce job
   * @param in
   * @param out
   * @param opts
   */
  public static Job plotMapReduce(Path in, Path out, UserOptions opts) throws IOException, ClassNotFoundException, InterruptedException {
    // Compute the input MBR
    Summary summary = SummaryMapReduce.computeMapReduce(new Path[] {in}, opts);
    opts.set("mbr", CSVEnvelopeEncoder.defaultEncoder.apply(summary));

    // Create the job
    Configuration conf = opts.loadIntoHadoopConfiguration(null);
    conf.setInt(LocalJobRunner.LOCAL_MAX_MAPS, Runtime.getRuntime().availableProcessors());
    Job job = Job.getInstance(conf, "Plotter");
    job.setJarByClass(SingleLevelPlot.class);
    Plotter plotter = SingleLevelPlotHelper.getConfiguredPlotter(opts);

    // Set input details
    job.setInputFormatClass(SpatialInputFormat.class);
    SpatialInputFormat.addInputPath(job, in);

    // Set mapper details
    job.setMapperClass(PlotterMap.class);
    job.setMapOutputKeyClass(NullWritable.class);
    job.setMapOutputValueClass(plotter.getCanvasClass());

    // Set reducer details
    job.setReducerClass(CanvasMerger.class);
    ClusterStatus clusterStatus = new JobClient(new JobConf()).getClusterStatus();
    job.setNumReduceTasks(opts.getBoolean(SingleLevelPlotHelper.MergeImages, true) ? 1 : 0);

    // Set output details
    job.setOutputFormatClass(ImageOutputFormat.class);
    ImageOutputFormat.setOutputPath(job, out);
    job.setOutputKeyClass(NullWritable.class);
    job.setOutputValueClass(plotter.getCanvasClass());

    // Start the job and wait for completion
    job.waitForCompletion(true);

    return job;
  }

  @Override
  public void run(UserOptions opts) throws IOException, InterruptedException {
    try {
      plotMapReduce(new Path(opts.getInput()), new Path(opts.getOutput()), opts);
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
}
