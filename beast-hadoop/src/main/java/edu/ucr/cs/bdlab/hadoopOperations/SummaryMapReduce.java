/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.util.OperationMetadata;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.LocalJobRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormatCounter;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileRecordReader;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;

import java.io.IOException;

@OperationMetadata(shortName = "summary",
    description = "Computes the minimum bounding rectangle (MBR), count, and size of a dataset.",
    inputArity = "+",
    outputArity = "0",
    inheritParams = {SpatialInputFormat.class, SpatialOutputFormat.class})
public class SummaryMapReduce implements CLIOperation {

  public static class SummaryMap extends Mapper<Object, IFeature, NullWritable, Summary> {
    @Override
    public void run(Context context) throws IOException, InterruptedException {
      this.setup(context);
      try {
        Summary summary = new WritableSummary();
        if (context.nextKeyValue()) {
          IFeature f = context.getCurrentValue();
          // First record
          summary.setCoordinateDimension(f.getGeometry().getCoordinateDimension());
          summary.merge(f.getGeometry());
          summary.size += f.getStorageSize();
          summary.numFeatures++;
        }
        while(context.nextKeyValue()) {
          IFeature f = context.getCurrentValue();
          summary.merge(f.getGeometry());
          summary.size += f.getStorageSize();
          summary.numFeatures++;
        }
        context.write(NullWritable.get(), summary);
      } finally {
        this.cleanup(context);
      }

    }
  }

  public static class SummaryReduce extends Reducer<NullWritable, Summary, NullWritable, Summary> {
    @Override
    protected void reduce(NullWritable key, Iterable<Summary> partialSummaries, Context context) throws IOException, InterruptedException {
      Summary finalSummary = null;
      for (Summary partialSummary : partialSummaries) {
        if (finalSummary == null) {
          finalSummary = new WritableSummary();
          finalSummary.setCoordinateDimension(partialSummary.getCoordinateDimension());
        }
        finalSummary.expandToSummary(partialSummary);
      }
      context.write(key, finalSummary);
    }
  }

  public static Summary computeMapReduce(Path in, UserOptions opts) throws IOException, ClassNotFoundException, InterruptedException {
    return computeMapReduce(new Path[] {in}, opts);
  }

  /**
   *
   * @param ins
   * @param opts
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   * @throws InterruptedException
   */
  public static Summary computeMapReduce(Path[] ins, UserOptions opts) throws IOException, ClassNotFoundException, InterruptedException {
    Configuration conf = opts.loadIntoHadoopConfiguration(null);
    conf.setInt(LocalJobRunner.LOCAL_MAX_MAPS, Runtime.getRuntime().availableProcessors());
    // Create the job
    Job job = Job.getInstance(conf, "Summary");
    job.setJarByClass(SummaryMapReduce.class);

    // Set input details
    job.setInputFormatClass(SpatialInputFormat.class);
    for (Path in : ins)
      SpatialInputFormat.addInputPath(job, in);

    // Set mapper details
    job.setMapperClass(SummaryMap.class);
    job.setMapOutputKeyClass(NullWritable.class);
    job.setMapOutputValueClass(WritableSummary.class);

    // Set reducer details
    job.setReducerClass(SummaryReduce.class);

    // Set output details
    Path out;
    FileSystem fileSystem = FileSystem.get(conf);
    do {
      out = new Path(String.format("summary-%d.temp", (int)(Math.random()* 1000000)));
    } while (fileSystem.exists(out));
    fileSystem.deleteOnExit(out);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    SequenceFileOutputFormat.setOutputPath(job, out);
    job.setOutputKeyClass(NullWritable.class);
    job.setOutputValueClass(WritableSummary.class);

    // Start the job and wait for completion
    job.waitForCompletion(true);

    if (!job.isSuccessful())
      throw new RuntimeException("Error running job!");
    long filesize = job.getCounters().findCounter(FileInputFormatCounter.BYTES_READ).getValue();
    // Read the result back
    SequenceFileRecordReader reader = new SequenceFileRecordReader();
    FileStatus[] summaryFiles = fileSystem.listStatus(out, path -> !path.getName().startsWith("_") && !path.getName().startsWith("."));
    assert summaryFiles.length == 1;
    FileSplit split = new FileSplit(summaryFiles[0].getPath(), 0, summaryFiles[0].getLen(), new String[0]);
    reader.initialize(split, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
    try {
      if (!reader.nextKeyValue())
        throw new RuntimeException("No output records found");
      Summary summary = (Summary) reader.getCurrentValue();
      // Override the size from the input counter
      summary.size = filesize;
      return summary;
    } finally {
      reader.close();
      fileSystem.delete(out, true);
    }
  }

  @Override
  public void run(UserOptions opts) throws IOException, InterruptedException, ClassNotFoundException {
    Path[] inputs = opts.getInputPaths();
    Summary summary = computeMapReduce(inputs, opts);
    Configuration conf = opts.loadIntoHadoopConfiguration(null);
    try (FileSystem fs = inputs[0].getFileSystem(conf)) {
      IFeature sampleRecord = SpatialInputFormat.readOne(fs, inputs[0], conf);
      try (JsonGenerator jsonGenerator = new JsonFactory().createGenerator(System.out)) {
        jsonGenerator.setPrettyPrinter(new DefaultPrettyPrinter());
        Summary.writeDatasetSchema(jsonGenerator, summary, sampleRecord);
      }
    }
  }
}
