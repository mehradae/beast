/*
 * Copyright 2019 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.cg.SpatialJoinAlgorithms;
import edu.ucr.cs.bdlab.io.CSVFeatureWriter;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.util.OperationMetadata;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.util.Stack;

/**
 * Runs SpatialJoin between two datasets
 */
@OperationMetadata(shortName = "sj",
    description = "Computes spatial join that finds all overlapping features from two files.",
    inputArity = "2",
    outputArity = "1",
    inheritParams = {SpatialInputFormat.class, SpatialOutputFormat.class})
public class SpatialJoinMapReduce implements CLIOperation {
  @OperationParam(description = "The spatial join algorithm to use. Currently, only 'dj,pbsm' are supported.", defaultValue = "dj")
  public static final String SpatialJoinMethod = "method";

  @OperationParam(description = "The spatial predicate to use in the join. Supported predicates are " +
      "{intersects, contains, mbrintersects}", defaultValue = "intersects")
  public static final String SpatialJoinPredicate = "predicate";

  @Override
  public void setup(UserOptions opts) {
    // Configure the output format to work with the spatial join
    String oFormat = opts.get(SpatialOutputFormat.OutputFormat);
    if (oFormat == null) {
      // Not set. Set a reasonable output format
      String iFormat = opts.get(SpatialInputFormat.InputFormat);
      if (iFormat.startsWith("point") || iFormat.startsWith("envelope")) {
        opts.set(SpatialOutputFormat.OutputFormat, "wkt");
        opts.set(CSVFeatureWriter.FieldSeparator, "\t");
      }
      return;
    }
    if (oFormat.startsWith("point") || oFormat.startsWith("envelope"))
      throw new RuntimeException(String.format("Unsupported output format '%s' for spatial join. Please use {wkt, shapefile, or geojson}",
          oFormat));
  }

  @Override
  public void addDependentClasses(UserOptions opts, Stack<Class<?>> parameterClasses) {
    SpatialJoinAlgorithms.ESJDistributedAlgorithm joinMethod =
        opts.getEnumIgnoreCase(SpatialJoinMethod, SpatialJoinAlgorithms.ESJDistributedAlgorithm.DJ);
    switch (joinMethod) {
      case DJ:
        parameterClasses.add(DistributedJoinMapReduce.class);
        break;
      case PBSM:
      case SJMR:
        parameterClasses.add(PBSMMapReduce.class);
        break;
      case BNLJ:
        parameterClasses.add(BNLJLocal.class);
    }
  }

  @Override
  public void run(UserOptions opts) throws IOException, InterruptedException, ClassNotFoundException {
    Path[] inputs = opts.getInputPaths();
    Path output = opts.getOutputPath();
    SpatialJoinAlgorithms.ESJDistributedAlgorithm joinMethod =
        opts.getEnumIgnoreCase(SpatialJoinMethod, SpatialJoinAlgorithms.ESJDistributedAlgorithm.DJ);
    switch (joinMethod) {
      case DJ:
        DistributedJoinMapReduce.DJMapReduce(inputs[0], inputs[1], output, opts);
        break;
      case PBSM:
      case SJMR:
          PBSMMapReduce.PBSMMapReduce(inputs[0], inputs[1], output, opts);
        break;
      case BNLJ:
        BNLJLocal.bnljLocal(opts, inputs[0], inputs[1]);
        break;
      default:
        throw new RuntimeException(String.format("Unsupported join method '%s'", joinMethod));
    }
  }
}
