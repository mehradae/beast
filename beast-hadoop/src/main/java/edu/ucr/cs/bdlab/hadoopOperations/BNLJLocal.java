/*
 * Copyright 2020 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.cg.SpatialJoinAlgorithms;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.FeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.util.IConfigurable;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A local implementation of block-nested loop spatial join. Used to test the scalability of the core processor.
 */
public class BNLJLocal implements IConfigurable {

  /**This configuration can be used to override the default grid dimension*/
  @OperationParam(description = "The local join algorithm to apply on every pair of splits {planesweep, rtree}",
      defaultValue = "planesweep")
  public static final String LocalJoinAlgorithm = "ljoin";

  private static final Log LOG = LogFactory.getLog(BNLJLocal.class);

  public static void bnljLocal(UserOptions opts, Path file1, Path file2) throws IOException {
    SpatialInputFormat inputFormat = new SpatialInputFormat();
    opts.setBoolean(SpatialInputFormat.ImmutableObjects, true);
    Job job = Job.getInstance(opts);
    SpatialInputFormat.addInputPath(job, file1);
    List<InputSplit> splits1 = inputFormat.getSplits(job);
    job = Job.getInstance(opts);
    SpatialInputFormat.addInputPath(job, file2);
    List<InputSplit> splits2 = inputFormat.getSplits(job);

    List<IFeature> features1 = new ArrayList<>();
    List<IFeature> features2 = new ArrayList<>();

    LongWritable count = new LongWritable();
    SpatialJoinAlgorithms.SJResult refinestep;
    String predicate = opts.get(SpatialJoinMapReduce.SpatialJoinPredicate);
    switch (predicate) {
      case "intersects":
        refinestep = (i, j, refPoint) -> {
          if (features1.get(i).getGeometry().intersects(features2.get(j).getGeometry()))
            count.set(count.get() + 1);
        };
        break;
      case "contains":
        refinestep = (i, j, refPoint) -> {
          if (features1.get(i).getGeometry().contains(features2.get(j).getGeometry()))
            count.set(count.get() + 1);
        };
        break;
      case "mbrintersects":
        refinestep = (i, j, refPoint) -> count.set(count.get() + 1);
        break;
      default:
        throw new RuntimeException("Unrecognized predicate "+predicate);
    }

    SpatialJoinAlgorithms.ESJSequentialAlgorithm ljoin = opts.getEnumIgnoreCase(LocalJoinAlgorithm,
        SpatialJoinAlgorithms.ESJSequentialAlgorithm.Planesweep);

    long t1, t2;
    for (InputSplit split1 : splits1) {
      FeatureReader reader1 = inputFormat.createAndInitRecordReader(split1, opts);
      features1.clear();
      for (IFeature feature1 : reader1)
        features1.add(feature1);
      for (InputSplit split2 : splits2) {
        count.set(0);
        FeatureReader reader2 = inputFormat.createAndInitRecordReader(split2, opts);
        features2.clear();
        for (IFeature feature2 : reader2)
          features2.add(feature2);

        t1 = System.nanoTime();
        long resultCount;
        switch (ljoin) {
          case Planesweep:
            resultCount = SpatialJoinAlgorithms.spatialJoinIntersectsPlaneSweepFeatures(features1, features2, refinestep);
            break;
          case RTree:
            resultCount = SpatialJoinAlgorithms.spatialJoinIntersectsIndexFeatures(features1, features2, refinestep);
            break;
          default:
            throw new RuntimeException(String.format("Unsupported local join algorithm '%s'", ljoin));
        }

        t2 = System.nanoTime();
        LOG.info(String.format("Local join %d x %d resulted in %d pairs and took %f seconds using the method '%s'",
            features1.size(), features2.size(), resultCount, (t2-t1)*1E-9, ljoin));
      }
    }
  }

}
