/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.io.CSVEnvelopeDecoder;
import edu.ucr.cs.bdlab.io.CSVEnvelopeEncoder;
import edu.ucr.cs.bdlab.io.FeatureWriter;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.stsynopses.EulerHistogram2D;
import edu.ucr.cs.bdlab.stsynopses.Prefix2DHistogram;
import edu.ucr.cs.bdlab.stsynopses.PrefixEulerHistogram2D;
import edu.ucr.cs.bdlab.stsynopses.UniformHistogram;
import edu.ucr.cs.bdlab.util.IntArray;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.LocalJobRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileRecordReader;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;

import java.io.IOException;

public class HistogramMapReduce {

  private static final Log LOG = LogFactory.getLog(HistogramMapReduce.class);

  /**
   * Number of buckets in the histogram
   */
  @OperationParam(
      description = "Total number of buckets in the histogram",
      defaultValue = "1000"
  )
  public static final String NumBuckets = "numbuckets";

  @OperationParam(
      description = "Type of histogram {simple, euler}",
      defaultValue = "simple"
  )
  public static final String HistogramType = "histogramtype";

  @OperationParam(
      description = "The value to compute in the histogram {count, size}",
      defaultValue = "count"
  )
  public static final String HistogramValue = "histogramvalue";

  @OperationParam(
      description = "Computes the prefix sum of the result before writing it",
      defaultValue = "false"
  )
  public static final String ComputePrefixSum = "prefix";

  /**
   * The name of the histogram class
   */
  public static final String HistogramClassName = "Histogram.ClassName";

  /**Comma separated list of integer dimensions for the histogram*/
  public static final String HistogramDimensions = "Histogram.Dimensions";

  abstract static class HistogramMap extends Mapper<Object, IFeature, NullWritable, AbstractHistogram> {
    protected final Envelope inputMBR = new Envelope();

    @Override
    public void run(Context context) throws IOException, InterruptedException {
      this.setup(context);
      AbstractHistogram h = createEmptyHistogram(context.getConfiguration());
      inputMBR.set(h);
      this.setHistogram(h);
      while (context.nextKeyValue()) {
        addFeature(context.getCurrentValue());
      }
      context.write(NullWritable.get(), h);
      this.cleanup(context);
    }

    public abstract void setHistogram(AbstractHistogram h);

    public abstract void addFeature(IFeature f);
  }

  /**
   * Mapper for computing a uniform histogram for counts
   */
  public static class UniformCountHistogramMap extends HistogramMap {
    UniformHistogram h;
    Point centroid;
    @Override
    public void setHistogram(AbstractHistogram h) {
      this.h = (UniformHistogram) h;
      centroid = new Point(h.getCoordinateDimension());
    }

    @Override
    public void addFeature(IFeature f) {
      f.getGeometry().centroid(centroid);
      h.addPoint(centroid.coords, 1);
    }
  }

  /**
   * Mapper for computing a simple size histogram
   */
  public static class UniformSizeHistogramMap extends HistogramMap {
    UniformHistogram h;
    Point centroid;
    FeatureWriter fakeWriter;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      SpatialOutputFormat outputFormat = new SpatialOutputFormat();
      Configuration conf = context.getConfiguration();
      Class<? extends FeatureWriter> writerClass = outputFormat.getConfiguredFeatureWriterClass(conf);
      try {
        this.fakeWriter = writerClass.newInstance();
        this.fakeWriter.initialize(new NullOutputStream(), conf);
      } catch (InstantiationException e) {
        throw new RuntimeException(e);
      } catch (IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }

    @Override
    public void setHistogram(AbstractHistogram h) {
      this.h = (UniformHistogram) h;
      centroid = new Point(h.getCoordinateDimension());
    }

    @Override
    public void addFeature(IFeature f) {
      long size = fakeWriter.estimateSize(f);
      f.getGeometry().centroid(centroid);
      h.addPoint(centroid.coords, size);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
      super.cleanup(context);
      this.fakeWriter.close(context);
    }
  }

  /**
   * Mapper for creating Euler histogram for counts
   */
  public static class EulerCountHistogramMap extends HistogramMap {
    EulerHistogram2D h;
    Envelope featureMBR;

    @Override
    public void setHistogram(AbstractHistogram h) {
      assert h.getCoordinateDimension() == 2;
      this.h = (EulerHistogram2D) h;
      featureMBR = new Envelope(h.getCoordinateDimension());
    }

    @Override
    public void addFeature(IFeature f) {
      f.getGeometry().envelope(featureMBR);
      featureMBR.shrink(inputMBR);
      if (!featureMBR.isEmpty())
        h.addEnvelope(featureMBR.minCoord[0], featureMBR.minCoord[1], featureMBR.maxCoord[0], featureMBR.maxCoord[1], 1);
    }
  }

  /**
   * Mapper for creating Euler histogram for size
   */
  public static class EulerSizeHistogramMap extends HistogramMap {
    EulerHistogram2D h;
    Envelope featureMBR;
    FeatureWriter fakeWriter;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      SpatialOutputFormat outputFormat = new SpatialOutputFormat();
      Configuration conf = context.getConfiguration();
      Class<? extends FeatureWriter> writerClass = outputFormat.getConfiguredFeatureWriterClass(conf);
      try {
        this.fakeWriter = writerClass.newInstance();
        this.fakeWriter.initialize(new NullOutputStream(), conf);
      } catch (InstantiationException e) {
        throw new RuntimeException(e);
      } catch (IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }

    @Override
    public void setHistogram(AbstractHistogram h) {
      assert h.getCoordinateDimension() == 2;
      this.h = (EulerHistogram2D) h;
      featureMBR = new Envelope(h.getCoordinateDimension());
    }

    @Override
    public void addFeature(IFeature f) {
      long size = fakeWriter.estimateSize(f);
      f.getGeometry().envelope(featureMBR);
      featureMBR.shrink(inputMBR);
      if (!featureMBR.isEmpty())
        h.addEnvelope(featureMBR.minCoord[0], featureMBR.minCoord[1], featureMBR.maxCoord[0], featureMBR.maxCoord[1], size);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
      super.cleanup(context);
      this.fakeWriter.close(context);
    }
  }

  /**
   * Creates an empty histogram according to the configuration
   * @param conf
   * @return
   */
  private static AbstractHistogram createEmptyHistogram(Configuration conf) {
    Class<? extends AbstractHistogram> histogramClass = conf.getClass(HistogramClassName, null, AbstractHistogram.class);
    String[] dims = conf.get(HistogramDimensions).split(",");
    int[] ddims = new int[dims.length];
    for (int d = 0; d < ddims.length; d++) {
      ddims[d] = Integer.parseInt(dims[d]);
    }
    Envelope mbr = CSVEnvelopeDecoder.instance.apply(conf.get("mbr"), null);
    if (histogramClass.equals(UniformHistogram.class)) {
      return new UniformHistogram(mbr, ddims);
    } else if (histogramClass.equals(EulerHistogram2D.class)) {
      assert ddims.length == 2;
      return new EulerHistogram2D(mbr, ddims[0], ddims[1]);
    } else {
      throw new RuntimeException(String.format("Cannot instantiate a histogram of type '%s'", histogramClass.getName()));
    }
  }


  /**
   * Reducer for uniform histogram
   */
  public static class UniformHistogramReduce extends Reducer<NullWritable, UniformHistogram, NullWritable, UniformHistogram> {
    @Override
    protected void reduce(NullWritable key, Iterable<UniformHistogram> values, Context context) throws IOException, InterruptedException {
      UniformHistogram finalHistogram = (UniformHistogram) createEmptyHistogram(context.getConfiguration());
      for (UniformHistogram partialHistogram : values)
        finalHistogram.mergeAligned(partialHistogram);
      if (context.getConfiguration().getBoolean(ComputePrefixSum, false)) {
        context.write(key, new Prefix2DHistogram(finalHistogram));
      } else {
        context.write(key, finalHistogram);
      }
    }
  }

  /**
   * Reducer for Euler histogram
   */
  public static class EulerHistogramReduce extends Reducer<NullWritable, EulerHistogram2D, NullWritable, EulerHistogram2D> {
    @Override
    protected void reduce(NullWritable key, Iterable<EulerHistogram2D> values, Context context) throws IOException, InterruptedException {
      EulerHistogram2D finalHistogram = (EulerHistogram2D) createEmptyHistogram(context.getConfiguration());
      for (EulerHistogram2D partialHistogram : values) {
        finalHistogram.mergeAligned(partialHistogram);
      }
      if (context.getConfiguration().getBoolean(ComputePrefixSum, false)) {
        context.write(key, new PrefixEulerHistogram2D(finalHistogram));
      } else {
        context.write(key, finalHistogram);
      }
    }
  }

  /**
   * Compute the histogram and save it to the given output path using {@link SequenceFileOutputFormat}.
   * @param ins a list of input paths. The histogram is computed for all these inputs combined.
   * @param output the output path where the resulting histogram will be stored in a binary format
   * @param opts the environment configuration
   */
  public static void computeAndWrite(Path[] ins, Path output, UserOptions opts) throws InterruptedException, IOException, ClassNotFoundException {
    Envelope mbr;
    if (opts.get("mbr") == null) {
      // MBR not set. Compute it and set it
      mbr = SummaryMapReduce.computeMapReduce(ins, opts);
      opts.set("mbr", CSVEnvelopeEncoder.defaultEncoder.apply(mbr));
    } else {
      mbr = CSVEnvelopeDecoder.instance.apply(opts.get("mbr"), null);
    }

    if (opts.get(HistogramDimensions) == null) {
      // Compute number of dimensions based on number of buckets
      int numBuckets = opts.getInt(NumBuckets, 1000);
      int[] numPartitions = UniformHistogram.computeNumPartitions(mbr, numBuckets);
      opts.set(HistogramDimensions, IntArray.join(',', numPartitions));
    }

    // Create the job
    Job job = Job.getInstance(opts.loadIntoHadoopConfiguration(null), "Histogram");
    job.setJarByClass(HistogramMapReduce.class);

    // Set input details
    job.setInputFormatClass(SpatialInputFormat.class);
    for (Path in : ins)
      SpatialInputFormat.addInputPath(job, in);

    String histogramType = opts.get(HistogramType, "simple");
    String valueType = opts.get(HistogramValue, "count");
    Class<? extends AbstractHistogram> intermediateHistogramClass;

    switch (histogramType+"/"+valueType) {
      case "simple/count":
        job.setMapperClass(UniformCountHistogramMap.class);
        intermediateHistogramClass = UniformHistogram.class;
        job.setReducerClass(UniformHistogramReduce.class);
        break;
      case "simple/size":
        job.setMapperClass(UniformSizeHistogramMap.class);
        intermediateHistogramClass = UniformHistogram.class;
        job.setReducerClass(UniformHistogramReduce.class);
        break;
      case "euler/count":
        job.setMapperClass(EulerCountHistogramMap.class);
        intermediateHistogramClass = EulerHistogram2D.class;
        job.setReducerClass(EulerHistogramReduce.class);
        break;
      case "euler/size":
        job.setMapperClass(EulerSizeHistogramMap.class);
        job.setReducerClass(EulerHistogramReduce.class);
        intermediateHistogramClass = EulerHistogram2D.class;
        break;
      default:
        throw new RuntimeException(String.format("Unknown histogram type '%s' or value '%s'", histogramType, valueType));
    }
    job.setMapOutputValueClass(intermediateHistogramClass);
    job.getConfiguration().setClass(HistogramClassName, intermediateHistogramClass, AbstractHistogram.class);
    job.setNumReduceTasks(1);

    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    SequenceFileOutputFormat.setOutputPath(job, output);
    job.setOutputKeyClass(NullWritable.class);
    Class<? extends AbstractHistogram> finalHistogramClass;
    if (opts.getBoolean(ComputePrefixSum, false)) {
      if (intermediateHistogramClass.equals(UniformHistogram.class))
        finalHistogramClass = Prefix2DHistogram.class;
      else
        finalHistogramClass = PrefixEulerHistogram2D.class;
    } else {
      finalHistogramClass = intermediateHistogramClass;
    }
    job.setOutputValueClass(finalHistogramClass);

    // Start the job and wait for completion
    job.waitForCompletion(true);

    if (!job.isSuccessful())
      throw new RuntimeException("Error running job!");
  }


  /**
   * Compute the histogram using MapReduce and returns the resulting histogram
   * @param ins
   * @param opts
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   * @throws InterruptedException
   */
  public static AbstractHistogram computeMapReduce(Path[] ins, UserOptions opts) throws IOException, InterruptedException, ClassNotFoundException {
    Configuration conf = opts.loadIntoHadoopConfiguration(null);
    conf.setInt(LocalJobRunner.LOCAL_MAX_MAPS, Runtime.getRuntime().availableProcessors());

    // Create a temporary output
    Path tempOutput;
    FileSystem fileSystem = FileSystem.get(conf);
    do {
      tempOutput = new Path(String.format("histogram-%06d.temp", (int)(Math.random() * 1000000)));
    } while (fileSystem.exists(tempOutput));
    fileSystem.deleteOnExit(tempOutput);

    // Read the result back from the temporary directory
    try {
      computeAndWrite(ins, tempOutput, opts);
      return readHistogram(fileSystem, tempOutput);
    } finally {
      fileSystem.delete(tempOutput, true);
    }
  }

  /**
   * Reads a histogram from a file that is stored using {@link SequenceFileOutputFormat}
   * @param fileSystem
   * @param tempOutput
   * @return
   * @throws IOException
   * @throws InterruptedException
   */
  public static AbstractHistogram readHistogram(FileSystem fileSystem, Path tempOutput) throws IOException, InterruptedException {
    SequenceFileRecordReader reader = new SequenceFileRecordReader();
    FileStatus[] histogramFiles = fileSystem.listStatus(tempOutput, path -> !path.getName().startsWith("_") && !path.getName().startsWith("."));
    assert histogramFiles.length == 1;
    FileSplit split = new FileSplit(histogramFiles[0].getPath(), 0, histogramFiles[0].getLen(), new String[0]);
    try {
      reader.initialize(split, new TaskAttemptContextImpl(new Configuration(), new TaskAttemptID()));
      if (!reader.nextKeyValue())
        throw new RuntimeException("No output records found");
      AbstractHistogram histogram = (AbstractHistogram) reader.getCurrentValue();
      return histogram;
    } finally {
      reader.close();
    }
  }
}
