/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.LocalJobRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.TaskCounter;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileRecordReader;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;

import java.io.IOException;
import java.util.Random;

/**
 * Computes a sample of an input geometry file into a set of points. The output points represent the centroid
 * of the sampled geometries.
 */
public class SampleMapReduce {

  /**Sampling ratio*/
  private static final String SamplingRatio = "SampleMapReduce.Ratio";

  /**Optional seed*/
  private static final String Seed = "SampleMapReduce.Seed";

  public static class SampleMap extends Mapper<Object, IFeature, NullWritable, Point> {
    /**Reusable output value*/
    protected Point p;

    /**Sampling ratio*/
    protected float ratio;

    /**Random number generator*/
    protected Random random;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      Configuration conf = context.getConfiguration();
      this.ratio = conf.getFloat(SamplingRatio, 0.1f);
      this.random = new Random(conf.getLong(Seed, System.currentTimeMillis()));
    }

    @Override
    protected void map(Object key, IFeature value, Context context) throws IOException, InterruptedException {
      if (p == null) {
        p = new WritablePoint(value.getGeometry().getCoordinateDimension());
      }
      if (this.random.nextDouble() < ratio) {
        value.getGeometry().centroid(p);
        context.write(NullWritable.get(), p);
      }
    }
  }

  /**
   * Read a point sample from the given file
   * @param ins
   * @param ratio
   * @param opts
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   * @throws InterruptedException
   */
  public static double[][] drawPointSample(Path[] ins, float ratio, UserOptions opts) throws IOException, ClassNotFoundException, InterruptedException {
    Configuration conf = opts.loadIntoHadoopConfiguration(null);
    conf.setInt(LocalJobRunner.LOCAL_MAX_MAPS, Runtime.getRuntime().availableProcessors());
    // Create the job
    Job job = Job.getInstance(conf, "Sample");
    job.setJarByClass(IndexMapReduce.class);
    job.getConfiguration().setFloat(SamplingRatio, ratio);

    // Set input details
    job.setInputFormatClass(SpatialInputFormat.class);
    for (Path in : ins)
      SpatialInputFormat.addInputPath(job, in);

    // Set mapper details
    job.setMapperClass(SampleMap.class);
    job.setMapOutputKeyClass(NullWritable.class);
    job.setMapOutputValueClass(WritablePoint.class);

    // Set reducer details
    job.setNumReduceTasks(0);

    // Set output details
    Path out;
    FileSystem fileSystem = FileSystem.get(conf);
    do {
      out = new Path(String.format("sample-%d.temp", (int)(Math.random()* 1000000)));
    } while (fileSystem.exists(out));
    fileSystem.deleteOnExit(out);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    SequenceFileOutputFormat.setOutputPath(job, out);
    job.setOutputKeyClass(NullWritable.class);
    job.setOutputValueClass(WritablePoint.class);

    // Start the job and wait for completion
    job.waitForCompletion(true);

    if (!job.isSuccessful())
      throw new RuntimeException("Error running job!");
    int sampleSize = (int) job.getCounters().findCounter(TaskCounter.MAP_OUTPUT_RECORDS).getValue();
    double[][] coords = null;
    // Read the result back
    SequenceFileRecordReader reader = new SequenceFileRecordReader();
    try {
      FileStatus[] sampleFiles = fileSystem.listStatus(out, path -> !path.getName().startsWith("_") && !path.getName().startsWith("."));
      for (FileStatus sampleFile : sampleFiles) {
        FileSplit split = new FileSplit(sampleFile.getPath(), 0, sampleFile.getLen(), new String[0]);
        reader.initialize(split, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
        while (reader.nextKeyValue()) {
          Point p = (Point) reader.getCurrentValue();
          if (coords == null)
            coords = new double[p.getCoordinateDimension()][sampleSize];
          sampleSize--;
          for (int d = 0; d < p.getCoordinateDimension(); d++)
            coords[d][sampleSize] = p.coords[d];
        }
        reader.close();
      }
      assert sampleSize == 0;
      return coords;
    } finally {
      fileSystem.delete(out, true);
    }
  }
}
