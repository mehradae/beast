package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.util.WritableExternalizable;

public class WritableFeature extends Feature implements WritableExternalizable {

  public WritableFeature() {}

  public WritableFeature(IFeature feature) {
    super(feature);
  }
}
