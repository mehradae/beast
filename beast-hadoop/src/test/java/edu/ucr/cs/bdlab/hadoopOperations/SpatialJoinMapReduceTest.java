/*
 * Copyright 2019 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;

public class SpatialJoinMapReduceTest extends SparkTest {

  public void testBNLJ() throws IOException, InterruptedException, ClassNotFoundException {
    Path in1 = makeFileCopy("/sj1.csv");
    Path in2 = makeFileCopy("/sj2.csv");
    Path output = new Path(scratchPath, "sjout");
    UserOptions opts = new UserOptions(in1.toString(), in2.toString(), output.toString(), "iformat:envelope", "separator:,");
    opts.assignAllInputsExceptOneOutput();

    new SpatialJoinMapReduce().run(opts);

    String[] result = readFilesInDirAsLines(new File(output.toString()));
    assertEquals(3, result.length);
  }

  public void testPBSM() throws IOException, InterruptedException, ClassNotFoundException {
    Path in1 = makeFileCopy("/sj1.csv");
    Path in2 = makeFileCopy("/sj2.csv");
    Path output = new Path(scratchPath, "sjout");
    UserOptions opts = new UserOptions(in1.toString(), in2.toString(), output.toString(), "iformat:envelope", "separator:,", "method:pbsm");
    opts.assignAllInputsExceptOneOutput();

    new SpatialJoinMapReduce().run(opts);

    String[] result = readFilesInDirAsLines(new File(output.toString()));
    assertEquals(3, result.length);
  }


  public void testPBSMWithBiggerGrid() throws IOException, InterruptedException, ClassNotFoundException {
    Path in1 = makeFileCopy("/sj1.csv");
    Path in2 = makeFileCopy("/sj2.csv");
    Path output = new Path(scratchPath, "sjout");
    UserOptions opts = new UserOptions(in1.toString(), in2.toString(), output.toString(), "iformat:envelope", "separator:,", "method:pbsm");
    opts.setInt(PBSMMapReduce.PBSMGridDimension, 3);
    opts.assignAllInputsExceptOneOutput();

    new SpatialJoinMapReduce().run(opts);

    String[] result = readFilesInDirAsLines(new File(output.toString()));
    assertEquals(3, result.length);
  }
}