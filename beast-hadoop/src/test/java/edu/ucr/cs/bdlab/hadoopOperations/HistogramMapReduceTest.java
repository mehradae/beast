/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.EulerHistogram2D;
import edu.ucr.cs.bdlab.stsynopses.Prefix2DHistogram;
import edu.ucr.cs.bdlab.stsynopses.PrefixEulerHistogram2D;
import edu.ucr.cs.bdlab.stsynopses.UniformHistogram;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;

public class HistogramMapReduceTest extends SparkTest {

  public void testSimpleCountHistogram() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "simple");
    opts.set(HistogramMapReduce.HistogramValue, "count");
    opts.set(HistogramMapReduce.HistogramDimensions, "4,4");
    opts.set("mbr", "0,0,4,4");

    UniformHistogram h = (UniformHistogram) HistogramMapReduce.computeMapReduce(new Path[]{input}, opts);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(8, h.sumRectangle(0, 0, 4.0, 4.0));
  }

  public void testSimpleCountHistogramWithPrefixSum() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "simple");
    opts.set(HistogramMapReduce.HistogramValue, "count");
    opts.set(HistogramMapReduce.HistogramDimensions, "4,4");
    opts.setBoolean(HistogramMapReduce.ComputePrefixSum, true);
    opts.set("mbr", "0,0,4,4");

    Prefix2DHistogram h = (Prefix2DHistogram) HistogramMapReduce.computeMapReduce(new Path[]{input}, opts);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(8, h.sumRectangle(0, 0, 4.0, 4.0));
  }

  public void testSimpleSizeHistogram() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(SpatialOutputFormat.OutputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "simple");
    opts.set(HistogramMapReduce.HistogramValue, "size");
    opts.set(HistogramMapReduce.HistogramDimensions, "4,4");
    opts.set("mbr", "0,0,4,4");

    UniformHistogram h = (UniformHistogram) HistogramMapReduce.computeMapReduce(new Path[]{input}, opts);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(8 * 8, h.sumRectangle(0, 0, 4.0, 4.0));
  }

  public void testEulerCountHistogram() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    copyResource("/test_small.rect", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "envelope");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "euler");
    opts.set(HistogramMapReduce.HistogramValue, "count");
    opts.set(HistogramMapReduce.HistogramDimensions, "4,4");
    opts.set("mbr", "0,0,4,4");

    EulerHistogram2D h = (EulerHistogram2D) HistogramMapReduce.computeMapReduce(new Path[]{input}, opts);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(4, h.sumEnvelope(0.0, 0.0, 4.0, 4.0));
  }

  public void testEulerCountHistogramWithPrefixSum() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    copyResource("/test_small.rect", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "envelope");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "euler");
    opts.set(HistogramMapReduce.HistogramValue, "count");
    opts.set(HistogramMapReduce.HistogramDimensions, "4,4");
    opts.setBoolean(HistogramMapReduce.ComputePrefixSum, true);
    opts.set("mbr", "0,0,4,4");

    PrefixEulerHistogram2D h = (PrefixEulerHistogram2D) HistogramMapReduce.computeMapReduce(new Path[]{input}, opts);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(4, h.sumEnvelope(0.0, 0.0, 4.0, 4.0));
  }

  public void testEulerSizeHistogram() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    copyResource("/test_small.rect", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "envelope");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "euler");
    opts.set(HistogramMapReduce.HistogramValue, "size");
    opts.set(HistogramMapReduce.HistogramDimensions, "4,4");
    opts.set("mbr", "0,0,4,4");

    EulerHistogram2D h = (EulerHistogram2D) HistogramMapReduce.computeMapReduce(new Path[]{input}, opts);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(4 * 16, h.sumEnvelope(0.0, 0.0, 4.0, 4.0));
  }

  public void testShouldComputeMBR() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "simple");
    opts.set(HistogramMapReduce.HistogramValue, "count");
    opts.set(HistogramMapReduce.HistogramDimensions, "4,4");

    UniformHistogram h = (UniformHistogram) HistogramMapReduce.computeMapReduce(new Path[]{input}, opts);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(8, h.sumRectangle(0, 0, 4.0, 4.0));
  }

  public void testShouldComputeDimensionBasedOnNumberOfBuckets() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "simple");
    opts.set(HistogramMapReduce.HistogramValue, "count");
    opts.set(HistogramMapReduce.NumBuckets, "16");
    opts.set("mbr", "0,0,4,4");

    UniformHistogram h = (UniformHistogram) HistogramMapReduce.computeMapReduce(new Path[]{input}, opts);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(8, h.sumRectangle(0, 0, 4.0, 4.0));
  }

  public void testComputeAndWriteToFile() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    Path output = new Path(scratchPath, "output");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "simple");
    opts.set(HistogramMapReduce.HistogramValue, "count");
    opts.set(HistogramMapReduce.HistogramDimensions, "4,4");
    opts.set("mbr", "0,0,4,4");

    HistogramMapReduce.computeAndWrite(new Path[]{input}, output, opts);
    FileSystem outFS = output.getFileSystem(opts);
    UniformHistogram h = (UniformHistogram) HistogramMapReduce.readHistogram(outFS, output);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(8, h.sumRectangle(0, 0, 4.0, 4.0));
  }


  public void testEulerHistogramWithOutOfBoundRecords() throws IOException, InterruptedException, ClassNotFoundException {
    Path input = new Path(scratchPath, "input");
    copyResource("/test_small.rect", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "envelope");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(HistogramMapReduce.HistogramType, "euler");
    opts.set(HistogramMapReduce.HistogramValue, "count");
    opts.set(HistogramMapReduce.HistogramDimensions, "4,4");
    opts.set("mbr", "0,0,2,2");

    EulerHistogram2D h = (EulerHistogram2D) HistogramMapReduce.computeMapReduce(new Path[]{input}, opts);
    assertEquals(2, h.getCoordinateDimension());
    assertEquals(4, h.getNumPartitions(0));
    assertEquals(4, h.getNumPartitions(1));
    assertEquals(2, h.sumEnvelope(0.0, 0.0, 4.0, 4.0));
  }
}