/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.indexing.GridPartitioner;
import edu.ucr.cs.bdlab.indexing.IndexerParams;
import edu.ucr.cs.bdlab.indexing.PartitionInfo;
import edu.ucr.cs.bdlab.indexing.SpatialPartitioner;
import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class IndexMapReduceTest extends SparkTest {

  public void testWriteIndexMapReduce() throws IOException, InterruptedException, ClassNotFoundException {
    Path path = new Path(scratchPath, "test.points");
    Path outPath = new Path(scratchPath, "test.index");
    copyResource("/test.rect", new File(path.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "pointk(4)");
    SpatialOutputFormat.setOutputFormat(opts, "pointk(4)");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(CSVFeatureReader.SkipHeader, "false");
    opts.set(IndexerParams.GlobalIndex, "grid");
    GridPartitioner partitioner = new GridPartitioner();
    partitioner.setup(opts, true, SpatialPartitioner.PartitionCriterion.FIXED, 1);
    Summary s = SummaryMapReduce.computeMapReduce(new Path[] {path}, opts);
    for (int d = 0; d < s.getCoordinateDimension(); d++)
      s.maxCoord[d] = Math.nextUp(s.maxCoord[d]);
    partitioner.construct(s, null, null);
    IndexerParams.savePartitionerToHadoopConfiguration(opts, partitioner);
    IndexMapReduce.buildIndexMapReduce(path, outPath, opts);

    FileSystem fileSystem = outPath.getFileSystem(new Configuration());
    Path masterFilePath = new Path(outPath, "_master.grid");
    assertTrue("Master file does not exist", fileSystem.exists(masterFilePath));
    // Test the MBR of the global index
    PartitionInfo[] partitions = PartitionInfo.readPartitions(fileSystem, masterFilePath);
    assertEquals(4, partitions[0].getCoordinateDimension());
    Envelope env = new Envelope(4);
    for (PartitionInfo p : partitions)
      env.merge(p);
    assertEquals(133.0, env.minCoord[0]);
    assertEquals(954.0, env.maxCoord[0]);
    assertEquals(51.0, env.minCoord[3]);
    assertEquals(1000.0, env.maxCoord[3]);

    // Make sure the coordinate headers are written
    String headerLine = readFile(masterFilePath.toString())[0];
    assertEquals("ID\tFile Name\tRecord Count\tData Size\tGeometry\txmin\tymin\tzmin\twmin\txmax\tymax\tzmax\twmax", headerLine);
  }

  public void testWriteHeaderWithMoreThan26Coordinates() throws IOException, InterruptedException, ClassNotFoundException {
    Path path = new Path(scratchPath, "test.points");
    Path outPath = new Path(scratchPath, "test.index");
    copyResource("/test.rect", new File(path.toString()));
    Configuration conf = new Configuration();
    FileSystem inFs = path.getFileSystem(conf);
    PrintWriter pw = new PrintWriter(inFs.create(path));
    int numDimensions = 32;
    for (int d$ = 0; d$ < numDimensions; d$++) {
      if (d$ > 0)
        pw.print(',');
      pw.print(10);
    }
    pw.close();

    conf.set(SpatialInputFormat.InputFormat, String.format("pointk(%d)", numDimensions));
    SpatialOutputFormat.setOutputFormat(conf, String.format("pointk(%d)", numDimensions));
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.set(CSVFeatureReader.SkipHeader, "false");
    conf.set(IndexerParams.GlobalIndex, "grid");

    GridPartitioner partitioner = new GridPartitioner();
    partitioner.setup(conf, true, SpatialPartitioner.PartitionCriterion.FIXED, 1);
    Summary s = new Summary();
    s.setCoordinateDimension(numDimensions);
    for (int d = 0; d < numDimensions; d++) {
      s.minCoord[d] = 10;
      s.maxCoord[d] = 10.1;
    }
    partitioner.construct(s, null, null);
    IndexerParams.savePartitionerToHadoopConfiguration(conf, partitioner);
    IndexMapReduce.buildIndexMapReduce(path, outPath, conf);

    FileSystem fileSystem = outPath.getFileSystem(new Configuration());
    Path masterFilePath = new Path(outPath, "_master.grid");
    assertTrue("Master file does not exist", fileSystem.exists(masterFilePath));
    // Make sure the coordinate headers are written
    String headerLine = readFile(masterFilePath.toString())[0];
    ArrayList<String> expectedHeader = new ArrayList<>();
    expectedHeader.add("ID");
    expectedHeader.add("File Name");
    expectedHeader.add("Record Count");
    expectedHeader.add("Data Size");
    expectedHeader.add("Geometry");
    for (int d$ = 0; d$ < 26; d$++)
      expectedHeader.add(IGeometry.DimensionNames[d$]+"min");
    for (int d$ = 26; d$ < numDimensions; d$++)
      expectedHeader.add(""+IGeometry.DimensionNames[0]+IGeometry.DimensionNames[d$-26]+"min");
    for (int d$ = 0; d$ < 26; d$++)
      expectedHeader.add(IGeometry.DimensionNames[d$]+"max");
    for (int d$ = 26; d$ < numDimensions; d$++)
      expectedHeader.add(""+IGeometry.DimensionNames[0]+IGeometry.DimensionNames[d$-26]+"max");
    assertEquals(String.join("\t", expectedHeader), headerLine);
  }

}