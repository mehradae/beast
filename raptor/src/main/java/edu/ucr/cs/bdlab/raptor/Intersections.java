/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor;

import edu.ucr.cs.bdlab.geolite.*;
import edu.ucr.cs.bdlab.util.DynamicArrays;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.IndexedSortable;
import org.apache.hadoop.util.QuickSort;
//import scala.collection.Traversable$;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Stores intersections between a set of polygons and a raster layer.
 * The intersections are represented as horizontal line segments in the form
 * (pid, tid, y, x1, x2) which represents an intersection between
 * polygon #pid in tile #tid scanline #y in the range [x1, x2] inclusive of both.
 * These segments are ordered by (tid, y, pid, x1) which ensures that scanning these intersections
 * in order will match the order of the raster file.
 */
public class Intersections{
  /**Total number of intersections*/
  private int numIntersections;

  /**Scan line numbers (row in the raster file)*/
  private int[] ys;
  /**x-coordinates of the intersections*/
  private int[] xs;
  /**Indexes of the intersecting polygons*/
  private int[] polygonIndexes;

  /**The IDs of the features that correspond to the given polygons.*/
  private long[] featureIDs;

  /**Tile IDs for each intersection*/
  private int[] tileID;

  /**
   * Header of the intersections file.
   * This is the first four bytes of the md5sum of &quot;UCR Raptor Intersections&quot;
   */
  public static final byte[] FileSignature = {(byte) 0x88, (byte) 0xf0, (byte) 0x6a, (byte) 0x84};

  /**
   * Compute the intersections between the given array of geometries and the raster layer.
   * @param geometries the list of geometries to compute the intersectios to
   * @param raster the raster file to compute the intersections against
   */
  public void compute(IGeometry[] geometries, RasterReader raster) {
    long[] featureIDs = new long[geometries.length];
    for (int i = 0; i < geometries.length; i++)
      featureIDs[i] = i;
    compute(geometries, featureIDs, raster);
  }

  public void compute(ArrayList<Long> IDs, IGeometry[] geometries, RasterReader raster) {
    long[] featureIDs = new long[IDs.size()];
    for (int i = 0; i < geometries.length; i++)
        featureIDs[i] = IDs.get(i);
    compute(geometries, featureIDs, raster);
  }


  /**
   * Computes the intersections for the given list of geometries against the given raster.
   * @param geometries the list of geometries to compute the intersections to
   * @param featureIDs the identifier of each feature corresponding to the given polygons
   * @param raster the raster file to compute the intersections against
   */
  public void compute(IGeometry[] geometries, long[] featureIDs, RasterReader raster) {
    this.featureIDs = Arrays.copyOf(featureIDs, featureIDs.length);
    // Compute the MBR of all geometries to find the area of interest in the raster
    Envelope mbr = new Envelope(2);
    for (IGeometry geometry : geometries)
      mbr.merge(geometry);
    // Map the MBR to the raster file to find the first and last scanlines to process
    Point2D.Double corner1 = new Point2D.Double();
    raster.modelToGrid(mbr.minCoord[0], mbr.minCoord[1], corner1);
    Point2D.Double corner2 = new Point2D.Double();
    raster.modelToGrid(mbr.maxCoord[0], mbr.maxCoord[1], corner2);

    int scanlineStart = Math.max(raster.getY1(), (int) Math.round(Math.min(corner1.y, corner2.y)));
    int scanlineEnd = Math.min(raster.getY2(), (int) Math.round(Math.max(corner1.y, corner2.y)));
    // The vector geometries are out of the bounds of the raster data
    if (scanlineStart >= scanlineEnd)
      return;
    double[] scanlinesY = new double[scanlineEnd - scanlineStart];
    Point2D.Double point = new Point2D.Double();
    for (int scanline = scanlineStart; scanline < scanlineEnd; scanline++) {
      raster.gridToModel((int) corner1.x, scanline, point);
      scanlinesY[scanline - scanlineStart] = point.getY();
    }

    if(geometries[0].getType()==GeometryType.MULTILINESTRING || geometries[0].getType()==GeometryType.LINESTRING) {
      for (int $i = 0; $i < geometries.length; $i++)
        appendIntersectionsLS($i, geometries[$i], raster, scanlineStart, scanlineEnd, scanlinesY);

    }
    else{
      for (int $i = 0; $i < geometries.length; $i++)
        appendIntersections($i, geometries[$i], raster, scanlineStart, scanlineEnd, scanlinesY);

    }
      // Compute the intersections for all geometries in any order

    if (numIntersections == 0)
      return;

    assert (numIntersections & 1) == 0 : "Number of intersections should be even";

    // If there are intersections, sort them
    tileID = new int[numIntersections];

    // Sort the intersection points by <y, polygon ID, x> (all tileIDs are now zeros)
    IndexedSortable intersectionSorter = new IndexedSortable() {
      @Override
      public int compare(int i, int j) {
        int diff = tileID[i] - tileID[j];
        if (diff == 0) {
          diff = ys[i] - ys[j];
          if (diff == 0) {
            diff = polygonIndexes[i] - polygonIndexes[j];
            if (diff == 0)
              diff = xs[i] - xs[j];
          }
        }
        return diff;
      }

      @Override
      public void swap(int i, int j) {
        // Swap y, x, pid, and tileID
        int temp = ys[i];
        ys[i] = ys[j];
        ys[j] = temp;
        temp = xs[i];
        xs[i] = xs[j];
        xs[j] = temp;
        temp = polygonIndexes[i];
        polygonIndexes[i] = polygonIndexes[j];
        polygonIndexes[j] = temp;
        temp = tileID[i];
        tileID[i] = tileID[j];
        tileID[j] = temp;
      }
    };
    // Sort the intersection points by <polygon ID,y, x> (all tileIDs are now zeros)
    IndexedSortable polygonIndexSorter = new IndexedSortable() {
      @Override
      public int compare(int i, int j) {
        int diff = tileID[i] - tileID[j];
        if (diff == 0) {
          diff = polygonIndexes[i] - polygonIndexes[j];
          if (diff == 0) {
            diff = ys[i] - ys[j];
            if (diff == 0)
              diff = xs[i] - xs[j];
          }
        }
        return diff;
      }

      @Override
      public void swap(int i, int j) {
        // Swap y, x, pid, and tileID
        int temp = ys[i];
        ys[i] = ys[j];
        ys[j] = temp;
        temp = xs[i];
        xs[i] = xs[j];
        xs[j] = temp;
        temp = polygonIndexes[i];
        polygonIndexes[i] = polygonIndexes[j];
        polygonIndexes[j] = temp;
        temp = tileID[i];
        tileID[i] = tileID[j];
        tileID[j] = temp;
      }
    };
    new QuickSort().sort(polygonIndexSorter, 0, numIntersections);
    //new QuickSort().sort(intersectionSorter,0, numIntersections);
    int numRemovedIntersections = 0;
    // Now, make one pass over all the intersections and set the tile IDs
    // For intersection segments that cross over tile boundaries, add fake points to separate them
    if(geometries[0].getType()==GeometryType.MULTILINESTRING || geometries[0].getType()==GeometryType.LINESTRING) {

      for (int $i = numIntersections - 1; $i >= 0; $i -= 1) {

          tileID[$i] = raster.getTileID(xs[$i], ys[$i]);
          if($i!=numIntersections -1 &&  tileID[$i+1]==tileID[$i] && polygonIndexes[$i+1] == polygonIndexes[$i] && ys[$i]==ys[$i+1] && xs[$i]<xs[$i+1])
          {
            tileID[$i + 1] = raster.getNumTiles();
            numRemovedIntersections += 1;
          }
          // Decrement the end of the intersection segment to make the range inclusive
          //tileID[$i + 1] = raster.getTileID(xs[$i + 1], ys[$i + 1]);
          //assert polygonIndexes[$i] == polygonIndexes[$i + 1];
          //if (tileID[$i] != tileID[$i + 1]) {
            // The intersection segment crosses the tile boundary.
            // Break the range into two at the tile boundary by adding two new intersections, one at each side
          else{  makeRoomForAdditionalIntersections(numIntersections + 1);
            tileID = DynamicArrays.expand(tileID, xs.length);
            xs[numIntersections +1] = xs[$i] + 1;
            ys[numIntersections + 1] = ys[$i];
            tileID[numIntersections + 1] = tileID[$i];
            polygonIndexes[numIntersections + 1] = polygonIndexes[$i];
            //tileID[$i + 1] = tileID[$i];
            numIntersections += 1;}

          //}

      }
    }
    else{
    for (int $i = numIntersections - 2; $i >= 0; $i -= 2) {
      if (xs[$i] >= xs[$i + 1]) {
        // An empty range. Mark it for removal.
        // The largest tileID will ensure that they will be at the end of the list after sorting
        // which will make them easy to remove
        tileID[$i] = tileID[$i + 1] = raster.getNumTiles();
        numRemovedIntersections += 2;
      } else {
        tileID[$i] = raster.getTileID(xs[$i], ys[$i]);
        // Decrement the end of the intersection segment to make the range inclusive
        xs[$i + 1]--;
        tileID[$i + 1] = raster.getTileID(xs[$i + 1], ys[$i + 1]);
        assert polygonIndexes[$i] == polygonIndexes[$i + 1];
        if (tileID[$i] != tileID[$i + 1]) {
          // The intersection segment crosses the tile boundary.
          // Break the range into two at the tile boundary by adding two new intersections, one at each side
          makeRoomForAdditionalIntersections(numIntersections + 2);
          tileID = DynamicArrays.expand(tileID, xs.length);
          xs[numIntersections] = raster.getTileX1(tileID[$i + 1]);
          xs[numIntersections + 1] = xs[$i + 1];
          ys[numIntersections] = ys[numIntersections + 1] = ys[$i];
          tileID[numIntersections] = tileID[numIntersections + 1] = tileID[$i + 1];
          polygonIndexes[numIntersections] = polygonIndexes[numIntersections + 1] = polygonIndexes[$i];
          xs[$i + 1] = raster.getTileX2(tileID[$i]);
          tileID[$i + 1] = tileID[$i];
          numIntersections += 2;

      }
    }
  }
    }
    //new QuickSort().sort(intersectionSorter, 0, numIntersections);
    // Sort again after adding the tile IDs. Sort order will be <tile ID, polygon index,y, x>
    new QuickSort().sort(polygonIndexSorter, 0, numIntersections);
    // Now, remove all the empty ranges that have been pushed to the end
    numIntersections -= numRemovedIntersections;
    // Finally, convert the intersections to ranges in the form (tile ID, y, polygon index, x1, x2)
         numIntersections /= 2;
      int[] newYs = new int[numIntersections];
      int[] newTileIDs = new int[numIntersections];
      int[] newPolygonIndex = new int[numIntersections];

      for (int i = 0; i < numIntersections; i++) {
        newYs[i] = ys[i * 2];
        newTileIDs[i] = tileID[i * 2];
        newPolygonIndex[i] = polygonIndexes[i * 2];
      }

      ys = newYs;
      tileID = newTileIDs;
      polygonIndexes = newPolygonIndex;

  }

  protected void makeRoomForAdditionalIntersections(int newSize) {
    int nextPowerOfTwo = Integer.highestOneBit(newSize) * 2;
    xs = DynamicArrays.expand(xs, nextPowerOfTwo);
    ys = DynamicArrays.expand(ys, nextPowerOfTwo);
    polygonIndexes = DynamicArrays.expand(polygonIndexes, nextPowerOfTwo);
  }

  /**
   * Appends intersections of the given geometry to the list of intersections in any order.
   * @param geometryID the ID of this geometry to add to the list of intersections
   * @param geometry the geometry to compute the intersections to
   * @param raster the raster file
   * @param scanlineStart the first scanline to consider
   * @param scanlineEnd the last scanline to consider
   * @param scanlinesY the y-coordinates of the scan lines being processed
   */
  protected void appendIntersections(int geometryID, IGeometry geometry, RasterReader raster,
                                     int scanlineStart, int scanlineEnd, double[] scanlinesY) {
    ILineString linestring;
    IPolygon polygon;
    final Point tempPointCoords = new Point(2);
    switch (geometry.getType()) {
      case POLYGON:
      case MULTIPOLYGON:
        polygon = (IPolygon) geometry;
        for (int $i = 0; $i < polygon.getNumRings(); $i++) {
          ILineString ring = polygon.getRingN($i);
          appendIntersections(geometryID, ring, raster, scanlineStart, scanlineEnd, scanlinesY);
        }
        break;
      case LINESTRING:
        linestring = (ILineString) geometry;
        Point2D.Double pt1 = new Point2D.Double();
        // The +0.5 is to ensure that we process the line only if the line segment crosses the middle line (scanline)
        double x1, y1, x2, y2;
        linestring.getPointN(0, tempPointCoords);
        raster.modelToGrid(x1 = tempPointCoords.coords[0], y1 = tempPointCoords.coords[1], pt1);
        Point2D.Double pt2 = new Point2D.Double();
        for (int $i = 1; $i < linestring.getNumPoints(); $i++) {
          linestring.getPointN($i, tempPointCoords);
          raster.modelToGrid(x2 = tempPointCoords.coords[0], y2 = tempPointCoords.coords[1], pt2);
          double dx = x2 - x1;
          double dy = y2 - y1;

          // Locate the first and last rows in the raster file for this line segment
          int row1 = Math.max(scanlineStart, (int) Math.round(Math.min(pt1.y, pt2.y)));
          int row2 = Math.min(scanlineEnd, (int) Math.round(Math.max(pt1.y, pt2.y)));


          for (int row = row1; row < row2; row++) {
            // Find the intersection of the line segment (p1, p2) and the straight line (y = scanLinesY[row])
            double xIntersection = x2 - dx * (y2 - scanlinesY[row - scanlineStart]) / dy;
            raster.modelToGrid(xIntersection, scanlinesY[row - scanlineStart], pt1);
            makeRoomForAdditionalIntersections(numIntersections + 1);
            xs[numIntersections] = Math.max(raster.getX1(), (int) Math.min(Math.round(pt1.x), raster.getX2()));
            ys[numIntersections] = (int) pt1.y;
            polygonIndexes[numIntersections] = geometryID;
            numIntersections++;
          }
          pt1.setLocation(pt2);
          x1 = x2;
          y1 = y2;

        }
        break;
      case POINT:
        Point point = (Point) geometry;
        Point2D.Double temp_pt = new Point2D.Double();
        raster.modelToGrid(point.coords[0],point.coords[1], temp_pt);
        if((scanlineStart <= (int) Math.round(temp_pt.y)) && (scanlineEnd > (int) Math.round(temp_pt.y)))
        {
          makeRoomForAdditionalIntersections(numIntersections + 1);

          xs[numIntersections] = Math.max(raster.getX1(), (int) Math.min(Math.round(temp_pt.x), raster.getX2()));
          ys[numIntersections] = (int) temp_pt.y;
          polygonIndexes[numIntersections] = geometryID;
          numIntersections++;

          makeRoomForAdditionalIntersections(numIntersections + 1);

          xs[numIntersections] = Math.max(raster.getX1(), (int) Math.min(Math.round(temp_pt.x), raster.getX2())) + 1;
          ys[numIntersections] = (int) temp_pt.y;
          polygonIndexes[numIntersections] = geometryID;
          numIntersections++;
        }

        break;

      default:
        throw new RuntimeException("Unsupported geometry type "+geometry.getType());
    }
  }

  protected void appendIntersectionsLS(int geometryID, IGeometry geometry, RasterReader raster,
                                     int scanlineStart, int scanlineEnd, double[] scanlinesY) {
    ILineString linestring;
    IPolygon polygon;
    IMultiLineString multiLineString;
    final Point tempPointCoords = new Point(2);
    switch (geometry.getType()) {
      case POLYGON:
      case MULTIPOLYGON:
        polygon = (IPolygon) geometry;
        for (int $i = 0; $i < polygon.getNumRings(); $i++) {
          ILineString ring = polygon.getRingN($i);
          appendIntersectionsLS(geometryID, ring, raster, scanlineStart, scanlineEnd, scanlinesY);
        }
        break;
      case MULTILINESTRING:
        //separate linestrings
        multiLineString = (IMultiLineString) geometry;
        for (int $i = 0; $i < multiLineString.getNumLineStrings(); $i++) {
          ILineString ring = multiLineString.getLineStringN($i);
          appendIntersectionsLS(geometryID, ring, raster, scanlineStart, scanlineEnd, scanlinesY);
        }
      case LINESTRING:
        linestring = (ILineString) geometry;
        Point2D.Double pt1 = new Point2D.Double();
        // The +0.5 is to ensure that we process the line only if the line segment crosses the middle line (scanline)
        double x1, y1, x2, y2;
        int Xa1,Xa2,Ya1,Ya2;
        linestring.getPointN(0, tempPointCoords);
        raster.modelToGrid(x1 = tempPointCoords.coords[0], y1 = tempPointCoords.coords[1], pt1);
        Point2D.Double pt2 = new Point2D.Double();
        for (int $i = 1; $i < linestring.getNumPoints(); $i++) {
          linestring.getPointN($i, tempPointCoords);
          raster.modelToGrid(x2 = tempPointCoords.coords[0], y2 = tempPointCoords.coords[1], pt2);
          //double dx = x2 - x1;
          //double dy = y2 - y1;

          if((int)pt1.x < (int)pt2.x)
          {
            Xa1 = (int)pt1.x;
            Xa2 = (int) pt2.x;
            Ya1 = (int) pt1.y;
            Ya2 = (int) pt2.y;

          }
          else
          {
            Xa1 = (int)pt2.x;
            Xa2 = (int) pt1.x;
            if((int)pt1.x == (int)pt2.x)
            {
              Ya1 = (int)pt1.y < (int)pt2.y?(int)pt1.y:(int)pt2.y;
              Ya2 = (int)pt1.y > (int)pt2.y?(int)pt1.y:(int)pt2.y;

            }
            else
            {
              Ya1 = (int) pt2.y;
              Ya2 = (int) pt1.y;
            }
          }


          // Locate the first and last rows in the raster file for this line segment
          int row1 = Math.max(scanlineStart, (int) Math.round(Math.min(pt1.y, pt2.y)));
          int row2 = Math.min(scanlineEnd, (int) Math.round(Math.max(pt1.y, pt2.y)));


          //more efficient for horizontal lines
          int dax = Xa2 - Xa1;
          int day = Ya2 - Ya1;
          //int da = day - (dax/2);
          int x = Xa1, y = Ya1;
          int stepy = 0;
          int stepx = 0;
          if (day < 0) { day = -day; stepy = -1; } else { stepy = 1; }

          if (dax < 0) { dax = -dax; stepx = -1; } else { stepx = 1; }
          if(day == 0)
          {
            while (x<=Xa2)
            {if(y<row2 && y>=row1 && x <= raster.getX2() && x >=raster.getX1()){
              makeRoomForAdditionalIntersections(numIntersections + 1);
              xs[numIntersections] = x;
              ys[numIntersections] = y;
              polygonIndexes[numIntersections] = geometryID;
              numIntersections++;
              //makeRoomForAdditionalIntersections(numIntersections + 1);
              //decrement later
              //xs[numIntersections] =  x==Xa2?x+1:Xa2;
              //ys[numIntersections] = y;
              //polygonIndexes[numIntersections] = geometryID;
              //numIntersections++;
            }
            x++;
            }
          }
          else{
            if(Ya1<row2 && Ya1>=row1 && Xa1 <= raster.getX2() && Xa1 >=raster.getX1()){
              makeRoomForAdditionalIntersections(numIntersections + 1);
              xs[numIntersections] = Xa1;
              ys[numIntersections] = Ya1;
              polygonIndexes[numIntersections] = geometryID;
              numIntersections++;
              //makeRoomForAdditionalIntersections(numIntersections + 1);
              //decrement later
              //xs[numIntersections] = Xa1 + 1;
              //ys[numIntersections] = Ya1;
              //polygonIndexes[numIntersections] = geometryID;
             // numIntersections++;
            }
            day*=2;
            dax*=2;
            if (dax > day) {
              int fraction = day - (dax/2);
              while (x != Xa2) {
                x += stepx;
                if (fraction >= 0) {
                  y += stepy;
                  fraction -= dax;
                }
                fraction += day;
                if(y<row2 && y>=row1 && x <= raster.getX2() && x >=raster.getX1()){
                  makeRoomForAdditionalIntersections(numIntersections + 1);
                  xs[numIntersections] = x;
                  ys[numIntersections] = y;
                  polygonIndexes[numIntersections] = geometryID;
                  numIntersections++;
                  //makeRoomForAdditionalIntersections(numIntersections + 1);
                  //decrement later
                 // xs[numIntersections] = x + 1;
                 // ys[numIntersections] = y;
                  //polygonIndexes[numIntersections] = geometryID;
                  //numIntersections++;
                }
              }
            } else {
              int fraction = dax - (day/2);
              while (y != Ya2) {
                if (fraction >= 0) {
                  x += stepx;
                  fraction -= day;
                }
                y += stepy;
                fraction += dax;
                if(y<row2 && y>=row1 && x <= raster.getX2() && x >=raster.getX1()){
                  makeRoomForAdditionalIntersections(numIntersections + 1);
                  xs[numIntersections] = x;
                  ys[numIntersections] = y;
                  polygonIndexes[numIntersections] = geometryID;
                  numIntersections++;
                  //makeRoomForAdditionalIntersections(numIntersections + 1);
                  //decrement later
                  //xs[numIntersections] = x + 1;
                 // ys[numIntersections] = y;
                 // polygonIndexes[numIntersections] = geometryID;
                 // numIntersections++;
                }
              }
            }
          }

          pt1.setLocation(pt2);
          x1 = x2;
          y1 = y2;
        }
        break;
      case POINT:
        Point point = (Point) geometry;
        Point2D.Double temp_pt = new Point2D.Double();
        raster.modelToGrid(point.coords[0],point.coords[1], temp_pt);

        if((scanlineStart <= (int) Math.round(temp_pt.y)) && (scanlineEnd > (int) Math.round(temp_pt.y)))
        {
          makeRoomForAdditionalIntersections(numIntersections + 1);

          xs[numIntersections] = Math.max(raster.getX1(), (int) Math.min(Math.round(temp_pt.x), raster.getX2()));
          ys[numIntersections] = (int) temp_pt.y;
          polygonIndexes[numIntersections] = geometryID;
          numIntersections++;

          makeRoomForAdditionalIntersections(numIntersections + 1);

          xs[numIntersections] = Math.max(raster.getX1(), (int) Math.min(Math.round(temp_pt.x), raster.getX2())) + 1;
          ys[numIntersections] = (int) temp_pt.y;
          polygonIndexes[numIntersections] = geometryID;
          numIntersections++;
        }

        break;

      default:
        throw new RuntimeException("Unsupported geometry type "+geometry.getType());
    }
  }

  /**
   * Returns the total number of intersection segments.
   * @return the total number of intersections
   */
  public int getNumIntersections() {
    return numIntersections;
  }

  /**
   * Returns the start x-coordinate of the i<sup>th</sup> intersection
   * @param i the intersection number
   * @return the start x-coordinate of that intersection (inclusive)
   */
  public int getX1(int i) {
    return xs[i * 2];
  }

  /**
   * Returns the end x-coordinate (inclusive) of the i<sup>th</sup> intersection
   * @param i the intersection number
   * @return the end x-coordinate of that intersection (inclusive)
   */
  public int getX2(int i) {
    return xs[i * 2 + 1];
  }

  /**
   * Returns the y-coordinate of the i<sup>th</sup> intersection
   * @param i the intersection number
   * @return the y-coordinate of that intersection
   */
  public int getY(int i) {
    return ys[i];
  }

  /**
   * Return the index of the polygon that corresponds to the given intersection.
   * @param i the position of the polygon to get its index
   * @return the index of the given polygon
   */
  public int getPolygonIndex(int i) {
    return polygonIndexes[i];
  }

  public long getFeatureID(int i){return featureIDs[polygonIndexes[i]];}

  public int getTileID(int i){return tileID[i];}

  /**
   * Write all the intersections to the given file.
   * The format of the file is as follows:
   * <ul>
   *   <li>4-bytes: A fixed signature of [0x88, 0xf0, 0x6a, 0x84].</li>
   *   <li>8-bytes: Number of polygons stored in this intersection file</li>
   *   <li>8-bytes * number of polygons: An array of feature IDs as 8-byte long integers for each feature.</li>
   *   <li>8-bytes:  Number of tiles in the intersection file that have intersections.</li>
   *   <li>16-bytes * number of tiles: An array of pairs (tileID, number of intersections),
   *   each pair as two 8-byte long integers.</li>
   *   <li>Intersections: The rest of the file is all intersections stored as tuples of
   *   (y, polygon index, x1, x2) with 4-byte integer for each of them. The intersections are sorted
   *   by (tile ID, y, polygon index) so all intersections for the first tile will appear
   *   first. This allows random access to a specific tile based on the header information.</li>
   * </ul>
   * @param fileSystem the file system to write to
   * @param path the path of the file to write
   * @throws IOException if an error happens while writing the file
   */
  public void writeAll(FileSystem fileSystem, Path path) throws IOException {
    try (FSDataOutputStream out = fileSystem.create(path)) {
      // 1. Write signature
      out.write(FileSignature);
      // 2. Write feature identifiers
      out.writeLong(featureIDs.length);
      for (long featureID : featureIDs)
        out.writeLong(featureID);
      // 3a. Compute the sizes of tiles and exclude empty tiles
      int numTiles = tileID[numIntersections - 1] + 1;
      int[] tileSizes = new int[numTiles];
      for (int i = 0; i < numIntersections; i++)
        tileSizes[tileID[i]]++;
      int numNonEmptyTiles = 0;
      for (int tileSize : tileSizes) {
        if (tileSize > 0)
          numNonEmptyTiles++;
      }
      // 3b.Write tile IDs and their sizes
      out.writeLong(numNonEmptyTiles);
      for (int tileIndex = 0; tileIndex < tileSizes.length; tileIndex++) {
        if (tileSizes[tileIndex] > 0) {
          out.writeLong(tileIndex);
          // Divide the size by two because each pair of intersections [x1, x2] is written as one tuple
          out.writeLong(tileSizes[tileIndex]);
        }
      }
      // 4. Write all intersection tuples (y, polygon index, x1, x2)
      for (int i = 0; i < numIntersections; i++) {
        out.writeInt(ys[i]);
        out.writeInt(polygonIndexes[i]);
        out.writeInt(xs[2*i]);
        out.writeInt(xs[2*i+1]);
      }
    }
  }

  /**
   * Read all intersections back from the given file.
   * @param fileSystem the file system that contains the file
   * @param path the path of the file
   */
  public void readAll(FileSystem fileSystem, Path path) {
    readIntersectionsInTiles(fileSystem, path, 0, Integer.MAX_VALUE);
  }

  /**
   * Read intersections in the given range of tileIDs [tileIDStart, tileIDEnd] inclusive of both.
   * @param fileSystem the file system to read the intersections from
   * @param path the path of the intersections file
   * @param tileIDStart the first tile ID to read from the file (inclusive)
   * @param tileIDEnd the last tile ID to read from the file (inclusive)
   */
  public void readIntersectionsInTiles(FileSystem fileSystem, Path path, int tileIDStart, int tileIDEnd) {
    try (FSDataInputStream in = fileSystem.open(path)) {
      // 1. Read signature
      byte[] signature = new byte[4];
      in.readFully(signature);
      if (!Arrays.equals(FileSignature, signature))
        throw new RuntimeException("File signature does not match");
      // 2. Read feature identifiers
      int numFeatures = (int) in.readLong();
      featureIDs = new long[numFeatures];
      for (int i = 0; i < numFeatures; i++)
        featureIDs[i] = in.readLong();
      // 3. Read tile IDs and sizes
      int numIntersectionsToSkip = 0;
      int currentTile = 0;
      numIntersections = 0;
      int numTiles = (int) in.readLong();
      int[] tileIDs = new int[numTiles];
      int[] tileSizes = new int[numTiles];
      for (int i = 0; i < tileIDs.length; i++) {
        tileIDs[i] = (int) in.readLong();
        tileSizes[i] = (int) in.readLong();
        if (tileIDs[i] < tileIDStart) {
          numIntersectionsToSkip += tileSizes[i];
          currentTile++;
        } else if (tileIDs[i] >= tileIDStart && tileIDs[i] <= tileIDEnd)
          numIntersections += tileSizes[i];
      }
      // 4. Read intersections in the given range
      xs = new int[2*numIntersections];
      ys = new int[numIntersections];
      polygonIndexes = new int[numIntersections];
      tileID = new int[numIntersections];
      in.skipBytes(numIntersectionsToSkip * 4 * 4);
      for (int i = 0; i < numIntersections; i++) {
        while (tileSizes[currentTile] == 0) {
          assert currentTile < tileIDEnd;
          currentTile++;
        }
        ys[i] = in.readInt();
        polygonIndexes[i] =in.readInt();
        xs[2*i] = in.readInt();
        xs[2*i+1] = in.readInt();
        tileID[i] = tileIDs[currentTile];
        tileSizes[currentTile]--;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
