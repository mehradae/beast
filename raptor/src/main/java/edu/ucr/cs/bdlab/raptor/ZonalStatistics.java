/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.ILineString;
import edu.ucr.cs.bdlab.geolite.IPolygon;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Computes the zonal statistics problem for a raster file given a set of computed intersections.
 */
public class ZonalStatistics {

  /**
   * Computes zonal statistics on the given raster for the given set of intersections.
   * Returns an array of statistics as one for each polygon ID the appears in the intersections.
   * @param raster the raster file
   * @param intersections the intersections
   * @param collectorClass the class of the collector used to accumulate pixel values
   * @return the list of collectors for the result for the input polygons
   */
  public static Collector[] computeZonalStatisticsScanline(RasterReader raster, Intersections intersections,
                                                           Class<? extends Collector> collectorClass) {
    try {
      // Find the largest polygon ID to create an array of the right size
      int lastPolygonID = -1;
      for (int $i = 0; $i < intersections.getNumIntersections(); $i++)
        lastPolygonID = Math.max(lastPolygonID, intersections.getPolygonIndex($i));
      // Prepare a list of results
      Collector[] results = new Collector[lastPolygonID + 1];
      // Compute the results
      float[] pixelValues = new float[raster.getNumComponents()];
      for (int $i = 0; $i < intersections.getNumIntersections(); $i ++) {
        int y = intersections.getY($i);
        int x1 = intersections.getX1($i);
        int x2 = intersections.getX2($i);
        int pid = intersections.getPolygonIndex($i);
        while (x1 <= x2) {
          raster.getPixelValueAsFloat(x1, y, pixelValues);
          if (pixelValues[0] != raster.getFillValue()) {
            if (results[pid] == null) {
              results[pid] = collectorClass.newInstance();
              results[pid].setNumBands(raster.getNumComponents());
            }
            results[pid].collect(x1, y, pixelValues);
          }
          x1++;
        }
      }
      return results;
    } catch (IllegalAccessException | InstantiationException e) {
      throw new RuntimeException("Error creating collector", e);
    } catch (IOException e) {
      throw new RuntimeException("Error reading raster data", e);
    }
  }

  public static Collector[] computeZonalStatisticsScanline(RasterReader raster, IGeometry[] geometries,
                                                           Class<? extends Collector> collectorClass) {
    Intersections intersections = new Intersections();
    intersections.compute(geometries, raster);
    return computeZonalStatisticsScanline(raster, intersections, collectorClass);
  }

  public static Collector[] computeZonalStatisticsNaive(RasterReader raster, IGeometry[] geometries,
                                                        Class<? extends Collector> collectorClass) {
    try {
      Collector[] results = new Collector[geometries.length];
      for (int iGeom = 0; iGeom < geometries.length; iGeom++) {
        results[iGeom] = collectorClass.newInstance();
        results[iGeom].setNumBands(raster.getNumComponents());
        computeZonalStatisticsNaive(raster, geometries[iGeom], results[iGeom]);
      }
      return results;
    } catch (IllegalAccessException | InstantiationException e) {
      throw new RuntimeException("Error creating collector", e);
    } catch (IOException e) {
      throw new RuntimeException("Error reading raster data", e);
    }
  }

  public static void computeZonalStatisticsNaive(RasterReader raster, IGeometry geometry, Collector result) throws IOException {
    Point2D.Double p = new Point2D.Double();
    Point2D.Double corner1 = new Point2D.Double();
    Point2D.Double corner2 = new Point2D.Double();
    float[] pixelValue = new float[raster.getNumComponents()];
    Envelope mbr = new Envelope(2);
    geometry.envelope(mbr);
    raster.modelToGrid(mbr.minCoord[0], mbr.minCoord[1], corner1);
    raster.modelToGrid(mbr.maxCoord[0], mbr.maxCoord[1], corner2);
    int i1 = (int) Math.max(0, Math.min(corner1.x, corner2.x));
    int i2 = (int) Math.min(raster.getRasterWidth(), Math.ceil(Math.max(corner1.x, corner2.x)));
    int j1 = (int) Math.max(0, Math.min(corner1.y, corner2.y));
    int j2 = (int) Math.min(raster.getRasterHeight(), Math.ceil(Math.max(corner1.y, corner2.y)));
    for (int iPixel = i1; iPixel < i2; iPixel++)
      for (int jPixel = j1; jPixel < j2; jPixel++) {
        raster.gridToModel(iPixel, jPixel, p);

        if (geometry.contains(new Point(p.x, p.y))) {
          raster.getPixelValueAsFloat(iPixel, jPixel, pixelValue);
          if (pixelValue[0] != raster.getFillValue())
            result.collect(iPixel, jPixel, pixelValue);
        }
      }
  }

  public static Collector[] computeZonalStatisticsQuadSplit(RasterReader raster, IGeometry[] geometries,
                                                        Class<? extends Collector> collectorClass) {
    try {
      Collector[] results = new Collector[geometries.length];
      for (int iGeom = 0; iGeom < geometries.length; iGeom++) {
        results[iGeom] = collectorClass.newInstance();
        results[iGeom].setNumBands(raster.getNumComponents());
        computeZonalStatisticsQuadSplit(raster, geometries[iGeom], results[iGeom]);
      }
      return results;
    } catch (IllegalAccessException | InstantiationException e) {
      throw new RuntimeException("Error creating collector", e);
    } catch (IOException e) {
      throw new RuntimeException("Error reading raster data", e);
    }
  }

  public static void computeZonalStatisticsQuadSplit(RasterReader raster, IGeometry geometry, Collector result) throws IOException {
    Stack<IGeometry> geoms = new Stack();
    geoms.push(geometry);
    Envelope mbr = new Envelope(2);
    Envelope quad = new Envelope(2);
    while (!geoms.isEmpty()) {
      IGeometry subgeom = geoms.pop();
      if (subgeom.getNumPoints() < 50) {
        // Geometry is simple enough to process using the naive algorithm
        computeZonalStatisticsNaive(raster, subgeom, result);
      } else {
        // Geometry is complex. Split into four
        subgeom.envelope(mbr);
        double centerX = mbr.getCenter(0);
        double centerY = mbr.getCenter(1);
        // First quadrant
        quad.minCoord[0] = mbr.minCoord[0];
        quad.minCoord[1] = mbr.minCoord[1];
        quad.maxCoord[0] = centerX;
        quad.maxCoord[1] = centerY;
        geoms.push(subgeom.intersection(quad, null));
        // Second quadrant
        quad.minCoord[0] = centerX;
        quad.minCoord[1] = mbr.minCoord[1];
        quad.maxCoord[0] = mbr.maxCoord[0];
        quad.maxCoord[1] = centerY;
        geoms.push(subgeom.intersection(quad, null));
        // Third quadrant
        quad.minCoord[0] = mbr.minCoord[0];
        quad.minCoord[1] = centerY;
        quad.maxCoord[0] = centerX;
        quad.maxCoord[1] = mbr.maxCoord[1];
        geoms.push(subgeom.intersection(quad, null));
        // Fourth quadrant
        quad.minCoord[0] = centerX;
        quad.minCoord[1] = centerY;
        quad.maxCoord[0] = mbr.maxCoord[0];
        quad.maxCoord[1] = mbr.maxCoord[1];
        geoms.push(subgeom.intersection(quad, null));
      }
    }
  }

  public static Collector[] computeZonalStatisticsMasking(RasterReader raster, IGeometry[] geometries,
                                                            Class<? extends Collector> collectorClass) {
    try {
      Collector[] results = new Collector[geometries.length];
      for (int iGeom = 0; iGeom < geometries.length; iGeom++) {
        results[iGeom] = collectorClass.newInstance();
        results[iGeom].setNumBands(raster.getNumComponents());
        computeZonalStatisticsMasking(raster, geometries[iGeom], results[iGeom]);
      }
      return results;
    } catch (IllegalAccessException | InstantiationException e) {
      throw new RuntimeException("Error creating collector", e);
    } catch (IOException e) {
      throw new RuntimeException("Error reading raster data", e);
    }
  }

  public static void computeZonalStatisticsMasking(RasterReader raster, IGeometry geometry, Collector result) throws IOException {
    Point2D.Double p = new Point2D.Double();
    Point2D.Double corner1 = new Point2D.Double();
    Point2D.Double corner2 = new Point2D.Double();
    float[] pixelValue = new float[raster.getNumComponents()];
    Envelope mbr = new Envelope(2);
    geometry.envelope(mbr);
    raster.modelToGrid(mbr.minCoord[0], mbr.minCoord[1], corner1);
    raster.modelToGrid(mbr.maxCoord[0], mbr.maxCoord[1], corner2);
    int i1 = (int) Math.max(0, Math.min(corner1.x, corner2.x));
    int i2 = (int) Math.min(raster.getRasterWidth(), Math.ceil(Math.max(corner1.x, corner2.x)));
    int j1 = (int) Math.max(0, Math.min(corner1.y, corner2.y));
    int j2 = (int) Math.min(raster.getRasterHeight(), Math.ceil(Math.max(corner1.y, corner2.y)));

    List<IPolygon> polygons = new ArrayList<IPolygon>();
    if (geometry.getType() == GeometryType.POLYGON) {
      polygons.add((IPolygon) geometry);
    } else if (geometry.getType() == GeometryType.MULTIPOLYGON) {
      MultiPolygon2D multipoly = (MultiPolygon2D) geometry;
      for (int iPolygon = 0; iPolygon < multipoly.numPolygons; iPolygon++) {
        Polygon2D poly = new Polygon2D();
        multipoly.getPolygonN(iPolygon, poly);
        polygons.add(poly);
      }
    }
    BufferedImage mask = new BufferedImage(i2 - i1, j2 - j1, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = mask.createGraphics();
    graphics.setColor(Color.WHITE);
    graphics.fillRect(0, 0, i2 - i1, j2 - j1);
    for (IPolygon poly : polygons) {
      ILineString outerRing = poly.getExteriorRing();
      graphics.setColor(Color.BLACK);
      fillPolygon(graphics, i2 - i1, j2 - j1, mbr, outerRing);
      graphics.setColor(Color.WHITE);
      for (int iRing = 0; iRing < poly.getNumInteriorRings(); iRing++) {
        ILineString innerRing = poly.getInteriorRingN(iRing);
        fillPolygon(graphics, i2 - i1, j2 - j1, mbr, innerRing);
      }
    }
    graphics.dispose();
    for (int iPixel = i1; iPixel < i2; iPixel++)
      for (int jPixel = j1; jPixel < j2; jPixel++) {
        int color = mask.getRGB(iPixel - i1, jPixel - j1) & 0xff;
        if (color == 0) {
          // Black pixel. Inside the polygon. Process it.
          raster.getPixelValueAsFloat(iPixel, jPixel, pixelValue);
          if (pixelValue[0] != raster.getFillValue())
            result.collect(iPixel, jPixel, pixelValue);
        }
      }
  }

  public static void fillPolygon(Graphics2D graphics, int imageWidth, int imageHeight, Envelope spaceMBR, ILineString ring) {
    int numPoints = (int) ring.getNumPoints();
    int[] xs = new int[numPoints];
    int[] ys = new int[numPoints];
    Point point = new Point(ring.getCoordinateDimension());
    for (int iPoint = 0; iPoint < numPoints; iPoint++) {
      ring.getPointN(iPoint, point);
      xs[iPoint] = (int) ((point.coords[0] - spaceMBR.minCoord[0]) * imageWidth / spaceMBR.getSideLength(0));
      ys[iPoint] = (int) ((point.coords[1] - spaceMBR.minCoord[1]) * imageHeight / spaceMBR.getSideLength(1));
    }
    graphics.fillPolygon(xs, ys, numPoints);
  }

}
