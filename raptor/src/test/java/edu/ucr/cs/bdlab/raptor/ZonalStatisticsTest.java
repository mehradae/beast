/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor;

import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.io.ShapefileGeometryReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ZonalStatisticsTest extends SparkTest {

  static final double[] expectedCounts = {17,25,15,2,14,13,17,27,15,17,25,17,11,20,8,8,12,12,16,14,10,7,39,1,3,1,0,2,9,9,
      10,7,5,9,8,6,7,8,5,8,14,3,0,0,2,0,10,8,6,17,178};
  static final double[] expectedSums = {203,260,210,40,119,113,169,229,133,180,228,164,126,201,90,128,164,168,228,147,
      124,40,461,2,24,6,0,4,30,105,95,57,58,144,132,54,82,118,60,32,96,10,0,0,4,0,66,58,28,246,2326};

  public void testEachPolygonSeparatelyAllAlgorithms() throws IOException {
    // Read the geometries from the shapefile
    Path shapefile = new Path(scratchPath, "shapefile.zip");
    copyResource("/vectors/ne_110m_admin_1_states_provinces.zip", new File(shapefile.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    IGeometry[] geometries;
    Configuration conf = new Configuration();
    try  {
      conf.setBoolean(SpatialInputFormat.ImmutableObjects, true);
      reader.initialize(shapefile, conf);
      List<IGeometry> geometryList = new ArrayList<>();
      while (reader.nextKeyValue()) {
        geometryList.add(reader.getCurrentValue());
      }
      geometries = geometryList.toArray(new IGeometry[geometryList.size()]);
      assertEquals(expectedCounts.length, geometries.length);
    } finally {
      reader.close();
    }

    // Open the GeoTIFF file
    Path geoTiffFile = new Path(scratchPath, "test.tif");
    copyResource("/rasters/glc2000_small.tif", new File(geoTiffFile.toString()));
    GeoTiffReader raster = new GeoTiffReader();
    FileSystem fileSystem = geoTiffFile.getFileSystem(conf);

    try {
      raster.initialize(fileSystem, geoTiffFile, 0);
      for (int $i = 0; $i < geometries.length; $i++) {
        Collector[] results;
        Statistics stats;
        for (int iAlgoritm = 0; iAlgoritm < 3; iAlgoritm++) {
          switch (iAlgoritm) {
            case 0:
              results = ZonalStatistics.computeZonalStatisticsScanline(raster, new IGeometry[] {geometries[$i]},
                  Statistics.class);
              break;
            case 1:
              results = ZonalStatistics.computeZonalStatisticsNaive(raster, new IGeometry[] {geometries[$i]},
                  Statistics.class);
              break;
            case 2:
              results = ZonalStatistics.computeZonalStatisticsQuadSplit(raster, new IGeometry[] {geometries[$i]},
                  Statistics.class);
              break;
            default:
              throw new RuntimeException(String.format("Unknown algorithm %d", iAlgoritm));
          }
          if (results.length == 0) {
            assertEquals(0.0, expectedCounts[$i]);
          } else {
            stats = (Statistics) results[0];
            assertEquals(String.format("Error in method #%d with the count of polygon #%d '%s'", iAlgoritm, $i, geometries[$i].asText()),
                    expectedCounts[$i], stats.count[0]);
            assertEquals(String.format("Error in method #%d with the sum of polygon #%d '%s'", iAlgoritm, $i, geometries[$i].asText()),
                    expectedSums[$i], stats.sum[0]);
          }
        }
      }
    } finally {
      fileSystem.close();
      raster.close();
    }
  }

  public void testAllPolygonsTogetherScanline() throws IOException {
    // Read the geometries from the shapefile
    Path shapefile = new Path(scratchPath, "shapefile.zip");
    copyResource("/vectors/ne_110m_admin_1_states_provinces.zip", new File(shapefile.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    IGeometry[] geometries;
    Configuration conf = new Configuration();
    try  {
      conf.setBoolean(SpatialInputFormat.ImmutableObjects, true);
      reader.initialize(shapefile, conf);
      List<IGeometry> geometryList = new ArrayList<>();
      while (reader.nextKeyValue()) {
        geometryList.add(reader.getCurrentValue());
      }
      geometries = geometryList.toArray(new IGeometry[geometryList.size()]);
      assertEquals(expectedCounts.length, geometries.length);
    } finally {
      reader.close();
    }

    // Open the GeoTIFF file
    Path geoTiffFile = new Path(scratchPath, "test.tif");
    copyResource("/rasters/glc2000_small.tif", new File(geoTiffFile.toString()));
    GeoTiffReader raster = new GeoTiffReader();
    FileSystem fileSystem = geoTiffFile.getFileSystem(conf);
    try {
      raster.initialize(fileSystem, geoTiffFile, 0);
      Collector[] results = ZonalStatistics.computeZonalStatisticsScanline(raster, geometries, Statistics.class);
      assertEquals(expectedCounts.length, results.length);

      for (int $i = 0; $i < results.length; $i++) {
        Statistics stats = (Statistics) results[$i];
        if (expectedCounts[$i] == 0.0) {
          assertNull(stats);
        } else {
          assertEquals(String.format("Error with the count of polygon #%d '%s'", $i, geometries[$i].asText()),
              expectedCounts[$i], stats.count[0]);
          assertEquals(String.format("Error with the sum of polygon #%d '%s'", $i, geometries[$i].asText()),
              expectedSums[$i], stats.sum[0]);
        }
      }
    } finally {
      fileSystem.close();
      raster.close();
    }
  }
}