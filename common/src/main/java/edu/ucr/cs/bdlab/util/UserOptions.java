/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;

/**
 * Stores user-defined parameters and options. These options can be passed from command line or programmatically.
 */
public class UserOptions extends Configuration implements WritableExternalizable {
  /**All paths combined into one list*/
  protected String[] inoutPaths;

  /**The number of paths at the beginning of the array {@link #inoutPaths} that are identified as inputs*/
  protected int numOutputs;

  /**The number of paths at the end of the array {@link #inoutPaths} that are identified as inputs*/
  protected int numInputs;

  /**The parameters explicitly declared by the user in the command line.*/
  public String[] declaredParameters;

  /**An array of declared parameters as they were provided by the user including the index suffixes, e.g., [0] or [1]*/
  protected String[] declaredParameteresWithIndexes;

  /**The default array position to read when some parameters are provided as arrays*/
  protected int arrayPosition;

  /**
   * Default constructor that initializes an empty options.
   */
  public UserOptions() {
    this(true);
  }

  public UserOptions(boolean loadDefaults) {
    // Never load Hadoop defaults
    super(false);
    try {
      if (loadDefaults) {
        Enumeration<URL> configFiles = OperationUtil.class.getClassLoader().getResources("beast.properties");
        while (configFiles.hasMoreElements()) {
          URL configFile = configFiles.nextElement();
          InputStream input = configFile.openStream();
          try {
            this.loadFromTextFile(input);
          } finally {
            input.close();
          }
        }
        // Try to also load from the working directory
        File localfile = new File("beast.properties");
        if (localfile.exists() && localfile.isFile()) {
          FileInputStream input = new FileInputStream(localfile);
          try {
            this.loadFromTextFile(input);
          } finally {
            input.close();
          }
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Parses the given command line arguments.
   * @param args a list of arguments in the format "key:value" for general options; or "-key" and "-no-key" for
   *             Boolean options. Any other keys are considered input/output paths.
   */
  public UserOptions(String ... args) {
    this(true);
    List<String> inout = new ArrayList<>();
    Set<String> declaredParams = new HashSet<>();
    List<String> declaredParamsWithSuffixes = new ArrayList<>();
    for (String arg : args) {
      int i = arg.indexOf(':');
      String key = null;
      if (i != -1 && i != arg.indexOf("://")) {
        // An option in the format 'key:value'
        // We exclude the case of input paths which might take the form of 'protocol://path'
        key = arg.substring(0, i);
        String value = arg.substring(i + 1);
        super.set(key, value);
      } else if (arg.charAt(0) == '-') {
        // A Boolean option in the form of '-option' to set it to true and '-no-option' for false
        boolean value;
        if (arg.startsWith("-no-")) {
          key = arg.substring(4); // Trim '-no-'
          value = false;
        } else {
          key = arg.substring(1); // Trim '-'
          value = true;
        }
        super.set(key, Boolean.toString(value));
      } else {
        // None of the above. Treat it as either an input or output
        inout.add(arg);
      }
      // Remove the indexing part if exists, e.g., [1]
      if (key != null) {
        declaredParamsWithSuffixes.add(key);
        // Remove the index if it exists
        int iBracket = key.indexOf('[');
        if (iBracket != -1)
          key = key.substring(0, iBracket);
        declaredParams.add(key);
      }
    }
    inoutPaths = inout.toArray(new String[inout.size()]);
    declaredParameters = declaredParams.toArray(new String[declaredParams.size()]);
    declaredParameteresWithIndexes = declaredParamsWithSuffixes.toArray(new String[declaredParamsWithSuffixes.size()]);
    // Use default assignment for input and output
    if (inoutPaths.length == 1)
      this.assignAllInputsAndNoOutputs();
    else if (inoutPaths.length > 1)
      this.assignAllInputsExceptOneOutput();
  }

  /**
   * Initializes this UserOptions from a Hadoop configuration
   * @param conf an existing Hadoop configuration
   */
  public UserOptions(Configuration conf) {
    super(conf);
    Set<String> declaredParams = new HashSet<>();
    List<String> declaredParamsWithSuffixes = new ArrayList<>();
    synchronized (conf) {
      for (Map.Entry<String, String> entry : conf) {
        String key = entry.getKey();
        declaredParamsWithSuffixes.add(key);
        // Remove the index if it exists
        int iBracket = key.indexOf('[');
        if (iBracket != -1)
          key = key.substring(0, iBracket);
        declaredParams.add(key);
      }
    }
    declaredParameters = declaredParams.toArray(new String[declaredParams.size()]);
    declaredParameteresWithIndexes = declaredParamsWithSuffixes.toArray(new String[declaredParamsWithSuffixes.size()]);
  }

  /**
   * A copy constructor
   * @param opts an existing user options
   */
  public UserOptions(UserOptions opts) {
    super(opts);
    this.numInputs = opts.numInputs;
    this.numOutputs = opts.numOutputs;
    this.inoutPaths = opts.inoutPaths == null? null : opts.inoutPaths.clone();
    this.declaredParameters = opts.declaredParameters == null? null : opts.declaredParameters;
    this.declaredParameteresWithIndexes = opts.declaredParameteresWithIndexes == null? null : opts.declaredParameteresWithIndexes;
    this.arrayPosition = opts.arrayPosition;
  }

  /**
   * If the -overwrite parameter is set, delete the output
   */
  public void deleteOutput() throws IOException {
    if (!this.getBoolean("overwrite", false))
      return;
    Path outputPath = new Path(getOutput());
    FileSystem fs = outputPath.getFileSystem(new Configuration());
    fs.delete(outputPath, true);
  }

  /**
   * Returns an array of input paths. If no input paths are provided, an empty array is returned.
   * @return the list of input paths as a string array or an empty array if not inputs are provided.
   */
  public String[] getInputs() {
    return Arrays.copyOfRange(inoutPaths, 0, numInputs);
  }

  public String getInput(int i) {
    return i < numInputs? inoutPaths[i] : null;
  }

  public Path getInputPath(int i) {
    return i < numInputs? new Path(inoutPaths[i]) : null;
  }

  /**
   * Returns an array of Paths to the inputs
   * @return an array of paths
   */
  public Path[] getInputPaths() {
    Path[] paths = new Path[numInputs];
    for (int $i = 0; $i < numInputs; $i++)
      paths[$i] = new Path(inoutPaths[$i]);
    return paths;
  }

  /**
   * Returns the filename of the first input.
   * @return the first input path as a string
   */
  public String getInput() {
    return inoutPaths[0];
  }

  /**
   * Return the output paths provided by the user or {@code null} if not provided.
   * @return the list of outputs as an array of string. An empty array if no outputs are defined.
   */
  public String[] getOutputs() {
    return Arrays.copyOfRange(inoutPaths, inoutPaths.length - numOutputs, inoutPaths.length);
  }

  @Override
  public String get(String name) {
    // Override the get method to account for array position
    String val = super.get(name);
    // If a non-indexed value is given, it overrides all other values
    if (val != null)
      return val;
    // Otherwise, try to use the current array index to retrieve the correct value
    return super.get(String.format("%s[%d]", name, arrayPosition));
  }

  @Override
  public String get(String name, String value) {
    // Override the get method to account for array position
    String val = super.get(name);
    // If a non-indexed value is given, it overrides all other values
    if (val != null)
      return val;
    // Otherwise, try to use the current array index to retrieve the correct value
    return super.get(String.format("%s[%d]", name, arrayPosition), value);
  }

  /**
   * Return the first output path
   * @return the first output path as a string
   */
  public String getOutput() {
    return inoutPaths[inoutPaths.length - numOutputs];
  }

  /**
   * Returns the output as a Hadoop {@link Path}
   * @return the first output path.
   */
  public Path getOutputPath() {
    return new Path(getOutput());
  }


  public String[] getDeclaredParameters() {
    return declaredParameters;
  }

  /**
   * Check if the user-provided paths are correct according to the input and output arity as follows.
   * <ol>
   *   <li>Make sure that the total number of paths are in the range
   *   [inputArity.min+outputArity.min, inputArity.max+outputArity.max]</li>
   *   <li>Assign the first inputArity.min paths as input</li>
   *   <li>Assign the last outputArity.min paths as output</li>
   *   <li>If there are still unassigned paths and the input arity is 0 or more, assign it one output path</li>
   *   <li>If there are still unassigned paths, assign as many as possible to the input</li>
   *   <li>Assign all the remaining paths as outputs</li>
   * </ol>
   * @param inputArity the allowed range of inputs as an array of [min, max] inclusive.
   * @param outputArity the allowed range of output as an array in the form [min, max] inclusive.
   * @return whether the input/output path count is valid or not according to the given ranges.
   */
  public boolean assignAndCheckInputsAndOutputs(int[] inputArity, int[] outputArity) {
    if (inoutPaths.length < inputArity[0] + outputArity[0])
      return false;
    if (inoutPaths.length > (long) inputArity[1] + outputArity[1])
      return false;
    int totalNumInouts = inoutPaths.length;
    numInputs = inputArity[0];
    numOutputs = outputArity[0];
    if (totalNumInouts > (numInputs + numOutputs) && outputArity[0] == 0 && outputArity[1] >= 1)
      numOutputs = 1;
    numInputs += Math.min(totalNumInouts - numInputs - numOutputs, inputArity[1] - numInputs);
    numOutputs += totalNumInouts - numInputs - numOutputs;
    return true;
  }

  /**
   * Make all given paths input except for the last one, make it output
   * @return {@code false} if not paths are given at all
   */
  public boolean assignAllInputsExceptOneOutput() {
    return assignAndCheckInputsAndOutputs(new int[] {0, Integer.MAX_VALUE}, new int[] {1, 1});
  }

  /**
   * Assign all paths as input paths and no output
   */
  public void assignAllInputsAndNoOutputs() {
    assignAndCheckInputsAndOutputs(new int[] {0, Integer.MAX_VALUE}, new int[] {0, 0});
  }


  public int getNumOutputs() {
    return numOutputs;
  }

  public int getNumInputs() {
    return numInputs;
  }

  /**
   * Loads all the configuration into a Hadoop configuration
   * @param conf an existing configuration or {@code null} if a new configuration is desired.
   * @return if {@code conf} is null {@code null}, it is returned. Otherwise, a new {@link Configuration} object is
   * created and returned.
   */
  public Configuration loadIntoHadoopConfiguration(Configuration conf) {
    if (conf == null)
      conf = new Configuration();
    for (Map.Entry<String, String> e : this)
      conf.set(e.getKey(), e.getValue());
    return conf;
  }

  /**
   * Loads all the configuration into a Hadoop configuration while upgrading indexed values to non-indexed values
   * if they match the given index. For example, if the configuration contains the keys "iformat[0]:point" and
   * "iformat[1]:wkt" and the passed index is 0, the resulting configuration will contain the following key-values
   * "iformat[0]:point", "iformat[1]:wkt", and "iformat:point"; the latter matches iformat[0] in the input.
   * @param conf an existing configuration or {@code null} if a new configuration is desired.
   * @param index for array values, only copy this index of the array
   * @return if {@code conf} is null {@code null}, it is returned. Otherwise, a new {@link Configuration} object is
   * created and returned.
   */

  public Configuration loadIntoHadoopConfiguration(Configuration conf, int index) {
    String indexSuffix = String.format("[%d]", index);
    if (conf == null)
      conf = new Configuration();
    for (Map.Entry<String, String> e : this) {
      conf.set(e.getKey(), e.getValue());
      if (e.getKey().endsWith(indexSuffix)) {
        conf.set(e.getKey().replace(indexSuffix, ""), e.getValue());
      }
    }
    return conf;
  }

  /**
   * Stores the configuration as a .properties file; one entry per line.
   * @param fs the file system to write to
   * @param p the file path to write to.
   * @throws IOException if an error happens while trying to write the file
   */
  public void storeToTextFile(FileSystem fs, Path p) throws IOException {
    Properties prop = new Properties();
    for (Map.Entry<String, String> e : this)
      prop.put(e.getKey(), e.getValue());

    FSDataOutputStream out = fs.create(p);
    OutputStreamWriter w = new OutputStreamWriter(out);

    try {
      prop.store(w, null);
    } finally {
      w.close();
    }
  }

  /**
   * Loads configuration back from a text file that was written using {@link #storeToTextFile(FileSystem, Path)}
   * @param fs the file system to write in
   * @param path the path to write to
   * @return the UserOptions instance that was read from the file.
   * @throws IOException if an error with the underlying file is encountered.
   */
  public static UserOptions loadFromTextFile(FileSystem fs, Path path) throws IOException {
    FSDataInputStream in = fs.open(path);
    try {
      UserOptions opts = new UserOptions();
      opts.loadFromTextFile(in);
      return opts;
    } finally {
      in.close();
    }
  }

  public void loadFromTextFile(InputStream in) throws IOException {
    Reader r = new InputStreamReader(in);
    Properties p = new Properties();
    p.load(r);
    for (Map.Entry<Object, Object> e : p.entrySet()) {
      this.set((String)e.getKey(), (String)e.getValue());
    }
  }

  /**
   * Set the input paths to the given input
   * @param inPaths a list of input paths
   */
  public void setInput(String ... inPaths) {
    if (numInputs != inPaths.length) {
      String[] newInOutPaths = new String[inPaths.length + this.numOutputs];
      // Copy the outputs
      if (this.numOutputs > 0)
        System.arraycopy(this.inoutPaths, this.numInputs, newInOutPaths, inPaths.length, this.numOutputs);
      this.inoutPaths = newInOutPaths;
    }
    this.numInputs = inPaths.length;
    System.arraycopy(inPaths, 0, this.inoutPaths, 0, inPaths.length);
  }

  /**
   * Override the output path and set it to the given value
   * @param outPath the new path to consider
   */
  public void setOutput(String outPath) {
    if (numOutputs != 1) {
      this.numOutputs = 1;
      // Create a new array for input/output paths
      String[] inoutPaths = new String[numInputs + numOutputs];
      // Copy any existing input paths
      if (this.inoutPaths != null)
        System.arraycopy(this.inoutPaths, 0, inoutPaths, 0, numInputs);
      this.inoutPaths = inoutPaths;
    }
    this.inoutPaths[numInputs] = outPath;
  }

  /**
   * If some input parameters contain an array of values, this function sets the position in the array that
   * subsequent get calls will receive
   * @param i sets the current array position that subsequent calls to getter functions will use.
   */
  public void setArrayPosition(int i) {
    this.arrayPosition = i;
  }

  public String[] getValues(String key) {
    List<String> values = new ArrayList<>();
    for (String p : declaredParameteresWithIndexes) {
      if (p.equals(key) || (p.startsWith(key) && p.indexOf('[') == key.length()))
        values.add(super.get(p));
    }
    return values.toArray(new String[values.size()]);
  }

  public <T extends Enum<T>> T getEnumIgnoreCase(String name, T defaultValue) {
    return getEnumIgnoreCase(this, name, defaultValue);
  }

  public static <T extends Enum<T>> T getEnumIgnoreCase(Configuration conf, String name, T defaultValue) {
    final String val = conf.get(name);
    if (val == null)
      return defaultValue;
    try {
      Method valuesMethod = defaultValue.getDeclaringClass().getMethod("values");
      T[] values = (T[]) valuesMethod.invoke(null);
      for (T value : values) {
        if (value.toString().equalsIgnoreCase(val))
          return value;
      }
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      // Just ignore all errors and fall through to return the default value
      e.printStackTrace();
    }
    return defaultValue;
  }

  @Override
  public String toString() {
    StringBuffer str = new StringBuffer();
    if (inoutPaths != null) {
      for (String path : inoutPaths) {
        str.append(path);
        str.append(" ");
      }
    }
    for (Map.Entry<String, String> entry : this) {
      if (entry.getValue().equals("true")) {
        str.append("-"+entry.getKey());
      } else if (entry.getValue().equals("false")) {
        str.append("-no-"+entry.getKey());
      } else {
        str.append(entry.getKey());
        str.append(":");
        str.append(entry.getValue());
      }
      str.append(" ");
    }
    return str.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Configuration conf = (Configuration) o;
    for (Map.Entry<String, String> entry : this) {
      if (!entry.getValue().equals(conf.get(entry.getKey())))
        return false;
    }
    return true;
  }

}
