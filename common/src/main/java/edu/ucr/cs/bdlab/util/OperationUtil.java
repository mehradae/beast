/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

/**
 * Some utilities that help in running command line operations
 */
public class OperationUtil {

  /**Logger for this class*/
  private static final Log LOG = LogFactory.getLog(OperationUtil.class);

  private static final Map<String, Map<String, List<String>>> configurations = new HashMap<>();

  /**
   * Parse a range written in a user-friendly way. Here are some examples of what can be written.
   * <ul>
   *   <li>0: Only zero is allowed</li>
   *   <li>1: Exactly one</li>
   *   <li>3-5: Any number between 3 and 5 (inclusive of both)</li>
   *   <li>+: At least one</li>
   *   <li>*: Zero or more</li>
   *   <li>?: Zero or one (i.e., optional)</li>
   * </ul>
   * @param str the encoded arity in one of the above forms
   * @return a range represented as an array in the form [min, max]
   */
  public static int[] parseArity(String str) {
    final int[] OneOrMore = new int[] {1, Integer.MAX_VALUE};
    final int[] ZeroOrMore = new int[] {0, Integer.MAX_VALUE};
    final int[] ZeroOrOne = new int[] {0, 1};
    if (str.equals("+"))
      return OneOrMore;
    if (str.equals("*"))
      return ZeroOrMore;
    if (str.equals("?"))
      return ZeroOrOne;
    int i = str.indexOf('-');
    if (i == -1) {
      // No dash, a single number
      int num = Integer.parseInt(str);
      return new int[] {num, num};
    }
    int num1 = Integer.parseInt(str.substring(0, i));
    int num2 = Integer.parseInt(str.substring(i+1, str.length()));
    return new int[] {num1, num2};
  }

  /**
   * Get the operation class and its metadata given the short name. If the given operation does not exist,
   * an error message is printed to stderr and the usage of the main function is printed out.
   * @param operationName the short name of the operation as defined by the annotation {@link OperationMetadata}
   * @return the operation with the given short name
   */
  public static Operation getOperation(String operationName) {
    Map<String, Operation> operations = loadOperations();
    Operation operation = operations.get(operationName);
    if (operation == null) {
      System.err.printf("Invalid operation '%s'!", operationName);
      String suggestion = null;
      for (String validOpName : operations.keySet()) {
        int distance = StringUtil.levenshteinDistance(operationName, validOpName);
        if (distance <= 2) {
          if (suggestion == null || distance < StringUtil.levenshteinDistance(operationName, suggestion))
            suggestion = validOpName;
        }
      }
      if (suggestion != null)
        System.err.printf(" Did you mean '%s'?", suggestion);
      System.err.println();
      printUsageAndExit();
    }
    return operation;
  }

  /**
   * Print the usage of this class by listing all the supported operations.
   */
  public static void printUsageAndExit() {
    Map<String, Operation> operations = loadOperations();
    System.err.println("****************************************************");
    System.err.println("Choose one of the following operations to run");
    for (Map.Entry<String, Operation> operation : operations.entrySet()) {
      System.err.printf("%s - %s\n", operation.getKey(), operation.getValue().metadata.description());
    }
    System.err.println("****************************************************");
    System.exit(1);
  }

  /**
   * Create the command-line options for a given operation and the list of command-line arguments. This function also
   * makes simple sanity checks for the input parameters. In particular, it makes the following checks:
   * <ul>
   *   <li>All required parameters are present in the command line</li>
   *   <li>Any parameter in the command line is expected, i.e., no invalid parameters</li>
   *   <li>Check that a valid number of inputs and outputs are provided</li>
   * </ul>
   * @param operation the operation name
   * @param args the list of command line arguments
   * @return the created UserOptions
   */
  public static UserOptions createAndCheckCLO(Operation operation, String[] args) {
    String[] argsSansCommand = Arrays.copyOfRange(args, 1, args.length);
    UserOptions commandLine = new UserOptions(argsSansCommand);

    try {
      // Use this flag to report all found errors and exit at the end
      boolean errorInParameters = false;
      int[] inputArity = OperationUtil.parseArity(operation.metadata.inputArity());
      int[] outputArity = OperationUtil.parseArity(operation.metadata.outputArity());
      if (!commandLine.assignAndCheckInputsAndOutputs(inputArity, outputArity)) {
        System.err.println("Error in input and/or output");
        errorInParameters = true;
      }
      // Check required parameters

      // Retrieve all the command line parameters that can be set
      OperationParameter[] allAllowedParams = getAllParameters(commandLine, operation.operationClass);

      // Check that all required parameters is given by the user
      Set<String> allAllowedParamNames = new HashSet<>();
      for (OperationParameter parameter : allAllowedParams) {
        allAllowedParamNames.add(parameter.name);
        if (parameter.metadata.required() && commandLine.get(parameter.name) == null) {
          // Parameter is required and not set by user and does not have a default value
          System.err.printf("! Parameter '%s' is required but not set\n", parameter.name);
          errorInParameters = true;
        }
      }

      // Check that every parameter given by the user is allowed, i.e., no unexpecte parameters are given
      for (String declaredParam : commandLine.getDeclaredParameters()) {
        if (!allAllowedParamNames.contains(declaredParam)) {
          System.err.printf("! Unexpected parameter '%s'.", declaredParam);
          List<String> candidateAlternatives = new ArrayList<>();
          for (String param : allAllowedParamNames) {
            if (StringUtil.levenshteinDistance(declaredParam, param) <= 2)
              candidateAlternatives.add(param);
          }
          if (candidateAlternatives.size() == 1)
            System.err.printf(" Did you mean '%s'?", candidateAlternatives.get(0));
          else if (candidateAlternatives.size() > 1)
            System.err.printf(" Did you mean one of these %s?", candidateAlternatives.toString());
          System.err.println();
          errorInParameters = true;
        }
      }

      if (errorInParameters)
        printUsageAndExit(operation, allDependentClasses(commandLine, operation.operationClass), allAllowedParams);
      // Delete the output if it already exists
      if (commandLine.getNumOutputs() != 0 && commandLine.getBoolean("overwrite", false)) {
        commandLine.deleteOutput();
      }
    } catch (IOException e) {
      throw new RuntimeException(String.format("Error in running the run method", operation.operationClass), e);
    } catch (IllegalAccessException e) {
      e.printStackTrace();
      return null;
    } catch (InstantiationException e) {
      e.printStackTrace();
    }
    return commandLine;
  }

  /**Combine the operation class with its annotation.*/
  public static class Operation {
    public Class<?> operationClass;
    public OperationMetadata metadata;

    public Operation(Class<?> opClass, OperationMetadata metadata) {
      this.operationClass = opClass;
      this.metadata = metadata;
    }
  }

  /**
   * Holds information about an operation parameter
   */
  public static class OperationParameter {

    /**The name of the field as users set it*/
    public final String name;

    /**The metadata associated with this parameter in the code*/
    public final OperationParam metadata;

    public OperationParameter(String name, OperationParam metadata) {
      this.name = name;
      this.metadata = metadata;
    }
  }

  public static OperationParameter[] getAllParameters(Class<?> klass)
      throws InstantiationException, IllegalAccessException {
    return getAllParameters(null, klass);
  }

  public static OperationParameter[] getAllParameters(UserOptions opts, Class<?> klass)
      throws IllegalAccessException, InstantiationException {
    Stack<Class<?>> classesToCheck = new Stack<>();
    classesToCheck.push(klass);
    List<OperationParameter> params = new ArrayList<>();
    while (!classesToCheck.isEmpty()) {
      // Check fields declared in this class
      klass = classesToCheck.pop();
      Object klassObj;
      // Check if this is a Scala object and get the corresponding class
      try {
        klass = Class.forName(klass.getName()+"$");
      } catch (Exception e) {
        // It is a regular Java class
      }
      try {
        // Scala object
        klassObj = klass.getField("MODULE$").get(klass);
      } catch (Exception e) {
        klassObj = klass;
      }
      for (Field f : klass.getDeclaredFields()) {
        OperationParam annotation = f.getAnnotation(OperationParam.class);
        if (annotation != null) {
          f.setAccessible(true);
          String name = (String) f.get(klassObj);
          params.add(new OperationParameter(name, annotation));
        }
      }
      // Check if there are super classes to inherit from defined in the metadata
      OperationMetadata opMetadata = klass.getAnnotation(OperationMetadata.class);
      if (opMetadata != null) {
        for (Class<?> subclass : opMetadata.inheritParams())
          classesToCheck.push(subclass);
      }
      // Add programmatically defined subclasses
      if (klassObj instanceof IConfigurable) {
        // A Scala class
        ((IConfigurable)klassObj).addDependentClasses(opts, classesToCheck);
      } else if (IConfigurable.class.isAssignableFrom(klass)) {
        // A Java class
        IConfigurable operation = klass.asSubclass(IConfigurable.class).newInstance();
        operation.addDependentClasses(opts, classesToCheck);
      }
    }
    return params.toArray(new OperationParameter[0]);
  }

  /**
   * Read all XML configuration files of the given name in the class path and merge them into one object.
   * This method internally caches the configuration so it does not have to be loaded multiple times.
   * The XML is organized in three levels. The first level is the root element and it is always
   * &lt;beast&gt;. The second level is a name of a collection, e.g., &lt;Indexers&gt;. Finally,
   * the third level contains the contents of the collection in their text part.
   * @param filename A path to an XML file that contains the configuration.
   * @return the beast configuration as a map from each key to all values under this key.
   */
  public static Map<String, List<String>> readConfigurationXML(String filename) {
    // We chose to load configuration from XML rather than YAML for two reasons
    // 1- It is natively supported in Java so it will reduce the dependencies.
    // 2- We can use maven-shade-plugin to merge the XML files while packaging the final version
    // We could not use .properties file as they do not support arrays

    // If the configuration is cached, return it
    if (configurations.containsKey(filename))
      return configurations.get(filename);
    // Otherwise, load it for the first time and cache it
    try {
      Map<String, List<String>> configuration = new HashMap<>();
      Enumeration<URL> configFiles = OperationUtil.class.getClassLoader().getResources(filename);
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      while (configFiles.hasMoreElements()) {
        URL configFile = configFiles.nextElement();
        InputStream configIS = configFile.openStream();
        Document doc = dBuilder.parse(configIS);
        Element documentElement = doc.getDocumentElement();
        documentElement.normalize();

        NodeList collections = documentElement.getChildNodes();
        for (int $i = 0; $i < collections.getLength(); $i++) {
          Node collection = collections.item($i);
          if (collection.getNodeType() != Node.ELEMENT_NODE)
            continue;
          String collectionName = collection.getNodeName();
          List<String> collectionItems = configuration.get(collectionName);
          if (collectionItems == null) {
            collectionItems = new ArrayList<>();
            configuration.put(collectionName, collectionItems);
          }
          NodeList collectionContents = collection.getChildNodes();
          for (int $j = 0; $j < collectionContents.getLength(); $j++) {
            Node collectionContentItem = collectionContents.item($j);
            if (collectionContentItem.getNodeType() != Node.ELEMENT_NODE)
              continue;
            String itemValue = collectionContentItem.getTextContent();
            collectionItems.add(itemValue);
          }
        }
        configIS.close();
      }
      synchronized (configurations) {
        // Check the cache again for the rare case of a concurrent call to this method with the same filename
        if (configurations.containsKey(filename))
          configuration = configurations.get(filename);
        else
          configurations.put(filename, configuration);
      }
      return configuration;
    } catch (IOException e) {
      throw new RuntimeException("Error loading configuration file", e);
    } catch (ParserConfigurationException e) {
      throw new RuntimeException("Error loading configuration file", e);
    } catch (SAXException e) {
      throw new RuntimeException("Error loading configuration file", e);
    }
  }

  /**
   * Load the list of operations from the XML configuration file.
   * @return a map from each user-friendly short name of an operation to an instance of {@link Operation}
   */
  public static Map<String, Operation> loadOperations() {
    Map<String, Operation> operations = new TreeMap();
    Map<String, List<String>> conf = readConfigurationXML("beast.xml");
    if (conf.containsKey("Operations")) {
      for (String op : conf.get("Operations")) {
        try {
          Class<?> opClass;
          try {
            // Try as a Scala operation first
            opClass = Class.forName(op+"$");
          } catch (ClassNotFoundException e) {
            opClass = Class.forName(op);
          }

          OperationMetadata opMetadata = opClass.getAnnotation(OperationMetadata.class);
          if (opMetadata == null) {
            LOG.warn(String.format("Skipping operation class '%s' not annotated with OperationMetadata", opClass.getName()));
          } else {
            operations.put(opMetadata.shortName().toLowerCase(), new Operation(opClass, opMetadata));
          }
        } catch (ClassNotFoundException e) {
          e.printStackTrace();
        }
      }
    }
    return operations;
  }

  /**
   * Prints the usage of a specific operation.
   * @param operation the operation to print the usage to
   * @param dependentClasses a list of all dependent classes to include their usages
   * @param allowedParameters all the parameters that are allowed by this operation as inferred from the metadata
   */
  public static void printUsageAndExit(Operation operation, Collection<Class<?>> dependentClasses, OperationParameter[] allowedParameters) {
    System.err.println("****************************************************");
    System.err.printf("Usage: %s", operation.metadata.shortName());
    int[] inputArity = OperationUtil.parseArity(operation.metadata.inputArity());
    if (inputArity[0] == 0 && inputArity[1] == 1)
      System.err.printf(" [input]");
    else if (inputArity[0] == 1 && inputArity[1] == 1)
      System.err.printf(" <input>");
    else if (inputArity[0] == 0 && inputArity[1] > 1)
      System.err.printf(" [inputs]");
    else if (inputArity[0] >= 1 && inputArity[1] > 1)
      System.err.printf(" <inputs>");
    int[] outputArity = OperationUtil.parseArity(operation.metadata.outputArity());
    if (outputArity[0] == 0 && outputArity[1] == 1)
      System.err.printf(" [output]");
    else if (outputArity[0] == 1 && outputArity[1] == 1)
      System.err.printf(" <output>");
    else if (outputArity[0] == 0 && outputArity[1] > 1)
      System.err.printf(" [outputs]");
    else if (outputArity[0] >= 1 && outputArity[1] > 1)
      System.err.printf(" <outputs>");
    System.err.println();
    // Retrieve the options and parameters
    System.err.println(" -- Additional parameters. (*) Marks required parameters");
    printUsageFields(System.err, allowedParameters);
    // Call an optional printUsage method in all the dependent classes
    for (Class opClass : dependentClasses) {
      try {
        Method method = opClass.getMethod("printUsage", PrintStream.class);
        method.invoke(opClass, System.err);
      } catch (NoSuchMethodException e) {
        // No problem. This operation does not define a custom printUsage which is only optional
      } catch (IllegalAccessException e) {
        LOG.warn(String.format("Could not access the method %s.printUsage", operation.operationClass.getName()), e);
      } catch (InvocationTargetException e) {
        LOG.warn(String.format("Error while running the method %s.printUsage", operation.operationClass.getName()), e);
      }
    }
    System.err.println("****************************************************");
    System.exit(1);
  }

  /**
   * Print usage for all the given fields.
   * @param out the print stream to write to
   * @param fields the list of fields to write their usage details.
   */
  public static void printUsageFields(PrintStream out, OperationParameter[] fields) {
    for (OperationParameter field : fields) {
      if (field.metadata.showInUsage()) {
        // This field indicates an operation parameter
        if (field.metadata.required())
          out.print("(*) ");
        out.print(field.name);
        out.print(": ");
        out.print(field.metadata.description());
        if (field.metadata.defaultValue() != null && field.metadata.defaultValue().length() > 0) {
          out.printf(" (Default: %s)", field.metadata.defaultValue());
        }
        out.println();
      }
    }
  }

  /**
   * Returns all the operation classes that the given opClass depends on. This list can be used to retrieve all
   * parameters of an operation or to find additional usage to print.
   * @param opts command line options. Can be used to identify some dependent classes in runtime, e.g., indexers.
   * @param klass the operation class
   * @return a list of all classes that this operation depends on.
   * @throws InstantiationException if the given class or one of its dependent classes could not be instantiated
   * @throws IllegalAccessException if the default constructor of one of the classes is not accessible.
   */
  public static List<Class<?>> allDependentClasses(UserOptions opts, Class<?> klass) throws IllegalAccessException, InstantiationException {
    List<Class<?>> classesToReturn = new ArrayList<>();
    Stack<Class<?>> classesToCheck = new Stack<>();
    classesToCheck.push(klass);
    while (!classesToCheck.isEmpty()) {
      // Check fields declared in this class
      klass = classesToCheck.pop();
      classesToReturn.add(klass);
      Object klassObj;
      // Check if this is a Scala object and get the corresponding class
      try {
        klass = Class.forName(klass.getName()+"$");
      } catch (Exception e) {
        // It is a regular Java class
      }
      try {
        // Scala object
        klassObj = klass.getField("MODULE$").get(klass);
      } catch (Exception e) {
        klassObj = klass;
      }
      // Check if there are super classes to inherit from defined in the metadata
      OperationMetadata opMetadata = klass.getAnnotation(OperationMetadata.class);
      if (opMetadata != null) {
        for (Class<?> subclass : opMetadata.inheritParams())
          classesToCheck.push(subclass);
      }
      // Add programmatically defined subclasses
      if (klassObj instanceof IConfigurable) {
        // A Scala class
        ((IConfigurable)klassObj).addDependentClasses(opts, classesToCheck);
      } else if (klass.isAssignableFrom(IConfigurable.class)) {
        // A Java class
        IConfigurable operation = klass.asSubclass(IConfigurable.class).newInstance();
        operation.addDependentClasses(opts, classesToCheck);
      }
    }
    return classesToReturn;
  }

  /**
   * Retrieve all the allowed parameters for an operation. This includes the parameters defined for this class and the
   * parameters inherited from other classes.
   * @param opClass the operation class to retrieve operations to
   * @return a list of parameter names that are allowed by this operation
   */
  public static List<String> allAllowedParams(Class opClass) {
    List<String> allAllowedParams = new ArrayList<>();
    try {
      Class<?> classToCheckParameters = opClass;
      Stack<Class> classesToCheckParameters = new Stack<>();
      do {
        // Add parameters to the current class
        for (Field f : classToCheckParameters.getFields()) {
          if (f.getAnnotation(OperationParam.class) != null)
            allAllowedParams.add((String) f.get(opClass));
        }
        // Check if this class has parent classes
        OperationMetadata metadata = classToCheckParameters.getAnnotation(OperationMetadata.class);
        if (metadata != null) {
          for (Class<?> parentClass : metadata.inheritParams())
            classesToCheckParameters.push(parentClass);
        }
        if (classesToCheckParameters.isEmpty())
          classToCheckParameters = null;
        else
          classToCheckParameters = classesToCheckParameters.pop();
      } while (classToCheckParameters != null);
      return allAllowedParams;
    } catch (IllegalAccessException e) {
      throw new RuntimeException("Error reading parameters", e);
    }
  }
}
