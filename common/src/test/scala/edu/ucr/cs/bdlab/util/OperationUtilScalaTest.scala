package edu.ucr.cs.bdlab.util

import org.scalatest.FunSuite

class OperationUtilScalaTest extends FunSuite {
  test("A base Scala operation") {
    val params = OperationUtil.getAllParameters(TestBaseClassScala.getClass)
    assert(params.length == 2)
  }

  test("A sub Scala operation") {
    val params = OperationUtil.getAllParameters(TestSubClassScala.getClass)
    assert(params.length == 3)
  }
}
