/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class UserOptionsTest extends SparkTest {

  public void testParse() {
    String[] args = {
        "key1:val1",
        "key2:5555",
        "-validate",
        "-no-overwrite",
        "input1",
        "input2",
        "output"
    };
    UserOptions clo = new UserOptions(args);
    clo.assignAndCheckInputsAndOutputs(new int[]{1, 10}, new int[]{0, 1});
    assertEquals( "val1", clo.get("key1"));
    assertEquals(5555, clo.getInt("key2", 0));
    assertEquals(true, clo.getBoolean("validate", false));
    assertEquals(false, clo.getBoolean("overwrite", true));
    assertEquals(2, clo.getNumInputs());
    assertEquals("output", clo.getOutput());

    assertTrue(clo.assignAndCheckInputsAndOutputs(new int[]{1, Integer.MAX_VALUE}, new int[]{1, 1}));
  }

  public void testInOut() {
    String[] args = {
        "key1:val1",
        "key2:5555",
        "-validate",
        "-no-overwrite",
        "input1",
        "input2",
        "output"
    };
    UserOptions clo = new UserOptions(args);
    clo.assignAndCheckInputsAndOutputs(new int[] {2, 2}, new int[] {1, 1});
    assertEquals(2, clo.getNumInputs());
    assertEquals(1, clo.getNumOutputs());
    clo.assignAndCheckInputsAndOutputs(new int[] {0, 5}, new int[] {0, 2});
    assertEquals(2, clo.getNumInputs());
    assertEquals(1, clo.getNumOutputs());
    clo.assignAndCheckInputsAndOutputs(new int[] {3, 5}, new int[] {0, 2});
    assertEquals(3, clo.getNumInputs());
    assertEquals(0, clo.getNumOutputs());
    clo.assignAndCheckInputsAndOutputs(new int[] {0, 1}, new int[] {0, 2});
    assertEquals(1, clo.getNumInputs());
    assertEquals(2, clo.getNumOutputs());
    assertFalse(clo.assignAndCheckInputsAndOutputs(new int[] {0, 1}, new int[] {0, 1}));
  }

  public void testReadFromProperties() throws IOException {
    Path p = new Path(scratchPath, "test.properties");
    copyResource("/test.properties", new File(p.toString()));
    Configuration conf = new Configuration();
    UserOptions opts = UserOptions.loadFromTextFile(p.getFileSystem(conf), p);
    assertEquals("val1", opts.get("key1"));
    assertEquals(123, opts.getInt("key2", 55));
  }

  public void testStoreToProperties() throws IOException {
    Path p = new Path(scratchPath, "test.properties");
    UserOptions opts = new UserOptions();
    opts.set("key1", "val1");
    opts.set("key2", "567");
    opts.storeToTextFile(p.getFileSystem(new Configuration()), p);

    opts = UserOptions.loadFromTextFile(p.getFileSystem(new Configuration()), p);
    assertEquals("val1", opts.get("key1"));
    assertEquals("567", opts.get("key2"));
  }

  public void testArrays() {
    String[] args = {
        "iformat[0]:wkt",
        "separator[0]:,",
        "-skipHeader[0]",
        "iformat[1]:point",
        "-no-skipHeader[1]",
    };
    UserOptions clo = new UserOptions(args);
    clo.setArrayPosition(0);
    assertEquals("wkt", clo.get("iformat"));
    assertEquals(",", clo.get("separator"));
    assertEquals(true, clo.getBoolean("skipHeader", false));
    clo.setArrayPosition(1);
    assertEquals("point", clo.get("iformat"));
    assertNull("Should not find a value for separator[1]", clo.get("separator"));
    assertEquals(false, clo.getBoolean("skipHeader", true));

    assertEquals(2, clo.getValues("iformat").length);

    // Make sure that the indexed values do not appear with their indexes as names
    String[] delcaredparameters = clo.getDeclaredParameters();
    assertEquals(3, delcaredparameters.length);

    Configuration conf = new Configuration();
    clo.loadIntoHadoopConfiguration(conf, 0);
    assertEquals("wkt", conf.get("iformat"));
    assertEquals(",", conf.get("separator"));
    assertEquals(true, conf.getBoolean("skipHeader", false));
    conf.clear();
    clo.loadIntoHadoopConfiguration(conf, 1);
    assertEquals("point", conf.get("iformat"));
    assertNull("Should not find a value for separator[1]", conf.get("separator"));
    assertEquals(false, conf.getBoolean("skipHeader", true));
  }

  public void testArraysFromConf() {
    Configuration conf = new Configuration();
    conf.set("iformat[0]", "wkt");
    conf.set("iformat[1]", "point");
    conf.set("separator[1]", ",");
    conf.setBoolean("overwrite", true);

    UserOptions opts = new UserOptions(conf);
    String[] iformats = opts.getValues("iformat");
    assertEquals(2, iformats.length);
    assertArrayEquals(new String[] {"wkt", "point"}, iformats);
    opts.setArrayPosition(0);
    assertNull(opts.get("separator"));
    assertTrue(opts.getBoolean("overwrite", false));
    opts.setArrayPosition(1);
    assertEquals(",", opts.get("separator"));
    assertEquals(",", opts.get("separator", "\t"));
    assertTrue(opts.getBoolean("overwrite", false));
  }

  enum TestEnum {Test1, teST2, test3};

  public void testGetEnumIgnoreCase() {
    UserOptions opts = new UserOptions();
    opts.set("k1", "test1");
    opts.set("k2", "test2");
    assertEquals(TestEnum.Test1, opts.getEnumIgnoreCase("k1", TestEnum.test3));
    assertEquals(TestEnum.teST2, opts.getEnumIgnoreCase("k2", TestEnum.test3));
    assertEquals(TestEnum.test3, opts.getEnumIgnoreCase("k3", TestEnum.test3));
  }

  public void testToString() {
    String[] args = {
            "key1:val1",
            "key2:5555",
            "-validate",
            "-no-overwrite",
            "input1",
            "input2",
            "output"
    };
    UserOptions clo = new UserOptions(args);
    clo.setBoolean("habal", true);
    clo.setBoolean("hatal", false);
    clo.setInt("xxx", 555);
    UserOptions clo2 = new UserOptions(clo.toString().split(" "));
    assertEquals(clo, clo2);
  }

  public void testLoadDefaults() throws IOException {
    UserOptions opts = new UserOptions();
    assertEquals("testvalue", opts.get("testkey"));

    opts = new UserOptions(false);
    assertNull(opts.get("testkey"));

    opts = new UserOptions(true);
    assertEquals("testvalue", opts.get("testkey"));

    opts = new UserOptions("key:value");
    assertEquals("testvalue", opts.get("testkey"));

    opts = new UserOptions("testkey:vvvv");
    assertEquals("vvvv", opts.get("testkey"));

    // Try loading an additional file from working directory
    File localfile = new File("beast.properties");
    FileWriter writer = new FileWriter(localfile);
    writer.write("testkey2=testvalue2");
    writer.close();
    localfile.deleteOnExit();

    opts = new UserOptions(true);
    assertEquals("testvalue", opts.get("testkey"));
    assertEquals("testvalue2", opts.get("testkey2"));
  }
}
