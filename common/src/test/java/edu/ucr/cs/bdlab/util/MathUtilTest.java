/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import junit.framework.TestCase;

public class MathUtilTest extends TestCase {

  public void testNumDecimalDigits() {
    assertEquals(1, MathUtil.getNumberOfDecimalDigits(1));
    assertEquals(1, MathUtil.getNumberOfDecimalDigits(9));
    assertEquals(3, MathUtil.getNumberOfDecimalDigits(589));
    assertEquals(9, MathUtil.getNumberOfDecimalDigits(214748364));
    assertEquals(10, MathUtil.getNumberOfDecimalDigits(2147483647));
    assertEquals(3, MathUtil.getNumberOfDecimalDigits(1000));
    assertEquals(4, MathUtil.getNumberOfDecimalDigits(10000));

    assertEquals(14, MathUtil.getNumberOfDecimalDigits(21474836470000L));
    assertEquals(19, MathUtil.getNumberOfDecimalDigits(2474836470000000000L));
  }

  public void testNumSignificantBits() {
    assertEquals(5, MathUtil.numberOfSignificantBits(31));
    assertEquals(24, MathUtil.numberOfSignificantBits(16000000));
    assertEquals(1, MathUtil.numberOfSignificantBits(0));
  }

  public void testGetBits() {
    byte[] data = {0x63, (byte) 0x94, 0x5f, 0x0C};
    assertEquals(0x39, MathUtil.getBits(data, 4, 8));
    assertEquals(0x394, MathUtil.getBits(data, 6, 10));
    assertEquals(0xA2F, MathUtil.getBits(data, 11, 12));
    assertEquals(0x63945F0C, MathUtil.getBits(data, 0, 32));
  }

  public void testLog2() {
    assertEquals(-1, MathUtil.log2(0));
    assertEquals(0, MathUtil.log2(1));
    assertEquals(1, MathUtil.log2(2));
    assertEquals(1, MathUtil.log2(3));
    assertEquals(2, MathUtil.log2(4));
    assertEquals(6, MathUtil.log2(64));
    assertEquals(5, MathUtil.log2(63));
  }
}