/*
 * Copyright 2020 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import edu.ucr.cs.bdlab.test.SparkTest;

import java.util.List;
import java.util.Map;
import java.util.Stack;

public class OperationUtilTest extends SparkTest {
  public void testParseArity() {
    assertArrayEquals(new int[] {0,1}, OperationUtil.parseArity("?"));
    assertArrayEquals(new int[] {0,Integer.MAX_VALUE}, OperationUtil.parseArity("*"));
    assertArrayEquals(new int[] {1,Integer.MAX_VALUE}, OperationUtil.parseArity("+"));
    assertArrayEquals(new int[] {4,4}, OperationUtil.parseArity("4"));
    assertArrayEquals(new int[] {5,7}, OperationUtil.parseArity("5-7"));
  }

  public void testReadXMLConfigFile() {
    Map<String, List<String>> conf = OperationUtil.readConfigurationXML("test-beast.xml");
    assertEquals(4, conf.get("Operations").size());
    assertEquals(1, conf.get("Indexers").size());
    assertEquals("Op1", conf.get("Operations").get(0));
  }

  @OperationMetadata(shortName = "baseclass", description = "...")
  public static class TestBaseClass implements IConfigurable {
    @OperationParam(
        description = "description",
        defaultValue = "false"
    )
    public static final String Param1 = "param1";
  }

  public void testGetParams() throws IllegalAccessException, InstantiationException, NoSuchFieldException {
    OperationUtil.OperationParameter[] params = OperationUtil.getAllParameters(TestBaseClass.class);
    assertEquals(1, params.length);
    assertEquals("param1", params[0].name);
  }

  @OperationMetadata(shortName = "subclass", description = "...", inheritParams = {TestBaseClass.class})
  public static class TestSubClass implements IConfigurable {
    @OperationParam(
        description = "description",
        defaultValue = "false"
    )
    public static final String Param2 = "param2";
  }

  public void testGetParams2() throws IllegalAccessException, InstantiationException, NoSuchFieldException {
    OperationUtil.OperationParameter[] params = OperationUtil.getAllParameters(TestSubClass.class);
    assertEquals(2, params.length);
  }

  @OperationMetadata(shortName = "subclass", description = "...")
  public static class TestSubClass3 implements IConfigurable {
    @OperationParam(
        description = "description",
        defaultValue = "false"
    )
    public static final String Param3 = "param3";

    @Override
    public void addDependentClasses(UserOptions opts, Stack<Class<?>> classes) {
      classes.add(TestBaseClass.class);
    }
  }

  public void testGetParams3() throws IllegalAccessException, InstantiationException {
    OperationUtil.OperationParameter[] params = OperationUtil.getAllParameters(TestSubClass3.class);
    assertEquals(2, params.length);
  }
}