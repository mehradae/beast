/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

/**
 * An exception with geometry
 */
public class GeometryException extends RuntimeException {

  public GeometryException() {
  }

  public GeometryException(IGeometry geometry) {
    this(String.format("Error with the geometry '%s'", geometry.toString()));
  }

  public GeometryException(String message) {
    super(message);
  }

  public GeometryException(String message, IGeometry geometry) {
    this(String.format("Geometry '%s': %s", geometry.toString(), message));
  }

  public GeometryException(String message, Throwable cause) {
    super(message, cause);
  }

  public GeometryException(Throwable cause) {
    super(cause);
  }

  public GeometryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
