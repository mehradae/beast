/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite.twod;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.ILineString;
import edu.ucr.cs.bdlab.geolite.IPolygon;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.util.Fundamentals;
import edu.ucr.cs.bdlab.geolite.GeometryException;
import edu.ucr.cs.bdlab.geolite.GeometryType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Polygon2D extends LineString2D implements IPolygon {

  /** The number of rings. This will be one if the polygon does not have holes.*/
  public int numRings;

  /**The index of the starting point of each ring including the outer shell.*/
  public int[] firstPointInRing;

  /**A flag that is set when the last ring has been closed. Used to start a new ring with the next add point.*/
  public boolean lastRingClosed;

  public Polygon2D() {
    // Initially, there are no rings so it is valid to say that the last ring is closed.
    lastRingClosed = true;
  }

  @Override
  public GeometryType getType() {
    return GeometryType.POLYGON;
  }

  @Override
  public void addPoint(double x, double y) {
    if (lastRingClosed) {
      // Start a new ring
      ensureRingCapacity(numRings+1);
      firstPointInRing[numRings++] = numPoints;
      // Last ring is no longer closed
      lastRingClosed = false;
      super.addPoint(x, y);
    } else {
      // Not the first or last point in the ring, jus add it.
      super.addPoint(x, y);
    }
  }

  @Override
  public void addPointXYM(double x, double y, double m) {
    if (lastRingClosed) {
      // Start a new ring
      ensureRingCapacity(numRings+1);
      firstPointInRing[numRings++] = numPoints;
      // Last ring is no longer closed
      lastRingClosed = false;
      super.addPointXYM(x, y, m);
    } else {
      // Not the first or last point in the ring, jus add it.
      super.addPointXYM(x, y, m);
    }
  }

  @Override
  public void addPointXYZM(double x, double y, double z, double m) {
    if (lastRingClosed) {
      // Start a new ring
      ensureRingCapacity(numRings+1);
      firstPointInRing[numRings++] = numPoints;
      // Last ring is no longer closed
      lastRingClosed = false;
      super.addPointXYZM(x, y, z, m);
    } else {
      // Not the first or last point in the ring, jus add it.
      super.addPointXYZM(x, y, z, m);
    }
  }

  /**
   * Signals that the last ring is closed.
   * @param pointWasRepeated signals that the first point was already repeated to close the polygon.
   *                         If {@code false}, then it is not needed to repeat the closing point at the end.
   */
  public void closeLastRing(boolean pointWasRepeated) {
    this.lastRingClosed = true;
    // Checks that the last
    if (pointWasRepeated) {
      if (xs[numPoints - 1] != xs[firstPointInRing[numRings - 1]] ||
          ys[numPoints - 1] != ys[firstPointInRing[numRings - 1]]) {
        throw new GeometryException("Polygon should be closed by repeating the first point at the end");
      }
      numPoints--;
    }
    firstPointInRing[numRings] = numPoints;
  }

  /**
   * Closes last ring while assuming that the last point was repeated without actually checking it.
   */
  public void closeLastRingNoCheck() {
    this.lastRingClosed = true;
    numPoints--;
    firstPointInRing[numRings] = numPoints;
  }

  protected void ensureRingCapacity(int newNumRings) {
    firstPointInRing = Fundamentals.expand(firstPointInRing, Fundamentals.nextPowerOfTwo(newNumRings + 1));
  }

  @Override
  public int getNumRings() {
    return numRings;
  }

  /**
   * Checks if the last ring has been closed correctly.
   * @return
   */
  public boolean isClosed() {
    return lastRingClosed;
  }

  @Override
  public double getVolume() {
    double area = 0.0;
    // Add the area of the outer ring
    area += getRingArea(0);

    for (int interiorRing = 1; interiorRing < numRings; interiorRing++)
      area -= getRingArea(interiorRing);

    return area;
  }

  /**
   * Computes the area of a closed ring
   * @param iRing
   * @return
   */
  protected double getRingArea(int iRing) {
    int i1Point = firstPointInRing[iRing];
    int i2Point = firstPointInRing[iRing + 1];
    if (i2Point - i1Point < 3)
      return 0.0;
    double area = 0.0;
    for (int iPoint = i1Point+2; iPoint < i2Point; iPoint++) {
      area += Fundamentals.signedTriangleArea(xs[i1Point], ys[i1Point], xs[iPoint-1], ys[iPoint-1],
          xs[iPoint], ys[iPoint]);
    }
    return Math.abs(area);
  }

  @Override
  public int getGeometryStorageSize() {
    return super.getGeometryStorageSize() + 4 + numRings * 4 + 1;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    super.write(out);
    out.writeInt(numRings);
    for (int iRing = 0; iRing < numRings; iRing++)
      out.writeInt(firstPointInRing[iRing]);
    out.writeBoolean(lastRingClosed);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    super.readFields(in);
    numRings = in.readInt();
    ensureRingCapacity(numRings);
    for (int iRing = 0; iRing < numRings; iRing++)
      firstPointInRing[iRing] = in.readInt();
    firstPointInRing[numRings] = numPoints;
    lastRingClosed = in.readBoolean();
  }

  @Override
  public void setEmpty() {
    super.setEmpty();
    this.numRings = 0;
    this.lastRingClosed = true;
  }

  /**
   * Writes this geometry as WKT
   * @param out
   */
  public void toWKT(StringBuilder out) {
    if (this.isEmpty()) {
      out.append("POLYGON EMPTY");
      return;
    }
    out.append("POLYGON(");
    for (int iRing = 0; iRing < numRings; iRing++) {
      if (iRing > 0)
        out.append(',');
      out.append('(');
      int i1Point = firstPointInRing[iRing];
      int i2Point = firstPointInRing[iRing + 1];
      for (int iPoint = i1Point; iPoint < i2Point; iPoint++) {
        out.append(xs[iPoint]); out.append(' '); out.append(ys[iPoint]); out.append(',');
      }
      // Close the loop by writing the first point again
      out.append(xs[i1Point]); out.append(' '); out.append(ys[i1Point]);
      out.append(')');
    }
    out.append(')');
  }

  @Override
  public ByteBuffer toWKB(ByteBuffer out) {
    byte byteOrder = (byte)(out.order() == ByteOrder.LITTLE_ENDIAN ? 1 : 0);
    out.put(byteOrder);
    out.putInt(3);
    out.putInt(numRings);
    for (int iRing = 0; iRing < numRings; iRing++) {
      int i1Point = firstPointInRing[iRing];
      int i2Point = firstPointInRing[iRing + 1];
      out.putInt(i2Point-i1Point);
      for (int iPoint = i1Point; iPoint < i2Point; iPoint++) {
        out.putDouble(xs[iPoint]);
        out.putDouble(ys[iPoint]);
      }
    }
    return out;
  }

  @Override
  public String toString() {
    return asText();
  }

  /**
   * Returns the index of the first point in the given ring
   * @param n
   * @return
   */
  public int getRingStart(int n) {
    return firstPointInRing[n];
  }

  /**
   * Returns the index after the last point in the given ring
   * @param n
   * @return
   */
  public int getRingEnd(int n) {
    return firstPointInRing[n + 1];
  }

  @Override
  public ILineString getRingN(int n) {
    return new PolygonRing(n);
  }

  /**
   * Point in polygon query
   * @param point
   * @return
   */
  public boolean containsPoint(Point point) {
    int numLineSegmentsToTheLeft = 0;
    int iRing = 0;
    for (int iPoint = 0; iPoint < numPoints; iPoint++) {
      double x1 = xs[iPoint];
      double y1 = ys[iPoint];
      double x2, y2;
      if (iPoint == firstPointInRing[iRing+1] - 1) {
        // Last point in ring, connect it with the first point to make a line segment
        x2 = xs[firstPointInRing[iRing]];
        y2 = ys[firstPointInRing[iRing]];
        iRing++;
      } else {
        x2 = xs[iPoint+1];
        y2 = ys[iPoint+1];
      }
      if ((point.coords[1] >= y1 && point.coords[1] <= y2) ||
          (point.coords[1] >= y2 && point.coords[1] <= y1)) {
        double xIntersection = x1 + (point.coords[1] - y1) * (x2 - x1) / (y2 - y1);
        if (xIntersection < point.coords[0])
          numLineSegmentsToTheLeft++;
      }
    }
    return (numLineSegmentsToTheLeft & 1) == 1;
  }

  class PolygonRing implements ILineString {
    private final int iRing;

    PolygonRing(int iRing) {
      this.iRing = iRing;
    }

    @Override
    public Envelope envelope(Envelope envelope) {
      if (envelope == null)
        envelope = new Envelope(this.getCoordinateDimension());
      envelope.setEmpty();
      for (int iPoint = firstPointInRing[iRing]; iPoint < firstPointInRing[iRing+1]; iPoint++) {
        envelope.minCoord[0] = Math.min(envelope.minCoord[0], xs[iPoint]);
        envelope.maxCoord[0] = Math.max(envelope.maxCoord[0], xs[iPoint]);
        envelope.minCoord[1] = Math.min(envelope.minCoord[1], ys[iPoint]);
        envelope.maxCoord[1] = Math.max(envelope.maxCoord[1], ys[iPoint]);
      }
      return envelope;
    }

    @Override
    public void setEmpty() {
      throw new RuntimeException("Not supported");
    }

    @Override
    public void toWKT(StringBuilder out) {
      throw new RuntimeException("Not supported");
    }

    @Override
    public ByteBuffer toWKB(ByteBuffer out) {
      throw new RuntimeException("Not supported");
    }

    @Override
    public Point centroid(Point centroid) {
      if (centroid == null)
        centroid = new Point(2);
      if (this.getNumPoints() == 0)
        centroid.setEmpty();
      double sumx = 0.0;
      double sumy = 0.0;
      for (int iPoint = firstPointInRing[iRing]; iPoint < firstPointInRing[iRing+1]; iPoint++) {
        sumx += xs[iPoint];
        sumy += ys[iPoint];
      }
      centroid.set(sumx / this.getNumPoints(), sumy / this.getNumPoints());
      return centroid;
    }

    @Override
    public int getCoordinateDimension() {
      return Polygon2D.this.getCoordinateDimension();
    }

    @Override
    public long getNumPoints() {
      return firstPointInRing[iRing + 1] - firstPointInRing[iRing] + 1;
    }

    @Override
    public void getPointN(long n, Point p) {
      if (n == this.getNumPoints() - 1)
        Polygon2D.this.getPointN(firstPointInRing[iRing], p);
      else
        Polygon2D.this.getPointN(n + firstPointInRing[iRing], p);
    }

    @Override
    public void write(DataOutput out) throws IOException {
      // Write the ring as a linestring so that it can be parsed back as a linestring
      out.writeInt((int) this.getNumPoints());
      out.writeBoolean(Polygon2D.this.hasMValues());
      for (int iPoint = firstPointInRing[iRing]; iPoint < firstPointInRing[iRing+1]; iPoint++) {
        out.writeDouble(xs[iPoint]);
        out.writeDouble(ys[iPoint]);
        if (ms != null)
          out.writeDouble(ms[iPoint]);
      }
      // Repeat first point
      out.writeDouble(xs[firstPointInRing[iRing]]);
      out.writeDouble(ys[firstPointInRing[iRing]]);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
      throw new RuntimeException("Not supported");
    }
  }
}
