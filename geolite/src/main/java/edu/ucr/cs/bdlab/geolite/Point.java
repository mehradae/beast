/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * A k-dimensional point shape
 */
public class Point implements IGeometry {

  /**The coordinate of the point in the k-dimension*/
  public double[] coords;

  public Point() {
    // An empty point with no initial coordinates
  }

  public Point(int numDimensions, double ... coords) {
    setCoordinateDimension(numDimensions);
    if (coords.length > 0)
      System.arraycopy(coords, 0, this.coords, 0, numDimensions);
  }

  /**
   * Initialize a point to the given coordinate
   * @param coords
   */
  public Point(double ... coords) {
    this.coords = Arrays.copyOf(coords, coords.length);
  }

  @Override
  public Envelope envelope(Envelope envelope) {
    assert envelope.getCoordinateDimension() == this.getCoordinateDimension();
    System.arraycopy(this.coords, 0, envelope.minCoord, 0, getCoordinateDimension());
    System.arraycopy(this.coords, 0, envelope.maxCoord, 0, getCoordinateDimension());
    return envelope;
  }

  @Override
  public double getVolume() {
    return 0;
  }

  @Override
  public GeometryType getType() {
    return GeometryType.POINT;
  }

  @Override
  public int getGeometryStorageSize() {
    return this.getCoordinateDimension() * 8;
  }

  @Override
  public boolean isEmpty() {
    if (getCoordinateDimension() == 0)
      return true;
    for (int d = 0; d < getCoordinateDimension(); d++) {
      if (!Double.isFinite(coords[d]))
        return true;
    }
    return false;
  }

  @Override
  public void setEmpty() {
    if (coords != null)
      coords[0] = Double.NaN;
  }

  @Override
  public void toWKT(StringBuilder out) {
    // TODO a correct WKT representation should use POINT M, POINT Z, or POINT MZ and does not support arbitrary k
    out.append("POINT(");
    for (int d = 0; d < getCoordinateDimension(); d++) {
      if (d > 0)
        out.append(' ');
      out.append(coords[d]);
    }
    out.append(')');
  }

  @Override
  public ByteBuffer toWKB(ByteBuffer out) {
    if (getCoordinateDimension() > 2) throw new RuntimeException("Not supported");
    else {
      byte byteOrder = (byte)(out.order() == ByteOrder.LITTLE_ENDIAN ? 1 : 0);
      out.put(byteOrder);
      out.putInt(1);
      out.putDouble(coords[0]);
      out.putDouble(coords[1]);
      return out;
    }
  }

  @Override
  public Point centroid(Point centroid) {
    centroid.set(this.coords);
    return centroid;
  }

  public void set(double ... coords) {
    assert this.getCoordinateDimension() == coords.length;
    System.arraycopy(coords, 0, this.coords, 0, this.coords.length);
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(getCoordinateDimension());
    for (int d = 0; d < getCoordinateDimension(); d++)
      out.writeDouble(coords[d]);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    int numDimensions = in.readInt();
    if (this.coords == null || numDimensions != this.coords.length)
      this.coords = new double[numDimensions];
    for (int d = 0; d < getCoordinateDimension(); d++)
      this.coords[d] = in.readDouble();
  }

  @Override
  public int getCoordinateDimension() {
    return coords == null ? 0 : coords.length;
  }

  @Override
  public long getNumPoints() {
    return 1;
  }

  public void setCoordinateDimension(int k) {
    if (k == 0){
      this.coords = null;
    } else if (this.coords == null || this.coords.length != k) {
      this.coords = new double[k];
      Arrays.fill(this.coords, Double.NaN);
    }
  }

  @Override
  public String toString() {
    StringBuffer b = new StringBuffer();
    b.append("Point (");
    for (int d = 0; d < getCoordinateDimension(); d++) {
      if (d != 0)
        b.append(", ");
      b.append(coords[d]);
    }
    b.append(')');
    return b.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Point))
      return false;
    Point that = (Point) obj;
    if (this.getCoordinateDimension() != that.getCoordinateDimension())
      return false;
    return Arrays.equals(this.coords, that.coords);
  }
}
