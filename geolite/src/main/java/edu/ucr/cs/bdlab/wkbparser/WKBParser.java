/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucr.cs.bdlab.wkbparser;

import edu.ucr.cs.bdlab.geolite.*;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;

import java.io.IOException;
import java.nio.ByteBuffer;

public class WKBParser {

    int index;

    /**
     * A default constructor for an empty parser
     */
    public WKBParser() {
    }

    public IGeometry parse(ByteBuffer wkb, IGeometry geometry) {
        index = 0;
        return parseInternal(wkb, geometry);
    }

    /**
     * Parses the given WKB and tries to reuse the existing geometry if possible.
     * If not possible, e.g., if geometry is {@code null} or if the wkb represents a geometry of a different type,
     * a new object is created and returned.
     *
     * @param wkb      the WKB representation of the geometry
     * @param geometry either an existing geometry to reuse, or {@code null} to create a new geometry.
     * @return if the given WKB represents a geometry of the same type as given, the same object is returned. Otherwise,
     * a new object is created and returned.
     */
    public IGeometry parseInternal(ByteBuffer wkb, IGeometry geometry) {
        index += 1;
        switch (wkb.getInt(index)) {
            case 1:
                if (geometry == null || geometry.getType() != GeometryType.POINT)
                    geometry = new Point(2);
                else
                    geometry.setEmpty();
                index += 4;
                double x = wkb.getDouble(index);
                index += 8;
                double y = wkb.getDouble(index);
                index += 8;
                ((Point) geometry).set(x, y);
                return geometry;
            case 2:
                if (geometry == null || geometry.getType() != GeometryType.LINESTRING)
                    geometry = new LineString2D();
                else
                    geometry.setEmpty();
                index += 4;
                int numPoints = wkb.getInt(index);
                index += 4;
                for (int i = 0; i < numPoints; i++) {
                    x = wkb.getDouble(index);
                    index += 8;
                    y = wkb.getDouble(index);
                    index += 8;
                    ((LineString2D) geometry).addPoint(x, y);
                }
                return geometry;
            case 3:
                if (geometry == null || geometry.getType() != GeometryType.POLYGON)
                    geometry = new Polygon2D();
                else
                    geometry.setEmpty();
                index += 4;
                int numRings = wkb.getInt(index);
                index += 4;
                double firstPointX = 0;
                double firstPointY = 0;
                for (int i = 0; i < numRings; i++) {
                    numPoints = wkb.getInt(index);
                    index += 4;
                    for (int p = 0; p < numPoints; p++) {
                        x = wkb.getDouble(index);
                        index += 8;
                        y = wkb.getDouble(index);
                        index += 8;
                        ((Polygon2D) geometry).addPoint(x, y);
                        if (p == 0) {
                            firstPointX = x;
                            firstPointY = y;
                        }
                    }
                    ((Polygon2D) geometry).addPoint(firstPointX, firstPointY);
                    ((Polygon2D) geometry).closeLastRingNoCheck();
                }
                return geometry;
            case 4:
                if (geometry == null || geometry.getType() != GeometryType.MULTIPOINT)
                    geometry = new MultiPoint();
                else
                    geometry.setEmpty();
                index += 4;
                numPoints = wkb.getInt(index);
                index += 9;
                for (int i = 0; i < numPoints; i++) {
                    x = wkb.getDouble(index);
                    index += 8;
                    y = wkb.getDouble(index);
                    index += 8;
                    double[] coors = { x, y };
                    ((MultiPoint) geometry).addPoint(coors);
                    if (i != numPoints - 1)
                        index += 5;
                }
                return geometry;
            case 5:
                if (geometry == null || geometry.getType() != GeometryType.MULTILINESTRING)
                    geometry = new MultiLineString2D();
                else
                    geometry.setEmpty();
                index += 4;
                int numLineStrings = wkb.getInt(index);
                index += 9;
                for (int i = 0; i < numLineStrings; i++) {
                    numPoints = wkb.getInt(index);
                    index += 4;
                    for (int p = 0; p < numPoints; p++) {
                        x = wkb.getDouble(index);
                        index += 8;
                        y = wkb.getDouble(index);
                        index += 8;
                        ((MultiLineString2D) geometry).addPoint(x, y);
                    }
                    ((MultiLineString2D) geometry).endCurrentLineString();
                    if (i != numLineStrings - 1)
                        index += 5;
                }
                return geometry;
            case 6:
                if (geometry == null || geometry.getType() != GeometryType.MULTIPOLYGON)
                    geometry = new MultiPolygon2D();
                else
                    geometry.setEmpty();
                index += 4;
                int numPolygons = wkb.getInt(index);
                index += 9;
                firstPointX = 0;
                firstPointY = 0;
                for (int i = 0; i < numPolygons; i++) {
                    Polygon2D polygon = new Polygon2D();
                    numRings = wkb.getInt(index);
                    index += 4;
                    for (int r = 0; r < numRings; r++) {
                        numPoints = wkb.getInt(index);
                        index += 4;
                        for (int p = 0; p < numPoints; p++) {
                            x = wkb.getDouble(index);
                            index += 8;
                            y = wkb.getDouble(index);
                            index += 8;
                            polygon.addPoint(x, y);
                            if (p == 0) {
                                firstPointX = x;
                                firstPointY = y;
                            }
                        }
                        polygon.addPoint(firstPointX, firstPointY);
                        polygon.closeLastRingNoCheck();
                    }
                    ((MultiPolygon2D) geometry).addPolygon(polygon);
                    if (i != numPolygons - 1)
                        index += 5;
                }
                return geometry;
            case 7:
                if (geometry == null || geometry.getType() != GeometryType.GEOMETRYCOLLECTION)
                    geometry = new GeometryCollection();
                else
                    geometry.setEmpty();
                index += 4;
                int numGeometries = wkb.getInt(index);
                index += 4;
                for (int i = 0; i < numGeometries; i++) {
                    IGeometry geom = this.parseInternal(wkb, null);
                    ((GeometryCollection) geometry).addGeometry(geom);
                }
                return geometry;
            default:
                throw new RuntimeException("Not supported");
        }
    }
}
