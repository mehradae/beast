/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

/**
 * Interface for a simple linestring
 */
public interface ILineString extends IGeometry {

  /**
   * Number of points in the line string
   * @return
   */
  long getNumPoints();

  /**
   * Get the coordinates of the point at position n
   * @param n
   * @param p
   */
  void getPointN(long n, Point p);

  @Override
  default GeometryType getType() {
    return GeometryType.LINESTRING;
  }

  @Override
  default double getVolume() {
    return 0.0;
  }

  @Override
  default boolean isEmpty() {
    return getNumPoints() == 0;
  }

  @Override
  default int getGeometryStorageSize() {
    return (int) (getNumPoints() * 8 * 2 + 4);
  }
}
