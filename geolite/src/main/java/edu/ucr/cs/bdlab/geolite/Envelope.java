/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.geolite.twod.LineString2D;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * A d-dimensional orthogonal box (minimum bounding box)
 */
public class Envelope implements IGeometry {
  /**The coordinate of the minimum corner*/
  public double[] minCoord;

  /**The coordinate of the maximum corner*/
  public double[] maxCoord;

  public Envelope() {}

  /**
   * Create an envelope from a list of coordinates. The passed arguments is a list in the form (x1, y1, z1, ...,
   * x2, y1, z2, ...)
   * @param numDimensions
   * @param args
   */
  public Envelope(int numDimensions, double ... args) {
    this.setCoordinateDimension(numDimensions);
    if (args.length == 0)
      return;
    if (args.length != numDimensions * 2)
      throw new GeometryException("The arguments should contain 2 * numDimensions numbers");
    for (int d = 0; d < numDimensions; d++) {
      this.minCoord[d] = args[d];
      this.maxCoord[d] = args[numDimensions + d];
    }
  }

  /**
   * Creates an envelope initialized to the given two corners.
   * @param minCoord
   * @param maxCoord
   */
  public Envelope(double[] minCoord, double[] maxCoord) {
    assert minCoord.length == maxCoord.length;
    this.minCoord = Arrays.copyOf(minCoord, minCoord.length);
    this.maxCoord = Arrays.copyOf(maxCoord, maxCoord.length);
  }

  /**
   * A copy constructor.
   * @param other
   */
  public Envelope(Envelope other) {
    this.minCoord = Arrays.copyOf(other.minCoord, other.minCoord.length);
    this.maxCoord = Arrays.copyOf(other.maxCoord, other.maxCoord.length);
  }

  /**
   * Returns the side length of dimension {@code d}.
   * @param d
   * @return
   */
  public double getSideLength(int d) {
    return this.maxCoord[d] - this.minCoord[d];
  }

  /**
   * Returns the center coordinate along the dimension {@code d}
   * @param d
   * @return
   */
  public double getCenter(int d) {
    return (this.minCoord[d] + this.maxCoord[d]) / 2.0;
  }

  /**
   * Expands this envelope to enclose the given geometry.
   * @param geom
   */
  public void merge(IGeometry geom) {
    if (geom.isEmpty())
      return;
    assert geom.getCoordinateDimension() == this.getCoordinateDimension();
    if (geom instanceof Envelope) {
      Envelope env = (Envelope) geom;
      this.setCoordinateDimension(env.getCoordinateDimension());
      for (int d = 0; d < getCoordinateDimension(); d++) {
        this.minCoord[d] = Math.min(this.minCoord[d], env.minCoord[d]);
        this.maxCoord[d] = Math.max(this.maxCoord[d], env.maxCoord[d]);
      }
    } else if (geom instanceof Point) {
      Point pt = (Point) geom;
      for (int d = 0; d < this.getCoordinateDimension(); d++) {
        this.minCoord[d] = Math.min(this.minCoord[d], pt.coords[d]);
        this.maxCoord[d] = Math.max(this.maxCoord[d], pt.coords[d]);
      }
    } else if (geom instanceof LineString2D) {
      LineString2D linestring = (LineString2D) geom;
      for (int iPoint = 0; iPoint < linestring.numPoints; iPoint++) {
        this.minCoord[0] = Math.min(this.minCoord[0], linestring.xs[iPoint]);
        this.maxCoord[0] = Math.max(this.maxCoord[0], linestring.xs[iPoint]);
        this.minCoord[1] = Math.min(this.minCoord[1], linestring.ys[iPoint]);
        this.maxCoord[1] = Math.max(this.maxCoord[1], linestring.ys[iPoint]);
      }
    } else if (geom instanceof GeometryCollection) {
      GeometryCollection geomCollection = (GeometryCollection) geom;
      for (int i$ = 0; i$ < geomCollection.numGeometries(); i$++)
        this.merge(geomCollection.getGeometry(i$));
    } else {
      throw new GeometryException("Cannot merge with this geometry", geom);
    }
  }

  /**
   * Sets the number of dimensions for the envelope. If the number of dimensions is different from the current one,
   * the envelope is updated to be an empty envelope, i.e., all minimum and maximum coordinates are set to
   * &infin; and -&infin;, respectively.
   * @param numDimensions
   */
  public void setCoordinateDimension(int numDimensions) {
    if (this.minCoord == null || numDimensions != this.getCoordinateDimension()) {
      this.minCoord = new double[numDimensions];
      this.maxCoord = new double[numDimensions];
      // Set to an infinite reversed envelope to mark it as empty in all dimensions
      Arrays.fill(this.minCoord, Double.POSITIVE_INFINITY);
      Arrays.fill(this.maxCoord, Double.NEGATIVE_INFINITY);
    }
  }

  /**
   * Returns the number of dimensions for a coordinate in this envelope.
   * @return
   */
  @Override
  public int getCoordinateDimension() {
    return minCoord == null ? 0 : minCoord.length;
  }

  @Override
  public long getNumPoints() {
    return 4;
  }

  /**
   * Adjusts the coordinate dimensions for this envelope and updates its boundaries to the given two corners.
   * It is assumed that minCoords[i] &le; maxCoords[i] for all i = 0, ..., d-1. No sanity check is done.
   * @param minCoords
   * @param maxCoords
   */
  public void set(double[] minCoords, double[] maxCoords) {
    if (this.getCoordinateDimension() != minCoords.length)
      this.setCoordinateDimension(minCoords.length);
    System.arraycopy(minCoords, 0, this.minCoord, 0, minCoords.length);
    System.arraycopy(maxCoords, 0, this.maxCoord, 0, maxCoords.length);
  }

  /**
   * Expands this envelope to enclose the given point coordinate. The given point should have the same coordinate
   * dimension of the envelope.
   * @param point
   */
  public void merge(double[] point) {
    assert point.length == getCoordinateDimension();
    for (int d = 0; d < getCoordinateDimension(); d++) {
      this.minCoord[d] = Math.min(this.minCoord[d], point[d]);
      this.maxCoord[d] = Math.max(this.maxCoord[d], point[d]);
    }
  }

  /**
   * Checks if this envelope overlaps the envelope defined by the given two coordinates.
   * @param min
   * @param max
   * @return
   */
  public boolean overlaps(double[] min, double[] max) {
    if (min.length != getCoordinateDimension())
      throw new GeometryException(String.format("The dimension of the coordinates are incompatible. Expected %d found %d!",
          getCoordinateDimension(), min.length));
    for (int d = 0; d < getCoordinateDimension(); d++) {
      if (max[d] <= this.minCoord[d] || this.maxCoord[d] <= min[d])
        return false;
    }
    return true;
  }

  public boolean overlapsEnvelope(Envelope envelope2) {
    return this.overlaps(envelope2.minCoord, envelope2.maxCoord);
  }

  public boolean containsPoint(double[] coord) {
    assert coord.length == getCoordinateDimension();
    for (int d = 0; d < coord.length; d++)
      if (coord[d] < minCoord[d] || coord[d] >= maxCoord[d])
        return false;
    return true;
  }


  @Override
  public Envelope envelope(Envelope envelope) {
    envelope.minCoord = Arrays.copyOf(this.minCoord, getCoordinateDimension());
    envelope.maxCoord = Arrays.copyOf(this.maxCoord, getCoordinateDimension());
    return envelope;
  }

  @Override
  public double getVolume() {
    double volume = 1.0;
    for (int d = 0; d < getCoordinateDimension(); d++)
      volume *= maxCoord[d] - minCoord[d];
    return volume;
  }

  @Override
  public GeometryType getType() {
    return GeometryType.ENVELOPE;
  }

  @Override
  public int getGeometryStorageSize() {
    return 8 * 2 * getCoordinateDimension();
  }

  @Override
  public boolean isEmpty() {
    if (getCoordinateDimension() == 0)
      return true;
    for (int d = 0; d < getCoordinateDimension(); d++)
      if (maxCoord[d] < minCoord[d] || Double.isNaN(minCoord[d]) || Double.isNaN(maxCoord[d]))
        return true;
    return false;
  }

  @Override
  public void setEmpty() {
    for (int d = 0; d < getCoordinateDimension(); d++) {
      this.minCoord[d] = Double.POSITIVE_INFINITY;
      this.maxCoord[d] = Double.NEGATIVE_INFINITY;
    }
  }

  @Override
  public void toWKT(StringBuilder out) {
    if (this.getCoordinateDimension() != 2)
      throw new GeometryException("WKT not supported for higher-dimension polygons");
    out.append("POLYGON((");
    out.append(this.minCoord[0]);
    out.append(' ');
    out.append(this.minCoord[1]);
    out.append(',');

    out.append(this.maxCoord[0]);
    out.append(' ');
    out.append(this.minCoord[1]);
    out.append(',');

    out.append(this.maxCoord[0]);
    out.append(' ');
    out.append(this.maxCoord[1]);
    out.append(',');

    out.append(this.minCoord[0]);
    out.append(' ');
    out.append(this.maxCoord[1]);
    out.append(',');

    out.append(this.minCoord[0]);
    out.append(' ');
    out.append(this.minCoord[1]);
    out.append(')'); // The ring
    out.append(')'); // The polygon
  }

  @Override
  public ByteBuffer toWKB(ByteBuffer out) {
    byte byteOrder = (byte)(out.order() == ByteOrder.LITTLE_ENDIAN ? 1 : 0);
    out.put(byteOrder);
    out.putInt(3);
    out.putInt(1);
    out.putInt(5);
    out.putDouble(this.minCoord[0]);
    out.putDouble(this.minCoord[1]);
    out.putDouble(this.maxCoord[0]);
    out.putDouble(this.minCoord[1]);
    out.putDouble(this.maxCoord[0]);
    out.putDouble(this.maxCoord[1]);
    out.putDouble(this.minCoord[0]);
    out.putDouble(this.maxCoord[1]);
    out.putDouble(this.minCoord[0]);
    out.putDouble(this.minCoord[1]);
    return out;
  }

  @Override
  public Point centroid(Point centroid) {
    centroid.setCoordinateDimension(this.getCoordinateDimension());
    for (int d$ = 0; d$ < this.getCoordinateDimension(); d$++)
      centroid.coords[d$] = (this.minCoord[d$] + this.maxCoord[d$]) / 2.0;
    return centroid;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(getCoordinateDimension());
    for (int d = 0; d < getCoordinateDimension(); d++) {
      out.writeDouble(minCoord[d]);
      out.writeDouble(maxCoord[d]);
    }
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    int numDimensions = in.readInt();
    this.setCoordinateDimension(numDimensions);
    for (int d = 0; d < getCoordinateDimension(); d++) {
      minCoord[d] = in.readDouble();
      maxCoord[d] = in.readDouble();
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof  Envelope))
      return false;
    Envelope env = (Envelope) obj;
    if (this.getCoordinateDimension() != env.getCoordinateDimension())
      return false;
    for (int d = 0; d < getCoordinateDimension(); d++)
      if (this.minCoord[d] != env.minCoord[d] || this.maxCoord[d] != env.maxCoord[d])
        return false;
    return true;
  }

  /**
   * Shrink this envelope to be fully contained in the given envelope. In other words, the result of this function
   * is the intersection of this envelope with the given envelope.
   * @param that
   */
  public void shrink(Envelope that) {
    assert this.getCoordinateDimension() == that.getCoordinateDimension();
    for (int d = 0; d < this.getCoordinateDimension(); d++) {
      this.minCoord[d] = Math.max(this.minCoord[d], that.minCoord[d]);
      this.maxCoord[d] = Math.min(this.maxCoord[d], that.maxCoord[d]);
    }
  }

  /**
   * Expand the envelope in all directions by the given buffer.
   * @param delta
   */
  public void buffer(double ... delta) {
    for (int d = 0; d < this.getCoordinateDimension(); d++) {
      this.minCoord[d] -= delta[d];
      this.maxCoord[d] += delta[d];
    }
  }

  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    str.append("Envelope (");
    if (getCoordinateDimension() == 0) {
      str.append("EMPTY");
    } else {
      str.append('[');
      for (int d = 0; d < minCoord.length; d++) {
        if (d > 0) {
          str.append(',');
          str.append(' ');
        }
        str.append(minCoord[d]);
      }
      str.append("]->[");
      for (int d = 0; d < maxCoord.length; d++) {
        if (d > 0) {
          str.append(',');
          str.append(' ');
        }
        str.append(maxCoord[d]);
      }
      str.append("]");
    }
    str.append(")");
    return str.toString();
  }

  public void set(Envelope other) {
    this.set(other.minCoord, other.maxCoord);
  }

  /**
   * Sets this envelope to cover the entire space (-&infin;, +&infin;)
   */
  public void setInfinite() {
    for (int d = 0; d < getCoordinateDimension(); d++) {
      this.minCoord[d] = Double.NEGATIVE_INFINITY;
      this.maxCoord[d] = Double.POSITIVE_INFINITY;
    }
  }

  /**
   * Returns {@code true} if this envelope contains the given envelope.
   * @param other
   * @return
   */
  public boolean containsEnvelope(Envelope other) {
    for (int d = 0; d < this.getCoordinateDimension(); d++) {
      if (other.minCoord[d] < this.minCoord[d] || other.maxCoord[d] > this.maxCoord[d])
        return false;
    }
    return true;
  }
}
