/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite.util;


import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;

import java.io.IOException;
import java.util.Arrays;

/**
 * A utility class that converts geolite geometries to JTS API geometries.
 */
public class JTSConverter {
  static org.locationtech.jts.geom.GeometryFactory factory = new org.locationtech.jts.geom.GeometryFactory();

  public static org.locationtech.jts.geom.Geometry createJTSGeometry(edu.ucr.cs.bdlab.geolite.IGeometry geom) {
    return convertGeometry(geom);
  }

  private static org.locationtech.jts.geom.Geometry convertGeometry(edu.ucr.cs.bdlab.geolite.IGeometry geom) {
    switch (geom.getType()) {
      case POINT:
        edu.ucr.cs.bdlab.geolite.Point point = (edu.ucr.cs.bdlab.geolite.Point) geom;
        org.locationtech.jts.geom.Coordinate coord;
        if (point.isEmpty())
          return factory.createPoint();
        else if (point.getCoordinateDimension() == 2)
          coord = new org.locationtech.jts.geom.Coordinate(point.coords[0], point.coords[1]);
        else if (point.getCoordinateDimension() == 3)
          coord = new org.locationtech.jts.geom.Coordinate(point.coords[0], point.coords[1], point.coords[2]);
        else
          throw new edu.ucr.cs.bdlab.geolite.GeometryException("JTS API does not support higher than 3D points");
        return factory.createPoint(coord);
      case ENVELOPE:
        edu.ucr.cs.bdlab.geolite.Envelope envelope = (edu.ucr.cs.bdlab.geolite.Envelope) geom;
        if (envelope.isEmpty())
          return factory.createPolygon();
        else if (envelope.getCoordinateDimension() == 2) {
          org.locationtech.jts.geom.Coordinate[] coords = new org.locationtech.jts.geom.Coordinate[5];
          coords[0] = new org.locationtech.jts.geom.Coordinate(envelope.minCoord[0], envelope.minCoord[1]);
          coords[1] = new org.locationtech.jts.geom.Coordinate(envelope.maxCoord[0], envelope.minCoord[1]);
          coords[2] = new org.locationtech.jts.geom.Coordinate(envelope.maxCoord[0], envelope.maxCoord[1]);
          coords[3] = new org.locationtech.jts.geom.Coordinate(envelope.minCoord[0], envelope.maxCoord[1]);
          coords[4] = coords[0];
          return factory.createPolygon(coords);
        }
        else
          throw new edu.ucr.cs.bdlab.geolite.GeometryException("JTS API does not support higher than 2D Envelopes");
      case LINESTRING:
        return convertLinestring((edu.ucr.cs.bdlab.geolite.twod.LineString2D) geom);
      case POLYGON:
        return convertPolygon((edu.ucr.cs.bdlab.geolite.twod.Polygon2D) geom);
      case MULTILINESTRING:
        return convertMultilLineString((edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D) geom);
      case MULTIPOLYGON:
        return convertMultiPolygon((edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D) geom);
      case GEOMETRYCOLLECTION:
        return convertGeometryCollection((edu.ucr.cs.bdlab.geolite.GeometryCollection) geom);
      default:
        return null;
    }
  }

  public static org.locationtech.jts.geom.LineString convertLinestring(edu.ucr.cs.bdlab.geolite.ILineString lineString) {
    if (lineString.isEmpty())
      return factory.createLineString();
    org.locationtech.jts.geom.Coordinate[] linestringCoords = new org.locationtech.jts.geom.Coordinate[(int) lineString.getNumPoints()];
    edu.ucr.cs.bdlab.geolite.Point ppt = new edu.ucr.cs.bdlab.geolite.Point(2);
    for (int iPoint = 0; iPoint < lineString.getNumPoints(); iPoint++) {
      lineString.getPointN(iPoint, ppt);
      linestringCoords[iPoint] = new org.locationtech.jts.geom.Coordinate(ppt.coords[0], ppt.coords[1]);
    }
    return factory.createLineString(linestringCoords);
  }

  private static org.locationtech.jts.geom.Geometry convertMultilLineString(edu.ucr.cs.bdlab.geolite.IMultiLineString multiLineString) {
    if (multiLineString.isEmpty())
      return factory.createMultiLineString();
    org.locationtech.jts.geom.LineString[] linestrings = new org.locationtech.jts.geom.LineString[multiLineString.getNumLineStrings()];
    for (int iLineString = 0; iLineString < multiLineString.getNumLineStrings(); iLineString++) {
      linestrings[iLineString] = convertLinestring(multiLineString.getLineStringN(iLineString));
    }
    return factory.createMultiLineString(linestrings);
  }

  public static org.locationtech.jts.geom.LinearRing convertRing(edu.ucr.cs.bdlab.geolite.ILineString lineString) {
    if (lineString.isEmpty())
      return factory.createLinearRing();
    org.locationtech.jts.geom.Coordinate[] linestringCoords = new org.locationtech.jts.geom.Coordinate[(int) lineString.getNumPoints()];
    edu.ucr.cs.bdlab.geolite.Point ppt = new edu.ucr.cs.bdlab.geolite.Point(2);
    for (int iPoint = 0; iPoint < lineString.getNumPoints(); iPoint++) {
      lineString.getPointN(iPoint, ppt);
      linestringCoords[iPoint] = new org.locationtech.jts.geom.Coordinate(ppt.coords[0], ppt.coords[1]);
    }
    return factory.createLinearRing(linestringCoords);
  }

  public static org.locationtech.jts.geom.Polygon convertPolygon(edu.ucr.cs.bdlab.geolite.IPolygon litePolygon) {
    if (litePolygon.isEmpty())
      return factory.createPolygon();
    org.locationtech.jts.geom.LinearRing[] rings = new org.locationtech.jts.geom.LinearRing[litePolygon.getNumRings()];
    for (int iRing = 0; iRing < litePolygon.getNumRings(); iRing++)
      rings[iRing] = convertRing(litePolygon.getRingN(iRing));
    return factory.createPolygon(rings[0], Arrays.copyOfRange(rings, 1, rings.length));
  }

  public static org.locationtech.jts.geom.MultiPolygon convertMultiPolygon(edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D liteMultipolygon) {
    if (liteMultipolygon.isEmpty())
      return factory.createMultiPolygon();
    org.locationtech.jts.geom.Polygon[] polygons = new org.locationtech.jts.geom.Polygon[liteMultipolygon.getNumPolygons()];
    edu.ucr.cs.bdlab.geolite.twod.Polygon2D litePolygon = new edu.ucr.cs.bdlab.geolite.twod.Polygon2D();
    for (int iPolygon = 0; iPolygon < liteMultipolygon.getNumPolygons(); iPolygon++) {
      liteMultipolygon.getPolygonN(iPolygon, litePolygon);
      polygons[iPolygon] = convertPolygon(litePolygon);
    }
    return factory.createMultiPolygon(polygons);
  }

  public static org.locationtech.jts.geom.GeometryCollection convertGeometryCollection(
      edu.ucr.cs.bdlab.geolite.GeometryCollection geomCollection) {
    if (geomCollection.isEmpty())
      return factory.createGeometryCollection();
    org.locationtech.jts.geom.Geometry[] geoms = new org.locationtech.jts.geom.Geometry[geomCollection.numGeometries()];
    for (int iGeom = 0; iGeom < geomCollection.numGeometries(); iGeom++) {
      geoms[iGeom] = convertGeometry(geomCollection.getGeometry(iGeom));
    }
    return factory.createGeometryCollection(geoms);
  }

  public static edu.ucr.cs.bdlab.geolite.IGeometry fromJTSGeometry(org.locationtech.jts.geom.Geometry jtsGeom,
                                                                   edu.ucr.cs.bdlab.geolite.IGeometry result) {
    // TODO use a WKB conversion rather than WKT for efficiency
    String wkt = jtsGeom.toText();
    try {
      return new WKTParser().parse(wkt, result);
    } catch (ParseException e) {
      e.printStackTrace();
      return null;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }
}
