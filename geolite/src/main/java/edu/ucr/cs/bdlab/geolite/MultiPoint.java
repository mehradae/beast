/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.geolite.util.Fundamentals;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class MultiPoint implements IGeometry {
  /**The coordinates for all the points. The first array index is for the dimension and the second index is for
   * the point*/
  protected double coords[][];

  /**The number of points currently stores*/
  protected int numPoints;

  @Override
  public long getNumPoints() {
    return numPoints;
  }

  public void getPointN(int n, Point p) {
    p.setCoordinateDimension(this.getCoordinateDimension());
    getPointN(n, p.coords);
  }

  public void getPointN(int n, double[] coords) {
    assert coords.length == this.getCoordinateDimension();
    for (int $d = 0; $d < coords.length; $d++)
      coords[$d] = this.coords[$d][n];
  }

  public void setPointN(int n, double[] coords) {
    assert coords.length == getCoordinateDimension();
    assert n < getNumPoints();
    for (int $d = 0; $d < coords.length; $d++)
      this.coords[$d][n] = coords[$d];
  }

  @Override
  public int getCoordinateDimension() {
    return coords == null ? 0 : coords.length;
  }

  public void addPoint(Point p) {
    addPoint(p.coords);
  }

  public void addPoint(double ... newCoords) {
    if (numPoints == 0) {
      this.coords = new double[newCoords.length][16];
    } else {
      if (newCoords.length != this.getCoordinateDimension())
        throw new GeometryException("All points in a multipoint should have the same number of dimensions");
      if (numPoints == this.coords[0].length) {
        // Need to expand the array
        for (int $d = 0; $d < newCoords.length; $d++)
          this.coords[$d] = Fundamentals.expand(this.coords[$d], numPoints * 2);
      }
    }
    for (int $d = 0; $d < newCoords.length; $d++)
      this.coords[$d][numPoints] = newCoords[$d];
    numPoints++;
  }

  @Override
  public Envelope envelope(Envelope envelope) {
    if (envelope == null) {
      envelope = new Envelope(this.getCoordinateDimension());
    } else {
      envelope.setCoordinateDimension(this.getCoordinateDimension());
      envelope.setEmpty();
    }
    for (int $d = 0; $d < coords.length; $d++) {
      for (int $i = 0; $i < coords[$d].length; $i++) {
        envelope.minCoord[$d] = Math.min(envelope.minCoord[$d], this.coords[$d][$i]);
        envelope.maxCoord[$d] = Math.max(envelope.maxCoord[$d], this.coords[$d][$i]);
      }
    }
    return envelope;
  }

  @Override
  public boolean intersects(IGeometry geometry) {
    return false;
  }

  @Override
  public double getVolume() {
    return 0.0;
  }

  @Override
  public GeometryType getType() {
    return GeometryType.MULTIPOINT;
  }

  @Override
  public int getGeometryStorageSize() {
    // eight bytes per coordinate
    return (int) (getCoordinateDimension() * getNumPoints() * 8);
  }

  @Override
  public boolean isEmpty() {
    return getNumPoints() > 0;
  }

  @Override
  public void setEmpty() {
    numPoints = 0;
  }

  @Override
  public void toWKT(StringBuilder out) {
    out.append("MULTIPOINT(");
    for (int $i = 0; $i < getNumPoints(); $i++) {
      if ($i > 0)
        out.append(',');
      out.append('(');
      for (int $d = 0; $d < coords.length; $d++) {
        out.append(' ');
        out.append(coords[$d][$i]);
      }
      out.append(')');
    }
    out.append(')');
  }

  @Override
  public ByteBuffer toWKB(ByteBuffer out) {
    if (coords.length > 2) throw new RuntimeException("Not supported");
    else{
      byte byteOrder = (byte)(out.order() == ByteOrder.LITTLE_ENDIAN ? 1 : 0);
      out.put(byteOrder);
      out.putInt(4);
      out.putInt(numPoints);
      for (int $i = 0; $i < getNumPoints(); $i++) {
        out.put(byteOrder);
        out.putInt(1);
        out.putDouble(coords[0][$i]);
        out.putDouble(coords[1][$i]);
      }
      return out;
    }
  }

  @Override
  public Point centroid(Point centroid) {
    centroid.setCoordinateDimension(this.getCoordinateDimension());
    if (numPoints == 0) {
      centroid.setEmpty();
      return centroid;
    }
    for (int $d = 0; $d < coords.length; $d++) {
      double sum = 0.0;
      for (int $i = 0; $i < numPoints; $i++)
        sum += coords[$d][$i];
      centroid.coords[$d] /= numPoints;
    }
    return centroid;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(getCoordinateDimension());
    out.writeInt(numPoints);
    for (int $d = 0; $d < coords.length; $d++)
      for (int $i = 0; $i < numPoints; $i++)
        out.writeDouble(coords[$d][$i]);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    int numDimensions = in.readInt();
    int numPointsToRead = in.readInt();
    // Create a new array if needed
    if (numDimensions != this.getCoordinateDimension() || this.coords.length < numPointsToRead)
      this.coords = new double[numDimensions][numPointsToRead];
    for (int $d = 0; $d < numDimensions; $d++)
      for (int $i = 0; $i < numPointsToRead; $i++)
        this.coords[$d][$i] = in.readDouble();
    this.numPoints = numPointsToRead;
  }
}
