/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite.twod;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.ILineString;
import edu.ucr.cs.bdlab.geolite.util.Fundamentals;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class LineString2D implements IGeometry, ILineString {
  public int numPoints;

  /**
   * The coordinates of the points in the linestring. Only the first {@code #numPoints} are valid.
   */
  public double[] xs, ys, zs;

  /**
   * If a measure value is associated with this geometry, they are stored in this array. Otherwise,
   * this array is null.
   */
  public double[] ms;

  public LineString2D() {}

  /**
   * Appends a point to the line string.
   * @param x
   * @param y
   */
  public void addPoint(double x, double y) {
    ensurePointCapacity(numPoints + 1);
    xs[numPoints] = x;
    ys[numPoints] = y;
    numPoints++;
  }

  /**
   * Add a point with a measure value
   * @param x
   * @param y
   * @param m
   */
  public void addPointXYM(double x, double y, double m) {
    ensurePointCapacityM(numPoints + 1);
    xs[numPoints] = x;
    ys[numPoints] = y;
    ms[numPoints] = m;
    numPoints++;
  }

  public void addPointXYZM(double x, double y, double z, double m) {
    ensurePointCapacityMZ(numPoints + 1);
    xs[numPoints] = x;
    ys[numPoints] = y;
    zs[numPoints] = z;
    ms[numPoints] = m;
    numPoints++;
  }

  protected void ensurePointCapacity(int newSize) {
    xs = Fundamentals.expand(xs, Fundamentals.nextPowerOfTwo(newSize));
    ys = Fundamentals.expand(ys, Fundamentals.nextPowerOfTwo(newSize));
  }

  protected void ensurePointCapacityM(int newSize) {
    xs = Fundamentals.expand(xs, Fundamentals.nextPowerOfTwo(newSize));
    ys = Fundamentals.expand(ys, Fundamentals.nextPowerOfTwo(newSize));
    ms = Fundamentals.expand(ms, Fundamentals.nextPowerOfTwo(newSize));
  }

  protected void ensurePointCapacityMZ(int newSize) {
    xs = Fundamentals.expand(xs, Fundamentals.nextPowerOfTwo(newSize));
    ys = Fundamentals.expand(ys, Fundamentals.nextPowerOfTwo(newSize));
    ms = Fundamentals.expand(ms, Fundamentals.nextPowerOfTwo(newSize));
    zs = Fundamentals.expand(zs, Fundamentals.nextPowerOfTwo(newSize));
  }

  @Override
  public int getCoordinateDimension() {
    return 2;
  }

  @Override
  public long getNumPoints() {
    return numPoints;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(numPoints);
    out.writeBoolean(hasMValues());
    for (int i = 0; i < numPoints; i++) {
      out.writeDouble(xs[i]);
      out.writeDouble(ys[i]);
      if (ms != null)
        out.writeDouble(ms[i]);
    }
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    numPoints = in.readInt();
    boolean hasMValues = in.readBoolean();
    if (xs == null || numPoints > xs.length) {
      xs = new double[numPoints];
      ys = new double[numPoints];
      if (hasMValues)
        ms = new double[numPoints];
      else
        ms = null;
    }
    for (int i = 0; i < numPoints; i++) {
      xs[i] = in.readDouble();
      ys[i] = in.readDouble();
      if (hasMValues)
        ms[i] = in.readDouble();
    }
  }

  @Override
  public Envelope envelope(Envelope envelope) {
    envelope.setEmpty();
    for (int i = 0; i < numPoints; i++) {
      envelope.minCoord[0] = Math.min(envelope.minCoord[0], xs[i]);
      envelope.maxCoord[0] = Math.max(envelope.maxCoord[0], xs[i]);
      envelope.minCoord[1] = Math.min(envelope.minCoord[1], ys[i]);
      envelope.maxCoord[1] = Math.max(envelope.maxCoord[1], ys[i]);
    }
    return envelope;
  }

  /**
   * Two line strings are equal if they have the same number of points and the points have the same coordinates.
   * @param obj
   * @return
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof LineString2D))
      return false;
    LineString2D ls = (LineString2D) obj;
    if (this.numPoints != ls.numPoints)
      return false;
    for (int i = 0; i < numPoints; i++) {
      if (xs[i] != ls.xs[i] || ys[i] != ls.ys[i])
        return false;
    }
    return true;
  }

  @Override
  public void setEmpty() {
    this.numPoints = 0;
  }


  @Override
  public void toWKT(StringBuilder out) {
    out.append("LINESTRING(");
    for (int iPoint = 0; iPoint < numPoints; iPoint++) {
      if (iPoint > 0)
        out.append(',');
      out.append(xs[iPoint]); out.append(' '); out.append(ys[iPoint]);
    }
    out.append(')');
  }

  @Override
  public ByteBuffer toWKB(ByteBuffer out) {
    byte byteOrder = (byte)(out.order() == ByteOrder.LITTLE_ENDIAN ? 1 : 0);
    out.put(byteOrder);
    out.putInt(2);
    out.putInt(numPoints);
    for (int iPoint = 0; iPoint < numPoints; iPoint++) {
      out.putDouble(xs[iPoint]);
      out.putDouble(ys[iPoint]);
    }
    return out;
  }

  @Override
  public Point centroid(Point centroid) {
    centroid.coords[0] = 0;
    centroid.coords[1] = 0;
    for (int iPoint = 0; iPoint < numPoints; iPoint++) {
      centroid.coords[0] += xs[iPoint];
      centroid.coords[1] += ys[iPoint];
    }
    centroid.coords[0] /= numPoints;
    centroid.coords[1] /= numPoints;
    return centroid;
  }

  @Override
  public String toString() {
    return asText();
  }

  /**
   * Get the coordinates of a point
   * @param i
   * @param p
   */
  public void getPointCoordinates(int i, Point p) {
    getPointCoordinates(i, p.coords);
  }

  public void getPointCoordinates(int i, double[] coords) {
    assert coords.length == this.getCoordinateDimension();
    coords[0] = this.xs[i];
    coords[1] = this.ys[i];
  }

  @Override
  public void getPointN(long n, Point p) {
    assert p.getCoordinateDimension() == this.getCoordinateDimension();
    p.coords[0] = this.xs[(int) n];
    p.coords[1] = this.ys[(int) n];
  }

  public boolean hasMValues() {
    return this.ms != null;
  }

  /**
   * Tests if a point lies on this linestring
   * @param point
   * @return
   */
  public boolean isPointOn(Point point) {
    if (point.getCoordinateDimension() != this.getCoordinateDimension())
      return false;
    // Test each line segment
    for (int $i = 1; $i < numPoints; $i++) {
      double x1 = xs[$i] - xs[$i-1];
      double y1 = ys[$i] - ys[$i-1];
      double x2 = point.coords[0] - xs[$i];
      double y2 = point.coords[1] - ys[$i];
      if (Fundamentals.pointOnLineSegment(xs[$i-1], ys[$i-1], xs[$i], ys[$i], point.coords[0], point.coords[1]))
        return true;
    }
    return false;
  }

  /**
   * Update the coordinate of the given point
   * @param $i
   * @param coords
   */
  public void setPointCoordinate(int $i, double[] coords) {
    assert coords.length == this.getCoordinateDimension();
    this.xs[$i] = coords[0];
    this.ys[$i] = coords[1];
  }
}
