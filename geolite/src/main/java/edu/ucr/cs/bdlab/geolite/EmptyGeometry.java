/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectInput;
import java.nio.ByteBuffer;

/**
 * A singleton class that represents the empty geometry.
 */
public class EmptyGeometry implements IGeometry {

  /**The singleton instance of the geometry*/
  public static final EmptyGeometry instance = new EmptyGeometry();

  /**The private constructor is used to create an immutable singleton*/
  private EmptyGeometry() {}

  @Override
  public Envelope envelope(Envelope envelope) {
    envelope.setEmpty();
    return envelope;
  }

  @Override
  public boolean intersects(IGeometry geometry) {
    return false;
  }

  @Override
  public double getVolume() {
    return 0;
  }

  @Override
  public GeometryType getType() {
    return GeometryType.EMPTY;
  }

  @Override
  public int getGeometryStorageSize() {
    return 0;
  }

  @Override
  public boolean isEmpty() {
    return true;
  }

  @Override
  public void setEmpty() {
    // Do nothing. It is already empty.
  }

  @Override
  public void toWKT(StringBuilder out) {
    out.append("EMPTY");
  }

  @Override
  public ByteBuffer toWKB(ByteBuffer out) {
    return null;
  }

  @Override
  public Point centroid(Point centroid) {
    centroid.setEmpty();
    return centroid;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    // Do nothing
  }

  @Override
  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    // Do nothing
  }

  @Override
  public int getCoordinateDimension() {
    return 0;
  }

  @Override
  public long getNumPoints() {
    return 0;
  }

  @Override
  public void readFields(DataInput dataInput) {
    // Do nothing
  }
}
