/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

/**
 * An interface for a multiline sstring
 */
public interface IMultiLineString extends ILineString {

  @Override
  default GeometryType getType() {
    return GeometryType.MULTILINESTRING;
  }

  /**
   * Number of line strings
   * @return
   */
  int getNumLineStrings();

  /**
   * Return a read-only line string for the given index (0-based)
   * @param n
   * @return
   */
  ILineString getLineStringN(int n);

  @Override
  default int getGeometryStorageSize() {
    return (int) (getNumPoints() * 8 * getCoordinateDimension() + getNumLineStrings() * 4);
  }
}
