package edu.ucr.cs.bdlab.geolite.util;

import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.OperatorIntersection;
import com.esri.core.geometry.SpatialReference;
import edu.ucr.cs.bdlab.geolite.IGeometry;

/**
 * Computes the intersection between two geometries
 */
public class GeometryIntersection {
  public static IGeometry intersection(IGeometry geom1, IGeometry geom2, IGeometry result) {
    Geometry esriGeom1 = EsriConverter.createEsriGeometry(geom1);
    Geometry esriGeom2 = EsriConverter.createEsriGeometry(geom2);
    Geometry esriResult = GeometryEngine.intersect(esriGeom1, esriGeom2, SpatialReference.create(4326));
    return EsriConverter.fromEsriGeometry(esriResult, result);
  }
}
