/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite.twod;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.ILineString;
import edu.ucr.cs.bdlab.geolite.IMultiLineString;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.util.Fundamentals;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * A geometry that contains a disjoint set of LineStrings.
 */
public class MultiLineString2D extends LineString2D implements IMultiLineString {

  /**Number of line strings stored in this multi linestring*/
  public int numLineStrings;

  /**The index of the first point in each line string*/
  public int[] firstPointInLineString;

  /**A flag that signals that the last linestring has ended and the next call to {@link #addPoint(double, double)}
   * will create a new polygon*/
  protected boolean lastLineStringEnded;

  public MultiLineString2D() {
    this.lastLineStringEnded = true;
  }

  @Override
  public void setEmpty() {
    super.setEmpty();
    numLineStrings = 0;
    lastLineStringEnded = true;
  }

  /**
   * Ends the current linestring and prepares the multilinestring to create a new linestring with the next call to
   * {@link #addPoint(double, double)}
   */
  public void endCurrentLineString() {
    lastLineStringEnded = true;
    firstPointInLineString[numLineStrings] = numPoints;
  }

  @Override
  public void addPoint(double x, double y) {
    if (lastLineStringEnded) {
      ensureLinestringCapacity(numLineStrings + 1);
      firstPointInLineString[numLineStrings] = numPoints;
      numLineStrings++;
      lastLineStringEnded = false;
    }
    super.addPoint(x, y);
  }

  protected void ensureLinestringCapacity(int newSize) {
    firstPointInLineString = Fundamentals.expand(firstPointInLineString, Fundamentals.nextPowerOfTwo(newSize + 1));
  }

  @Override
  public void addPointXYM(double x, double y, double m) {
    if (lastLineStringEnded) {
      ensureLinestringCapacity(numLineStrings+1);
      firstPointInLineString[numLineStrings] = numPoints;
      numLineStrings++;
      lastLineStringEnded = false;
    }
    super.addPointXYM(x, y, m);
  }

  @Override
  public void addPointXYZM(double x, double y, double z, double m) {
    if (lastLineStringEnded) {
      ensureLinestringCapacity(numLineStrings+1);
      firstPointInLineString[numLineStrings] = numPoints;
      numLineStrings++;
      lastLineStringEnded = false;
    }
    super.addPointXYZM(x, y, z, m);
  }

  @Override
  public int getNumLineStrings() {
    return numLineStrings;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    super.write(out);
    out.writeInt(numLineStrings);
    for (int iPoly = 0; iPoly < numLineStrings; iPoly++)
      out.writeInt(firstPointInLineString[iPoly]);
    out.writeBoolean(lastLineStringEnded);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    super.readFields(in);
    numLineStrings = in.readInt();
    ensureLinestringCapacity(numLineStrings);
    for (int iLineString = 0; iLineString < numLineStrings; iLineString++)
      firstPointInLineString[iLineString] = in.readInt();
    firstPointInLineString[numLineStrings] = numPoints;
    lastLineStringEnded = in.readBoolean();
  }

  @Override
  public void toWKT(StringBuilder out) {
    out.append("MULTILINESTRING(");
    int iLineString = -1;
    for (int iPoint = 0; iPoint < numPoints; iPoint++) {
      if (iLineString+1 < numLineStrings && firstPointInLineString[iLineString+1] == iPoint) {
        // First point in linestring
        iLineString++;
        if (iLineString > 0) {
          // Close previous linestring
          out.append(')');
          out.append(',');
        }
        // Open the new linestring
        out.append('(');
      }
      if (iPoint != firstPointInLineString[iLineString])
        out.append(',');
      out.append(xs[iPoint]);
      out.append(' ');
      out.append(ys[iPoint]);
    }
    out.append(')'); // Close the last linestring
    out.append(')'); // Close the Multilinestring
  }

  @Override
  public ByteBuffer toWKB(ByteBuffer out) {
    byte byteOrder = (byte)(out.order() == ByteOrder.LITTLE_ENDIAN ? 1 : 0);
    out.put(byteOrder);
    out.putInt(5);
    out.putInt(numLineStrings);
    int iLineString = -1;
    for (int iPoint = 0; iPoint < numPoints; iPoint++) {
      if (iLineString+2 < numLineStrings && firstPointInLineString[iLineString+1] == iPoint){
        iLineString++;
        out.put(byteOrder);
        out.putInt(2);
        //number of points in the linestring
        out.putInt(firstPointInLineString[iLineString+1]-firstPointInLineString[iLineString]);
      }
      //last linestring
      if (iLineString+2 == numLineStrings && firstPointInLineString[iLineString+1] == iPoint){
        out.put(byteOrder);
        out.putInt(2);
        //number of points in the linestring
        out.putInt(numPoints-firstPointInLineString[iLineString+1]);
      }
      out.putDouble(xs[iPoint]);
      out.putDouble(ys[iPoint]);
    }
    return out;
  }

  /**
   * Returns the index of the first point in the given line string
   * @param n
   * @return
   */
  public int getLineStringStart(int n) {
    return firstPointInLineString[n];
  }

  /**
   * Returns the index after the last point in the given line string
   * @param n
   * @return
   */
  public int getLineStringEnd(int n) {
    return firstPointInLineString[n + 1];
  }

  @Override
  public ILineString getLineStringN(int n) {
    return new InternalLineString(n);
  }

  class InternalLineString implements ILineString {

    final int iLineString;

    InternalLineString(int i) {
      this.iLineString = i;
    }

    @Override
    public Envelope envelope(Envelope envelope) {
      if (envelope == null) {
        envelope = new Envelope(MultiLineString2D.this.getCoordinateDimension());
      } else {
        envelope.setCoordinateDimension(MultiLineString2D.this.getCoordinateDimension());
        envelope.setEmpty();
      }
      for (int iPoint = firstPointInLineString[iLineString]; iPoint < firstPointInLineString[iLineString+1]; iPoint++) {
        envelope.minCoord[0] = Math.min(envelope.minCoord[0], xs[iPoint]);
        envelope.maxCoord[0] = Math.max(envelope.maxCoord[0], xs[iPoint]);
        envelope.minCoord[1] = Math.min(envelope.minCoord[1], ys[iPoint]);
        envelope.maxCoord[1] = Math.max(envelope.maxCoord[1], ys[iPoint]);
      }
      return envelope;
    }

    @Override
    public void setEmpty() {
      throw new RuntimeException("Not supported");
    }

    @Override
    public void toWKT(StringBuilder out) {
      throw new RuntimeException("Not supported");
    }

    @Override
    public ByteBuffer toWKB(ByteBuffer out) {
      throw new RuntimeException("Not supported");
    }

    @Override
    public Point centroid(Point centroid) {
      if (centroid == null) {
        centroid = new Point(MultiLineString2D.this.getCoordinateDimension());
      } else {
        centroid.setCoordinateDimension(MultiLineString2D.this.getCoordinateDimension());
        centroid.setEmpty();
      }
      if (getNumPoints() == 0)
        return centroid;
      double sumx = 0.0, sumy = 0.0;
      for (int iPoint = firstPointInLineString[iLineString]; iPoint < firstPointInLineString[iLineString+1]; iPoint++) {
        sumx += xs[iPoint];
        sumy += ys[iPoint];
      }
      centroid.set(sumx / getNumPoints(), sumy / getNumPoints());
      return centroid;
    }

    @Override
    public int getCoordinateDimension() {
      return MultiLineString2D.this.getCoordinateDimension();
    }

    @Override
    public long getNumPoints() {
      return firstPointInLineString[iLineString+1] - firstPointInLineString[iLineString];
    }

    @Override
    public void getPointN(long n, Point p) {
      MultiLineString2D.this.getPointN(n + firstPointInLineString[iLineString], p);
    }

    @Override
    public void write(DataOutput out) throws IOException {
      // Write this substring as a linestring so that it can be parsed back as a linestring
      out.writeInt((int) this.getNumPoints());
      out.writeBoolean(MultiLineString2D.this.hasMValues());
      for (int iPoint = firstPointInLineString[iLineString]; iPoint < firstPointInLineString[iLineString+1]; iPoint++) {
        out.writeDouble(xs[iPoint]);
        out.writeDouble(ys[iPoint]);
        if (ms != null)
          out.writeDouble(ms[iPoint]);
      }
    }

    @Override
    public void readFields(DataInput in) throws IOException {
      throw new RuntimeException("Not supported");
    }
  }
}
