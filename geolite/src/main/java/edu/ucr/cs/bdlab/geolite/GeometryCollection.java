/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A geometry that internally stores a collection of geometries.
 */
public class GeometryCollection implements IGeometry, Iterable<IGeometry> {

  /**The list of all geometries stored in this geometry collection*/
  public final List<IGeometry> geometries = new ArrayList<>();

  public GeometryCollection() {
  }

  public GeometryCollection(IGeometry ... geoms) {
    for (IGeometry geom : geoms)
      addGeometry(geom);
  }

  @Override
  public Envelope envelope(Envelope envelope) {
    envelope.setEmpty();
    for (IGeometry geom : geometries)
      envelope.merge(geom);
    return envelope;
  }

  @Override
  public boolean intersects(IGeometry other) {
    for (IGeometry geom : geometries)
      if (geom.intersects(other))
        return true;
    return false;
  }

  @Override
  public double getVolume() {
    double area = 0.0;
    for (IGeometry geom : geometries)
      area += geom.getVolume();
    return area;
  }

  @Override
  public GeometryType getType() {
    return GeometryType.GEOMETRYCOLLECTION;
  }

  @Override
  public int getGeometryStorageSize() {
    int totalSize = 4; // Four bytes for storing the number of geometries
    for (IGeometry geom : geometries)
      totalSize += geom.getGeometryStorageSize() + 4; // Four bytes for storing the type of each geometry
    return totalSize;
  }

  @Override
  public void toWKT(StringBuilder out) {
    out.append("GEOMETRYCOLLECTION(");
    boolean first = true;
    for (IGeometry geom : geometries) {
      if (!first)
        out.append(',');
      geom.toWKT(out);
      first = false;
    }
    out.append(')');
  }

  @Override
  public ByteBuffer toWKB(ByteBuffer out) {
    byte byteOrder = (byte)(out.order() == ByteOrder.LITTLE_ENDIAN ? 1 : 0);
    out.put(byteOrder);
    out.putInt(7);
    out.putInt(numGeometries());
    for (IGeometry geom : geometries) {
      geom.toWKB(out);
    }
    return out;
  }

  @Override
  public Point centroid(Point centroid) {
    double sumx = 0;
    double sumy = 0;
    int numPoints = 0;
    for (IGeometry geom : geometries) {
      if (geom.isEmpty())
        continue;
      geom.centroid(centroid);
      sumx += centroid.coords[0] * geom.getNumPoints();
      sumy += centroid.coords[1] * geom.getNumPoints();
      numPoints += geom.getNumPoints();
    }
    centroid.set(sumx / numPoints, sumy / numPoints);
    return centroid;
  }

  protected boolean isMultiPolygon() {
    for (IGeometry geom : geometries) {
      GeometryType type = geom.getType();
      switch (type) {
        case POLYGON:
        case MULTIPOLYGON:
        case EMPTY:
        case ENVELOPE:
          // Do nothing.
          break;
        case POINT:
        case LINESTRING:
          // These are incompatible with MultiPolygon
          return false;
        case GEOMETRYCOLLECTION:
          if (!((GeometryCollection)geom).isMultiPolygon())
            return false;
        default:
          throw new GeometryException(String.format("Unexpected geometry type '%s'", type.toString()));
      }
    }
    return true;
  }

  @Override
  public void writeExternal(ObjectOutput out) throws IOException {
    write(out);
  }

  @Override
  public int getCoordinateDimension() {
    int dim = 0;
    for (IGeometry geom : geometries)
      dim = Math.max(dim, geom.getCoordinateDimension());
    return dim;
  }

  @Override
  public long getNumPoints() {
    long numPoints = 0;
    for  (IGeometry g : geometries)
      numPoints += g.getNumPoints();
    return numPoints;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(geometries.size());
    for (IGeometry geom : geometries) {
      out.writeInt(geom.getType().ordinal());
      geom.write(out);
    }
  }

  @Override
  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    readFields(in);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    geometries.clear();
    int numGeometries = in.readInt();
    while (numGeometries-- > 0) {
      int code = in.readInt();
      GeometryType type = GeometryType.values()[code];
      IGeometry geom = type.createInstance();
      geom.readFields(in);
      geometries.add(geom);
    }
  }

  public void addGeometry(IGeometry poly) {
    geometries.add(poly);
  }

  @Override
  public boolean isEmpty() {
    if (geometries.isEmpty())
      return true;
    for (IGeometry geom : geometries)
      if (!geom.isEmpty())
        return false;
    return true;
  }

  @Override
  public void setEmpty() {
    geometries.clear();
  }

  public int numGeometries() {
    return geometries.size();
  }

  public IGeometry getGeometry(int i) {
    return geometries.get(i);
  }

  public void setGeometry(int i, IGeometry g) {
    this.geometries.set(i, g);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof GeometryCollection))
      return false;
    GeometryCollection gCollection = (GeometryCollection) obj;
    if (this.numGeometries() != gCollection.numGeometries())
      return false;
    for (int i = 0; i < numGeometries(); i++)
      if (!getGeometry(i).equals(gCollection.getGeometry(i)))
        return false;
    return true;
  }

  @Override
  public String toString() {
    return asText();
  }

  @Override
  public Iterator<IGeometry> iterator() {
    return new GeometryIterator();
  }

  class GeometryIterator implements Iterator<IGeometry> {
    int i = -1;

    @Override
    public boolean hasNext() {
      return (i+1) < numGeometries();
    }

    @Override
    public IGeometry next() {
      return getGeometry(++i);
    }
  }
}
