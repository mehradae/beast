package edu.ucr.cs.bdlab.geolite;

import junit.framework.TestCase;

public class PointTest extends TestCase {

  public void testSetEmptyWithZeroCoordinates() {
    Point p = new Point();
    p.setCoordinateDimension(0);
    p.setEmpty();

    p = new Point(0);
    p.setEmpty();
  }
}