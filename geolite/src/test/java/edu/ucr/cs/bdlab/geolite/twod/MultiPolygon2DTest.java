package edu.ucr.cs.bdlab.geolite.twod;

import edu.ucr.cs.bdlab.geolite.ILineString;
import edu.ucr.cs.bdlab.geolite.Point;
import junit.framework.TestCase;

public class MultiPolygon2DTest extends TestCase {
  public void testCreateSimple() {
    MultiPolygon2D polygon = new MultiPolygon2D();
    polygon.addPointXYM(0.0, 0.0, 3.0);
    polygon.addPointXYM(1.0, 0.0, 3.0);
    polygon.addPointXYM(1.0, 1.0, 3.0);
    polygon.addPointXYM(0.0, 0.0, 3.0);
    polygon.closeLastRing(true);
  }

  public void testCreateFromPolygons() {
    Polygon2D p1, p2;
    p1 = new Polygon2D();
    p1.addPoint(1.0, 1.0);
    p1.addPoint(9.0, 3.0);
    p1.addPoint(3.0, 5.0);
    p1.closeLastRing(false);

    p2 = new Polygon2D();
    p2.addPoint(12.0, 3.0);
    p2.addPoint(18.0, 5.0);
    p2.addPoint(15.0, 7.0);
    p2.closeLastRing(false);

    MultiPolygon2D mp = new MultiPolygon2D(p1, p2);
    int numPoints = 0;
    for (int iRing = 0; iRing < mp.getNumRings(); iRing++) {
      numPoints += mp.getRingN(iRing).getNumPoints();
    }
    assertEquals(8, numPoints);
  }

  public void testGetPolygon() {
    Polygon2D p1, p2;
    p1 = new Polygon2D();
    p1.addPoint(1.0, 1.0);
    p1.addPoint(9.0, 3.0);
    p1.addPoint(3.0, 5.0);
    p1.closeLastRing(false);

    MultiPolygon2D mp = new MultiPolygon2D(p1);
    Polygon2D poly1 = new Polygon2D();
    mp.getPolygonN(0, poly1);
    assertEquals(p1.getRingN(0).getNumPoints(), poly1.getRingN(0).getNumPoints());
  }


  public void testGetPolygonWithTwoPolygons() {
    Polygon2D p1, p2;
    p1 = new Polygon2D();
    for (int i = 0; i < 10; i++) {
      p1.addPoint(1.0, 1.0);
      p1.addPoint(9.0, 3.0);
      p1.addPoint(3.0, 5.0);
      p1.closeLastRing(false);
    }

    p2 = new Polygon2D();
    p2.addPoint(12.0, 3.0);
    p2.addPoint(18.0, 5.0);
    p2.addPoint(15.0, 7.0);
    p2.closeLastRing(false);
    p2.addPoint(12.0, 13.0);
    p2.addPoint(18.0, 15.0);
    p2.addPoint(15.0, 17.0);
    p2.closeLastRing(false);

    MultiPolygon2D mp = new MultiPolygon2D(p1, p2);
    Polygon2D poly2 = new Polygon2D();
    mp.getPolygonN(1, poly2);
    ILineString expectedRing = p2.getRingN(1);
    ILineString actualRing = poly2.getRingN(1);
    assertEquals(expectedRing.getNumPoints(), actualRing.getNumPoints());
    Point expectedPoint = new Point(2);
    Point actualPoint = new Point(2);
    for (int i = 0; i < expectedRing.getNumPoints(); i++) {
      expectedRing.getPointN(i, expectedPoint);
      actualRing.getPointN(i, actualPoint);
      assertEquals(expectedPoint, actualPoint);
    }
  }

  public void testToWKTWithTwoPolygons() {
    Polygon2D p1 = new Polygon2D();
    p1.addPoint(40,40);
    p1.addPoint(20,45);
    p1.addPoint(45,30);
    p1.addPoint(40,40);
    p1.closeLastRing(true);
    Polygon2D p2 = new Polygon2D();
    p2.addPoint(20,35);
    p2.addPoint(10,30);
    p2.addPoint(10,10);
    p2.addPoint(30,5);
    p2.addPoint(40,25);
    p2.addPoint(30,5);
    p2.addPoint(20,35);
    p2.closeLastRing(true);
    p2.addPoint(30,20);
    p2.addPoint(20,15);
    p2.addPoint(20,25);
    p2.addPoint(30,20);
    p2.closeLastRing(true);
    MultiPolygon2D multiPolygon2D = new MultiPolygon2D();
    multiPolygon2D.addPolygon(p1);
    multiPolygon2D.addPolygon(p2);

    String expectedWKT = "MULTIPOLYGON(((40.0 40.0,20.0 45.0,45.0 30.0,40.0 40.0))," +
        "((20.0 35.0,10.0 30.0,10.0 10.0,30.0 5.0,40.0 25.0,30.0 5.0,20.0 35.0)," +
        "(30.0 20.0,20.0 15.0,20.0 25.0,30.0 20.0)))";
    String actualWKT = multiPolygon2D.asText();
    assertEquals(expectedWKT, actualWKT);
  }
}