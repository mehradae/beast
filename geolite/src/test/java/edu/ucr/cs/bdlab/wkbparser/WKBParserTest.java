/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucr.cs.bdlab.wkbparser;

import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import org.junit.Test;

import java.io.IOException;
import java.nio.ByteBuffer;

import static org.junit.Assert.*;

public class WKBParserTest {

    @Test public void testParsePoints() throws IOException {
        WKBParser wkb = new WKBParser();
        ByteBuffer out = ByteBuffer.allocate(1024);
        double[] coors = { 12.3, 45.6 };
        Point expectedPoint = new Point(coors);
        ByteBuffer pWKB = expectedPoint.toWKB(out);
        IGeometry actualPoint = wkb.parse(pWKB, null);
        assertEquals(expectedPoint, actualPoint);
    }

    @Test public void testParseMultipleGeometries() throws IOException {
        WKBParser wkb = new WKBParser();
        ByteBuffer out = ByteBuffer.allocate(1024);
        double[] coors = { 12.3, 45.6 };
        Point expectedPoint = new Point(coors);
        ByteBuffer pWKB = expectedPoint.toWKB(out);
        IGeometry actualPoint = wkb.parse(pWKB, null);
        assertEquals(expectedPoint, actualPoint);

        out = ByteBuffer.allocate(1024);
        LineString2D expectedLineString = new LineString2D();
        expectedLineString.addPoint(23.4, 34.5);
        expectedLineString.addPoint(1.23, 4.56);
        expectedLineString.addPoint(344.5, 691.8);
        ByteBuffer lWKB = expectedLineString.toWKB(out);
        IGeometry actualLineString = wkb.parse(lWKB, null);
        assertEquals(expectedLineString, actualLineString);
    }

    @Test public void testParseLineString() throws IOException {
        WKBParser wkb = new WKBParser();
        ByteBuffer out = ByteBuffer.allocate(1024);
        LineString2D expectedLineString = new LineString2D();
        expectedLineString.addPoint(23.4, 34.5);
        expectedLineString.addPoint(1.23, 4.56);
        expectedLineString.addPoint(344.5, 691.8);
        ByteBuffer lWKB = expectedLineString.toWKB(out);
        IGeometry actualLineString = wkb.parse(lWKB, null);
        assertEquals(expectedLineString, actualLineString);
    }

    @Test public void testParsePolygon() throws IOException {
        WKBParser wkb = new WKBParser();
        ByteBuffer out = ByteBuffer.allocate(1024);
        Polygon2D expectedPolygon = new Polygon2D();
        expectedPolygon.addPoint(12.3, 23.5);
        expectedPolygon.addPoint(122.6, 256.5);
        expectedPolygon.addPoint(3.45, 9.37);
        expectedPolygon.addPoint(12.3, 23.5);
        expectedPolygon.closeLastRingNoCheck();
        expectedPolygon.addPoint(23.4, 34.5);
        expectedPolygon.addPoint(1.23, 4.56);
        expectedPolygon.addPoint(344.5, 691.8);
        expectedPolygon.addPoint(23.4, 34.5);
        expectedPolygon.closeLastRingNoCheck();
        ByteBuffer pWKB = expectedPolygon.toWKB(out);
        IGeometry actualPolygon = wkb.parse(pWKB, null);
        assertEquals(expectedPolygon, actualPolygon);
    }

    @Test public void testParseMultiLineString() throws IOException {
        WKBParser wkb = new WKBParser();
        ByteBuffer out = ByteBuffer.allocate(1024);
        MultiLineString2D expectedMultiLineString = new MultiLineString2D();
        expectedMultiLineString.addPoint(12.3, 23.5);
        expectedMultiLineString.addPoint(122.6, 256.5);
        expectedMultiLineString.endCurrentLineString();
        expectedMultiLineString.addPoint(162.9, 245.5);
        expectedMultiLineString.addPoint(74.3, 28.5);
        expectedMultiLineString.endCurrentLineString();
        ByteBuffer lWKB = expectedMultiLineString.toWKB(out);
        IGeometry actualMultiLineString = wkb.parse(lWKB, null);
        assertEquals(expectedMultiLineString, actualMultiLineString);
    }

    @Test public void testParseMultiPolygon() throws IOException {
        WKBParser wkb = new WKBParser();
        ByteBuffer out = ByteBuffer.allocate(1024);
        Polygon2D polygon1 = new Polygon2D();
        polygon1.addPoint(12.3, 23.5);
        polygon1.addPoint(122.6, 256.5);
        polygon1.addPoint(3.45, 9.37);
        polygon1.addPoint(12.3, 23.5);
        polygon1.closeLastRingNoCheck();
        polygon1.addPoint(23.4, 34.5);
        polygon1.addPoint(1.23, 4.56);
        polygon1.addPoint(344.5, 691.8);
        polygon1.addPoint(23.4, 34.5);
        polygon1.closeLastRingNoCheck();
        Polygon2D polygon2 = new Polygon2D();
        polygon2.addPoint(33.6, 98.5);
        polygon2.addPoint(132.8, 974.2);
        polygon2.addPoint(4.78, 8.36);
        polygon2.addPoint(33.6, 98.5);
        polygon2.closeLastRingNoCheck();
        MultiPolygon2D expectedMultiPolygon = new MultiPolygon2D();
        expectedMultiPolygon.addPolygon(polygon1);
        expectedMultiPolygon.addPolygon(polygon2);
        ByteBuffer pWKB = expectedMultiPolygon.toWKB(out);
        IGeometry actualMultiPolygon = wkb.parse(pWKB, null);
        assertEquals(expectedMultiPolygon, actualMultiPolygon);
    }

    @Test public void testParseGeometryCollection() throws IOException {
        WKBParser wkb = new WKBParser();
        ByteBuffer out = ByteBuffer.allocate(1024);
        double[] coors = { 12.3, 45.6 };
        Point point = new Point(coors);
        LineString2D lineString = new LineString2D();
        lineString.addPoint(23.4, 34.5);
        lineString.addPoint(1.23, 4.56);
        lineString.addPoint(344.5, 691.8);
        Polygon2D polygon = new Polygon2D();
        polygon.addPoint(33.6, 98.5);
        polygon.addPoint(132.8, 974.2);
        polygon.addPoint(4.78, 8.36);
        polygon.addPoint(33.6, 98.5);
        polygon.closeLastRingNoCheck();
        MultiLineString2D multiLineString = new MultiLineString2D();
        multiLineString.addPoint(12.3, 23.5);
        multiLineString.addPoint(122.6, 256.5);
        multiLineString.endCurrentLineString();
        multiLineString.addPoint(162.9, 245.5);
        multiLineString.addPoint(74.3, 28.5);
        multiLineString.endCurrentLineString();
        GeometryCollection expectedGeometryCollection = new GeometryCollection();
        expectedGeometryCollection.addGeometry(point);
        expectedGeometryCollection.addGeometry(lineString);
        expectedGeometryCollection.addGeometry(polygon);
        expectedGeometryCollection.addGeometry(multiLineString);
        ByteBuffer gWKB = expectedGeometryCollection.toWKB(out);
        IGeometry actualGeometryCollection = wkb.parse(gWKB, null);
        assertEquals(expectedGeometryCollection, actualGeometryCollection);
    }
}