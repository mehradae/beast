package edu.ucr.cs.bdlab.geolite.twod;

import edu.ucr.cs.bdlab.geolite.ILineString;
import edu.ucr.cs.bdlab.geolite.Point;
import junit.framework.TestCase;

public class Polygon2DTest extends TestCase {
  public void testCreateSimple() {
    Polygon2D polygon = new Polygon2D();
    polygon.addPointXYM(0.0, 0.0, 3.0);
    polygon.addPointXYM(1.0, 0.0, 3.0);
    polygon.addPointXYM(1.0, 1.0, 3.0);
    polygon.addPointXYM(0.0, 0.0, 3.0);
    polygon.closeLastRing(true);
  }

  public void testPointInPolygon() {
    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(0.0, 0.0);
    polygon.addPoint(1.0, 0.0);
    polygon.addPoint(1.0, 1.0);
    polygon.addPoint(0.0, 0.0);
    polygon.closeLastRing(true);

    assertTrue(polygon.containsPoint(new Point(0.5, 0.25)));
    assertFalse(polygon.containsPoint(new Point(0.0, 0.25)));
    assertFalse(polygon.containsPoint(new Point(1.5, 0.25)));
    assertFalse(polygon.containsPoint(new Point(0.0, 1.0)));
    assertFalse(polygon.containsPoint(new Point(0.5, -0.25)));
  }

  public void testReversedPolygonPointInPolygon() {
    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(0.0, 0.0);
    polygon.addPoint(1.0, 1.0);
    polygon.addPoint(1.0, 0.0);
    polygon.addPoint(0.0, 0.0);
    polygon.closeLastRing(true);

    assertTrue(polygon.containsPoint(new Point(0.5, 0.25)));
    assertFalse(polygon.containsPoint(new Point(0.0, 0.25)));
    assertFalse(polygon.containsPoint(new Point(1.5, 0.25)));
    assertFalse(polygon.containsPoint(new Point(0.0, 1.0)));
    assertFalse(polygon.containsPoint(new Point(0.5, -0.25)));
  }

  public void testPointInPolygonWithHoles() {
    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(0.0, 0.0);
    polygon.addPoint(1.0, 1.0);
    polygon.addPoint(1.0, 0.0);
    polygon.addPoint(0.0, 0.0);
    polygon.closeLastRing(true);
    polygon.addPoint(0.5, 0.0);
    polygon.addPoint(0.5, 0.5);
    polygon.addPoint(1.0, 0.5);
    polygon.addPoint(0.5, 0.0);
    polygon.closeLastRing(true);

    assertTrue(polygon.containsPoint(new Point(0.75, 0.125)));
    assertFalse(polygon.containsPoint(new Point(0.0, 0.25)));
    assertFalse(polygon.containsPoint(new Point(1.5, 0.25)));
    assertFalse(polygon.containsPoint(new Point(0.0, 1.0)));
    assertFalse(polygon.containsPoint(new Point(0.5, -0.25)));
  }

  public void testGetRingWhenClosedWithNoCheck() {
    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(0, 0);
    polygon.addPoint(1, 1);
    polygon.addPoint(0, 1);
    polygon.addPoint(0.1, 0.1);
    polygon.closeLastRingNoCheck();

    int numPoints = 0;
    for (int iRing = 0; iRing < polygon.getNumRings(); iRing++) {
      ILineString ring = polygon.getRingN(iRing);
      numPoints += ring.getNumPoints();
    }
    assertEquals(4, numPoints);
  }

  public void testEmptyPolygonWKT() {
    Polygon2D polygon = new Polygon2D();
    assertEquals("POLYGON EMPTY", polygon.asText());
  }
}