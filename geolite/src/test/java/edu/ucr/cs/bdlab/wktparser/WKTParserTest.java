/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.wktparser;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.GeometryException;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import junit.framework.TestCase;

import java.io.IOException;

public class WKTParserTest extends TestCase {

  public void testParsePoints() throws IOException, ParseException {
    WKTParser parser = new WKTParser();
    String pointWKT = "POINT(1 2)";
    Point expectedOutput = new Point(2,1,2);

    IGeometry actualOutput = parser.parse(pointWKT, null);
    assertEquals(expectedOutput, actualOutput);

    pointWKT = "Point(1 2)";
    IGeometry mutableObject = actualOutput;
    actualOutput = parser.parse(pointWKT, actualOutput);
    assertTrue(mutableObject == actualOutput);
    assertEquals(expectedOutput, actualOutput);

    // Test negative numbers
    pointWKT = "Point(-1 +2)";
    actualOutput = parser.parse(pointWKT, actualOutput);
    expectedOutput.set(-1, 2);
    assertEquals(expectedOutput, actualOutput);
  }

  public void testParseLinestring() throws IOException, ParseException {
    WKTParser parser = new WKTParser();
    String linestringWKT = "LINESTRING(0 0, 2 1, 0 2)";
    LineString2D expectedOutput = new LineString2D();
    expectedOutput.addPoint(0, 0);
    expectedOutput.addPoint(2, 1);
    expectedOutput.addPoint(0, 2);

    IGeometry actualOutput = parser.parse(linestringWKT, null);
    assertEquals(expectedOutput, actualOutput);
  }

  public void testShouldReuseObjects() throws IOException, ParseException {
    WKTParser parser = new WKTParser();
    String linestringWKT = "POLYGON((0 0, 2 1, 0 2, 0 0))";
    Polygon2D expectedOutput = new Polygon2D();
    expectedOutput.addPoint(0, 0);
    expectedOutput.addPoint(2, 1);
    expectedOutput.addPoint(0, 2);
    expectedOutput.addPoint(0, 0);
    expectedOutput.closeLastRing(true);

    IGeometry actualOutput = parser.parse(linestringWKT, null);
    assertEquals(expectedOutput, actualOutput);

    expectedOutput.addPoint(0.5, 0.5);
    expectedOutput.addPoint(1.5, 0.5);
    expectedOutput.addPoint(0.5, 1.5);
    expectedOutput.addPoint(0.5, 0.5);
    expectedOutput.closeLastRing(true);

    linestringWKT = "POLYGON((0 0, 2 1, 0 2, 0 0), (0.5 0.5, 1.5 0.5, 0.5 1.5, 0.5 0.5))";
    actualOutput = parser.parse(linestringWKT, actualOutput);
    assertEquals(expectedOutput, actualOutput);
  }

  public void testParsePolygon() throws IOException, ParseException {
    WKTParser parser = new WKTParser();
    String polygonWKT = "POLYGON((0 0, 2 1, 0 2, 0 0))";
    Polygon2D expectedOutput = new Polygon2D();
    expectedOutput.addPoint(0, 0);
    expectedOutput.addPoint(2, 1);
    expectedOutput.addPoint(0, 2);
    expectedOutput.closeLastRing(false);

    IGeometry actualOutput = parser.parse(polygonWKT, null);
    assertEquals(expectedOutput, actualOutput);

    expectedOutput.addPoint(0.5, 0.5);
    expectedOutput.addPoint(1.5, 0.5);
    expectedOutput.addPoint(0.5, 1.5);
    expectedOutput.closeLastRing(false);

    polygonWKT = "POLYGON((0 0, 2 1, 0 2, 0 0), (0.5 0.5, 1.5 0.5, 0.5 1.5, 0.5 0.5))";
    actualOutput = parser.parse(polygonWKT, null);
    assertEquals(expectedOutput, actualOutput);

    // Test a non-closed polygon
    polygonWKT = "POLYGON((0 0, 2 1, 0 2))";
    boolean exceptionRaised = false;
    try {
      parser.parse(polygonWKT, null);
    } catch (GeometryException e) {
      exceptionRaised = true;
    }
    assertTrue("Should raise a parse exception with a non-closed polygon", exceptionRaised);

    polygonWKT = "POLYGON ((-42.0089798 -20.8096542, -42.0089798 -20.8096542, -42.008522 -20.8099747, " +
        "-42.0081367 -20.809494, -42.0081406 -20.8094177, -42.0085602 -20.8091316, -42.0089798 -20.8096542))";
    actualOutput = parser.parse(polygonWKT, null);
    expectedOutput.setEmpty();
    expectedOutput.addPoint(-42.0089798, -20.8096542);
    expectedOutput.addPoint(-42.0089798, -20.8096542);
    expectedOutput.addPoint(-42.008522, -20.8099747);
    expectedOutput.addPoint(-42.0081367, -20.809494);
    expectedOutput.addPoint(-42.0081406, -20.8094177);
    expectedOutput.addPoint(-42.0085602, -20.8091316);
    expectedOutput.addPoint(-42.0089798, -20.8096542);
    expectedOutput.closeLastRing(true);
    assertEquals(expectedOutput, actualOutput);
  }

  public void testParseEmptyGeometries() throws IOException, ParseException {
    WKTParser parser = new WKTParser();
    assertEquals(EmptyGeometry.instance, parser.parse("EMPTY", null));
    assertEquals(EmptyGeometry.instance, parser.parse("", null));
    IGeometry emptyPolygon = parser.parse("POLYGON EMPTY", null);
    assertTrue("Should be a polygon", emptyPolygon instanceof Polygon2D);
    assertTrue("Should be an empty polygon", emptyPolygon.isEmpty());
    IGeometry emptyLinestring = parser.parse("LINESTRING EMPTY", null);
    assertTrue("Should be a linestring", emptyLinestring instanceof LineString2D);
    assertTrue("Should be empty", emptyLinestring.isEmpty());
    IGeometry emptyMultipolygon = parser.parse("MULTIPOLYGON EMPTY", null);
    assertTrue("Should be a multipolygon", emptyMultipolygon instanceof MultiPolygon2D);
    assertTrue("Should be empty", emptyMultipolygon.isEmpty());
    IGeometry emptyMultilinestring = parser.parse("MULTILINESTRING EMPTY", null);
    assertTrue("Should be a multilinestring", emptyMultilinestring instanceof MultiLineString2D);
    assertTrue("Should be empty", emptyMultilinestring.isEmpty());
    IGeometry emptyCollection = parser.parse("GEOMETRYCOLLECTION EMPTY", null);
    assertTrue("Should be a geometry collection", emptyCollection instanceof GeometryCollection);
    assertTrue("Should be empty", emptyCollection.isEmpty());
  }

  public void testParseMultiPolygon() throws IOException, ParseException {
    WKTParser parser = new WKTParser();
    String multipolygonWKT = "MultiPolygon(((0 0, 2 1, 0 2, 0 0)), ((10 7, 12 8, 12 10, 10 7)))";

    MultiPolygon2D mpoly = new MultiPolygon2D();
    mpoly.addPoint(0, 0);
    mpoly.addPoint(2, 1);
    mpoly.addPoint(0, 2);
    mpoly.addPoint(0, 0);
    mpoly.closeLastRing(true);
    mpoly.endCurrentPolygon();
    mpoly.addPoint(10, 7);
    mpoly.addPoint(12, 8);
    mpoly.addPoint(12, 10);
    mpoly.addPoint(10, 7);
    mpoly.closeLastRing(true);
    mpoly.endCurrentPolygon();

    IGeometry actualOutput = parser.parse(multipolygonWKT, null);
    assertEquals(mpoly, actualOutput);
  }

  public void testParseGeometryCollection() throws IOException, ParseException {
    WKTParser parser = new WKTParser();
    String geomCollectionWKT = "GeometryCollection(POLYGON((0 0, 2 1, 0 2, 0 0)), POINT(10 7))";

    GeometryCollection expectedOutput = new GeometryCollection();
    Polygon2D poly = new Polygon2D();
    poly.addPoint(0, 0);
    poly.addPoint(2, 1);
    poly.addPoint(0, 2);
    poly.addPoint(0, 0);
    poly.closeLastRing(true);
    expectedOutput.addGeometry(poly);
    expectedOutput.addGeometry(new Point(2, 10.0, 7.0));

    IGeometry actualOutput = parser.parse(geomCollectionWKT, null);
    assertEquals(expectedOutput, actualOutput);
  }

  public void testToWKT() throws IOException {
    WKTParser parser = new WKTParser();
    String[] testData = {
        "POINT(1 3)",
        "LINESTRING(0 1, 5 5, 7 2)",
        "POLYGON((0 1, 5 5, 7 2, 0 1))",
        "MULTIPOLYGON(((0 1, 5 5, 7 2, 0 1),(1 2, 3 3, 3 4, 1 2)),((4 4, 7 6, 10 20, 4 4)))",
        "MULTILINESTRING((0 1, 5 5, 7 2),(1 2, 3 3, 3 4),(4 4, 7 6, 10 20, 4 4))",
        "GEOMETRYCOLLECTION(POINT(0 1), LINESTRING(5 5, 7 2, 0 1))",
    };
    for (String wkt : testData) {
      try {
        IGeometry geom = parser.parse(wkt, null);
        StringBuilder wkt2 = new StringBuilder();
        geom.toWKT(wkt2);

        try {
          IGeometry geom2 = parser.parse(wkt2.toString(), null);
          assertEquals(String.format("Error with the shape '%s'", wkt), geom, geom2);
        } catch (ParseException e) {
          e.printStackTrace();
          fail(String.format("Error parsing '%s' generated from the shape '%s'", wkt2.toString(), wkt));
        }
      } catch (ParseException e) {
        e.printStackTrace();
        fail(String.format("Error while parsing '%s'", wkt));
      }
    }
  }

  public void testReusePointMismatchedDimensions() throws IOException, ParseException {
    WKTParser wkt = new WKTParser();
    Point p = new Point();
    String str1 = "POINT (30 10)";
    wkt.parse(str1,p);
  }

  public void testEmptyPolygon() throws IOException, ParseException {
    WKTParser parser = new WKTParser();
    String polygonWKT = "POLYGON()";
    IGeometry polygon = parser.parse(polygonWKT, null);
    assertEquals(GeometryType.POLYGON, polygon.getType());
    assertTrue("Polygon should be empty", polygon.isEmpty());
  }
}
