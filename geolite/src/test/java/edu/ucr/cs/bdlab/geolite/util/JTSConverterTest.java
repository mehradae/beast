package edu.ucr.cs.bdlab.geolite.util;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;
import junit.framework.TestCase;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.io.WKTReader;

import java.io.IOException;

public class JTSConverterTest extends TestCase {

  public void testCreateJTSGeometry() throws IOException, ParseException, org.locationtech.jts.io.ParseException {
    String[] wkts = {
        "POINT (1.0 2.0)",
        "LINESTRING (1.0 3.0, 2.5 5.0)",
        "POLYGON ((2.0 3.0, 2.0 7.0, 5.0 8.0, 2.0 3.0))",
        "MULTILINESTRING ((1.0 3.0, 2.5 5.0), (11.0 3.0, 12.5 5.0))",
        "MULTIPOLYGON (((2.0 3.0, 2.0 7.0, 5.0 8.0, 2.0 3.0)), ((12.0 3.0, 12.0 7.0, 15.0 8.0, 12.0 3.0)))",
    };
    WKTParser wktParser = new WKTParser();
    for (String wkt : wkts) {
      IGeometry expectedGeom = wktParser.parse(wkt, null);
      Geometry JTSGeom = JTSConverter.createJTSGeometry(expectedGeom);
      Geometry expectedJTSGeom = new WKTReader().read(wkt);
      assertEquals(expectedJTSGeom, JTSGeom);
    }
  }

  public void testCreateJTSGeometryFromEmpty() {
    IGeometry[] geometries = {
        new Point(),
        new Envelope(),
        new LineString2D(),
        new MultiLineString2D(),
        new Polygon2D(),
        new MultiPolygon2D()
    };
    for (IGeometry geometry : geometries) {
      Geometry jtsGeom = JTSConverter.createJTSGeometry(geometry);
      assertTrue(String.format("Geometry '%s' should be empty", geometry), jtsGeom.isEmpty());
    }
  }

  public void testFromJTSGeometryPoint() {
    GeometryFactory factory = new GeometryFactory();
    org.locationtech.jts.geom.Point JTSPoint = factory.createPoint(new Coordinate(123.0, 555.0));
    Point litePoint = (Point) JTSConverter.fromJTSGeometry(JTSPoint, null);
    assertNotNull(litePoint);
    assertEquals(2, litePoint.getCoordinateDimension());
    assertEquals(123.0, litePoint.coords[0], 1E-3);
    assertEquals(555.0, litePoint.coords[1], 1E-3);
  }

  public void testFromJTSGeometryMixed() throws IOException, ParseException, org.locationtech.jts.io.ParseException {
    String[] wkts = {
        "POINT (1.0 2.0)",
        "LINESTRING (1.0 3.0, 2.5 5.0)",
        "POLYGON ((2.0 3.0, 5.0 8.0, 2.0 7.0, 2.0 3.0))",
        "MULTILINESTRING ((1.0 3.0, 2.5 5.0), (11.0 3.0, 12.5 5.0))",
        "MULTIPOLYGON (((2.0 3.0, 5.0 8.0, 2.0 7.0, 2.0 3.0)), ((12.0 3.0, 15.0 8.0, 12.0 7.0, 12.0 3.0)))",
    };
    for (String wkt : wkts) {
      IGeometry expectedGeom = new WKTParser().parse(wkt, null);
      IGeometry actualGeom = JTSConverter.fromJTSGeometry(new WKTReader().read(wkt), null);
      assertEquals(expectedGeom, actualGeom);
    }
  }
}