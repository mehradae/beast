/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite.util;

import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.util.Fundamentals;
import junit.framework.TestCase;

public class FundamentalsTest extends TestCase {

  public void testRelate() {
    Point p1 = new Point(2, 0.0, 0.0);
    Point p2 = new Point(2, 2.0, 1.0);
    Point p3 = new Point(2, 4.0, 2.0);
    Point p4 = new Point(2, 2.0, 0.0);
    Point p5 = new Point(2, 0.0, 3.0);

    assertTrue(Fundamentals.relate(p1.coords[0], p1.coords[1], p2.coords[0], p2.coords[1], p3.coords[0], p3.coords[1]) == 0);
    assertTrue(Fundamentals.relate(p1.coords[0], p1.coords[1], p2.coords[0], p2.coords[1], p4.coords[0], p4.coords[1]) < 0);
    assertTrue(Fundamentals.relate(p1.coords[0], p1.coords[1], p2.coords[0], p2.coords[1], p5.coords[0], p5.coords[1]) > 0);
  }

  public void testPointOnLineSegment() {
    Point p1 = new Point(2, 0.0, 0.0);
    Point p2 = new Point(2, 2.0, 1.0);
    Point p3 = new Point(2, 4.0,2.0);
    Point p4 = new Point(2, 2.0,0.0);
    Point p5 = new Point(2, 1.0,0.5);
    assertFalse(Fundamentals.pointOnLineSegment(p1.coords[0], p1.coords[1], p2.coords[0], p2.coords[1], p3.coords[0], p3.coords[1]));
    assertFalse(Fundamentals.pointOnLineSegment(p1.coords[0], p1.coords[1], p2.coords[0], p2.coords[1], p4.coords[0], p4.coords[1]));
    assertTrue( Fundamentals.pointOnLineSegment(p1.coords[0], p1.coords[1], p2.coords[0], p2.coords[1], p5.coords[0], p5.coords[1]));
    assertTrue( Fundamentals.pointOnLineSegment(p2.coords[0], p2.coords[1], p1.coords[0], p1.coords[1], p5.coords[0], p5.coords[1]));
  }

  public void testLineSegmentOverlap() {
    Point p1 = new Point(2, 0.0, 0.0);
    Point p2 = new Point(2, 2.0, 1.0);
    Point p3 = new Point(2, 2.0, 0.0);
    Point p4 = new Point(2, 0.0, 5.0);
    assertTrue(Fundamentals.lineSegmentOverlap(p1.coords[0], p1.coords[1], p2.coords[0], p2.coords[1], p3.coords[0], p3.coords[1], p4.coords[0], p4.coords[1]));
    assertFalse(Fundamentals.lineSegmentOverlap(p1.coords[0], p1.coords[1], p3.coords[0], p3.coords[1], p2.coords[0], p2.coords[1], p4.coords[0], p4.coords[1]));
  }

  public void testTriangleArea() {
    Point p1 = new Point(2, 0.0, 0.0);
    Point p2 = new Point(2, 2.0, 0.0);
    Point p3 = new Point(2, 0.0, 2.0);
    assertEquals(2.0, Fundamentals.absTriangleArea(p1.coords[0], p1.coords[1], p2.coords[0], p2.coords[1], p3.coords[0], p3.coords[1]));
    assertEquals(2.0, Fundamentals.signedTriangleArea(p1.coords[0], p1.coords[1], p2.coords[0], p2.coords[1], p3.coords[0], p3.coords[1]));
    assertEquals(-2.0, Fundamentals.signedTriangleArea(p1.coords[0], p1.coords[1], p3.coords[0], p3.coords[1], p2.coords[0], p2.coords[1]));
  }
}