/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import junit.framework.TestCase;
import org.junit.Test;

public class PolygonTest extends TestCase {

  @Test
  public void testCreation() {
    Polygon2D polygon = new Polygon2D();
    // Create an outer shell
    polygon.addPoint(0, 0);
    polygon.addPoint(4, 0);
    polygon.addPoint(0, 4);
    polygon.addPoint(0, 0);
    polygon.closeLastRing(true);
    assertEquals(0, polygon.getNumInteriorRings());
    assertEquals(3, polygon.getNumPoints());
    assertTrue(polygon.isClosed());
    // Add an interior ring
    polygon.addPoint(1, 1);
    polygon.addPoint(2, 1);
    polygon.addPoint(1, 2);
    polygon.addPoint(1, 1);
    polygon.closeLastRing(true);
    assertEquals(1, polygon.getNumInteriorRings());
    assertTrue(polygon.isClosed());
  }

  @Test
  public void testArea() {
    Polygon2D polygon = new Polygon2D();
    // Create an outer shell
    polygon.addPoint(0, 0);
    polygon.addPoint(4, 0);
    polygon.addPoint(0, 4);
    polygon.addPoint(0, 0);
    polygon.closeLastRing(true);
    assertEquals(8.0, polygon.getVolume());

    // Add an interior ring
    polygon.addPoint(1, 1);
    polygon.addPoint(2, 1);
    polygon.addPoint(1, 2);
    polygon.addPoint(1, 1);
    polygon.closeLastRing(true);
    assertEquals(7.5, polygon.getVolume());

    polygon = new Polygon2D();
    // Create an outer shell in CW order
    polygon.addPoint(0, 0);
    polygon.addPoint(0, 4);
    polygon.addPoint(4, 0);
    polygon.addPoint(0, 0);
    polygon.closeLastRing(true);
    assertEquals(8.0, polygon.getVolume());
  }
}