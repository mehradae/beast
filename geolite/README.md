GeoLite
=======

GeoLite is a light-weight computational geometry library designed specifically
for big spatial data analytics. It follows the
[OGC standard](https://www.opengeospatial.org/) for [Simple Feature Access](https://www.opengeospatial.org/standards/sfa).
It is also compatible with the [Esri Geometry API](https://github.com/Esri/geometry-api-java)
so you can convert GeoLite geometries to Esri geometries and use it
with other Esri operations.

Main Features
-------------

* Compact memory representation for memory-intensive systems such as Spark.
* Mutable design that allows object reuse for disk-resident processing systems
such as Hadoop and AsterixDB.
* Compatible with common standards such as [WKT](https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry)
and [GeoJSON](https://geojson.org/) .

Setup
-----

Add GeoLite dependency to your existing Java or Scala project.

### Java/Maven

    <dependency>
      <groupId>edu.ucr.cs.bdlab</groupId>
      <artifactId>geolite</artifactId>
      <version>0.2.1</version>
    </dependency>

### Scala/SBT

    libraryDependencies += "edu.ucr.cs.bdlab" % "geolite" % "0.1.0-RC2"

Examples
--------

Create a 2D point object initialized to the coordinates (1.3, 3.5).

    Point pt = new Point(1.3, 3.5);
    
Create a 3D point object initialized to the coordinates (1.3, 3.5, 5.7).

    Point pt = new Point(1.3, 3.5, 5.7);

Create a 2D envelope (MBR) with the two corner points located at (2.3, 3.4) and (5.8, 10.5).

    Envelope envelope = new Envelope(2, 2.3, 3.4, 5.8, 10.5);

Create a MultiPoint.

    MultiPoint mpoint = new MultiPoint();
    mpoint.add(1.3, 4.5);
    mpoint.add(3.0, 5.1);
    mpoint.add(4.2, 3.9);

Create a 2D linestring and add three points to it.

    LineString2D linestring = new LineString2D();
    lineString.addPoint(1.3, 4.5);
    lineString.addPoint(3.0, 5.1);
    lineString.addPoint(4.2, 3.9);
    
Create a 2D polygon and add three points to it.

    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(1.3, 4.5);
    polygon.addPoint(3.0, 5.1);
    polygon.addPoint(4.2, 3.9);
    polygon.closeLastRing();
    
Create a 2D multilinestring.

    MultiLineString2D linestring = new MultiLineString2D();
    lineString.addPoint(1.3, 4.5);
    lineString.addPoint(3.0, 5.1);
    lineString.addPoint(4.2, 3.9);
    lineString.endCurrentLineString();
    lineString.addPoint(12.2, 13.1);
    lineString.addPoint(15.3, 14.2);
    lineString.endCurrentLineString();

Create a 2D multipolygon.

    MultiPolygon2D polygon = new MultiPolygon2D();
    // Add first ring
    polygon.addPoint(1.3, 4.5);
    polygon.addPoint(3.0, 5.1);
    polygon.addPoint(4.2, 3.9);
    polygon.closeLastRing();
    // Add a hole in the ring
    polygon.addPoint(12.2, 13.1);
    polygon.addPoint(15.3, 14.2);
    // Add a new polygon
    polygon.endCurrentPolygon();
    polygon.addPoint(11.3, 4.5);
    polygon.addPoint(13.0, 5.1);
    polygon.addPoint(14.2, 3.9);
    polygon.closeLastRing();

Create a geometry collection.

    GeometryCollection geometryCollection = new GeometryCollection();
    geometryCollection.addGeometry(pt);
    geometryCollection.addGeometry(polygon);
    geometryCollection.addGeometry(linestring);
    
Convert a geometry to WKT.

    // For one-time conversion of a single geometry
    linestring.asText();
    
    // A more efficient way to convert many geometries
    StringBuilder str = new StringBuilder();
    linestring.toWKT(str);
    str.append('\n');
    polygon.toWKT(str);
    str.toString();

Parse a WKT-encoded geometry.

    WKTParser parser = new WKTParser();
    String wkt = "POINT (1.3 4.5)";
    // Convert a single geometry
    IGeometry geom = parser.parse(wkt, null);
    // Convert many geometries while reusing objects if possible
    String[] wkts = {...};
    IGeometry geom = null;
    for (String wkt : wkts) {
      geom = parser.parse(wkt, geom);
      // process geom ...
    }

Convert a geometry to Esri geometry.

    Geometry esriGeom = EsriConverter.createEsriGeometry(liteGeom);


    