package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.util.IConfigurable;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.Stack;

/**
 * Some helper functions for visualization
 */
public class CommonVisualizationHelper implements IConfigurable {

  /**The minimum bounding rectangle (MBR) of the input space or the region to visualize*/
  @OperationParam(
      description = "The MBR of the input or the area to visualized"
  )
  public static final String InputMBR = "mbr";
  /**Keep aspect ratio of the input*/
  @OperationParam(
      description = "Makes the aspect ratio of the image similar to that of the input",
      defaultValue = "true"
  )
  public static final String KeepRatio = "keepratio";
  /**Flip the final image vertically*/
  @OperationParam(
      description = "Flip the final image vertically",
      defaultValue = "true"
  )
  public static final String VerticalFlip = "vflip";

  /**The configuration entry for the plotter class name*/
  public static final String PlotterClassName = "Visualization.PlotterClassName";

  /**Type of plotter to use (short user-friendly name)*/
  @OperationParam(
      description = "Type of plotter to use",
      required = true
  )
  public static final String PlotterName = "plotter";

  /**Instruct the visualization module to apply the Mercator projection on the fly*/
  @OperationParam(
      description = "Reproject the geometries to the Web Mercator projection before visualization",
      defaultValue = "false"
  )
  public static final String UseMercatorProjection = "mercator";

  /**
   * Set this parameter to {@code true} to flatten features before plotting them.
   * This helps with big geometries that contain a lot of small geometries, e.g., archipelago.
   */
  @OperationParam(
      description = "Breaks complex geometries into simpler ones to speed up the plotting",
      defaultValue = "false"
  )
  public static final String FlattenFeatures = "FlattenFeatures";

  public static Color getColor(String name) {
    name = name.toLowerCase();
    try {
      Field field = Color.class.getField(name);
      if (field != null)
        return (Color) field.get(Color.class);
    } catch (NoSuchFieldException e) {
    } catch (IllegalAccessException e) {
    }
    if (name.equals("none"))
      return new Color(0, true);
    // TODO handle colors written in standard CSS formats (e.g., #rgb, #rrggbb, or rgb(r,g,b)
    return null;
  }

  /**The maximum latitude (in degrees) that is supported in Web Mercator*/
  public static final double WebMercatorMaxLatitude = Math.toDegrees(2 * Math.atan(Math.exp(Math.PI)) - Math.PI / 2.0);

  @Override
  public void addDependentClasses(UserOptions opts, Stack<Class<?>> parameterClasses) {
    String plotterName = opts.get(PlotterName);
    if (plotterName == null)
      return;
    Class<? extends Plotter> plotterClass = Plotter.plotters.get(plotterName);
    if (plotterClass != null)
      parameterClasses.push(plotterClass);
  }

}
