/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.ILineString;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.util.OperationParam;
import org.apache.hadoop.conf.Configuration;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.OutputStream;

/**
 * A plotter that draws the geometry of the features (e.g., the point location or polygon boundary)
 */
@Plotter.Metadata(
    shortname = "gplot",
    imageExtension = ".png",
    description = "A plotter that draws the geometric shape of objects on a raster canvas"
)
public class GeometricPlotter extends Plotter {

  @OperationParam(
      description = "The color of the stroke of the objects. Used for points, envelopes, lines, and polygons.",
      defaultValue = "black"
  )
  public static final String StrokeColor = "stroke";

  @OperationParam(
     description = "The color of the fill of the objects. Used for envelopes and polygons",
      defaultValue = "none"
  )
  public static final String FillColor = "fill";

  @OperationParam(
     description = "The size of points in pixels when there are few points in the canvas",
      defaultValue = "7"
  )
  public static final String PointSize = "pointsize";

  @OperationParam(
     description = "Use anitaliasing for plotting",
      defaultValue = "false"
  )
  public static final String Antialiasing = "antialias";

  /**Color for the outer stroke or for points*/
  private Color strokeColor;

  /**Color for the fill of polygons or envelopes*/
  private Color fillColor;

  /**Size of a point in pixels*/
  private int pointsize;

  /**Whether to use antialiasing or not*/
  private boolean antialiasing;

  @Override
  public void setup(Configuration clo) {
    super.setup(clo);
    this.strokeColor = CommonVisualizationHelper.getColor(clo.get(StrokeColor, "Black"));
    this.fillColor = CommonVisualizationHelper.getColor(clo.get(FillColor, "None"));
    this.pointsize = clo.getInt(PointSize, 7);
    this.antialiasing = clo.getBoolean(Antialiasing, false);
  }

  @Override
  public int getBufferSize() {
    return pointsize;
  }

  @Override
  public Canvas createCanvas(int width, int height, Envelope mbr) {
    ImageCanvas imageCanvas = new ImageCanvas(mbr, width, height, pointsize);
    return imageCanvas;
  }

  @Override
  public boolean plot(Canvas canvasLayer, IFeature shape) {
    return plotGeometry(canvasLayer, shape.getGeometry());
  }

  /**
   * Plots the given geometry to the canvas. The temporary arrays xs and ys are used for converting geometry
   * objects from model space to image space.
   * @param canvas the canvas to plot the geometry to
   * @param geom the geometry to plot
   * @return {@code true} if the canvas was updated as a result of this plot operation
   */
  protected boolean plotGeometry(Canvas canvas, IGeometry geom) {
    ImageCanvas iCanvas = (ImageCanvas) canvas;
    Graphics2D g = iCanvas.getOrCreateGraphics(antialiasing);

    if (geom.isEmpty())
      return false;
    assert geom.getCoordinateDimension() == 2;

    boolean changed = false;
    int startPoint, endPoint;
    switch (geom.getType()) {
      case POINT:
        int x = (int) canvas.transformX(((Point)geom).coords[0]);
        int y = (int) canvas.transformY(((Point)geom).coords[1]);
        plotPoint(iCanvas, g, x, y);
        changed = true;
        break;
      case ENVELOPE:
        Envelope env = (Envelope) geom;
        int x1, y1, x2, y2;
        x1 = (int) canvas.transformX(env.minCoord[0]);
        y1 = (int) canvas.transformY(env.minCoord[1]);
        x2 = (int) canvas.transformX(env.maxCoord[0]);
        y2 = (int) canvas.transformY(env.maxCoord[1]);
        if (fillColor.getAlpha() != 0) {
          g.setColor(fillColor);
          g.fillRect(x1, y1, x2 - x1, y2 - y1);
          changed = true;
        }
        if (strokeColor.getAlpha() != 0) {
          g.setColor(strokeColor);
          g.drawRect(x1, y1, x2 - x1 - 1, y2 - y1 - 1);
        }
        changed = changed || x1 >= 0 || y1 >= 0 || x2 < canvas.getWidth() || y2 < canvas.getHeight();
        break;
      case LINESTRING:
        ILineString lineString = (ILineString) geom;
        startPoint = 0;
        endPoint = (int) lineString.getNumPoints();
        changed = plotLineSubstring(canvas, g, lineString, startPoint, endPoint, false, false) || changed;
        break;
      case POLYGON:
      case MULTIPOLYGON:
        Polygon2D polygon = (Polygon2D) geom;
        // TODO handle filled polygons with holes
        for (int iRing = 0; iRing < polygon.getNumRings(); iRing++) {
          startPoint = polygon.getRingStart(iRing);
          endPoint = polygon.getRingEnd(iRing);
          changed = plotLineSubstring(canvas, g, polygon, startPoint, endPoint, true, iRing == 0) || changed;
        }
        break;
      case MULTILINESTRING:
        MultiLineString2D mline = (MultiLineString2D) geom;
        for (int i = 0; i < mline.getNumLineStrings(); i++) {
          startPoint = mline.getLineStringStart(i);
          endPoint = mline.getLineStringEnd(i);
          changed = plotLineSubstring(canvas, g, mline, startPoint, endPoint, false, false) || changed;
        }
        break;
      case GEOMETRYCOLLECTION:
        GeometryCollection geomCollection = (GeometryCollection) geom;
        for (int iGgeom = 0; iGgeom < geomCollection.numGeometries(); iGgeom++)
          plotGeometry(canvas, geomCollection.getGeometry(iGgeom));
        break;
      default:
        throw new RuntimeException(String.format("Cannot plot geometries of type '%s'", geom.getType()));
    }
    return changed;
  }

  private boolean plotLineSubstring(Canvas canvas, Graphics2D g, ILineString lineString,
                                    int startPoint, int endPoint, boolean closeRing, boolean fill) {
    boolean canvasChanged = false;
    int x;
    int y;
    int arraySize = 0;
    // The mask of a point has four bits set as described below.
    // Bit #0: If set, it means that the point is outside the canvas from the right
    // Bit #1: If set, it means that the point is outside the canvas from the left
    // Bit #2: If set, it means that the point is outside the canvas from the top
    // Bit #3: If set, it means that the point is outside the canvas from the bottom
    // If two consecutive points have the same bit set (whatever the bit is), it indicates a line segment that
    // is not visible on the image.
    int prevMask = 0;
    Point tempPoint = new Point(2);
    int[] xs = new int[(int) lineString.getNumPoints()];
    int[] ys = new int[(int) lineString.getNumPoints()];
    for (int iPoint = startPoint; iPoint < endPoint; iPoint++) {
      lineString.getPointN(iPoint, tempPoint);
      x = (int) canvas.transformX(tempPoint.coords[0]);
      y = (int) canvas.transformY(tempPoint.coords[1]);
      int mask = 0;
      if (x < 0) mask |= 1;
      if (x > canvas.getWidth()) mask |= 2;
      if (y < 0) mask |= 4;
      if (y > canvas.getHeight()) mask |= 8;
      if ((mask & prevMask) != 0) {
        // Line segment not visible. Plot whatever we have right now and only add the last point
        if (arraySize > 1) {
          drawPolyline(xs, ys, g, arraySize, fill);
          canvasChanged = true;
        }
        xs[0] = x;
        ys[0] = y;
        arraySize = 1;
      } else {
        // Segment might be visible
        xs[arraySize] = x;
        ys[arraySize] = y;
        if (arraySize == 0 || xs[arraySize] != xs[arraySize-1] || ys[arraySize] != ys[arraySize-1])
          arraySize++;
      }
      prevMask = mask;
      if (arraySize == xs.length || iPoint == endPoint - 1) {
        // Plot the points that we converted so far and advance to the next batch
        if (arraySize > 1) {
          drawPolyline(xs, ys, g, arraySize, fill);
          canvasChanged = true;
        }
        xs[0] = xs[arraySize - 1];
        ys[0] = ys[arraySize - 1];
        arraySize = 1;
        if (iPoint == endPoint - 1 && closeRing) {
          // If the ring needs to be closed, draw one more line
          lineString.getPointN(startPoint, tempPoint);
          x = (int) canvas.transformX(tempPoint.coords[0]);
          y = (int) canvas.transformY(tempPoint.coords[1]);
          xs[arraySize] = x;
          ys[arraySize] = y;
          arraySize++;
          drawPolyline(xs, ys, g, arraySize, fill);
        }
      }
    }
    return canvasChanged;
  }

  private void drawPolyline(int[] xs, int[] ys, Graphics2D g, int numPoints, boolean fill) {
    if (fill) {
      g.setColor(fillColor);
      g.fillPolygon(xs, ys, numPoints);
    }
    g.setColor(strokeColor);
    g.drawPolyline(xs, ys, numPoints);
  }

  @Override
  public Class<? extends Canvas> getCanvasClass() {
    return ImageCanvas.class;
  }

  @Override
  public Canvas merge(Canvas finalLayer, Canvas intermediateLayer) {
    ImageCanvas finalLayer1 = (ImageCanvas) finalLayer;
    ImageCanvas intermediateLayer1 = (ImageCanvas) intermediateLayer;
    finalLayer1.mergeWith(intermediateLayer1);
    return finalLayer;
  }

  protected void plotPoint(ImageCanvas canvas, Graphics2D g, int x, int y) {
    // If pointsize is zero, then just plot it as a single pixel
    if (pointsize == 0) {
      g.setColor(strokeColor);
      g.fillRect(x, y, 1, 1);
      return;
    }
    // Mark the corresponding pixel in the bitmap as occupied
    canvas.setPixelOccupied1(x, y);
  }

  protected void plotAllRemainingPoints(ImageCanvas canvas, Graphics2D g) {
    // Consider only pixel locations that have exactly one point. Plot a circle centered at that point depending
    // on how much space is available around it.
    g.setColor(strokeColor);
    for (int x = -canvas.buffer; x < canvas.getWidth() + canvas.buffer; x++) {
      for (int y = -canvas.buffer; y < canvas.getHeight() + canvas.buffer; y++) {
        if (canvas.getPixelOccupied1(x, y)) {
          // Exactly only point at that location.
          // Calculate the empty buffer around it
          int emptyBufferSize = 0;
          boolean isBufferClear = true;
          while (isBufferClear && emptyBufferSize <= pointsize) {
            emptyBufferSize++;
            for (int dx = -emptyBufferSize; isBufferClear && dx <= emptyBufferSize; dx++) {
              for (int dy = -emptyBufferSize; isBufferClear && dy <= emptyBufferSize; dy++) {
                if (dx != 0 || dy != 0)
                  isBufferClear = !canvas.getPixelOccupied1(x + dx, y + dy);
              }
            }
            if (!isBufferClear)
              emptyBufferSize--;
          }
          emptyBufferSize /= 2;
          if (emptyBufferSize == 0)
            g.fillRect(x, y, 1,  1);
          else
            g.fillOval(x - emptyBufferSize, y - emptyBufferSize, emptyBufferSize * 2 + 1, emptyBufferSize * 2 + 1);
        }
      }
    }
  }

  @Override
  public void writeImage(Canvas layer, OutputStream out, boolean vflip) throws IOException {
    BufferedImage finalImage;
    ImageCanvas imageCanvas = (ImageCanvas) layer;
    if (pointsize > 0) {
      Graphics2D g = imageCanvas.getOrCreateGraphics(antialiasing);
      plotAllRemainingPoints(imageCanvas, g);
    }
    finalImage = imageCanvas.getImage();
    // Flip image vertically if needed
    if (vflip) {
      AffineTransform tx = AffineTransform.getScaleInstance(1, -1);
      tx.translate(0, -finalImage.getHeight());
      AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
      finalImage = op.filter(finalImage, null);
    }
    ImageIO.write(finalImage, "png", out);
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(strokeColor.getRGB());
    out.writeInt(fillColor.getRGB());
    out.writeInt(pointsize);
    out.writeBoolean(antialiasing);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    this.strokeColor = new Color(in.readInt());
    this.fillColor = new Color(in.readInt());
    this.pointsize = in.readInt();
    this.antialiasing = in.readBoolean();
  }
}
