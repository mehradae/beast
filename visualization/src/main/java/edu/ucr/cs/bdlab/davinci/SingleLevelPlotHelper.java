/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.CSVEnvelopeDecoder;
import edu.ucr.cs.bdlab.io.CSVEnvelopeEncoder;
import edu.ucr.cs.bdlab.io.FeatureReader;
import edu.ucr.cs.bdlab.io.MercatorProjector;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.Parallel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Ahmed Eldawy
 *
 */
public class SingleLevelPlotHelper {

  /**The width of the image in pixels*/
  @OperationParam(
      description = "The width of the image in pixels",
      defaultValue = "1000"
  )
  public static final String ImageWidth = "width";
  /**The height of the image in pixels*/
  @OperationParam(
      description = "The height of the image in pixels",
      defaultValue = "1000"
  )
  public static final String ImageHeight = "height";

  /**Merge the partial images into one for single level plot*/
  @OperationParam(
      description = "Merges the output into a single image",
      defaultValue = "true"
  )
  public static final String MergeImages = "merge";

  private static final Log LOG = LogFactory.getLog(SingleLevelPlotHelper.class);

  /**
   * Creates and configures a plotter from the given Hadoop configuration. The given configuration should contain
   * at least the following configurations.
   * <ul>
   *   <li>{@link CommonVisualizationHelper#PlotterName} the short name of the plotter</li>
   *   <li>{@link SingleLevelPlotHelper#ImageWidth} the width of the image in pixels</li>
   *   <li>{@link SingleLevelPlotHelper#ImageHeight} the height of the image in pixels</li>
   *   <li>{@link CommonVisualizationHelper#InputMBR} the MBR of the input or the region to visualize</li>
   * </ul>
   * @param conf the configuration used to find the plotter
   * @return an instance of type Plotter as configured in the given configuration
   */
  public static Plotter getConfiguredPlotter(Configuration conf) {
    Envelope inputMBR = CSVEnvelopeDecoder.instance.apply(conf.get("mbr"), null);
    int imageWidth = conf.getInt(SingleLevelPlotHelper.ImageWidth, 1000);
    int imageHeight = conf.getInt(SingleLevelPlotHelper.ImageHeight, 1000);
    String plotterName = conf.get(CommonVisualizationHelper.PlotterName);
    return Plotter.getConfiguredPlotter(plotterName, conf);
  }

  /**
   * Plots a file using a single-machine algorithm
   * @param inPath the path to the input
   * @param output the output stream to write the image to
   * @param plotterClass the class to use as a plotter
   * @param imageWidth the width of the desired image in pixels
   * @param imageHeight the height of the desired image in pixels
   * @param keepRatio whether to keep the aspect ratio of the input or not
   * @param conf environment configuration
   * @throws IOException if an error happens while reading the input
   */
  public static void plotLocal(Path inPath, DataOutputStream output,
                               final Class<? extends Plotter> plotterClass,
                               int imageWidth, int imageHeight,
                               boolean keepRatio,
                               final Configuration conf) throws IOException {
    Envelope mbr = CSVEnvelopeDecoder.instance.apply(conf.get("mbr"), null);

    // Use the given MBR to filter unnecessary data
    try {
      Plotter plotter = plotterClass.newInstance();
      plotter.setup(conf);
      Envelope filterMBR = new Envelope(mbr);
      filterMBR.buffer(plotter.getBufferSize() * filterMBR.getSideLength(0) / imageWidth,
          plotter.getBufferSize() * filterMBR.getSideLength(1) / (double) imageHeight);
      conf.set(SpatialInputFormat.FilterMBR, CSVEnvelopeEncoder.defaultEncoder.apply(filterMBR));
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }

    boolean mercator = conf.getBoolean(SpatialInputFormat.Mercator, false);
    if (mercator) {
      // Convert the given MBR to the Mercator projection
      MercatorProjector.worldToMercator(mbr);
    }
    if (keepRatio) {
      // Adjust width and height to maintain aspect ratio and store the adjusted
      // values back in params in case the caller needs to retrieve them
      if (mbr.getSideLength(0) / mbr.getSideLength(1) > (double) imageWidth / imageHeight)
        imageHeight = (int) Math.round(mbr.getSideLength(1) * imageWidth / mbr.getSideLength(0));
      else
        imageWidth = (int) Math.round(mbr.getSideLength(0) * imageHeight / mbr.getSideLength(1));
    }
    // Store width and height in final variable to be able to access them in inner classes
    final int fwidth = imageWidth, fheight = imageHeight;

    // Start reading input file
    List<InputSplit> splits = new ArrayList<InputSplit>();
    final SpatialInputFormat inputFormat = new SpatialInputFormat();
    Job dummyJob = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(dummyJob, inPath);
    splits.addAll(inputFormat.getSplits(dummyJob));

    try {
      List<Canvas> partialCanvases = Parallel.forEach(splits.size(), (i1, i2) -> {
        try {
          Plotter plotter = plotterClass.newInstance();
          plotter.setup(conf);
          // Create the partial layer that will contain the plot of the assigned partitions
          Canvas partialCanvas = plotter.createCanvas(fwidth, fheight, mbr);

          for (int i = i1; i < i2; i++) {
            FeatureReader reader = inputFormat.createAndInitRecordReader(splits.get(i), conf);

            Iterable<? extends IFeature> features = reader;
            if (conf.getBoolean(CommonVisualizationHelper.FlattenFeatures, false))
              features = new FlattenFeatures(features);

            for (IFeature feature : features) {
              plotter.plot(partialCanvas, feature);
            }

            reader.close();
          }
          return partialCanvas;
        } catch (IOException e) {
          throw new RuntimeException("Error reading the file ", e);
        } catch (IllegalAccessException e) {
          throw new RuntimeException(String.format("Cannot access the constructor of '%s'", plotterClass), e);
        } catch (InstantiationException e) {
          throw new RuntimeException(String.format("Error while instantiating '%s'", plotterClass), e);
        }
      });
      Plotter plotter = plotterClass.newInstance();
      plotter.setup(conf);

      // Whether we should vertically flip the final image or not
      boolean vflip = conf.getBoolean("vflip", true);
      LOG.info("Merging "+partialCanvases.size()+" partial canvases");
      // Create the final canvas that will contain the final image
      Canvas finalCanvas = plotter.createCanvas(fwidth, fheight, mbr);
      for (Canvas partialCanvas : partialCanvases) {
        plotter.merge(finalCanvas, partialCanvas);
      }

      // Finally, write the resulting image to the given output path
      LOG.info("Writing final image");
      plotter.writeImage(finalCanvas, output, vflip);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(String.format("Cannot access the constructor of '%s'", plotterClass), e);
    } catch (InstantiationException e) {
      throw new RuntimeException(String.format("Error while instantiating '%s'", plotterClass), e);
    } catch (InterruptedException e) {
      throw new RuntimeException("Error while processing the file", e);
    }
  }
}
