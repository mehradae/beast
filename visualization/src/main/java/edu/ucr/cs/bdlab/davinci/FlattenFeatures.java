/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.IMultiLineString;
import edu.ucr.cs.bdlab.geolite.IPolygon;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;

import java.util.Iterator;

/**
 * An Iterable that wraps another Iterable of {@link edu.ucr.cs.bdlab.geolite.IFeature} and flattens complex geometries
 * into simple ones as follows:
 * <ul>
 *   <li>TODO: A {@link edu.ucr.cs.bdlab.geolite.MultiPoint} is flattened into single points</li>
 *   <li>A {@link edu.ucr.cs.bdlab.geolite.twod.Polygon2D} is flattened into rings</li>
 *   <li>A {@link edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D} is flattened into line strings</li>
 *   <li>A {@link edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D} is flattened into rings</li>
 *   <li>All contents of a {@link edu.ucr.cs.bdlab.geolite.GeometryCollection} are flattened (TODO recursively)</li>
 * </ul>
 */
public class FlattenFeatures implements Iterable<IFeature>, Iterator<IFeature> {

  protected final Iterator<? extends IFeature> wrapped;

  /**
   * The geometry being flattened
   */
  protected IGeometry geometry;

  /**
   * The index of the subgeometry that was returned by the last call of next()
   */
  protected int i;

  /**
   * Number of subgeometries contained in the current geometry.
   */
  protected int n;

  /**A wrapper around the feature that was last returned*/
  protected IFeature currentValue;

  public FlattenFeatures(Iterable<? extends IFeature> iFeatures) {
    this.wrapped = iFeatures.iterator();
    n = -1; // Force hasNext to read the next geometry
  }

  public FlattenFeatures(Iterator<? extends IFeature> iFeatures) {
    this.wrapped = iFeatures;
    n = -1; // Force hasNext to read the next geometry
  }

  @Override
  public Iterator<IFeature> iterator() {
    return this;
  }

  @Override
  public boolean hasNext() {
    return i < n - 1 || wrapped.hasNext();
  }

  @Override
  public IFeature next() {
    i++;
    if (i >= n) {
      // Fetch next geometry
      currentValue = wrapped.next();
      this.geometry = currentValue.getGeometry();
      // Set the value of n (# of subgeometries)
      switch (this.geometry.getType()) {
        case POLYGON:
        case MULTIPOLYGON:
          n = ((IPolygon) this.geometry).getNumRings();
          break;
        case MULTILINESTRING:
          n = ((IMultiLineString) this.geometry).getNumLineStrings();
          break;
        case GEOMETRYCOLLECTION:
          n = ((GeometryCollection) this.geometry).numGeometries();
          break;
        default:
          n = 1;
      }
      // Move to first subgeometry
      i = 0;
    }
    switch (this.geometry.getType()) {
      case POLYGON:
      case MULTIPOLYGON:
        currentValue.setGeometry(((Polygon2D) this.geometry).getRingN(i));
        break;
      case MULTILINESTRING:
        currentValue.setGeometry(((IMultiLineString) this.geometry).getLineStringN(i));
        break;
      case GEOMETRYCOLLECTION:
        currentValue.setGeometry(((GeometryCollection) this.geometry).getGeometry(i));
        break;
      default:
        // A primitive feature, return it as is
        currentValue.setGeometry(this.geometry);
    }
    return currentValue;
  }
}
