package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

public class SingleLevelPlotHelperTest extends SparkTest {

  public void testPlotLocalKeepRatio() throws IOException, InterruptedException {
    Path inPath = new Path(scratchPath, "in.rect");
    copyResource("/test.rect", new File(inPath.toString()));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "envelopek(2)");
    SpatialOutputFormat.setOutputFormat(conf, "envelopek(2)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.set("mbr", "-180.0,-45.0,180.0,45.0");
    SingleLevelPlotHelper.plotLocal(inPath, dos, GeometricPlotter.class, 1000, 1000, true, conf);
    dos.close();

    BufferedImage i = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    assertEquals(1000, i.getWidth());
    assertEquals(250, i.getHeight());
  }

  public void testPlotLocalMercatorProjection() throws IOException, InterruptedException {
    Path inPath = new Path(scratchPath, "in.rect");
    copyResource("/test.rect", new File(inPath.toString()));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "envelopek(2)");
    SpatialOutputFormat.setOutputFormat(conf, "envelopek(2)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.setBoolean(SpatialInputFormat.Mercator, true);
    conf.set("mbr", "-180.0,-45.0,180.0,45.0");
    SingleLevelPlotHelper.plotLocal(inPath, dos, GeometricPlotter.class, 1000, 1000, true, conf);
    dos.close();

    BufferedImage i = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    assertEquals(1000.0, (double) i.getWidth(), 1.0);
    assertEquals(280.0, (double)i.getHeight(), 1.0);
  }

  public void testPlotLocalMercatorProjectionWithFlatten() throws IOException, InterruptedException {
    Path inPath = new Path(scratchPath, "in.rect");
    copyResource("/test_flatten.wkt", new File(inPath.toString()));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "wkt");
    conf.setBoolean(SpatialInputFormat.Mercator, true);
    conf.setBoolean(CommonVisualizationHelper.FlattenFeatures, true);
    conf.set("mbr", "0,0,90,66.51326044311185");
    int imageSize = 256;
    SingleLevelPlotHelper.plotLocal(inPath, dos, GeometricPlotter.class, imageSize, imageSize, true, conf);
    dos.close();

    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    assertEquals(imageSize, img.getWidth());
    assertEquals(imageSize, img.getHeight());

    // Count number of pixels
    int count = 0;
    for (int $x = 0; $x < imageSize; $x++) {
      for (int $y = 0; $y < imageSize; $y++) {
        int alpha = new Color(img.getRGB($x, $y), true).getAlpha();
        if (alpha != 0)
          count++;
      }
    }
    assertEquals(1020, count);
  }


  public void testPlotLocalFilterWithBuffer() throws IOException, InterruptedException {
    Path inPath = new Path(scratchPath, "in.rect");
    copyResource("/test.rect", new File(inPath.toString()));
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "envelopek(2)");
    SpatialOutputFormat.setOutputFormat(conf, "envelopek(2)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.setInt(GeometricPlotter.PointSize, 10);
    conf.set("mbr", "-180.0,-45.0,180.0,45.0");
    SingleLevelPlotHelper.plotLocal(inPath, dos, GeometricPlotter.class, 360, 90, true, conf);
    dos.close();

    String filterMBR = conf.get(SpatialInputFormat.FilterMBR);
    assertEquals("-190.0,-55.0,190.0,55.0", filterMBR);
  }
}