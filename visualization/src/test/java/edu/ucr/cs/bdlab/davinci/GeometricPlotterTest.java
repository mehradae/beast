/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.util.UserOptions;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;
import junit.framework.TestCase;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class GeometricPlotterTest extends TestCase {

  public void testPlotMultiPolygon() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    plotter.setup(opts);
    Canvas canvas = plotter.createCanvas(100, 100, mbr);
    MultiPolygon2D poly = new MultiPolygon2D();
    poly.addPoint(0.0, 0.0);
    poly.addPoint(0.0, 0.5);
    poly.closeLastRing(false);
    poly.addPoint(0.5, 0.5);
    poly.addPoint(0.5, 0.0);
    poly.closeLastRing(false);
    plotter.plot(canvas, new Feature(poly));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    // There should not be any set pixel in the column half way between 0 and 50
    int x = 25;
    for (int $y = 0; $y < 100; $y++) {
      int rgb = img.getRGB(x, $y);
      assertEquals(String.format("A pixel is incorrectly set at (%d, %d)", x, $y), 0, rgb);
    }
    // There should be exactly two pixels set in the first row
    int y = 0;
    int count = 0;
    for (int $x = 0; $x < 100; $x++) {
      int rgb = img.getRGB($x, y);
      if (rgb != 0)
        count++;
    }
    assertEquals(2, count);
  }

  public void testLinestring() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    opts.setBoolean(GeometricPlotter.Antialiasing, false);
    plotter.setup(opts);
    Canvas canvas = plotter.createCanvas(100, 100, mbr);
    LineString2D linestring = new LineString2D();
    linestring.addPoint(0.1, 0.1);
    linestring.addPoint(0.2, 0.98);
    linestring.addPoint(0.99, 0.99);
    plotter.plot(canvas, new Feature(linestring));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    // Count non-transparent pixels along the middle lines
    int count = 0;
    for (int $i = 0; $i < 100; $i++) {
      if (new Color(img.getRGB(50, $i), true).getAlpha() != 0)
        count++;
      if (new Color(img.getRGB($i, 50), true).getAlpha() != 0)
        count++;
    }
    assertEquals(2, count);
  }

  public void testPlotMultiPolygon2() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    opts.setBoolean(GeometricPlotter.Antialiasing, false);
    plotter.setup(opts);
    Canvas canvas = plotter.createCanvas(100, 100, mbr);
    MultiPolygon2D poly = new MultiPolygon2D();
    poly.addPoint(0.01, 0.01);
    poly.addPoint(0.98, 0.02);
    poly.addPoint(0.99, 0.99);
    poly.addPoint(0.02, 0.98);
    poly.closeLastRing(false);
    plotter.plot(canvas, new Feature(poly));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));

    // Count non-transparent pixels along the middle lines
    int count = 0;
    for (int $i = 0; $i < 100; $i++) {
      if (new Color(img.getRGB(50, $i), true).getAlpha() != 0)
        count++;
      if (new Color(img.getRGB($i, 50), true).getAlpha() != 0)
        count++;
    }
    assertEquals(4, count);
  }

  public void testLinestring2() throws IOException, ParseException {
    // Try both the fast plotter and the memory saving one.
    GeometricPlotter plotter = new GeometricPlotter();
    LineString2D linestring = new LineString2D();
    new WKTParser().parse("LINESTRING(61 897,65 891)", linestring);
    Envelope mbr = new Envelope(2);
    linestring.envelope(mbr);
    UserOptions opts = new UserOptions();
    opts.setBoolean(GeometricPlotter.Antialiasing, false);
    int imageSize = 100;
    plotter.setup(opts);
    Canvas canvas = plotter.createCanvas(imageSize, imageSize, mbr);
    plotter.plot(canvas, new Feature(linestring));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    // Count non-transparent pixels along the middle lines
    int count = 0;
    for (int $i = 0; $i < imageSize; $i++) {
      if (new Color(img.getRGB($i, imageSize/2), true).getAlpha() != 0)
        count++;
    }
    assertEquals(1, count);
  }

  public void testLinestringThatGoesOffCanvas() throws IOException, ParseException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    opts.setBoolean(GeometricPlotter.Antialiasing, false);
    plotter.setup(opts);
    int imageSize = 100;
    Canvas canvas = plotter.createCanvas(imageSize, imageSize, mbr);
    LineString2D linestring = new LineString2D();
    linestring.addPoint(0.1, 0.1);
    linestring.addPoint(1.2, 0.1);
    linestring.addPoint(1.2, 1.2);
    linestring.addPoint(0.1, 1.2);
    linestring.addPoint(0.1, 0.1);
    plotter.plot(canvas, new Feature(linestring));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));

    // Count number of pixels
    int count = 0;
    for (int $x = 0; $x < imageSize; $x++) {
      for (int $y = 0; $y < imageSize; $y++) {
        int alpha = new Color(img.getRGB($x, $y), true).getAlpha();
        if (alpha != 0)
          count++;
      }
    }
    assertEquals(179, count);
  }
  public void testMerge() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    opts.setBoolean(GeometricPlotter.Antialiasing, false);
    plotter.setup(opts);
    Canvas canvas1 = plotter.createCanvas(100, 100, mbr);
    MultiPolygon2D poly = new MultiPolygon2D();
    poly.addPoint(0.0, 0.0);
    poly.addPoint(0.0, 0.5);
    poly.closeLastRing(false);
    plotter.plot(canvas1, new Feature(poly));

    Canvas canvas2 = plotter.createCanvas(100, 100, mbr);
    poly = new MultiPolygon2D();
    poly.addPoint(0.5, 0.5);
    poly.addPoint(0.5, 0.0);
    poly.closeLastRing(false);
    plotter.plot(canvas2, new Feature(poly));

    plotter.merge(canvas1, canvas2);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas1, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    // There should be exactly two pixels set in the first row
    int y = 0;
    int count = 0;
    for (int $x = 0; $x < 100; $x++) {
      int rgb = img.getRGB($x, y);
      if (rgb != 0)
        count++;
    }
    assertEquals(2, count);
  }

  public void testPlotMultiPolygonWithManyPoints() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1000.0, 1000.0);
    UserOptions opts = new UserOptions();
    opts.setBoolean(GeometricPlotter.Antialiasing, false);
    plotter.setup(opts);
    int imageSize = 1000;
    Canvas canvas = plotter.createCanvas(imageSize, imageSize, mbr);
    MultiPolygon2D poly = new MultiPolygon2D();
    for (int $i = 0; $i < 750; $i++)
      poly.addPoint($i, 0.0);
    poly.addPoint(749.0, 10);
    poly.addPoint(0.0, 10);
    poly.closeLastRing(false);
    // Add second polygon
    for (int $i = 0; $i < 7500; $i++)
      poly.addPoint($i/10.0, 50.0);
    poly.addPoint(749.0, 60);
    poly.addPoint(0.0, 60);
    poly.closeLastRing(false);

    plotter.plot(canvas, new Feature(poly));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    // Count number of black pixels
    int count = 0;
    for (int $x = 0; $x < imageSize; $x++) {
      for (int $y = 0; $y < imageSize; $y++) {
        int alpha = new Color(img.getRGB($x, $y), true).getAlpha();
        if (alpha != 0)
          count++;
      }
    }
    assertEquals((750 + 9) * 2 * 2, count);
  }

  public void testPlotPointsAsPixels() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    // Set up a plotter with five-pixel point size
    UserOptions opts = new UserOptions("pointsize:5");
    opts.setBoolean(GeometricPlotter.Antialiasing, false);
    plotter.setup(opts);

    Envelope mbr = new Envelope(2, 0.0, 0.0, 256.0, 256.0);
    Canvas canvas = plotter.createCanvas(256, 256, mbr);
    plotter.plotGeometry(canvas, new Point(2, 5.0, 5.0));
    plotter.plotGeometry(canvas, new Point(2, 6.0, 5.0));
    plotter.plotGeometry(canvas, new Point(2, 7.0, 5.0));

    int countBlackPixels = 0;
    int blackPixel = new Color(0, 0, 0, 255).getRGB();
    BufferedImage image = getImage(plotter, canvas);
    for (int x = 0; x < image.getWidth(); x++) {
      for (int y = 0; y < image.getHeight(); y++) {
        if (image.getRGB(x, y) == blackPixel)
          countBlackPixels++;
      }
    }
    assertEquals(3, countBlackPixels);
  }

  public void testPlotPointsAsPixelsWithPointSizeZero() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    // Set up a plotter with five-pixel point size
    UserOptions opts = new UserOptions("pointsize:0");
    opts.setBoolean(GeometricPlotter.Antialiasing, false);
    plotter.setup(opts);

    Envelope mbr = new Envelope(2, 0.0, 0.0, 256.0, 256.0);
    Canvas canvas = plotter.createCanvas(256, 256, mbr);
    plotter.plotGeometry(canvas, new Point(2, 5.0, 5.0));
    plotter.plotGeometry(canvas, new Point(2, 17.0, 5.0));
    plotter.plotGeometry(canvas, new Point(2, 29.0, 5.0));

    int countBlackPixels = 0;
    int blackPixel = new Color(0, 0, 0, 255).getRGB();
    BufferedImage image = getImage(plotter, canvas);
    for (int x = 0; x < image.getWidth(); x++) {
      for (int y = 0; y < image.getHeight(); y++) {
        if (image.getRGB(x, y) == blackPixel)
          countBlackPixels++;
      }
    }
    assertEquals(3, countBlackPixels);
  }

  public void testPlotPointsAsCircles() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    // Set up a plotter with five-pixel point size
    UserOptions opts = new UserOptions("pointsize:5");
    plotter.setup(opts);

    Envelope mbr = new Envelope(2, 0.0, 0.0, 256.0, 256.0);
    Canvas canvas = plotter.createCanvas(256, 256, mbr);
    plotter.plotGeometry(canvas, new Point(2, 5.0, 5.0));
    plotter.plotGeometry(canvas, new Point(2, 17.0, 5.0));
    plotter.plotGeometry(canvas, new Point(2, 29.0, 5.0));

    int countBlackPixels = 0;
    int blackPixel = new Color(0, 0, 0, 255).getRGB();
    BufferedImage image = getImage(plotter, canvas);
    for (int x = 0; x < image.getWidth(); x++) {
      for (int y = 0; y < image.getHeight(); y++) {
        if (image.getRGB(x, y) == blackPixel)
          countBlackPixels++;
      }
    }
    assertTrue(String.format("Too few black pixels %d", countBlackPixels), countBlackPixels >= 13 * 3);
  }

  public void testPlotPointsAsCirclesWithMerge() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    // Set up a plotter with five-pixel point size
    UserOptions opts = new UserOptions("pointsize:5");
    plotter.setup(opts);

    Envelope mbr = new Envelope(2, 0.0, 0.0, 256.0, 256.0);
    Canvas canvas1 = plotter.createCanvas(256, 256, mbr);
    plotter.plotGeometry(canvas1, new Point(2, 5.0, 5.0));
    plotter.plotGeometry(canvas1, new Point(2, 17.0, 5.0));
    plotter.plotGeometry(canvas1, new Point(2, 29.0, 5.0));

    Canvas canvas2 = plotter.createCanvas(256, 256, mbr);
    plotter.plotGeometry(canvas2, new Point(2, 5.0, 15.0));
    plotter.plotGeometry(canvas2, new Point(2, 17.0, 15.0));
    plotter.plotGeometry(canvas2, new Point(2, 29.0, 15.0));

    canvas1 = plotter.merge(canvas1, canvas2);

    int blackPixel = new Color(0, 0, 0, 255).getRGB();
    int countBlackPixels = 0;
    BufferedImage image = getImage(plotter, canvas1);
    for (int x = 0; x < image.getWidth(); x++) {
      for (int y = 0; y < image.getHeight(); y++) {
        if (image.getRGB(x, y) == blackPixel)
          countBlackPixels++;
      }
    }
    assertTrue(String.format("Too few black pixels %d", countBlackPixels), countBlackPixels >= 13 * 6);
  }

  public void testPlotPointsWithBuffer() throws IOException {
    Envelope mbr = new Envelope(2, 0.0, 100.0, 100.0, 200.0);
    int imageSize = 100;
    int pointsize = 5;
    GeometricPlotter plotter = new GeometricPlotter();
    UserOptions opts = new UserOptions();
    opts.setInt("width", imageSize);
    opts.setInt("height", imageSize);
    opts.setInt(GeometricPlotter.PointSize, pointsize);
    plotter.setup(opts);

    Canvas canvas = plotter.createCanvas(100, 100, mbr);
    plotter.plot(canvas, new Feature(new Point(2, 0.0, 100.0)));
    plotter.plot(canvas, new Feature(new Point(2, 2.0, 102.0)));
    plotter.plot(canvas, new Feature(new Point(2, 50.0, 150.0)));
    plotter.plot(canvas, new Feature(new Point(2, 20.0, 120.0)));
    plotter.plot(canvas, new Feature(new Point(2, 20.0, 120.0)));

    BufferedImage img = getImage(plotter, canvas);
    assertEquals(100, img.getWidth());
    assertEquals(100, img.getHeight());
    assertTrue(isPixelOpaque(img, 0, 0));
    assertTrue(isPixelOpaque(img, 2, 2));
    assertFalse(isPixelOpaque(img, 1, 1));
    assertTrue(isPixelOpaque(img, 50, 50));
    assertTrue(isPixelOpaque(img, 49, 49));
    assertTrue(isPixelOpaque(img, 51, 51));

    assertTrue(isPixelOpaque(img, 20, 20));
    assertTrue(isPixelOpaque(img, 21, 21));
    assertTrue(isPixelOpaque(img, 19, 19));
  }

  public void testPlotPointsWithBufferAndMerge() throws IOException {
    Envelope mbr = new Envelope(2, 0.0, 100.0, 100.0, 200.0);
    int imageSize = 100;
    int pointsize = 5;
    GeometricPlotter plotter = new GeometricPlotter();
    UserOptions opts = new UserOptions();
    opts.setInt("width", imageSize);
    opts.setInt("height", imageSize);
    opts.setInt(GeometricPlotter.PointSize, pointsize);
    plotter.setup(opts);

    Canvas canvas1 = plotter.createCanvas(100, 100, mbr);
    Canvas canvas2 = plotter.createCanvas(100, 100, mbr);
    plotter.plot(canvas1, new Feature(new Point(2, 0.0, 100.0)));
    plotter.plot(canvas2, new Feature(new Point(2, 2.0, 102.0)));
    plotter.plot(canvas2, new Feature(new Point(2, 50.0, 150.0)));
    plotter.plot(canvas1, new Feature(new Point(2, 20.0, 120.0)));
    plotter.plot(canvas2, new Feature(new Point(2, 20.0, 120.0)));

    plotter.merge(canvas1, canvas2);

    BufferedImage img = getImage(plotter, canvas1);
    assertEquals(100, img.getWidth());
    assertEquals(100, img.getHeight());
    assertTrue(isPixelOpaque(img, 0, 0));
    assertTrue(isPixelOpaque(img, 2, 2));
    assertFalse(isPixelOpaque(img, 1, 1));
    assertTrue(isPixelOpaque(img, 50, 50));
    assertTrue(isPixelOpaque(img, 49, 49));
    assertTrue(isPixelOpaque(img, 51, 51));

    assertTrue(isPixelOpaque(img, 20, 20));
    assertTrue(isPixelOpaque(img, 21, 21));
    assertTrue(isPixelOpaque(img, 19, 19));
  }

  protected BufferedImage getImage(Plotter plotter, Canvas canvas) throws IOException {
    ByteArrayOutputStream tempOut = new ByteArrayOutputStream();
    plotter.writeImage(canvas, tempOut, false);
    tempOut.close();
    return ImageIO.read(new ByteArrayInputStream(tempOut.toByteArray()));
  }

  public static boolean isPixelOpaque(BufferedImage img, int x,int y) {
    return new Color(img.getRGB(x, y), true).getAlpha() > 0;
  }
}