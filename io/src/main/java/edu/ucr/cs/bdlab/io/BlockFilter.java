/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import org.apache.hadoop.conf.Configuration;

import java.util.List;

/**
 * A filter that can be applied to blocks to prune unnecessary blocks of pairs of blocks.
 */
public interface BlockFilter {

  /**
   * Set up the filter based on environment configuration
   * @param conf
   */
  default void setup(Configuration conf) {}

  /**
   * Returns {@code true} if the given split matches the filter, i.e., should be processed.
   * @param s
   * @return
   */
  default boolean match(SpatialFileSplit s) {
    return true;
  }

  /**
   * Filters teh given list of splits by purning (removing) all splits that should not be processed.
   * @param ss
   */
  default void filter(List<? extends SpatialFileSplit> ss) {
    int iSplit = 0;
    while (iSplit < ss.size()) {
      if (!match(ss.get(iSplit)))
        ss.remove(iSplit);
      else
        iSplit++;
    }
  }

  /**
   * Returns {@code true} if the given composite split should be processed for binary operations.
   * @param cs
   * @return
   */
  default boolean matchComposite(CompositeSpatialFileSplit cs) {
    return true;
  }

  default void filterComposite(List<? extends CompositeSpatialFileSplit> ss) {
    int iSplit = 0;
    while (iSplit < ss.size()) {
      if (!matchComposite(ss.get(iSplit)))
        ss.remove(iSplit);
      else
        iSplit++;
    }
  }
}
