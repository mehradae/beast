/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.TokenMgrError;
import edu.ucr.cs.bdlab.wktparser.WKTParser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.LineRecordReader;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A record reader that reads CSV file with custom field delimiter
 */
@FeatureReader.Metadata(
    description = "Parses comma- or tab-separated text files which contains either point coordinates," +
        "envelope boundaries or WKT-encoded geometries",
    shortName = "csv",
    extension = ".csv"
)
public class CSVFeatureReader extends FeatureReader {
  private static final Log LOG = LogFactory.getLog(CSVFeatureReader.class);

  /**Default quote characters including single and double quote characters*/
  public static final String DefaultQuoteCharacters = "\'\'\"\"";

  @OperationParam(
      description = "The field separator for text files. Defaults to the tab character",
      defaultValue = "\t"
  )
  public static final String FieldSeparator = "separator";
  @OperationParam(
      description = "When reading a CSV file, skip the first line in the file. {true, false}",
      defaultValue = "false"
  )
  public static final String SkipHeader = "skipheader";

  @OperationParam(
      description = "Characters used to quote fields. Every pair of characters are used as start and end quote characters.",
      defaultValue = "\'\'\"\""
  )
  public static final String QuoteCharacters = "quotes";

  /**An underlying reader to read the input line-by-line*/
  protected final LineRecordReader lineReader = new LineRecordReader();

  /**Directs the record reader to skip the first line in the file*/
  protected boolean skipHeaderLine;

  /**The mutable CSV feature*/
  protected CSVFeature feature;

  /**A single character for separating fields in the input*/
  protected char fieldSeparator;

  /**A list of quote characters*/
  protected String quotes;

  /**The names or indexes of the columns where geometry WKT or coordinates are encoded*/
  protected String[] coordinateColumns;

  /**The index of columns that contain the coordinates in order*/
  protected int[] coordinateColumnIndexes;

  /**Type of geometry (TODO use an enumerated type instead)*/
  protected String geometryType;

  /**If the input contains wkt-encoded geometries, this is used to parse them*/
  protected WKTParser wktParser;

  /**An optional attributed to filter the geometries in the input file*/
  private Envelope filterMBR;

  /**This flag signals the record reader to use immutable objects (a new object per record) which is useful with some
   * Spark operations that is not designed for mutable objects, e.g., partitionBy used in indexing*/
  protected boolean immutable;

  /**Path of the input file. Used mainly for reporting errors.*/
  protected Path filePath;

  @Override
  public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) throws IOException {
    lineReader.initialize(inputSplit, taskAttemptContext);
    if (inputSplit instanceof FileSplit)
      filePath = ((FileSplit)inputSplit).getPath();
    Configuration conf = taskAttemptContext.getConfiguration();
    if (inputSplit instanceof SpatialFileSplit) {
      UserOptions opts = new UserOptions(conf);
      opts.setArrayPosition(((SpatialFileSplit)inputSplit).getIndex());
      conf = opts;
    }
    this.fieldSeparator = conf.get(FieldSeparator, "\t").charAt(0);
    this.quotes = conf.get(QuoteCharacters, "\'\'\"\"");
    setGeometryTypeAndColumnIndexes(conf.get(SpatialInputFormat.InputFormat));
    this.immutable = conf.getBoolean(SpatialInputFormat.ImmutableObjects, false);
    this.skipHeaderLine = conf.getBoolean(SkipHeader, false);
    String filterMBRStr = conf.get(SpatialInputFormat.FilterMBR);
    if (filterMBRStr != null) {
      String[] parts = filterMBRStr.split(",");
      double[] dblParts = new double[parts.length];
      for (int i = 0; i < parts.length; i++)
        dblParts[i] = Double.parseDouble(parts[i]);
      this.filterMBR = new Envelope(dblParts.length/2, dblParts);
    }
  }

  /**
   * An initializer that can be used outside the regular MapReduce context.
   * @param inputFile
   * @param conf
   * @throws IOException
   */
  public void initialize(Path inputFile, Configuration conf) throws IOException {
    FileSystem fileSystem = inputFile.getFileSystem(conf);
    FileStatus fileStatus = fileSystem.getFileStatus(inputFile);
    BlockLocation[] fileBlockLocations = fileSystem.getFileBlockLocations(fileStatus, 0, fileStatus.getLen());
    List<String> hosts = new ArrayList<String>();
    for (int i = 0; i < fileBlockLocations.length; i++)
      for (String host : fileBlockLocations[i].getHosts())
        hosts.add(host);
    FileSplit fileSplit = new FileSplit(inputFile, 0, fileStatus.getLen(), hosts.toArray(new String[hosts.size()]));
    TaskAttemptContext context = new TaskAttemptContextImpl(conf, new TaskAttemptID());
    this.initialize(fileSplit, context);
  }

  /**
   * Infers the geometry type and column indexes from the given user-friendly format
   * @param userFriendlyFormat
   */
  protected void setGeometryTypeAndColumnIndexes(String userFriendlyFormat) {
    detectGeometryTypeAndColumnIndexes(userFriendlyFormat);

    // Initialize the WKTParser if needed
    if (geometryType.equals("wkt"))
      this.wktParser = new WKTParser();
  }

  protected void detectGeometryTypeAndColumnIndexes(String userFriendlyFormat) {
    String[] args; // The arguments between the parentheses or an empty array if not parentheses
    int openParenthesis = userFriendlyFormat.indexOf('(');
    int closeParenthesis = userFriendlyFormat.indexOf(')', openParenthesis + 1);

    if (openParenthesis != -1) {
      geometryType = userFriendlyFormat.substring(0, openParenthesis).toLowerCase();
      args = userFriendlyFormat.substring(openParenthesis + 1, closeParenthesis).split(",");
    } else {
      geometryType = userFriendlyFormat.toLowerCase();
      args = new String[0];
    }
    // The indexes of the columns that contain the geometry
    int numCoords = 0;
    int skipFirst = 0;
    switch (this.geometryType) {
      case "point":
        // A two-dimensional point
        this.geometryType = "point";
        numCoords = 2;
        break;
      case "envelope":
        // A two-dimensional envelope
        this.geometryType = "envelope";
        numCoords = 4;
        break;
      case "pointk":
        // A k-dimensional point where the first integer indicates the number of dimensions
        this.geometryType = "point";
        numCoords = Integer.parseInt(args[0]);
        skipFirst = 1;
        break;
      case "envelopek":
        // A k-dimensional envelope where the first integer indicates the number of dimensions
        this.geometryType = "envelope";
        numCoords = Integer.parseInt(args[0]) * 2;
        skipFirst = 1;
        break;
      case "wkt":
        this.geometryType = "wkt";
        numCoords = 1;
        break;
      case "nogeom":
        this.geometryType = "nogeom";
        numCoords = 0;
        break;
    }
    this.coordinateColumns = new String[numCoords];
    System.arraycopy(args, skipFirst, this.coordinateColumns, 0, args.length - skipFirst);
  }

  /**
   * Returns true if the given user-friendly format is detected as a supported format.
   * @param userFriendlyFormat
   * @return
   */
  protected static boolean detect(String userFriendlyFormat) {
    String geometryType;
    int openParenthesis = userFriendlyFormat.indexOf('(');

    if (openParenthesis != -1) {
      geometryType = userFriendlyFormat.substring(0, openParenthesis).toLowerCase();
    } else {
      geometryType = userFriendlyFormat.toLowerCase();
    }
    // TODO Include these in a set to make it easier to test
    if (geometryType.equals("point")) {
      return true;
    } else if (geometryType.equals("envelope")) {
      return true;
    } else if (geometryType.equals("pointk")) {
      return true;
    } else if (geometryType.equals("envelopek")) {
      return true;
    } else if (geometryType.equals("wkt")) {
      return true;
    } else if (geometryType.equals("nogeom")) {
      return true;
    } else {
      // No valid input format detected
      return false;
    }
  }

  protected CSVFeature createValue() {
    CSVFeature feature = new CSVFeature();
    feature.fieldSeparator = (byte) fieldSeparator;
    feature.setQuoteCharacters(this.quotes);
    if (geometryType.equals("point")) {
      feature.setGeometry(new Point(this.coordinateColumns.length));
    } else if (this.geometryType.equals("envelope")) {
      feature.setGeometry(new Envelope(this.coordinateColumns.length/2));
    } else if (this.geometryType.equals("nogeom")) {
      feature.setGeometry(EmptyGeometry.instance);
    }
    return feature;
  }

  @Override
  public boolean nextKeyValue() throws IOException {
    // Create a new value if needed
    if (this.feature == null || immutable) {
      CSVFeature newFeature = createValue();
      if (this.feature != null) {
        // Copy header data from the old feature
        newFeature.copyFieldNames(this.feature);
      }
      this.feature = newFeature;
    }
    boolean recordExists = lineReader.nextKeyValue();
    if (!recordExists)
      return false;
    // If this is the first record, this contains the values from the header line
    List<String> headerValues = null;
    // Skip the first line if needed (the one at position 0)
    if (lineReader.getCurrentKey().get() == 0 && skipHeaderLine) {
      Text headerLine = lineReader.getCurrentValue();
      headerValues = new ArrayList<>();
      while (headerLine.getLength() > 0)
        headerValues.add(CSVFeature.deleteAttribute(headerLine, fieldSeparator, 0, quotes));
      recordExists = lineReader.nextKeyValue();
    }
    if (this.coordinateColumnIndexes == null) {
      // Initialize coordinate column indexes by either parsing the names as integer or locating them in the header
      coordinateColumnIndexes = new int[coordinateColumns.length];
      for (int $i = 0; $i < coordinateColumnIndexes.length; $i++) {
        if (coordinateColumns[$i] == null) {
          // If nothing is set by the user, assume a default value of 0 for the first one or an increment of one
          coordinateColumnIndexes[$i] = $i == 0 ? 0 : coordinateColumnIndexes[$i - 1] + 1;
        } else {
          try {
            // Try to parse it as integer
            coordinateColumnIndexes[$i] = Integer.parseInt(coordinateColumns[$i]);
          } catch (NumberFormatException e) {
            if (headerValues == null)
              throw new RuntimeException(
                  String.format("Attribute '%s' is not an integer and -skipheader is not set in file '%s'",
                  coordinateColumnIndexes[$i], filePath));
            // If it's not an integer, it could be a column name
            coordinateColumnIndexes[$i] = headerValues.indexOf(coordinateColumns[$i]);
            if (coordinateColumnIndexes[$i] == -1)
              throw new RuntimeException(String.format("Column '%s' not found in the header line of file '%s'",
                  coordinateColumns[$i], filePath));
          }
        }
      }
      // Adjust coordinate column indexes to be in the correct order to parse
      adjustCoordinateColumnIndexes(this.coordinateColumnIndexes, headerValues);
      if (headerValues != null)
        this.feature.setFieldNames(headerValues.toArray(new String[headerValues.size()]));
    }
    // Parse the next line into CSV and skip if it does not overlap the filter MBR
    while (recordExists) {
      Text line = lineReader.getCurrentValue();
      if (this.geometryType.equals("wkt")) {
        String wkt = CSVFeature.deleteAttribute(line, this.fieldSeparator, this.coordinateColumnIndexes[0], quotes);
        if (wkt == null) {
          IGeometry geom = feature.getGeometry();
          if (geom == null)
            feature.setGeometry(EmptyGeometry.instance);
          else
            geom.setEmpty();
        } else {
          try {
            feature.setGeometry(wktParser.parse(wkt, feature.getGeometry()));
          } catch (ParseException | TokenMgrError e) {
            throw new RuntimeException(String.format("Error parsing line '%s' wkt '%s' in file '%s'", line, wkt, filePath), e);
          }
        }
      } else if (this.geometryType.equals("point")) {
        Point p = (Point) feature.getGeometry();
        for (int iCoord = 0; iCoord < this.coordinateColumnIndexes.length; iCoord++) {
          String val = CSVFeature.deleteAttribute(line, this.fieldSeparator, this.coordinateColumnIndexes[iCoord], quotes);
          try {
            p.coords[iCoord] = (val == null || val.length() == 0) ? Double.NaN : Double.parseDouble(val);
          } catch (NumberFormatException e) {
            throw new RuntimeException(String.format("Error parsing dimension #%d column #%d, value '%s', text line '%s' in file '%s'",
                iCoord, this.coordinateColumnIndexes[iCoord], val, line.toString(), filePath), e);
          }
        }
      } else if (this.geometryType.equals("envelope")) {
        Envelope env = (Envelope) feature.getGeometry();
        for (int iCoord = 0; iCoord < this.coordinateColumnIndexes.length; iCoord++) {
          String val = CSVFeature.deleteAttribute(line, this.fieldSeparator, this.coordinateColumnIndexes[iCoord], quotes);
          try {
            double coordValue = (val == null || val.length() == 0) ? Double.NaN : Double.parseDouble(val);
            (iCoord < env.getCoordinateDimension() ? env.minCoord : env.maxCoord)[iCoord % env.getCoordinateDimension()] = coordValue;
          } catch (NumberFormatException e) {
            throw new RuntimeException(String.format("Error parsing dimension #%d column #%d, value '%s', text line '%s' in file '%s'",
                iCoord, this.coordinateColumnIndexes[iCoord], val, line.toString(), filePath), e);
          }
        }
      }
      if (filterMBR == null || feature.getGeometry().intersects(filterMBR)) {
        // A valid geometry that overlaps the filter MBR
        feature.setFieldValues(line.getBytes(), 0, line.getLength());
        return true;
      } else {
        // Skip to the next record
        recordExists = lineReader.nextKeyValue();
      }
    }
    return false;
  }

  /**
   * Adjust the coordinate column indexes so that they can be retrieved from a line in order.
   * If a coordinate index is followed by another index that appears after it, then this function
   * reduces the value of the second coordinate. For example, if the input is
   * [1, 2, 3], the output is [1, 1, 1], because removing attributes in this order from an input line
   * will produce the correct result. In other words, after removing attribute #1, then attribute #2 is now #1.
   * Similarly, after removing the second attribute, the third attribute also becomes at position #1.
   * On the other hand, if the input is [3, 2, 1], the output is also [3, 2, 1] because removing the first attribute
   * does not modify the position of the other two attributes because they appear earlier in the line.
   * @param coordinateColumnIndexes
   */
  protected static void adjustCoordinateColumnIndexes(int[] coordinateColumnIndexes, List<String> names) {
    for (int $i = 0; $i < coordinateColumnIndexes.length; $i++) {
      if (names != null && names.size() > coordinateColumnIndexes[$i])
        names.remove(coordinateColumnIndexes[$i]);
      for (int $j = $i + 1; $j < coordinateColumnIndexes.length; $j++) {
        if (coordinateColumnIndexes[$j] > coordinateColumnIndexes[$i])
          coordinateColumnIndexes[$j]--;
      }
    }
  }

  @Override
  public Envelope getCurrentKey() {
    return null;
  }

  @Override
  public CSVFeature getCurrentValue() {
    return feature;
  }

  @Override
  public float getProgress() throws IOException {
    return lineReader.getProgress();
  }

  @Override
  public void close() throws IOException {
    lineReader.close();
  }

  /**
   * Holds some meta information about columns that can be used to autodetect the format of an input shape
   */
  protected static class ColumnMetadata {
    enum ColumnType {Numeric, WKT, String};
    /**Name of the column or its index if the input file contains no names*/
    String name;
    /**The type of the column*/
    ColumnType type;
    /**Only for numeric columns, the range of values for that column*/
    double minValue, maxValue;

    public ColumnMetadata() {
      // Start with the most restrictive type
      this.type = ColumnType.Numeric;
      // Initialize the range to inverse infinite range
      this.minValue = Double.POSITIVE_INFINITY;
      this.maxValue = Double.NEGATIVE_INFINITY;
    }
  }

  @Override
  public boolean autoDetect(Configuration conf, String input) {
    // Detect by reading the first few lines of the input;
    try {
      Path inputPath = new Path(input);
      FileSystem fs = inputPath.getFileSystem(conf);
      FileStatus fileStatus = fs.getFileStatus(inputPath);
      if (fileStatus.isDirectory()) {
        FileStatus[] contents = fs.listStatus(inputPath, SpatialInputFormat.HiddenFileFilter);
        if (contents.length == 0)
          return false;
        fileStatus = contents[0];
      }
      long length = fileStatus.getLen();
      FileSplit split = new FileSplit(fileStatus.getPath(), 0, Math.min(length, 8192), new String[0]);
      lineReader.initialize(split, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
      List<String> sampleInput = new ArrayList<>();
      while (lineReader.nextKeyValue())
        sampleInput.add(lineReader.getCurrentValue().toString());
      // Use the sample lines to detect the geometry, the separator, and whether a header line exists or not

      // 1- Detect the field separator by trying the common ones and finding the one that gives a consistent
      // number of columns for all lines
      feature = new CSVFeature();
      List<Character> candidateSeparators;
      if (conf.get(FieldSeparator) != null) {
        // If the user already provided a field separator, stick to it
        char fieldSeparator = conf.get(FieldSeparator).charAt(0);
        candidateSeparators = new ArrayList<>(Arrays.asList(fieldSeparator));
      } else {
        // Consider all the common field separators
        candidateSeparators = new ArrayList<>(Arrays.asList(' ', ',', '\t', ':', ';'));
      }
      for (char candidateSeparator : candidateSeparators) {
        // Collect some metadata about the columns to use in auto-detection with and without a header line
        boolean useHeaderLine = conf.getBoolean(SkipHeader, false);
        do {
          // Collect column metadata
          ColumnMetadata[] columnsMetadata = collectColumnMetadata(sampleInput, candidateSeparator, useHeaderLine);
          if (columnsMetadata != null) {
            // Now let's see if we can infer the input format from the column metadata
            // Try to infer from the combination of column names and types
            int longitudeColumn = -1;
            int latitudeColumn = -1;
            int wktColumn = -1;
            for (int $iAtt = 0; $iAtt < columnsMetadata.length; $iAtt++) {
              ColumnMetadata columnMetadata = columnsMetadata[$iAtt];
              if (columnMetadata.type == ColumnMetadata.ColumnType.WKT)
                wktColumn = $iAtt;
              if (columnMetadata.type == ColumnMetadata.ColumnType.Numeric) {
                if (columnMetadata.name.toLowerCase().contains("latitude") ||
                    columnMetadata.name.equalsIgnoreCase("lat") ||
                    columnMetadata.name.equalsIgnoreCase("y"))
                  latitudeColumn = $iAtt;
                else if (columnMetadata.name.toLowerCase().contains("longitude") ||
                    columnMetadata.name.equalsIgnoreCase("long") ||
                    columnMetadata.name.equalsIgnoreCase("x"))
                  longitudeColumn = $iAtt;
                else if (longitudeColumn == -1)
                  longitudeColumn = $iAtt;
                else if (latitudeColumn == -1)
                  latitudeColumn = $iAtt;
              }
            }
            String detectedFormat = null;
            if (wktColumn != -1) {
              // Found a WKT column, use it
              detectedFormat = String.format("wkt(%d)", wktColumn);
            } else if (longitudeColumn != -1 && latitudeColumn != -1) {
              // Found two columns for longitude and latitude
              detectedFormat = String.format("point(%d,%d)", longitudeColumn, latitudeColumn);
            }
            if (detectedFormat != null) {
              conf.set(SpatialInputFormat.InputFormat, detectedFormat);
              conf.set(FieldSeparator, Character.toString(candidateSeparator));
              if (useHeaderLine)
                conf.setBoolean(SkipHeader, true);
              return true;
            }
          }
          useHeaderLine = !useHeaderLine;
        } while (useHeaderLine);
      }
      // Could not detect the input format with any of the candidate separators
      return false;
    } catch (IOException e) {
      // Could not open the file for autodetection
      LOG.warn(String.format("Could not open the input file '%s' for autodetection", input));
      return false;
    } finally {
      try {
        lineReader.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Collects some metadata about the columns in the given sample input to help in input format auto detection.
   * @param sampleLines
   * @param separator
   * @param useHeaderLine
   * @return
   * @throws IOException
   */
  private ColumnMetadata[] collectColumnMetadata(List<String> sampleLines, char separator, boolean useHeaderLine) throws IOException {
    feature.setFieldSeparator((byte) separator);
    feature.setFieldValues(sampleLines.get(0));
    int numColumns = feature.getNumAttributes();
    ColumnMetadata[] columnsMetadata = new ColumnMetadata[numColumns];
    for (int $iAtt = 0; $iAtt < numColumns; $iAtt++) {
      columnsMetadata[$iAtt] = new ColumnMetadata();
      columnsMetadata[$iAtt].name = Integer.toString($iAtt);
    }
    WKTParser parser = new WKTParser();
    for (int $iLine = 0; $iLine < sampleLines.size(); $iLine++) {
      feature.setFieldValues(sampleLines.get($iLine));
      // If the number of columns in all features is not the same, fail
      if (numColumns != feature.getNumAttributes())
        return null;
      for (int $iAtt = 0; $iAtt < numColumns; $iAtt++) {
        String value = feature.getAttributeValue($iAtt);
        // Empty values should be allowed for columns without demoting its status or affecting the ranges
        if (value == null || value.length() == 0)
          continue;
        ColumnMetadata columnMetata = columnsMetadata[$iAtt];
        if ($iLine == 0 && useHeaderLine) {
          // Treat first line differently if directed to use the header line
          columnsMetadata[$iAtt].name = value;
        } else if (columnMetata.type == ColumnMetadata.ColumnType.Numeric) {
          // Try to consider this column as numeric
          try {
            double numericValue = Double.parseDouble(value);
            columnMetata.minValue = Double.min(columnMetata.minValue, numericValue);
            columnMetata.maxValue = Double.max(columnMetata.maxValue, numericValue);
          } catch (NumberFormatException e) {
            // Move to the more relaxed format of WKT
            columnMetata.type = ColumnMetadata.ColumnType.WKT;
          }
        }
        if (columnMetata.type == ColumnMetadata.ColumnType.WKT) {
          // Try to parse it as a WKT object
          try {
            parser.parse(value, null);
          } catch (ParseException e) {
            // Not a valid WKT format. Demote it to a regular string
            columnMetata.type = ColumnMetadata.ColumnType.String;
          } catch (TokenMgrError e) {
            // Not a valid WKT format. Demote it to a regular string
            columnMetata.type = ColumnMetadata.ColumnType.String;
          }
        }
      }
    }
    return columnsMetadata;
  }
}
