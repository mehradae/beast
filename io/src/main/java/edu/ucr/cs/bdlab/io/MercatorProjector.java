/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;

/**
 * A wrapper class around any {@link FeatureReader} that projects all geometries to web mercator.
 */
public class MercatorProjector extends FeatureReader {

  /**The underlying feature reader*/
  final protected FeatureReader wrapped;

  public MercatorProjector(FeatureReader _wrapped) {
    this.wrapped = _wrapped;
  }

  public static final double MaxLatitudeRadians = 2 * Math.atan(Math.exp(Math.PI)) - Math.PI / 2;
  public static final double MaxLatitudeDegrees = Math.toDegrees(MaxLatitudeRadians);
  public static final Envelope WorldBoundaries = new Envelope(2, -180, -MaxLatitudeDegrees,
          +180, +MaxLatitudeDegrees);

  /**
   * Converts the given geometry (in-place) from world coordinates (latitude and longitude) to
   * the web Mercator projection.
   * @param geom the geometry to reproject. The geometry is changed in place.
   * @see <a href="https://en.wikipedia.org/wiki/Web_Mercator_projection">Web Mercator Projection</a>
   */
  public static IGeometry worldToMercator(IGeometry geom) {
    if (geom instanceof Point) {
      Point p = (Point) geom;
      p.coords[0] = 256.0 / 2.0 / Math.PI * (Math.toRadians(p.coords[0]) + Math.PI);
      p.coords[1] = 256.0 / 2.0 / Math.PI *
          (Math.PI - Math.log(Math.tan(Math.PI / 4.0 + Math.toRadians(p.coords[1]) / 2.0)));
    } else if (geom instanceof Envelope) {
      Envelope e = (Envelope) geom;
      e.minCoord[0] = Math.max(e.minCoord[0], -180.0);
      e.maxCoord[0] = Math.min(e.maxCoord[0], 180.0);
      e.minCoord[1] = Math.max(e.minCoord[1], -MaxLatitudeDegrees);
      e.maxCoord[1] = Math.min(e.maxCoord[1], MaxLatitudeDegrees);
      e.minCoord[0] = 256.0 / 2.0 / Math.PI * (Math.toRadians(e.minCoord[0]) + Math.PI);
      e.maxCoord[0] = 256.0 / 2.0 / Math.PI * (Math.toRadians(e.maxCoord[0]) + Math.PI);
      double maxy = 256.0 / 2.0 / Math.PI *
          (Math.PI - Math.log(Math.tan(Math.PI / 4.0 + Math.toRadians(e.minCoord[1]) / 2.0)));
      double miny = 256.0 / 2.0 / Math.PI *
          (Math.PI - Math.log(Math.tan(Math.PI / 4.0 + Math.toRadians(e.maxCoord[1]) / 2.0)));
      e.minCoord[1] = miny;
      e.maxCoord[1] = maxy;
    } else if (geom instanceof LineString2D) {
      // If the geometry partially goes outside the boundaries of the Mercator space, clip it
      Envelope mbr = new Envelope(2);
      geom.envelope(mbr);
      if (!WorldBoundaries.containsEnvelope(mbr)) {
        geom = geom.intersection(WorldBoundaries, geom);
      }
      LineString2D l = (LineString2D) geom;
      for (int $i = 0; $i < l.numPoints; $i++) {
        l.xs[$i] = 256.0 / 2.0 / Math.PI * (Math.toRadians(l.xs[$i]) + Math.PI);
        l.ys[$i] = 256.0 / 2.0 / Math.PI *
            (Math.PI - Math.log(Math.tan(Math.PI / 4.0 + Math.toRadians(l.ys[$i]) / 2.0)));
      }
    } else if (geom instanceof GeometryCollection) {
      GeometryCollection gc = (GeometryCollection) geom;
      for (int $i = 0; $i < gc.numGeometries(); $i++)
        gc.setGeometry($i, worldToMercator(gc.getGeometry($i)));
    } else if (geom instanceof EmptyGeometry) {
      // Do nothing. Skip
    } else {
      throw new RuntimeException("Unsupported geometry type " + geom.getClass());
    }
    return geom;
  }

  /**
   * Projects a geometry in-place from Web Mercator coordinates to world coordinates.
   * @param geom the geometry to project (changed in place)
   * @see <a href="https://en.wikipedia.org/wiki/Web_Mercator_projection">Web Mercator projection</a>
   */
  public static void mercatorToWorld(IGeometry geom) {
    if (geom instanceof Point) {
      Point p = (Point) geom;
      p.coords[0] = Math.toDegrees(p.coords[0] * Math.PI * 2.0 / 256.0 - Math.PI);
      p.coords[1] = Math.toDegrees(2.0 * (Math.atan(Math.exp((Math.PI - p.coords[1] * Math.PI * 2.0 / 256.0))) - Math.PI / 4.0));
    } else if (geom instanceof Envelope) {
      Envelope e = (Envelope) geom;
      e.minCoord[0] = Math.toDegrees(e.minCoord[0] * Math.PI * 2.0 / 256.0 - Math.PI);
      double maxy = Math.toDegrees(2.0 * (Math.atan(Math.exp((Math.PI - e.minCoord[1] * Math.PI * 2.0 / 256.0))) - Math.PI / 4.0));
      e.maxCoord[0] = Math.toDegrees(e.maxCoord[0] * Math.PI * 2.0 / 256.0 - Math.PI);
      double miny = Math.toDegrees(2.0 * (Math.atan(Math.exp((Math.PI - e.maxCoord[1] * Math.PI * 2.0 / 256.0))) - Math.PI / 4.0));
      e.minCoord[1] = miny;
      e.maxCoord[1] = maxy;
    } else if (geom instanceof LineString2D) {
      LineString2D l = (LineString2D) geom;
      for (int $i = 0; $i < l.numPoints; $i++) {
        l.xs[$i] = Math.toDegrees(l.xs[$i] * Math.PI * 2.0 / 256.0 - Math.PI);
        l.ys[$i] = Math.toDegrees(2.0 * (Math.atan(Math.exp((Math.PI - l.ys[$i] * Math.PI * 2.0 / 256.0))) - Math.PI / 4.0));
      }
    } else if (geom instanceof GeometryCollection) {
      GeometryCollection gc = (GeometryCollection) geom;
      for (int $i = 0; $i < gc.numGeometries(); $i++)
        worldToMercator(gc.getGeometry($i));
    } else if (geom instanceof EmptyGeometry) {
      // Do nothing. Skip
    } else {
      throw new RuntimeException("Unsupported geometry type " + geom.getType());
    }
  }

  @Override
  public void initialize(InputSplit inputSplit, TaskAttemptContext context) throws IOException, InterruptedException {
    wrapped.initialize(inputSplit, context);
  }

  @Override
  public boolean nextKeyValue() throws IOException, InterruptedException {
    if (!wrapped.nextKeyValue())
      return false;
    IFeature feature = wrapped.getCurrentValue();
    feature.setGeometry(worldToMercator(feature.getGeometry()));
    return true;
  }

  @Override
  public Envelope getCurrentKey() throws IOException, InterruptedException {
    return wrapped.getCurrentKey();
  }

  @Override
  public IFeature getCurrentValue() throws IOException, InterruptedException {
    return wrapped.getCurrentValue();
  }

  @Override
  public float getProgress() throws IOException, InterruptedException {
    return wrapped.getProgress();
  }

  @Override
  public void close() throws IOException {
    wrapped.close();
  }

}
