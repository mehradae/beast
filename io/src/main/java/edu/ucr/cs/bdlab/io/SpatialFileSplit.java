/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Extends the regular file split with envelope bounding function.
 */
public class SpatialFileSplit extends FileSplit {

  /**The index of the input*/
  protected int index;

  /**
   * The envelope of the underlying split.
   */
  protected final Envelope envelope = new Envelope();

  public SpatialFileSplit() {}

  public SpatialFileSplit(Path path, long start, long length, String[] hosts) {
    super(path, start, length, hosts);
    envelope.setCoordinateDimension(2);
    envelope.setInfinite();
  }

  public SpatialFileSplit(Path path, long start, long length, String[] hosts, String[] inMemoryHosts, int index) {
    super(path, start, length, hosts, inMemoryHosts);
    this.index = index;
    envelope.setCoordinateDimension(2);
    envelope.setInfinite();
  }

  public SpatialFileSplit(Path path, long start, long length, String[] hosts, String[] inMemoryHosts,
                          int index, Envelope mbb) {
    super(path, start, length, hosts, inMemoryHosts);
    this.index = index;
    envelope.setCoordinateDimension(mbb.getCoordinateDimension());
    envelope.set(mbb);
  }

  /**
   * Returns the minimum bounding box of this split. If unknown, an infinite envelope is returned.
   * @param e an existing envelope to reuse or {@code null} if a new envelope to be created.
   * @return
   */
  public Envelope getEnvelope(Envelope e) {
    if (e == null)
      e = new Envelope(envelope.getCoordinateDimension());
    e.set(envelope);
    return e;
  }

  /**
   * Returns the index of the input file if multiple inputs are passed.
   * @return
   */
  public int getIndex() {
    return index;
  }

  @Override
  public String toString() {
    StringBuilder str = new StringBuilder(super.toString());
    str.append(" input #");
    str.append(index);
    if (Double.isFinite(envelope.getSideLength(0))) {
      str.append("MBR: ");
      str.append(envelope.toString());
    } else {
      str.append("MBR not set");
    }
    return str.toString();
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(index);
    envelope.write(out);
    super.write(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    this.index = in.readInt();
    envelope.readFields(in);
    super.readFields(in);
  }
}
