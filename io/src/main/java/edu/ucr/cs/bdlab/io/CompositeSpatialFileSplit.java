/*
 * Copyright 2019 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.util.WritableExternalizable;
import org.apache.hadoop.mapreduce.InputSplit;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Similar to {@link org.apache.hadoop.mapreduce.lib.join.CompositeInputSplit} but for spatial splits.
 * It adds a functionality to retrieve number of internal splits which is, unfortunately, not provided by the
 * regular CompositeInputSplit
 */
public class CompositeSpatialFileSplit extends InputSplit implements WritableExternalizable {

  protected int numSplits;

  protected SpatialFileSplit[] splits;

  /**The summation of all lengths*/
  protected transient long totalLength;

  public CompositeSpatialFileSplit() {
  }

  public CompositeSpatialFileSplit(int size) {
    this.splits = new SpatialFileSplit[size];
    this.numSplits = 0;
    this.totalLength = 0;
  }

  @Override
  public long getLength() {
    return totalLength;
  }

  @Override
  public String[] getLocations() throws IOException, InterruptedException {
    Set<String> locations = new HashSet<>();
    for (InputSplit s : splits) {
      for (String l : s.getLocations())
        locations.add(l);
    }
    return locations.toArray(new String[locations.size()]);
  }

  public int getNumSplits() {
    return numSplits;
  }

  public SpatialFileSplit get(int i) {
    return splits[i];
  }


  public void add(SpatialFileSplit s) {
    this.totalLength += s.getLength();
    this.splits[numSplits++] = s;
  }

  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    str.append("CompositeFileSplit: ");
    for (int i = 0; i < numSplits; i++) {
      if (i > 0)
        str.append(" X ");
      str.append(splits[i].toString());
    }
    return str.toString();
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(numSplits);
    for (SpatialFileSplit s : splits)
      s.write(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    this.numSplits = in.readInt();
    this.totalLength = 0;
    if (splits == null || splits.length != numSplits) {
      splits = new SpatialFileSplit[numSplits];
      for (int i = 0; i < numSplits; i++)
        splits[i] = new SpatialFileSplit();
    }
    for (int i = 0; i < numSplits; i++) {
      splits[i].readFields(in);
      this.totalLength += splits[i].getLength();
    }
  }

}
