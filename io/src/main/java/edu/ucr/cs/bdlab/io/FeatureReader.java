package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.util.FileUtil;
import edu.ucr.cs.bdlab.util.OperationUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;
import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * An abstract class for all record readers that read features. It adds a new initialize method that can be initialized
 * from a {@link Configuration} rather than {@link org.apache.hadoop.mapreduce.TaskAttemptContext}.
 * This makes it accessible for single-machine algorithms.
 */
public abstract class FeatureReader extends RecordReader<Envelope, IFeature> implements Iterable<IFeature> {

  @Target(ElementType.TYPE)
  @Retention(RetentionPolicy.RUNTIME)
  public @interface Metadata {
    /**An optional description of this format*/
    String description() default "";

    /**An optional extension that can be used to automatically detect file format*/
    String extension() default "";

    /**A short name that can be assigned from command line*/
    String shortName();
  }

  /**
   * initialize from an input split and a configuration.
   * @param split
   * @param conf
   */
  public void initialize(InputSplit split, Configuration conf) throws IOException, InterruptedException {
    this.initialize(split, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
  }

  /**Maps the short name of feature readers to their classes*/
  public static Map<String, Class<? extends FeatureReader>> featureReaders = loadFeatureReaders();

  private static Map<String, Class<? extends FeatureReader>> loadFeatureReaders() {
    Map<String, Class<? extends FeatureReader>> featureReaders = new HashMap<>();
    List<String> readers = OperationUtil.readConfigurationXML("beast.xml").get("Readers");
    if (readers != null) {
      for (String readerClassName : readers) {
        try {
          Class<? extends FeatureReader> readerClass = Class.forName(readerClassName).asSubclass(FeatureReader.class);
          Metadata metadata = readerClass.getAnnotation(Metadata.class);
          featureReaders.put(metadata.shortName(), readerClass);
        } catch (ClassNotFoundException e) {
          throw new RuntimeException(String.format("Could not find FeatureReader class '%s'", readerClassName), e);
        }
      }
    }
    return featureReaders;
  }

  /**
   * Returns the metadata of this FeatureReader or {@code null} if the class is not annotated
   * @return
   */
  public FeatureReader.Metadata getMetadata() {
    return this.getClass().getAnnotation(Metadata.class);
  }

  /**
   * Returns the default file extension of the given input format
   * @param iformat
   * @return
   */
  public static String getFileExtension(String iformat) {
    Class<? extends FeatureReader> readerClass = SpatialInputFormat.getFeatureReaderClass(iformat);
    if (readerClass == null)
      return null;
    Metadata metadata = readerClass.getAnnotation(Metadata.class);
    if (metadata == null)
      return null;
    return metadata.extension();
  }

  /**
   * Try to autodetect a valid input format from the input path (e.g., extension). If a valid input format is detected,
   * the necessary parameters are added to the given configuration and a {@code true} is returned. Otherwise, if
   * no input format could be detected, the given configuration is left intact and a false is returned.
   * @param input
   * @return
   */
  public boolean autoDetect(Configuration conf, String input) {
    FeatureReader.Metadata metadata = this.getMetadata();
    boolean detected = false;
    try {
      Path inputPath = new Path(input);
      FileSystem fs = inputPath.getFileSystem(conf);
      FileStatus inputFileStatus = fs.getFileStatus(inputPath);
      if (inputFileStatus.isFile()) {
        detected = FileUtil.extensionMatches(inputPath.getName(), metadata.extension());
      } else if (inputFileStatus.isDirectory()) {
        // Test all files within the directory until one of them matches
        FileStatus[] files = fs.listStatus(inputPath, SpatialInputFormat.HiddenFileFilter);
        for (int $i = 0; $i < files.length && !detected; $i++) {
          detected = FileUtil.extensionMatches(files[$i].getPath().getName(), metadata.extension());
        }
      }
    } catch (IOException e) {
      // Just try with the file name
      detected = FileUtil.extensionMatches(input, metadata.extension());
    }
    if (detected)
      conf.set(SpatialInputFormat.InputFormat, metadata.shortName());
    return detected;
  }

  /**
   * Returns the coordinate reference system of geometries read by this reader.
   * @return
   */
  public CoordinateReferenceSystem getCRS() {
    // The default CRS is WGS 84 (EPSG 4326) but child classes can override this function
    try {
      return CRS.decode("EPSG:4326");
    } catch (FactoryException e) {
      return null;
    }
  }

  @Override
  public Iterator<IFeature> iterator() {
    return new FeatureIterator();
  }

  class FeatureIterator implements Iterator<IFeature> {

    FeatureIterator() {
    }

    @Override
    public boolean hasNext() {
      try {
        return nextKeyValue();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return false;
    }

    @Override
    public IFeature next() {
      try {
        return getCurrentValue();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return null;
    }
  }
}
