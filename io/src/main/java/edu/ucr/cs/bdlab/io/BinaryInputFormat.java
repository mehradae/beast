/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * An input format that concurrently reads two files
 */
public class BinaryInputFormat extends FileInputFormat<Envelope[], Iterable<? extends IFeature>[]> {

  /**
   * Creates a record reader that reads all underlying records
   */
  public static class BinaryRecordReader extends RecordReader<Envelope[], Iterable<? extends IFeature>[]> {

    /**An array of spatial readers, one for each input*/
    protected FeatureReader[] readers;

    /**This reader always returns a single record*/
    protected boolean valueReturned;

    protected Envelope[] key;

    public BinaryRecordReader(Class<? extends FeatureReader>[] readerClasses) throws IllegalAccessException, InstantiationException {
      readers = new FeatureReader[readerClasses.length];
      for (int iReader = 0; iReader < readerClasses.length; iReader++) {
        readers[iReader] = readerClasses[iReader].newInstance();
      }
      key = new Envelope[readerClasses.length];
    }

    @Override
    public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
      // Load the configuration into UserOptions to allow using array methods
      UserOptions opts = new UserOptions(context.getConfiguration());
      CompositeSpatialFileSplit compositeSplit = (CompositeSpatialFileSplit) split;
      for (int iReader = 0; iReader < readers.length; iReader++) {
        // Read configuration parameters for the given index and initialize the corresponding reader
        opts.setArrayPosition(iReader);
        SpatialFileSplit subSplit = compositeSplit.get(iReader);
        readers[iReader].initialize(subSplit, opts);
        key[iReader] = subSplit.getEnvelope(null);
      }
      valueReturned = false;
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
      if (valueReturned)
        return false;
      valueReturned = true;
      return true;
    }

    @Override
    public Envelope[] getCurrentKey() throws IOException, InterruptedException {
      return key;
    }

    @Override
    public Iterable<? extends IFeature>[] getCurrentValue() throws IOException, InterruptedException {
      return readers;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
      return readers[0].getProgress();
    }

    @Override
    public void close() throws IOException {
      for (FeatureReader reader : readers)
        reader.close();
    }
  }

  @Override
  public RecordReader<Envelope[], Iterable<? extends IFeature>[]> createRecordReader(
      InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
    CompositeSpatialFileSplit csplit = (CompositeSpatialFileSplit) split;
    UserOptions opts = new UserOptions(context.getConfiguration());
    int numInputs = csplit.getNumSplits();
    Class<? extends FeatureReader>[] readerClasses = new Class[numInputs];
    for (int i = 0; i < numInputs; i++) {
      opts.setArrayPosition(i);
      readerClasses[i] = SpatialInputFormat.getFeatureReaderClass(opts.get(SpatialInputFormat.InputFormat));
    }

    try {
      return new BinaryRecordReader(readerClasses);
    } catch (IllegalAccessException | InstantiationException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Returns composite input splits between all the inputs.
   * @param job
   * @return
   * @throws IOException
   */
  @Override
  public List<InputSplit> getSplits(JobContext job) throws IOException {
    Configuration conf = job.getConfiguration();
    // Create a list of splits for individual files
    List<InputSplit> unaryInputSplits = new SpatialInputFormat().getSplits(job);
    // To determine number of files, find the maximum index in the returned unary splits
    unaryInputSplits.sort(Comparator.comparingInt(is -> ((SpatialFileSplit) is).index));
    int numFiles = ((SpatialFileSplit)unaryInputSplits.get(unaryInputSplits.size() - 1)).index + 1;
    // The index that ends the range of splits for each file
    int[] indexOfFirstSplitPerFile = new int[numFiles + 1];
    indexOfFirstSplitPerFile[numFiles] = unaryInputSplits.size();
    int lastFileIndex = -1;
    int totalNumCompositeSplits = 1;
    for (int iSplit = 0; iSplit < unaryInputSplits.size(); iSplit++) {
      int currentFileIndex = ((SpatialFileSplit)unaryInputSplits.get(iSplit)).index;
      if (currentFileIndex != lastFileIndex) {
        indexOfFirstSplitPerFile[lastFileIndex = currentFileIndex] = iSplit;
        if (lastFileIndex > 0)
          totalNumCompositeSplits *= indexOfFirstSplitPerFile[lastFileIndex] - indexOfFirstSplitPerFile[lastFileIndex - 1];
      }
    }
    totalNumCompositeSplits *= indexOfFirstSplitPerFile[lastFileIndex + 1] - indexOfFirstSplitPerFile[lastFileIndex];

    BlockFilter blockFilter = null;
    Class<? extends BlockFilter> blockFilterClass = conf.getClass(SpatialInputFormat.BlockFilterClass,
        null, BlockFilter.class);
    if (blockFilterClass != null) {
      try {
        blockFilter = blockFilterClass.newInstance();
        blockFilter.setup(conf);
      } catch (InstantiationException | IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }

    // Create composite splits
    List<CompositeSpatialFileSplit> splits = new ArrayList(totalNumCompositeSplits);
    for (int iSplit = 0; iSplit < totalNumCompositeSplits; iSplit++) {
      CompositeSpatialFileSplit compositeSplit = new CompositeSpatialFileSplit(numFiles);
      int n = iSplit;
      for (int iFile = 0; iFile < numFiles; iFile++) {
        int numSplitsInFile = indexOfFirstSplitPerFile[iFile + 1] - indexOfFirstSplitPerFile[iFile];
        int unarySplitIndex = indexOfFirstSplitPerFile[iFile] + (n % numSplitsInFile);
        compositeSplit.add((SpatialFileSplit) unaryInputSplits.get(unarySplitIndex));
        n /= numSplitsInFile;
      }
      if (blockFilter == null || blockFilter.matchComposite(compositeSplit))
        splits.add(compositeSplit);
    }
    if (blockFilter != null)
      blockFilter.filterComposite(splits);
    return (List) splits;
  }


}
