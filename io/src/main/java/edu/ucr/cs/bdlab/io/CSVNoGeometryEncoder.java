package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.util.WritableExternalizable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.function.BiFunction;

/**
 * An encoder that produces only the attributes and ignores the geometry of the feature.
 */
public class CSVNoGeometryEncoder implements BiFunction<IFeature, StringBuilder, StringBuilder>, WritableExternalizable {

  /**Field separator*/
  protected char fieldSeparator;

  public CSVNoGeometryEncoder() {
    // Default constructor for the Externalizable and Writable interfaces
  }

  public CSVNoGeometryEncoder(char fieldSeparator) {
    this.fieldSeparator = fieldSeparator;
  }

  @Override
  public StringBuilder apply(IFeature feature, StringBuilder str) {
    if (str == null)
      str = new StringBuilder();
    int numCols = feature.getNumAttributes();
    int iAtt = 0; // The index of the next feature attribute to write
    for (int iCol = 0; iCol < numCols; iCol++) {
      if (iCol > 0)
        str.append(fieldSeparator);
      // Write the next feature attribute
      str.append(feature.getAttributeValue(iAtt++));
    }
    return str;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeChar(fieldSeparator);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    this.fieldSeparator = in.readChar();
  }
}
