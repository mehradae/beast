/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.util.DynamicArrays;
import org.apache.hadoop.io.Text;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * A feature is a geometry annotated with zero or many additional attributes. The additional attributes might be of
 * any type and are identified by either a string name or an integer position.
 */
public class CSVFeature implements IFeature {

  /**The geometry of this feature*/
  protected IGeometry geometry;

  /**
   * All the attributes of this feature. For efficiency, we store all the attributes in one array which contains
   * the string representation of all the attributes and are separated by a single-character field separator.
   * This design makes it easier to deserialize objects from text while efficiently storing them and reusing the same
   * memory without much object reallocation.
   * While storing the attributes as a list of character might be easier to support unicode characters, a byte array
   * saves memory and does not require much conversion overhead when reading from a stream or a string.
   */
  protected byte[] attributeValues;

  /**The number of bytes actually used in the {@link #attributeValues}*/
  protected int attributeValuesLength;

  /**The single character that separates the {@link #attributeValues}*/
  protected byte fieldSeparator;

  /**A string that contains quote characters*/
  protected String quoteCharacters;

  /**Names of fields (optional)*/
  protected String[] fieldNames;

  /**Map each field name to its position in the array*/
  private Map<String, Integer> fieldNameMap;

  public CSVFeature() {
    // Default constructor for {@link Externalizable} interface
    this.geometry = EmptyGeometry.instance;
  }

  public CSVFeature(IGeometry geometry) {
    // Initializes the feature with a default geometry
    this.geometry = geometry;
  }

  public CSVFeature(IGeometry geometry, String[] values, char fieldSeparator, String quoteCharacters) {
    this.geometry = geometry;
    this.fieldSeparator = (byte) fieldSeparator;
    this.quoteCharacters = quoteCharacters;
    StringBuffer str = new StringBuffer();
    for (int i$ = 0; i$ < values.length; i$++) {
      if (i$ > 0)
        str.append(fieldSeparator);
      str.append(values[i$]);
    }
    this.attributeValues = str.toString().getBytes();
    this.attributeValuesLength = this.attributeValues.length;
  }

  /**
   * Drops an attribute from a line that represents a CSV line
   * @param line the text line to delete the attribute from
   * @param fieldSeparator the field separator
   * @param fieldToDrop the index of the field to drop (0-based)
   * @param quotes characters to use as quoting characters. Every pair of characters denote start and end quotes
   * @return the contents of the field that has been deleted without field separators
   */
  public static String deleteAttribute(Text line, char fieldSeparator, int fieldToDrop, String quotes) {
    if (line.getLength() == 0)
      return null;
    // Delete the desired field from the given text line and return it as a string
    int i1 = 0;
    int iField = 0;
    byte[] bytes = line.getBytes();
    String fieldValue = null;
    do {
      // i1 is the offset of the first character in the field.
      // i2 iterates until reaching the first index after then last character in the attribute
      int i2 = i1;
      boolean quoted = false;
      if (quotes != null) {
        int iQuote = quotes.indexOf(bytes[i1]);
        if (iQuote >= 0 && (iQuote & 1) == 0) {
          quoted = true;
          // Quoted field. Skip until the closing quote
          byte endQuote = (byte) quotes.charAt(iQuote + 1);
          do
            i2++;
          while (i2 < line.getLength() && bytes[i2] != endQuote);
        }
      }
      // Non-quoted field, skip until the field separator
      while (i2 < line.getLength() && bytes[i2] != fieldSeparator)
        i2++;
      if (iField == fieldToDrop) {
        // Check an empty range
        if (i1 == i2) {
          // Remove the field separator
          System.arraycopy(bytes, i2 + 1, bytes, i1, line.getLength() - i2 - 1);
          line.set(bytes, 0, line.getLength() - (i2 - i1 + 1));
          return null;
        }
        // Parse and remove the desired field
        if (quoted)
          fieldValue = new String(bytes, i1 + 1, i2 - i1 - 2); // Quoted field
        else
          fieldValue = new String(bytes, i1, i2 - i1);
        if (i2 < line.getLength()) {
          System.arraycopy(bytes, i2 + 1, bytes, i1, line.getLength() - i2 - 1);
          line.set(bytes, 0, line.getLength() - (i2 - i1 + 1));
        } else if (i1 != 0) {
          line.set(bytes, 0, i1-1);
        } else {
          line.clear();
        }
      }
      i1 = i2+1;
      iField++;
    } while (i1 < line.getLength() && iField <= fieldToDrop);
    if (iField == fieldToDrop && i1 == line.getLength())
      fieldValue = "";
    return fieldValue;
  }

  public IGeometry getGeometry() {
    return geometry;
  }

  @Override
  public void setGeometry(IGeometry geometry) {
    this.geometry = geometry;
  }

  public void setFieldNames(String ... fieldNames) {
    this.fieldNames = fieldNames;
  }

  /**
   * Appends part of an array to the byte array
   * @param data
   * @param start
   * @param end
   */
  public void appendToAttributes(byte[] data, int start, int end) {
    attributeValues = DynamicArrays.expand(attributeValues, attributeValuesLength + (end - start));
    System.arraycopy(data, start, attributeValues, attributeValuesLength, end - start);
    attributeValuesLength += end - start;
  }

  public void setFieldSeparator(byte fieldSeparator) {
    this.fieldSeparator = fieldSeparator;
  }

  public byte getFieldSeparator() {
    return fieldSeparator;
  }

  /**
   * Removes all the data stored in this feature to prepare it store another feature.
   */
  public void clear() {
    attributeValuesLength = 0;
    if (geometry != null)
      geometry.setEmpty();
  }

  public void toText(StringBuilder out) {
    geometry.toWKT(out);
    if (attributeValuesLength > 0) {
      out.append(fieldSeparator);
      // TODO find a way to append attributes without creating a String object
      out.append(new String(attributeValues, 0, attributeValuesLength));
    }
  }

  @Override
  public String toString() {
    String str = geometry.toString();
    if (attributeValuesLength > 0)
      str += (char)fieldSeparator + new String(attributeValues, 0, attributeValuesLength);
    return str;
  }

  /**
   * Returns the string value of the attribute at the given index. A {@code null} is returned if the given attribute
   * cannot be found.
   * @param i
   * @return
   */
  @Override
  public String getAttributeValue(int i) {
    int iAttr = 0;
    int i1 = 0; // First byte in the current attribute
    int i2; // The terminator of the current attribute
    do {
      // Search for the terminator of the attribute
      int iQuote = quoteCharacters != null ? quoteCharacters.indexOf(attributeValues[i1]) : -1;
      boolean quoted = iQuote >= 0 && (iQuote & 1) == 0;
      if (quoted) {
        // Quoted field. Skip until the closing quote
        byte endQuote = (byte) quoteCharacters.charAt(iQuote + 1);
        // Quoted attribute, skip until the terminator
        i2 = DynamicArrays.indexOf(attributeValues, endQuote, i1+1, attributeValuesLength) + 1;
      } else {
        // Unquoted attribute, skip until the field separator
        i2 = DynamicArrays.indexOf(attributeValues, fieldSeparator, i1, attributeValuesLength);
      }

      if (iAttr == i) {
        if (i1 == i2)
          return null;
        if (quoted)
          return new String(attributeValues, i1 + 1, i2 - i1 - 2); // Skip quotes
        return new String(attributeValues, i1, i2 - i1);
      }
      iAttr++;
      i1 = i2 + 1;
    } while (i1 < attributeValuesLength);
    return null;
  }

  @Override
  public Object getAttributeValue(String name) {
    if (fieldNameMap == null)
      fieldNameMap = createFieldNameMap();
    Integer iField = fieldNameMap.get(name);
    return iField == null ? null : getAttributeValue(iField);
  }

  private Map<String, Integer> createFieldNameMap() {
    Map<String, Integer> fieldNameMap = new HashMap<>();
    for (int iField = 0; iField < fieldNames.length; iField++) {
      fieldNameMap.put(fieldNames[iField], iField);
    }
    return fieldNameMap;
  }

  @Override
  public int getNumAttributes() {
    if (fieldNames != null)
      return fieldNames.length;
    if (attributeValuesLength == 0)
      return  0;
    int count = 0;
    int i1 = 0; // The offset of the first character in the attribute
    while (i1 < attributeValuesLength) {
      // Skip until the end of the current attribute
      int i2 = i1;
      int iQuote = quoteCharacters != null? quoteCharacters.indexOf(attributeValues[i1]) : -1;
      if ((iQuote & 1) == 0) {
        // Skip the starting quote
        i2++;
        // Skip until the ending quote
        byte endCharacter = (byte) quoteCharacters.charAt(iQuote + 1);
        while (i2 < attributeValuesLength && attributeValues[i2] != endCharacter)
          i2++;
      }
      // Skip until the field separator
      while (i2 < attributeValuesLength && attributeValues[i2] != fieldSeparator)
        i2++;

      i1 = i2 + 1;
      count++;
    }
    if (attributeValues[attributeValuesLength - 1] == fieldSeparator)
      count++;
    return count;
  }

  @Override
  public String getAttributeName(int i) {
    return fieldNames == null ? null : fieldNames[i];
  }

  @Override
  public int getStorageSize() {
    return getGeometry().getGeometryStorageSize() + attributeValuesLength;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof CSVFeature))
      return false;
    CSVFeature f = (CSVFeature) obj;
    if (!this.geometry.equals(f.geometry))
      return false;
    if (this.attributeValuesLength != f.attributeValuesLength)
      return false;
    if (this.fieldSeparator != f.fieldSeparator)
      return false;
    if (this.attributeValues == f.attributeValues)
      return true;
    for (int i = 0; i < attributeValuesLength; i++)
      if (attributeValues[i] != f.attributeValues[i])
        return false;
    return true;
  }

  public void setFieldValues(String values) {
    byte[] bytes = values.getBytes();
    this.setFieldValues(bytes, 0, bytes.length);
  }

  public void setFieldValues(byte[] bytes, int offset, int length) {
    if (this.attributeValues == null || this.attributeValues.length < length)
      this.attributeValues = new byte[length];
    System.arraycopy(bytes, offset, this.attributeValues, 0, length);
    this.attributeValuesLength = length;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.write(fieldSeparator);
    // Write geometry
    out.writeInt(geometry.getType().ordinal());
    geometry.write(out);
    // Write attributes
    out.writeInt(attributeValuesLength);
    if (attributeValuesLength > 0)
      out.write(attributeValues, 0, attributeValuesLength);
    out.writeUTF(quoteCharacters == null? "" : quoteCharacters);
  }

  @Override
  public void writeExternal(ObjectOutput out) throws IOException {
    this.write(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    fieldSeparator = in.readByte();
    // Read geometry
    GeometryType geometryType = GeometryType.values()[in.readInt()];
    if (geometryType != geometry.getType()) {
      geometry = geometryType.createInstance();
    }
    geometry.readFields(in);
    // Read attribute
    attributeValuesLength = in.readInt();
    if (attributeValuesLength > 0) {
      attributeValues = DynamicArrays.expand(attributeValues, attributeValuesLength);
      in.readFully(attributeValues, 0, attributeValuesLength);
    }
    quoteCharacters = in.readUTF();
  }

  @Override
  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    this.readFields(in);
  }

  public void copyFieldNames(CSVFeature feature) {
    this.setFieldNames(feature.fieldNames);
    this.fieldNameMap = feature.fieldNameMap;
  }

  public static int[] createColumnIndexes(int numDimensionColumns, int ... columns) {
    if (columns.length == numDimensionColumns)
      return columns;
    int[] columnIndexes = new int[numDimensionColumns];
    System.arraycopy(columns, 0, columnIndexes, 0, columns.length);
    for (int $i = columns.length; $i < columnIndexes.length; $i++) {
      columnIndexes[$i] = $i == 0 ? 0 : (columnIndexes[$i-1] + 1);
    }
    return columnIndexes;
  }

  /**
   * Set quote characters that can surround attributes.
   * @param quoteCharacters
   */
  public void setQuoteCharacters(String quoteCharacters) {
    this.quoteCharacters = quoteCharacters;
  }
}
