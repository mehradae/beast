/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.util.IOUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads a DBase file as a sequence of features.
 * This class follows the file specifications at http://www.dbase.com/Knowledgebase/INT/db7_file_fmt.htm
 * and http://www.dbf2002.com/dbf-file-format.html
 */
public class DBFReader extends RecordReader<Object, IFeature> {

  public static class FieldDescriptor implements Externalizable, Writable {
    /** Field name in ASCII (zero-filled). */
    byte[] fieldName;

    /** Field type in ASCII (B, C, D, N, L, M, @, I, +, F, 0 or G). */
    short fieldType;

    /** Field length in binary. */
    short fieldLength;

    /** Field decimal count in binary. */
    byte decimalCount;

    /**
     * Production .MDX field flag; 0x01 if field has an index tag in the production .MDX file;
     * 0x00 if the field is not indexed.
     */
    byte mdxFlag;

    /** Next Autoincrement value, if the Field type is Autoincrement, 0x00 otherwise. */
    int nextAutoIncrementValue;

    public String getFieldName() {
      // Search for the null terminator
      int fieldNameLength = 0;
      while (fieldNameLength < this.fieldName.length && this.fieldName[fieldNameLength] != 0)
        fieldNameLength++;
      return new String(this.fieldName, 0, fieldNameLength);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
      write(out);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException {
      readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
      out.writeByte(fieldName.length);
      out.write(fieldName);
      out.writeByte(fieldType);
      out.writeByte(fieldLength);
      out.writeByte(decimalCount);
      out.writeByte(mdxFlag);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
      int fieldNameLength = in.readUnsignedByte();
      if (fieldName == null || fieldNameLength != fieldName.length)
        fieldName = new byte[fieldNameLength];
      in.readFully(fieldName);
      fieldType = in.readByte();
      fieldLength = (short) in.readUnsignedByte();
      decimalCount = (byte) in.readUnsignedByte();
      mdxFlag = in.readByte();
    }
  }

  /**
   * For dBase files version 7 or later, this class is used to store field properties
   */
  public static class FieldProperties {
    // TODO not yet supported
  }

  /**
   * Header of the DBF file.
   */
  public static class DBFHeader {
    /**
     * Valid dBASE for Windows table file, bits 0-2 indicate version number: 3 for dBASE Level 5, 4 for dBASE Level 7.
     * Bit 3 and bit 7 indicate presence of a dBASE IV or dBASE for Windows memo file; bits 4-6 indicate
     * the presence of a dBASE IV SQL table; bit 7 indicates the presence of any .DBT memo file
     * (either a dBASE III PLUS type or a dBASE IV or dBASE for Windows memo file).
     */
    byte version;

    /**
     * Date of last update; in YYMMDD format. Each byte contains the number as a binary. YY is added to a base
     * of 1900 decimal to determine the actual year. Therefore, YY has possible values from 0x00-0xFF, which allows
     * for a range from 1900-2155.
     */
    short dateLastUpdatedYY;
    short dateLastUpdatedMM;
    short dateLastUpdatedDD;

    /** Number of records in the table. (Least significant byte first.) */
    int numRecords;

    /** Number of bytes in the header. (Least significant byte first.) */
    int headerSize;

    /** Number of bytes in the record. (Least significant byte first.) */
    int recordSize;

    /** Flag indicating incomplete dBASE IV transaction. */
    byte incomplete;

    /** dBASE IV encryption flag. */
    byte encryption;

    /** Production MDX flag; 0x01 if a production .MDX file exists for this table; 0x00 if no .MDX file exists. */
    byte mdxFlag;

    /** Language driver ID. */
    byte driverID;

    /** Language driver name */
    final byte[] driverName = new byte[32];

    /** Field Descriptor Array */
    FieldDescriptor[] fieldDescriptors;

    /** Field Properties Structure */
    FieldProperties[] fieldProperties;
  }

  /**The header of the current file*/
  protected DBFHeader header;

  /**Total size of a record in bytes*/
  protected int recordSize;

  /**An array to hold the values of all fields of one record*/
  protected byte[] recordValue;

  /**A mutable value*/
  protected DBFFeature feature;

  /**The input stream to the DBF file*/
  protected DataInputStream in;

  /**This flag is raised when the EOF marker is reached*/
  protected boolean eofReached;

  /**The index of the current record one-based. Used to calculate the progress in an easy way.*/
  protected int iRecord;

  /**A flag that indicates that we should create a new object for each record to work with Spark*/
  protected boolean immutableObjects;

  @Override
  public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
    this.initialize(((FileSplit)inputSplit).getPath(), taskAttemptContext.getConfiguration());
  }

  public void initialize(Path dbfPath, Configuration conf) throws IOException {
    FileSystem fs = dbfPath.getFileSystem(conf);
    initialize(fs.open(dbfPath), conf);
  }

  public void initialize(DataInputStream in, Configuration conf) throws IOException {
    this.immutableObjects = conf.getBoolean(SpatialInputFormat.ImmutableObjects, false);
    this.in = in;
    this.header = new DBFHeader();
    readHeader(this.in, header);
    readFieldDescriptors(this.in, header);
    if ((header.version & 0x7) == 4) {
      readFieldProperties(this.in, header);
    }
    iRecord = 0;
    eofReached = false;
    this.feature = new DBFFeature(header.fieldDescriptors);
    this.recordValue = new byte[this.recordSize];
  }

  private void readFieldProperties(DataInputStream in, DBFHeader header) {
    throw new RuntimeException("Not supported yet");
  }

  /**
   * Read the first part of the header before the field descriptors
   * @param in
   * @param header
   * @throws IOException
   */
  protected void readHeader(DataInputStream in, DBFHeader header) throws IOException {
    header.version = in.readByte();
    header.dateLastUpdatedYY = (short) in.readUnsignedByte();
    header.dateLastUpdatedMM = (short) in.readUnsignedByte();
    header.dateLastUpdatedDD = (short) in.readUnsignedByte();
    header.numRecords = IOUtil.readIntLittleEndian(in);
    header.headerSize = IOUtil.readShortLittleEndian(in);
    header.recordSize = IOUtil.readShortLittleEndian(in);
    in.skipBytes(2); // Reserved; filled with zeros.
    header.incomplete = in.readByte();
    header.encryption = in.readByte();
    in.skipBytes(12); // Reserved for multi-user processing.
    header.mdxFlag = in.readByte();
    header.driverID = in.readByte();
    in.skipBytes(2); // Reserved; filled with zeros.
    if ((header.version & 0x7) == 4) {
      // dBase version 7
      in.read(header.driverName);
      in.skipBytes(4); // Reserved
    }
  }

  protected void readFieldDescriptors(DataInputStream in, DBFHeader header) throws IOException {
    byte terminator = in.readByte();
    List<FieldDescriptor> descriptors = new ArrayList<>();
    recordSize = 0;
    while (terminator != 0x0D) {
      // The terminator is not actually a terminator; it is the first byte in the first attribute
      FieldDescriptor descriptor = new FieldDescriptor();
      if ((header.version & 0x7) == 4) {
        // dBase version 7
        descriptor.fieldName = new byte[32];
        descriptor.fieldName[0] = terminator;
        in.read(descriptor.fieldName, 1, descriptor.fieldName.length - 1);
        descriptor.fieldType = in.readByte();
        descriptor.fieldLength = (short) in.readUnsignedByte();
        descriptor.decimalCount = (byte) in.readUnsignedByte();
        in.skipBytes(2); // Reserved.
        descriptor.mdxFlag = in.readByte();
        in.skipBytes(2); // Reserved.
        descriptor.nextAutoIncrementValue = IOUtil.readIntLittleEndian(in);
        in.skipBytes(4); // Reserved.
      } else {
        // dBase older than version 7
        descriptor.fieldName = new byte[11];
        descriptor.fieldName[0] = terminator;
        in.read(descriptor.fieldName, 1, descriptor.fieldName.length - 1);
        descriptor.fieldType = in.readByte();
        in.skipBytes(4);
        descriptor.fieldLength = (short) in.readUnsignedByte();
        descriptor.decimalCount = (byte) in.readUnsignedByte();
        in.skipBytes(13);
        descriptor.mdxFlag = in.readByte();
      }

      recordSize += descriptor.fieldLength;
      descriptors.add(descriptor);
      terminator = in.readByte();
    }
    header.fieldDescriptors = descriptors.toArray(new FieldDescriptor[descriptors.size()]);
  }

  public static final byte EOFMarker = 0x1A;
  public static final byte ValidRecordMarker = ' ';
  public static final byte DeletedRecordMarker = '*';

  @Override
  public boolean nextKeyValue() throws IOException, InterruptedException {
    if (eofReached)
      return false;
    while (true) {
      byte marker = in.readByte();
      iRecord++;
      if (marker == EOFMarker) {
        eofReached = true;
        return false;
      } else if (marker == ValidRecordMarker) {
        // A valid record. Read it.
        in.readFully(this.recordValue);
        if (immutableObjects)
          this.feature = new DBFFeature(header.fieldDescriptors);
        feature.setFieldValues(recordValue);
        return true;
      } else if (marker == DeletedRecordMarker) {
        // A deleted record. Skip over it
        in.readFully(this.recordValue);
      } else {
        throw new RuntimeException("Error parsing DBF file. Invalid marker " + marker);
      }
    }
  }

  @Override
  public Object getCurrentKey() throws IOException, InterruptedException {
    return null;
  }

  @Override
  public IFeature getCurrentValue() throws IOException, InterruptedException {
    return feature;
  }

  @Override
  public float getProgress() throws IOException, InterruptedException {
    return (float)iRecord / header.numRecords;
  }

  @Override
  public void close() throws IOException {
    if (in != null) {
      in.close();
      in = null;
    }
  }
}
