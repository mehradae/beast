/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.util.FileUtil;
import edu.ucr.cs.bdlab.util.IOUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A record writer that writes geometries as shapes. The keys are ignored and the values (geometries) are written
 * to the output. This class writes the shapefile (.shp) and the index file (.shx) only. It should be combined with
 * a {@link DBFWriter} that writes the associated .dbf file that completes the minimal required structure
 * for the shapefile.
 */
public class ShapefileGeometryWriter extends RecordWriter<Object, IGeometry> {

  /**The path of the desired shapefile (.shp)*/
  protected Path shpPath;

  /**The configuration*/
  protected Configuration conf;

  /**The temporary shapefile used to write the records locally before writing the final .shp file to HDFS*/
  protected File tempShpFile;

  /**The output stream that writes to the temporary shapefile (.shp)*/
  protected DataOutputStream tempShpOut;

  /**The temporary shape index file used to write the records locally before writing the final .shx file to HDFS*/
  protected File tempShxFile;

  /**The output stream that writes to the temporary shape index file (.shx)*/
  protected DataOutputStream tempShxOut;

  /**The type of the first shape written to the shapefile*/
  protected int shapeType;

  /**Current offset of the shapefile assuming that a 100-byte header has been written*/
  protected int currentOffset;

  /**An auto-increment counter for record number*/
  protected int nextRecordNumber;

  /**The minimum bounding rectangle (MBR) of the input*/
  protected Envelope fileMBR;

  /**A temporary object to hold the MBR of a single shape*/
  protected Envelope shapeMBR;

  /**
   * Prepares the record writer to write to the given path. The given path is assumed to be for the shapefile (.shp).
   * This class will write the shapefile (.shp) and the corresponding index file (.shx) as well. The .shx file will
   * have the same name and path but a different extension (i.e., .shp is replaced with .shx).
   * @param shpPath the path of the .shp file
   * @param conf
   */
  public void initialize(Path shpPath, Configuration conf) throws IOException {
    this.shpPath = shpPath;
    this.conf = conf;
    // HDFS does not allow random updates which makes it impossible to write the file header unless we know the total
    // number of records. To overcome this problem, we write the contents to a temporary file and write the actual
    // shapefile only after the file is finalized.
    tempShpFile = File.createTempFile(shpPath.getName(), ".shp.tmp");
    tempShpOut = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(tempShpFile)));
    tempShpFile.deleteOnExit();

    tempShxFile = File.createTempFile(shpPath.getName(), ".shx.tmp");
    tempShxOut = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(tempShxFile)));
    tempShxFile.deleteOnExit();

    // Initially, invalidate the shape type
    shapeType = -1;
    fileMBR = new Envelope(2, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY,
        Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
    currentOffset = 100; // Assuming that the 100-byte header will be written
    nextRecordNumber = 1; // Record numbers start at 1
    shapeMBR = new Envelope(2); // As for now, this class only support 2D geometries
  }

  @Override
  public void write(Object key, IGeometry value) throws IOException {

    GeometryType geometryType = value.getType();
    if (geometryType == GeometryType.GEOMETRYCOLLECTION) {
      // Geometry collections are not supported in Shapefile. We flatten it to write all its contents.
      for (IGeometry geometry : (GeometryCollection)value) {
        write(key, geometry);
      }
      return;
    }

    if (shapeType == -1) {
      // First geometry, record the geometry type
      switch (geometryType) {
        case EMPTY:
          shapeType = ShapefileGeometryReader.NullShape;
          break;
        case POINT:
          shapeType = ShapefileGeometryReader.PointShape;
          break;
        case ENVELOPE:
        case POLYGON:
        case MULTIPOLYGON:
          shapeType = ShapefileGeometryReader.PolygonShape;
          break;
        case LINESTRING:
        case MULTILINESTRING:
          shapeType = ShapefileGeometryReader.PolylineShape;
          break;
        default:
          throw new RuntimeException("Unsupported geometry type " + value.getType());
      }
    }

    tempShpOut.writeInt(nextRecordNumber++);
    tempShxOut.writeInt(currentOffset / 2);
    // The content length of this record in bytes. Initially, 4 bytes for the shape type
    int contentLength = 4;

    // Expand the file MBR
    fileMBR.merge(value);
    // Write the record to the shape file and the index to the index file
    switch (value.getType()) {
      case EMPTY:
        contentLength += 8 * 2;
        tempShpOut.writeInt(contentLength / 2);
        IOUtil.writeIntLittleEndian(tempShpOut, ShapefileGeometryReader.NullShape);
        break;
      case POINT:
        contentLength += 8 * 2;
        tempShpOut.writeInt(contentLength / 2);
        IOUtil.writeIntLittleEndian(tempShpOut, ShapefileGeometryReader.PointShape);
        Point p = (Point) value;
        IOUtil.writeDoubleLittleEndian(tempShpOut, p.coords[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, p.coords[1]);
        break;
      case ENVELOPE:
        Envelope envelope = (Envelope) value;
        // Box (4 doubles) + numParts (int) + numPoints (int) + parts (one entry int) + 5 points (2 doubles each)
        contentLength += 8 * 4 + 4 + 4 + 4 + 8 * 5 * 2;
        tempShpOut.writeInt(contentLength / 2);
        IOUtil.writeIntLittleEndian(tempShpOut, ShapefileGeometryReader.PolygonShape); // Shape type
        // Write MBR
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.minCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.minCoord[1]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.maxCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.maxCoord[1]);
        // Number of parts (1)
        IOUtil.writeIntLittleEndian(tempShpOut, 1);
        // Number of points (5)
        IOUtil.writeIntLittleEndian(tempShpOut, 5);
        // Only one part and stars at 0
        IOUtil.writeIntLittleEndian(tempShpOut, 0);
        // Write the five points in CW order
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.minCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.minCoord[1]);

        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.minCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.maxCoord[1]);

        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.maxCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.maxCoord[1]);

        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.maxCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.minCoord[1]);

        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.minCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, envelope.minCoord[1]);
        break;
      case LINESTRING:
        LineString2D linestring = (LineString2D) value;
        linestring.envelope(shapeMBR);
        // MBR (4 doubles) + num parts + num points + parts + points
        contentLength += 8 * 4 + 4 + 4 + 4 + linestring.numPoints * 8 * 2;
        tempShpOut.writeInt(contentLength / 2);
        IOUtil.writeIntLittleEndian(tempShpOut, ShapefileGeometryReader.PolylineShape); // Shape type
        // Shape MBR
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.minCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.minCoord[1]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.maxCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.maxCoord[1]);
        IOUtil.writeIntLittleEndian(tempShpOut, 1); // Number of parts (always one for a LineString)
        IOUtil.writeIntLittleEndian(tempShpOut, linestring.numPoints); // Number of points
        IOUtil.writeIntLittleEndian(tempShpOut, 0); // With one part, the offset is always zero
        for (int iPoint = 0; iPoint < linestring.numPoints; iPoint++) {
          IOUtil.writeDoubleLittleEndian(tempShpOut, linestring.xs[iPoint]);
          IOUtil.writeDoubleLittleEndian(tempShpOut, linestring.ys[iPoint]);
        }
        break;
      case MULTILINESTRING:
        MultiLineString2D multilinestring = (MultiLineString2D) value;
        multilinestring.envelope(shapeMBR);
        // MBR (4 doubles) + num parts + num points + parts + points
        contentLength += 8 * 4 + 4 + 4 + multilinestring.numLineStrings * 4 + multilinestring.numPoints * 8 * 2;
        tempShpOut.writeInt(contentLength / 2);
        IOUtil.writeIntLittleEndian(tempShpOut, ShapefileGeometryReader.PolylineShape); // Shape type
        // Shape MBR
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.minCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.minCoord[1]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.maxCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.maxCoord[1]);
        IOUtil.writeIntLittleEndian(tempShpOut, multilinestring.numLineStrings); // Number of parts
        IOUtil.writeIntLittleEndian(tempShpOut, multilinestring.numPoints); // Number of points
        for (int iPart = 0; iPart < multilinestring.numLineStrings; iPart++) {
          IOUtil.writeIntLittleEndian(tempShpOut, multilinestring.firstPointInLineString[iPart]);
        }
        for (int iPoint = 0; iPoint < multilinestring.numPoints; iPoint++) {
          IOUtil.writeDoubleLittleEndian(tempShpOut, multilinestring.xs[iPoint]);
          IOUtil.writeDoubleLittleEndian(tempShpOut, multilinestring.ys[iPoint]);
        }
        break;
      case POLYGON:
      case MULTIPOLYGON:
        Polygon2D polygon = (Polygon2D) value;
        polygon.envelope(shapeMBR);
        // MBR (4 doubles) + num parts + num points + parts + points
        // Notice that each ring will have the first point repeated at the end and we need to accommodate for this
        contentLength += 8 * 4 + 4 + 4 + polygon.numRings * 4 + (polygon.numPoints + polygon.numRings) * 8 * 2;
        tempShpOut.writeInt(contentLength / 2);
        IOUtil.writeIntLittleEndian(tempShpOut, ShapefileGeometryReader.PolygonShape); // Shape type
        // Shape MBR
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.minCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.minCoord[1]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.maxCoord[0]);
        IOUtil.writeDoubleLittleEndian(tempShpOut, shapeMBR.maxCoord[1]);
        IOUtil.writeIntLittleEndian(tempShpOut, polygon.numRings); // Number of parts
        IOUtil.writeIntLittleEndian(tempShpOut, polygon.numPoints + polygon.numRings); // Number of points (each ring has a repeated point)
        for (int iPart = 0; iPart < polygon.numRings; iPart++)
          IOUtil.writeIntLittleEndian(tempShpOut, polygon.firstPointInRing[iPart] + iPart);
        // TODO make sure that a hole is written in CCW order while the outer ring is written in CW order
        for (int iPart = 0; iPart < polygon.numRings; iPart++) {
          int firstPoint = polygon.firstPointInRing[iPart ];
          int lastPoint = iPart == (polygon.numRings - 1) ? polygon.numPoints : polygon.firstPointInRing[iPart + 1];
          for (int iPoint = firstPoint; iPoint < lastPoint; iPoint++) {
            IOUtil.writeDoubleLittleEndian(tempShpOut, polygon.xs[iPoint]);
            IOUtil.writeDoubleLittleEndian(tempShpOut, polygon.ys[iPoint]);
          }
          // Repeat the first point to close the polygon
          IOUtil.writeDoubleLittleEndian(tempShpOut, polygon.xs[firstPoint]);
          IOUtil.writeDoubleLittleEndian(tempShpOut, polygon.ys[firstPoint]);
        }
        break;
      default:
        throw new RuntimeException("Unsupported shape type :" + value.getType());
    }
    currentOffset += 8 + contentLength;
    tempShxOut.writeInt(contentLength / 2);
  }

  /**
   * Get the current size of the .shp file
   * @return
   */
  public long getCurrentSize() {
    return currentOffset;
  }

  @Override
  public void close(TaskAttemptContext context) throws IOException {
    // Close the temporary file
    tempShpOut.close();
    tempShxOut.close();
    // Write the final shape and index files
    FileSystem fs = shpPath.getFileSystem(conf);
    FSDataOutputStream shpOut = fs.create(shpPath);
    ShapefileGeometryReader.Header header = new ShapefileGeometryReader.Header();
    header.fileLength = (int) ((100 + tempShpFile.length()) / 2);
    header.shapeType = shapeType;
    header.version = 1000;
    header.xmin = fileMBR.minCoord[0];
    header.ymin = fileMBR.minCoord[1];
    header.xmax = fileMBR.maxCoord[0];
    header.ymax = fileMBR.maxCoord[1];
    header.write(shpOut);
    // Write the file contents
    InputStream tempShpIn = new FileInputStream(tempShpFile);
    IOUtils.copyBytes(tempShpIn, shpOut, 16 * 1024);
    tempShpIn.close();
    shpOut.close();


    // Write the shape index file
    Path shxPath = new Path(shpPath.getParent(), FileUtil.replaceExtension(shpPath.getName(), "shx"));
    FSDataOutputStream shxOut = fs.create(shxPath);
    header.fileLength = (int) ((100 + tempShxFile.length()) / 2);
    header.write(shxOut);
    // Write the file contents
    InputStream tempShxIn = new FileInputStream(tempShxFile);
    IOUtils.copyBytes(tempShxIn, shxOut, 16 * 1024);
    tempShxIn.close();
    shxOut.close();

    // Delete the temporary file even though we set them up to be deleted on exit for early clean up
    tempShpFile.delete();
    tempShxFile.delete();
  }
}
