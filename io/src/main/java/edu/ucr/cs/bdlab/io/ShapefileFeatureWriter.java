/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.util.CounterOutputStream;
import edu.ucr.cs.bdlab.util.FileUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Writes {@link IFeature} values in a shapefile format which consists of mainly three files, .shp, .shx, and .dbf files.
 */
@FeatureWriter.Metadata(extension = ".shp", shortName = "shapefile")
public class ShapefileFeatureWriter extends FeatureWriter {

  /**Writes the geometry part of the output*/
  protected ShapefileGeometryWriter shpWriter = new ShapefileGeometryWriter();

  /**Writes the database (DBF) file*/
  protected DBFWriter dbfWriter = new DBFWriter();

  /**True when this is used to estimate the size only*/
  protected boolean counterMode;

  /**
   * Initializes the write on the given path. The path points to the main (.shp) file. The .shx and .dbf files are
   * written at the same path and the same filename but different extensions.
   * @param shpPath
   * @param conf
   */
  @Override
  public void initialize(Path shpPath, Configuration conf) throws IOException {
    shpWriter.initialize(shpPath, conf);
    Path dbfPath = new Path(shpPath.getParent(), FileUtil.replaceExtension(shpPath.getName(), "dbf"));
    dbfWriter.initialize(dbfPath, conf);
  }

  @Override
  public void initialize(OutputStream out, Configuration conf) {
    if (out instanceof CounterOutputStream) {
      counterMode = true;
      return;
    }
    // TODO implement this by writing a ZIP file that combines all the files
    throw new RuntimeException("Not implemented!");
  }

  @Override
  public void write(Object dummy, IFeature iFeature) throws IOException, InterruptedException {
    shpWriter.write(dummy, iFeature.getGeometry());
    dbfWriter.write(dummy, iFeature);
  }

  @Override
  public int estimateSize(IFeature f) {
    // Binary storage size + MBR + record id and length + record in index file
    return f.getStorageSize() + 4 * 8 + 8 + 8;
  }

  @Override
  public void close(TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
    if (counterMode) {
      // This happens when the writer is used to estimate the output size but not writing any actual records
      return;
    }

    shpWriter.close(taskAttemptContext);
    dbfWriter.close(taskAttemptContext);
  }

  public long getShapefileSize() {
    return shpWriter.getCurrentSize();
  }
}
