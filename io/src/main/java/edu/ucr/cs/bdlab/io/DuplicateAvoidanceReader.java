package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.util.LineReader;

import java.io.IOException;
import java.util.Iterator;

/**
 * A wrapper class around any {@link FeatureReader} to avoid duplicate records.
 * This ensures that duplicate records are not reported twice when the data is partitioned using
 * a disjoint (replicate) index. This class uses the reference point technique which assigns a rectangle to the
 * reader (region of interest) and only reports the records that have a top-left corner (reference point) inside the
 * region of interest.
 */
public class DuplicateAvoidanceReader extends FeatureReader {

  /**The underlying feature reader*/
  final protected FeatureReader wrapped;

  /**The region in which the reference point should lie*/
  final protected Envelope areaOfInterest = new Envelope();

  /**A temporary envelope that holds the MBR of records*/
  final transient protected Envelope recordMBR = new Envelope();

  /**The search range to compute the reference point correctly*/
  final protected Envelope filterMBR = new Envelope();

  /**Path to the master file*/
  final protected Path masterFilePath;

  public DuplicateAvoidanceReader(FeatureReader _wrapped, Path masterFilePath) {
    this.wrapped = _wrapped;
    this.masterFilePath = masterFilePath;
  }

  public void setAreaOfInterest(Envelope aoi) {
    this.areaOfInterest.setCoordinateDimension(aoi.getCoordinateDimension());
    this.areaOfInterest.set(aoi.minCoord, aoi.maxCoord);
  }

  @Override
  public Iterator<IFeature> iterator() {
    return wrapped.iterator();
  }

  @Override
  public void initialize(InputSplit inputSplit, TaskAttemptContext context) throws IOException, InterruptedException {
    wrapped.initialize(inputSplit, context);
    Configuration conf = context.getConfiguration();
    FileSystem fileSystem = masterFilePath.getFileSystem(conf);
    FileSplit fileSplit = (FileSplit) inputSplit;
    if (fileSplit.getPath().getName().equals(masterFilePath.getName())) {
      // If the input file is the master file, we should not try to apply any duplicate avoidance
      Path fullPath1 = fileSplit.getPath().makeQualified(fileSystem.getUri(), fileSystem.getWorkingDirectory());
      Path fullPath2 = masterFilePath.makeQualified(fileSystem.getUri(), fileSystem.getWorkingDirectory());
      if (fullPath1.equals(fullPath2)) {
        // Set the filter MBR to cover the entire region
        for (int $d = 0; $d < filterMBR.getCoordinateDimension(); $d++) {
          filterMBR.minCoord[$d] = Double.NEGATIVE_INFINITY;
          filterMBR.maxCoord[$d] = Double.POSITIVE_INFINITY;
        }
        return;
      }
    }

    // Read the MBR from the master file
    LineReader reader = new LineReader(fileSystem.open(masterFilePath));
    try {
      Text line = new Text();
      boolean found = false;
      while (!found && reader.readLine(line) > 0) {
        if (line.find(fileSplit.getPath().getName()) >= 0) {
          found = true;
          // Found the line that corresponds to the current split
          String[] parts = line.toString().split("\t");
          int numDimensions = (parts.length - 5) / 2;
          areaOfInterest.setCoordinateDimension(numDimensions);
          for (int $d = 0; $d < numDimensions; $d++) {
            areaOfInterest.minCoord[$d] = Double.parseDouble(parts[5 + $d]);
            areaOfInterest.maxCoord[$d] = Double.parseDouble(parts[5 + numDimensions + $d]);
          }
        }
      }

      if (!found)
        throw new RuntimeException("Could not find the master file entry for the split " + inputSplit);
    } finally {
      reader.close();
    }
    // Retrieve filter MBR if exists
    if (conf.get(SpatialInputFormat.FilterMBR) != null) {
      CSVEnvelopeDecoder.instance.apply(conf.get(SpatialInputFormat.FilterMBR), filterMBR);
    } else {
      filterMBR.setEmpty();
    }
  }

  @Override
  public boolean nextKeyValue() throws IOException, InterruptedException {
    do {
      if (!wrapped.nextKeyValue())
        return false;
      // Check for duplicate avoidance
      // TODO reuse the MBR from the underlying record reader to avoid recomputing it
      IGeometry wrappedGeom = wrapped.getCurrentValue().getGeometry();
      if (recordMBR.getCoordinateDimension() != wrappedGeom.getCoordinateDimension())
        recordMBR.setCoordinateDimension(wrappedGeom.getCoordinateDimension());
      wrappedGeom.envelope(recordMBR);
      boolean match = true;
      if (filterMBR.isEmpty() && !recordMBR.isEmpty()) {
        // Check the top-left corner of the record against the areaOfInterest
        for (int $d = 0; match && $d < areaOfInterest.getCoordinateDimension(); $d++) {
          double coord = recordMBR.minCoord[$d];
          match = coord >= areaOfInterest.minCoord[$d] && coord < areaOfInterest.maxCoord[$d];
        }
      } else if (!recordMBR.isEmpty()) {
        for (int $d = 0; match && $d < areaOfInterest.getCoordinateDimension(); $d++) {
          double coord = Math.max(recordMBR.minCoord[$d], filterMBR.minCoord[$d]);
          match = coord >= areaOfInterest.minCoord[$d] && coord < areaOfInterest.maxCoord[$d];
        }
      }
      if (match)
        return true;
      // Skip to next record
    } while (true);
  }

  @Override
  public Envelope getCurrentKey() throws IOException, InterruptedException {
    return wrapped.getCurrentKey();
  }

  @Override
  public IFeature getCurrentValue() throws IOException, InterruptedException {
    return wrapped.getCurrentValue();
  }

  @Override
  public float getProgress() throws IOException, InterruptedException {
    return wrapped.getProgress();
  }

  @Override
  public void close() throws IOException {
    wrapped.close();
  }

}
