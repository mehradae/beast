/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShapefileGeometryReaderTest extends SparkTest {

  public void testReadHeader() throws IOException {
    DataInputStream in = new DataInputStream(getClass().getResourceAsStream("/usa-major-cities/usa-major-cities.shp"));
    ShapefileGeometryReader.Header header = new ShapefileGeometryReader.Header();
    header.readFields(in);
    in.close();

    assertEquals(1000, header.version);
    assertEquals(3460 / 2, header.fileLength);
    assertEquals(1, header.shapeType);
    assertEquals(-157.8, header.xmin, 1E-1);
    assertEquals(-69.8, header.xmax, 1E-1);
  }

  public void testReadPoints() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.shp");
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(shapefilePath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertTrue(reader.getCurrentValue() instanceof Point);
        recordCount++;
      }
      assertEquals(120, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testReadFromZipFile() throws IOException {
    Path zipPath = new Path(scratchPath, "temp.zip");
    copyResource("/usa-major-cities.zip", new File(zipPath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(zipPath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertTrue(reader.getCurrentValue() instanceof Point);
        recordCount++;
      }
      assertEquals(120, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testReadSparseFiles() throws IOException {
    // This test reads a file that has gaps in the .shp file that can only be detected through the .shx file
    Path zipPath = new Path(scratchPath, "temp.zip");
    copyResource("/sparselines.zip", new File(zipPath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(zipPath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertTrue(reader.getCurrentValue() instanceof MultiLineString2D);
        recordCount++;
      }
      assertEquals(2, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testReadMultipoint() throws IOException {
    Path zipPath = new Path(scratchPath, "temp.zip");
    copyResource("/multipoint.zip", new File(zipPath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(zipPath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertEquals(GeometryType.MULTIPOINT, reader.getCurrentValue().getType());
        recordCount++;
      }
      assertEquals(3, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testReadLineMZ() throws IOException {
    Path zipPath = new Path(scratchPath, "temp.zip");
    copyResource("/polylinemz.zip", new File(zipPath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(zipPath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertEquals(GeometryType.MULTILINESTRING, reader.getCurrentValue().getType());
        recordCount++;
      }
      assertEquals(1, recordCount);
    } finally {
      reader.close();
    }
  }


  public void testReadLines() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.shp");
    copyResource("/usa-major-highways/usa-major-highways.shp", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(shapefilePath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertTrue(reader.getCurrentValue() instanceof LineString2D);
        recordCount++;
      }
      assertEquals(233, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testReadPolygons() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.zip");
    copyResource("/tl_2017_44_elsd.zip", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(shapefilePath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertEquals(MultiPolygon2D.class, reader.getCurrentValue().getClass());
        recordCount++;
      }
      assertEquals(5, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testReadPolygonsMZ() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.zip");
    copyResource("/polygonmz.zip", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(shapefilePath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertEquals(MultiPolygon2D.class, reader.getCurrentValue().getClass());
        recordCount++;
      }
      assertEquals(1, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testFilter() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.shp");
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      Configuration conf = new Configuration();
      conf.set(SpatialInputFormat.FilterMBR, "-160,18,-140,64");
      reader.initialize(shapefilePath, conf);
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        recordCount++;
      }
      assertEquals(2, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testReadPolygonsWithHoles() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.zip");
    copyResource("/simpleshape.zip", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(shapefilePath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertEquals(MultiPolygon2D.class, reader.getCurrentValue().getClass());
        recordCount++;
      }
      assertEquals(2, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testBadPolygon() throws IOException {
    // Test a polygon that does not have the starting and ending points matching
    Path shapefilePath = new Path(scratchPath, "temp.zip");
    copyResource("/badpolygon.zip", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(shapefilePath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertEquals(GeometryType.MULTIPOLYGON, reader.getCurrentValue().getType());
        recordCount++;
      }
      assertEquals(1, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testReadLinesWithM() throws IOException {
    Path shapefilePath = new Path(scratchPath, "linetest.zip");
    copyResource("/linetest.zip", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(shapefilePath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        IGeometry geom = reader.getCurrentValue();
        assertEquals(MultiLineString2D.class, geom.getClass());
        recordCount++;
        MultiLineString2D l = (MultiLineString2D) geom;
        assertEquals(4, l.getNumPoints());
        assertEquals(1.1, l.ms[0]);
        assertEquals(2.2, l.ms[1]);
        assertEquals(3.3, l.ms[2]);
        assertEquals(4.4, l.ms[3]);
      }
      assertEquals(1, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testReadMutableObjects() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.shp");
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    List<IGeometry> geometries = new ArrayList<>();
    try {
      Configuration conf = new Configuration();
      conf.setBoolean(SpatialInputFormat.ImmutableObjects, false);
      reader.initialize(shapefilePath, conf);
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        geometries.add(reader.getCurrentValue());
        assertTrue(reader.getCurrentValue() instanceof Point);
        recordCount++;
      }
      assertEquals(120, recordCount);
      assertSame(geometries.get(0), geometries.get(1));
    } finally {
      reader.close();
    }
  }
  public void testReadImmutableObjects() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.shp");
    Path indexfilePath = new Path(scratchPath, "temp.shx");
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(shapefilePath.toString()));
    copyResource("/usa-major-cities/usa-major-cities.shx", new File(indexfilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    List<IGeometry> geometries = new ArrayList<>();
    try {
      Configuration conf = new Configuration();
      conf.setBoolean(SpatialInputFormat.ImmutableObjects, true);
      reader.initialize(shapefilePath, conf);
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        geometries.add(reader.getCurrentValue());
        assertTrue(reader.getCurrentValue() instanceof Point);
        recordCount++;
      }
      assertEquals(120, recordCount);
      assertNotSame(geometries.get(0), geometries.get(1));
    } finally {
      reader.close();
    }
  }

  public void testReadFileWithEmptyGeometries() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.zip");
    copyResource("/test_empty.zip", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      reader.initialize(shapefilePath, new Configuration());
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertTrue(reader.getCurrentValue() instanceof Point);
        if (reader.iShape == 2)
          assertTrue(reader.getCurrentValue().isEmpty());
        recordCount++;
      }
      assertEquals(2, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testSkipEmptyGeometriesWhenFiltering() throws IOException {
    Path shapefilePath = new Path(scratchPath, "temp.zip");
    copyResource("/test_empty.zip", new File(shapefilePath.toString()));
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    try {
      Configuration conf = new Configuration();
      conf.set(SpatialInputFormat.FilterMBR, "0,0,10,10");
      reader.initialize(shapefilePath, conf);
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        assertTrue(reader.getCurrentValue() instanceof Point);
        recordCount++;
      }
      assertEquals(1, recordCount);
    } finally {
      reader.close();
    }
  }

}