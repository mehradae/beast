package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.test.SparkTest;

public class CSVEnvelopeEncoderTest extends SparkTest {

  public void testEncode2DEnvelope() {
    IFeature feature = new CSVFeature(new Envelope(2, 1.0, 2.0, 5.0, 3.0),
        new String[]{"abc", "def"}, ',', null);
    CSVEnvelopeEncoder encoder = new CSVEnvelopeEncoder(',', new int[] {1,2,4,5});
    assertArrayEquals(new int[] {-1,0,1,-1,2,3}, encoder.orderedColumns);
    String encoded = encoder.apply(feature, new StringBuilder()).toString();
    assertEquals("abc,1.0,2.0,def,5.0,3.0", encoded);
  }
}