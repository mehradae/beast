package edu.ucr.cs.bdlab.io;

import junit.framework.TestCase;

public class DBFFeatureTest extends TestCase {

  public void testParseStringWithSpace() {
    DBFFeature feature = new DBFFeature();
    feature.fieldValues = "Test Field    ".getBytes();
    DBFReader.FieldDescriptor descriptor = new DBFReader.FieldDescriptor();
    descriptor.fieldLength = 14;
    descriptor.fieldName = "name       ".getBytes();
    descriptor.fieldType = 'C';
    feature.descriptors = new DBFReader.FieldDescriptor[] { descriptor };
    feature.fieldStartOffset = new int[] {0, 14};
    assertEquals("Test Field", feature.getAttributeValue(0));
  }

}