/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.File;
import java.io.IOException;

public class MercatorProjectorTest extends SparkTest {

  public void testMercatorProjectorIterator() throws IOException, InterruptedException {
    Path inPath = new Path(scratchPath, "in.rect");
    copyResource("/test-noheader.csv", new File(inPath.toString()));
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "point(1,0)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.setBoolean(SpatialInputFormat.Mercator, true);
    FileSystem fs = inPath.getFileSystem(conf);

    SpatialInputFormat inputFormat = new SpatialInputFormat();
    FileSplit fileSplit = new FileSplit(inPath, 0, fs.getFileStatus(inPath).getLen(), new String[0]);
    try (FeatureReader reader = inputFormat.createAndInitRecordReader(fileSplit, conf)) {
      int count = 0;
      Point p = new Point(123.0, 33.0);
      MercatorProjector.worldToMercator(p);
      for (IFeature feature : reader) {
        if (count == 0)
          assertEquals(p, feature.getGeometry());
        count++;
      }
      assertEquals(2, count);
    }
  }

  public void testGeometryOutOfBounds() {
    Envelope geom = new Envelope(2, 0, 0, 20, 90);
    MercatorProjector.worldToMercator(geom);
    assertEquals(0, geom.minCoord[1], 1E-5);

    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(0, 0);
    polygon.addPoint(20, 0);
    polygon.addPoint(20, 90);
    polygon.closeLastRing(false);
    MercatorProjector.worldToMercator(polygon);
    assertEquals(4, polygon.numPoints);
    polygon.envelope(geom);
    assertEquals(0, geom.minCoord[1], 1E-5);
  }
}