package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class CSVFeatureWriterTest extends SparkTest {

  public void testWriteNoheader() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "test.csv");
    Configuration conf = getSC().hadoopConfiguration();
    conf.setBoolean(CSVFeatureWriter.WriteHeader, false);
    SpatialOutputFormat.setOutputFormat(conf, "point(1,2)");

    CSVFeatureWriter writer = new CSVFeatureWriter();
    writer.initialize(csvPath, conf);
    Point p = new Point(2, 0.1, 0.3);
    CSVFeature f = new CSVFeature(p);
    f.setFieldSeparator((byte) ',');
    byte[] values = "abc,def".getBytes();
    f.setFieldValues(values, 0, values.length);

    writer.write(null, f);
    writer.close(null);

    String[] writtenFile = readFile(csvPath.toString());
    assertEquals(1, writtenFile.length);
    assertEquals("abc\t0.1\t0.3\tdef", writtenFile[0]);
  }

  public void testWriteWithheader() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "test.csv");
    Configuration conf = getSC().hadoopConfiguration();
    conf.setBoolean(CSVFeatureWriter.WriteHeader, true);
    SpatialOutputFormat.setOutputFormat(conf, "point(1,2)");

    CSVFeatureWriter writer = new CSVFeatureWriter();
    writer.initialize(csvPath, conf);
    Point p = new Point(2, 0.1, 0.3);
    CSVFeature f = new CSVFeature(p);
    f.setFieldSeparator((byte) ',');
    byte[] values = "abc,def".getBytes();
    f.setFieldValues(values, 0, values.length);
    f.setFieldNames(new String[]{"name", "value"});

    writer.write(null, f);
    writer.close(null);

    String[] writtenFile = readFile(csvPath.toString());
    assertEquals(2, writtenFile.length);
    assertEquals("name\tx\ty\tvalue", writtenFile[0]);
    assertEquals("abc\t0.1\t0.3\tdef", writtenFile[1]);
  }

  public void testWriteWKTWithHeader() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "test.csv");
    Configuration conf = getSC().hadoopConfiguration();
    conf.setBoolean(CSVFeatureWriter.WriteHeader, true);
    SpatialOutputFormat.setOutputFormat(conf, "wkt(1)");

    CSVFeatureWriter writer = new CSVFeatureWriter();
    writer.initialize(csvPath, conf);
    Point p = new Point(2, 0.1, 0.3);
    CSVFeature f = new CSVFeature(p);
    f.setFieldSeparator((byte) ',');
    byte[] values = "abc,def".getBytes();
    f.setFieldValues(values, 0, values.length);
    f.setFieldNames(new String[]{"name", "value"});

    writer.write(null, f);
    writer.close(null);

    String[] writtenFile = readFile(csvPath.toString());
    assertEquals(2, writtenFile.length);
    assertEquals("name\tgeometry\tvalue", writtenFile[0]);
    assertEquals("abc\tPOINT(0.1 0.3)\tdef", writtenFile[1]);
  }

  public void testWriteEnvelopeWithHeader() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "test.csv");
    Configuration conf = getSC().hadoopConfiguration();
    conf.setBoolean(CSVFeatureWriter.WriteHeader, true);
    SpatialOutputFormat.setOutputFormat(conf, "envelopek(2,1,2,4,5)");
    conf.set(CSVFeatureWriter.FieldSeparator, ",");

    CSVFeatureWriter writer = new CSVFeatureWriter();
    writer.initialize(csvPath, conf);
    Envelope e = new Envelope(2, 0.1, 0.3, 0.5, 0.8);
    CSVFeature f = new CSVFeature(e);
    f.setFieldSeparator((byte) ',');
    byte[] values = "abc,def".getBytes();
    f.setFieldValues(values, 0, values.length);
    f.setFieldNames(new String[]{"name", "value"});

    writer.write(null, f);
    writer.close(null);

    String[] writtenFile = readFile(csvPath.toString());
    assertEquals(2, writtenFile.length);
    assertEquals("name,xmin,ymin,value,xmax,ymax", writtenFile[0]);
    assertEquals("abc,0.1,0.3,def,0.5,0.8", writtenFile[1]);
  }
}