/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GeoJSONFeatureWriterTest extends SparkTest {

  public void testCreationWithPoints() throws IOException, InterruptedException {
    IGeometry[] geometries = {
        new Point(2, 0.0,1.0),
        new Point(2, 3.0, 10.0)
    };

    Configuration conf = getActualHadoopConfiguration(getSC());
    Path outPath = new Path(scratchPath, "test");
    Path outputFileName = new Path(outPath, "test.geojson");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    GeoJSONFeatureWriter writer = new GeoJSONFeatureWriter();
    try {
      writer.initialize(outputFileName, conf);
      for (IGeometry geom : geometries) {
        CSVFeature f = new CSVFeature(geom);
        f.setFieldNames("name");
        f.setFieldValues("name-value");
        writer.write(null, f);
      }
    } finally {
      writer.close(null);
    }

    assertTrue("Output file not found", fileSystem.exists(outputFileName));
    ObjectMapper mapper = new ObjectMapper();
    JsonNode rootNode = mapper.readTree(new File(outputFileName.toString()));
    assertEquals("FeatureCollection", rootNode.get("type").asText());
    JsonNode features = rootNode.get("features");
    assertTrue(features.isArray());
    assertEquals(2, features.size());
    JsonNode firstFeature = features.get(0);
    assertEquals("Feature", firstFeature.get("type").asText());
    JsonNode firstGeometry = firstFeature.get("geometry");
    assertEquals("Point", firstGeometry.get("type").asText());
    assertEquals(2, firstGeometry.get("coordinates").size());
  }

  public void testWriteWithAnonymousAttributes() throws IOException, InterruptedException {
    IGeometry[] geometries = {
        new Point(2, 0.0,1.0),
        new Point(2, 3.0, 10.0)
    };

    Configuration conf = getActualHadoopConfiguration(getSC());
    Path outPath = new Path(scratchPath, "test");
    Path outputFileName = new Path(outPath, "test.geojson");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    GeoJSONFeatureWriter writer = new GeoJSONFeatureWriter();
    try {
      writer.initialize(outputFileName, conf);
      for (IGeometry geom : geometries) {
        CSVFeature f = new CSVFeature(geom);
        f.setFieldValues("name-value");
        writer.write(null, f);
      }
    } finally {
      writer.close(null);
    }

    assertTrue("Output file not found", fileSystem.exists(outputFileName));
    ObjectMapper mapper = new ObjectMapper();
    JsonNode rootNode = mapper.readTree(new File(outputFileName.toString()));
    assertEquals("FeatureCollection", rootNode.get("type").asText());
    JsonNode features = rootNode.get("features");
    assertTrue(features.isArray());
    assertEquals(2, features.size());
    JsonNode firstFeature = features.get(0);
    assertEquals(1, firstFeature.get("properties").size());
  }


  public void testWriteWithNullAttributes() throws IOException, InterruptedException {
    IGeometry[] geometries = {
        new Point(2, 0.0,1.0),
        new Point(2, 3.0, 10.0)
    };

    Configuration conf = getActualHadoopConfiguration(getSC());
    Path outPath = new Path(scratchPath, "test");
    Path outputFileName = new Path(outPath, "test.geojson");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    GeoJSONFeatureWriter writer = new GeoJSONFeatureWriter();
    try {
      writer.initialize(outputFileName, conf);
      for (IGeometry geom : geometries) {
        CSVFeature f = new CSVFeature(geom);
        f.setFieldSeparator((byte) '\t');
        f.setFieldValues("\tfield-value");
        writer.write(null, f);
      }
    } finally {
      writer.close(null);
    }

    assertTrue("Output file not found", fileSystem.exists(outputFileName));
    ObjectMapper mapper = new ObjectMapper();
    JsonNode rootNode = mapper.readTree(new File(outputFileName.toString()));
    assertEquals("FeatureCollection", rootNode.get("type").asText());
    JsonNode features = rootNode.get("features");
    assertTrue(features.isArray());
    assertEquals(2, features.size());
    JsonNode firstFeature = features.get(0);
    assertEquals(1, firstFeature.get("properties").size());
  }

  public void testCreationWithAllGeometries() throws IOException, InterruptedException {
    List<IGeometry> geometries = new ArrayList<>();
    geometries.add(new Point(2, 0.0, 1.0));

    LineString2D lineString = new LineString2D();
    lineString.addPoint(12.0, 13.5);
    lineString.addPoint(15.3, -27.5);
    geometries.add(lineString);

    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(25.0, 33.0);
    polygon.addPoint(45.0, 77.0);
    polygon.addPoint(10.0, 88.0);
    polygon.closeLastRing(false);
    geometries.add(polygon);

    MultiLineString2D multiLineString = new MultiLineString2D();
    multiLineString.addPoint(1.0, 2.0);
    multiLineString.addPoint(3.0, 4.0);
    multiLineString.addPoint(1.0, 5.0);
    multiLineString.endCurrentLineString();
    multiLineString.addPoint(11.0, 2.0);
    multiLineString.addPoint(13.0, 4.0);
    multiLineString.addPoint(11.0, 5.0);
    multiLineString.endCurrentLineString();
    geometries.add(multiLineString);

    MultiPolygon2D multiPolygon = new MultiPolygon2D();
    multiPolygon.addPoint(15.0, 33.0);
    multiPolygon.addPoint(25.0, 35.0);
    multiPolygon.addPoint(-10.0, 7.0);
    multiPolygon.closeLastRing(false);
    multiPolygon.endCurrentPolygon();
    multiPolygon.addPoint(115.0, 33.0);
    multiPolygon.addPoint(125.0, 35.0);
    multiPolygon.addPoint(-110.0, 7.0);
    multiPolygon.closeLastRing(false);
    multiPolygon.endCurrentPolygon();
    geometries.add(multiPolygon);

    GeometryCollection geometryCollection = new GeometryCollection();
    geometryCollection.addGeometry(new Point(2, 2.0, 12.0));
    geometryCollection.addGeometry(multiLineString);
    geometryCollection.addGeometry(polygon);
    geometries.add(geometryCollection);

    geometries.add(new Envelope(2, 0.0, 1.0, 25.0, 13.0));

    Configuration conf = getActualHadoopConfiguration(getSC());
    Path outPath = new Path(scratchPath, "test");
    Path outputFileName = new Path(outPath, "test.geojson");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    GeoJSONFeatureWriter writer = new GeoJSONFeatureWriter();
    try {
      writer.initialize(outputFileName, conf);
      for (IGeometry geom : geometries) {
        CSVFeature f = new CSVFeature(geom);
        f.setFieldNames("name");
        f.setFieldValues("name-value");
        writer.write(null, f);
      }
    } finally {
      writer.close(null);
    }

    // Read them back
    GeoJSONFeatureReader reader = new GeoJSONFeatureReader();
    reader.initialize(outputFileName, conf);
    try {
      int iGeom = 0;
      for (IFeature feature : reader) {
        assertEquals(geometries.get(iGeom).asText(), feature.getGeometry().asText());
        iGeom++;
      }
    } finally {
      reader.close();
    }
  }
}