package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class CSVFeatureReaderTest extends SparkTest {

  public void testDeleteAttributeWithEmptyColumns() throws IOException {
    Text text = new Text("abc,def,,123");
    String s = CSVFeature.deleteAttribute(text, ',', 3, null);
    assertEquals("123", s);
    s = CSVFeature.deleteAttribute(text, ',', 2, null);
    assertEquals("", s);
  }

  public void testDeleteAttributeWithQuotedColumns() throws IOException {
    Text text = new Text("abc,'def,df,sdf,ds,f',,123");
    String s = CSVFeature.deleteAttribute(text, ',', 3, "\'\'");
    assertEquals("123", s);
    s = CSVFeature.deleteAttribute(text, ',', 2, "\'\'");
    assertEquals("", s);
  }

  public void testDeleteAttributeWithSpecialQuotes() throws IOException {
    Text text = new Text("abc,'def,[df,sdf,ds,f],,123");
    assertEquals("\'def", CSVFeature.deleteAttribute(text, ',', 1, "[]"));
    assertEquals("df,sdf,ds,f", CSVFeature.deleteAttribute(text, ',', 1, "[]"));
  }

  public void testImmutableReadWithHeader() throws IOException, InterruptedException {
    Path inPath = new Path(scratchPath, "in.points");
    copyResource("/test_points.csv", new File(inPath.toString()));
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      Configuration conf = new Configuration();
      conf.set(SpatialInputFormat.InputFormat, "point(1,2)");
      conf.setBoolean(SpatialInputFormat.ImmutableObjects, true);
      conf.set(CSVFeatureReader.FieldSeparator, ",");
      conf.setBoolean(CSVFeatureReader.SkipHeader, true);
      reader.initialize(inPath, conf);
      while (reader.nextKeyValue()) {
        CSVFeature f = reader.getCurrentValue();
        assertNotNull(f.getAttributeValue("id"));
      }
    } finally {
      reader.close();
    }
  }

  public void testUseColumnNames() throws IOException, InterruptedException {
    Path inPath = new Path(scratchPath, "in.points");
    copyResource("/test_points.csv", new File(inPath.toString()));
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      Configuration conf = new Configuration();
      conf.set(SpatialInputFormat.InputFormat, "point(x,y)");
      conf.set(CSVFeatureReader.FieldSeparator, ",");
      conf.setBoolean(CSVFeatureReader.SkipHeader, true);
      reader.initialize(inPath, conf);
      while (reader.nextKeyValue()) {
        CSVFeature f = reader.getCurrentValue();
        assertNotNull(f.getAttributeValue("id"));
      }
    } finally {
      reader.close();
    }
  }

  public void testEmptyFirstAttribute() {
    Text line = new Text(",a,b,c");
    String val = CSVFeature.deleteAttribute(line, ',', 0, null);
    assertNull("Attribute should be null", val);
    assertEquals(5, line.getLength());
    assertEquals("a", CSVFeature.deleteAttribute(line, ',', 0, null));
  }

  public void testDisableQuoting() throws IOException, InterruptedException {
    Path inPath = new Path(scratchPath, "in.points");
    copyResource("/test_points_quote.csv", new File(inPath.toString()));
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      Configuration conf = new Configuration();
      conf.set(CSVFeatureReader.QuoteCharacters, "");
      conf.set(SpatialInputFormat.InputFormat, "point(1,2)");
      conf.set(CSVFeatureReader.FieldSeparator, ",");
      conf.setBoolean(CSVFeatureReader.SkipHeader, true);
      reader.initialize(inPath, conf);
      int count = 0;
      while (reader.nextKeyValue()) {
        count++;
        if (count == 1)
          assertEquals("\'Eua", reader.getCurrentValue().getAttributeValue("name"));
      }
      assertEquals(2, count);
    } finally {
      reader.close();
    }
  }

  public void testEmptyPoints() throws IOException, InterruptedException {
    Configuration conf = getSC().hadoopConfiguration();
    Path inPath = new Path(scratchPath, "in.points");
    FileSystem fs = inPath.getFileSystem(conf);
    PrintStream ps = new PrintStream(fs.create(inPath));
    ps.println("name1,value1,100.0,25.0");
    ps.println("name2,value2,,");
    ps.println("name3,value3");
    ps.close();
    CSVFeatureReader reader = new CSVFeatureReader();
    conf.set(SpatialInputFormat.InputFormat, "point(2,3)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    reader.initialize(inPath, conf);
    try {
      int i = 0;
      while (reader.nextKeyValue()) {
        CSVFeature f = reader.getCurrentValue();
        if (i == 1 || i == 2)
          assertTrue("Point should be empty", f.getGeometry().isEmpty());
        i++;
      }
    } finally {
      reader.close();
    }
  }

  public void testEmptyWKT() throws IOException, InterruptedException {
    Configuration conf = getSC().hadoopConfiguration();
    Path inPath = new Path(scratchPath, "in.wkt");
    FileSystem fs = inPath.getFileSystem(conf);
    PrintStream ps = new PrintStream(fs.create(inPath));
    ps.println("name1;value1;POLYGON((12 13, 15 17, 20 20, 12 13))");
    ps.println("name2;value2;;");
    ps.println("name3;value3");
    ps.close();
    CSVFeatureReader reader = new CSVFeatureReader();
    conf.set(SpatialInputFormat.InputFormat, "wkt(2)");
    conf.set(CSVFeatureReader.FieldSeparator, ";");
    reader.initialize(inPath, conf);
    try {
      int i = 0;
      while (reader.nextKeyValue()) {
        CSVFeature f = reader.getCurrentValue();
        if (i == 1 || i == 2)
          assertTrue("Geometry should be empty", f.getGeometry().isEmpty());
        i++;
      }
    } finally {
      reader.close();
    }
  }

  public void testAutoDetect() throws IOException {
    String inputFile = new Path(scratchPath, "test.csv").toString();
    CSVFeatureReader csvFeatureReader = new CSVFeatureReader();

    // Test autodetect field separator and columns for a point dataset
    copyResource("/test-noheader.csv", new File(inputFile));
    UserOptions opts = new UserOptions();
    boolean autodetected = csvFeatureReader.autoDetect(opts, inputFile);
    assertTrue("Should be able to autodetect the file", autodetected);
    assertEquals("point(0,1)", opts.get(SpatialInputFormat.InputFormat));
    assertEquals(",", opts.get(CSVFeatureReader.FieldSeparator));

    // Test use the provided field separator (even if it is wrong)
    opts = new UserOptions();
    opts.set(CSVFeatureReader.FieldSeparator, "\t");
    autodetected = csvFeatureReader.autoDetect(opts, inputFile);
    assertFalse("Should not be able to autodetect the file", autodetected);

    // Test autodetect points with a header
    copyResource("/test-header.csv", new File(inputFile), true);
    opts = new UserOptions();
    autodetected = csvFeatureReader.autoDetect(opts, inputFile);
    assertTrue("Should be able to autodetect the file", autodetected);
    assertEquals("point(0,1)", opts.get(SpatialInputFormat.InputFormat));
    assertEquals(",", opts.get(CSVFeatureReader.FieldSeparator));
    assertTrue("Should set -skipheader", opts.getBoolean(CSVFeatureReader.SkipHeader, false));

    // Test autodetect points with a header
    copyResource("/test_wkt.csv", new File(inputFile), true);
    opts = new UserOptions();
    autodetected = csvFeatureReader.autoDetect(opts, inputFile);
    assertTrue("Should be able to autodetect the file", autodetected);
    assertEquals("wkt(1)", opts.get(SpatialInputFormat.InputFormat));
    assertEquals("\t", opts.get(CSVFeatureReader.FieldSeparator));
    assertTrue("Should set -skipheader", opts.getBoolean(CSVFeatureReader.SkipHeader, false));

    // Test autodetect points from the header with some empty points
    PrintStream ps = new PrintStream(new FileOutputStream(inputFile));
    ps.println("ID,count,Longitude,Latitude");
    ps.println("10,100,123.44,55.7");
    ps.println("20,500,,");
    ps.close();
    opts = new UserOptions();
    autodetected = csvFeatureReader.autoDetect(opts, inputFile);
    assertTrue("Should be able to autodetect the file", autodetected);
    assertEquals("point(2,3)", opts.get(SpatialInputFormat.InputFormat));
    assertEquals(",", opts.get(CSVFeatureReader.FieldSeparator));
    assertTrue("Should set -skipheader", opts.getBoolean(CSVFeatureReader.SkipHeader, false));

    // Should work with input directories
    Path inputPath = new Path(new Path(scratchPath, "subdir"), "subfile.csv");
    FileSystem fs = inputPath.getFileSystem(opts);
    fs.mkdirs(inputPath.getParent());
    copyResource("/test-header.csv", new File(inputPath.toString()));
    opts = new UserOptions();
    autodetected = csvFeatureReader.autoDetect(opts, inputPath.getParent().toString());
    assertTrue("Should be able to autodetect the file", autodetected);
    assertEquals("point(0,1)", opts.get(SpatialInputFormat.InputFormat));
    assertEquals(",", opts.get(CSVFeatureReader.FieldSeparator));
    assertTrue("Should set -skipheader", opts.getBoolean(CSVFeatureReader.SkipHeader, false));
  }
}