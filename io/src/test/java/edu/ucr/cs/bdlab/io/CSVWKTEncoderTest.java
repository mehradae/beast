package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.test.SparkTest;

public class CSVWKTEncoderTest extends SparkTest {

  public void testEncodePointWithAttributes() {
    Point p = new Point(2, 0.5, 0.1);
    CSVFeature f = new CSVFeature(p);
    String dataAttributes = "att1,att2,att3,att4";
    char fieldSeparator = ',';
    f.setFieldSeparator((byte) fieldSeparator);
    f.setGeometry(p);
    byte[] bytes = dataAttributes.getBytes();
    f.setFieldValues(bytes, 0, bytes.length);
    CSVWKTEncoder writer = new CSVWKTEncoder(fieldSeparator, 1);
    String s = writer.apply(f, null).toString();
    assertEquals("att1,POINT(0.5 0.1),att2,att3,att4", s);
  }
}