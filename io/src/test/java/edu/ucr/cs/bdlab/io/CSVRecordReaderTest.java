package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVRecordReaderTest extends SparkTest {

  public void testReadFileWithoutHeader() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "temp.csv");
    copyResource("/test-noheader.csv", new File(csvPath.toString()));
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      Configuration conf = new Configuration();
      conf.set(SpatialInputFormat.InputFormat, "nogeom");
      conf.set(CSVFeatureReader.FieldSeparator, ",");
      FileSystem fs = csvPath.getFileSystem(conf);
      long fileLength = fs.getFileStatus(csvPath).getLen();
      FileSplit fsplit = new FileSplit(csvPath, 0, fileLength, new String[0]);
      reader.initialize(fsplit, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
      assertTrue(reader.nextKeyValue());
      assertEquals("33", reader.getCurrentValue().getAttributeValue(0));
      assertTrue(reader.nextKeyValue());
      assertEquals("another name", reader.getCurrentValue().getAttributeValue(2));
      assertFalse(reader.nextKeyValue());
    } finally {
      reader.close();
    }
  }

  public void testSkipHeaderNoGeometry() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "temp.csv");
    copyResource("/test-header.csv", new File(csvPath.toString()));
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      Configuration conf = new Configuration();
      conf.set(SpatialInputFormat.InputFormat, "nogeom");
      conf.set(CSVFeatureReader.FieldSeparator, ",");
      conf.setBoolean(CSVFeatureReader.SkipHeader, true);
      FileSystem fs = csvPath.getFileSystem(conf);
      long fileLength = fs.getFileStatus(csvPath).getLen();
      FileSplit fsplit = new FileSplit(csvPath, 0, fileLength, new String[0]);
      reader.initialize(fsplit, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
      assertTrue(reader.nextKeyValue());
      assertEquals("33", reader.getCurrentValue().getAttributeValue(0));
      assertTrue(reader.nextKeyValue());
      assertEquals("another name", reader.getCurrentValue().getAttributeValue(2));
      assertEquals("66", reader.getCurrentValue().getAttributeValue("id"));
      assertFalse(reader.nextKeyValue());
    } finally {
      reader.close();
    }
  }

  public void testReadFileWithGeometries() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "temp.csv");
    copyResource("/test-geometries.csv", new File(csvPath.toString()));
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      Configuration conf = new Configuration();
      conf.set(SpatialInputFormat.InputFormat, "wkt(1)");
      conf.set(CSVFeatureReader.FieldSeparator, "\t");
      FileSystem fs = csvPath.getFileSystem(conf);
      long fileLength = fs.getFileStatus(csvPath).getLen();
      FileSplit fsplit = new FileSplit(csvPath, 0, fileLength, new String[0]);
      reader.initialize(fsplit, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
      assertTrue(reader.nextKeyValue());
      CSVFeature feature = reader.getCurrentValue();
      assertEquals("1", feature.getAttributeValue(0));
      assertEquals("test", feature.getAttributeValue(1));
      assertEquals(new Point(2, 0, 1), feature.getGeometry());
      assertTrue(reader.nextKeyValue());
      assertEquals("test2", reader.getCurrentValue().getAttributeValue(1));
      assertFalse(reader.nextKeyValue());
    } finally {
      reader.close();
    }
  }

  public void testReadFileWithPoints() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "temp.csv");
    copyResource("/test-noheader.csv", new File(csvPath.toString()));
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      Configuration conf = new Configuration();
      conf.set(SpatialInputFormat.InputFormat, "point");
      conf.set(CSVFeatureReader.FieldSeparator, ",");
      FileSystem fs = csvPath.getFileSystem(conf);
      long fileLength = fs.getFileStatus(csvPath).getLen();
      FileSplit fsplit = new FileSplit(csvPath, 0, fileLength, new String[0]);
      reader.initialize(fsplit, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
      assertTrue(reader.nextKeyValue());
      CSVFeature feature = reader.getCurrentValue();
      assertEquals("name", feature.getAttributeValue(0));
      assertEquals(new Point(2, 33.0, 123.0), feature.getGeometry());
      assertTrue(reader.nextKeyValue());
      assertEquals("another name", reader.getCurrentValue().getAttributeValue(0));
      assertEquals(new Point(2, 66.0, 154.0), feature.getGeometry());
      assertFalse(reader.nextKeyValue());
    } finally {
      reader.close();
    }
  }

  public void testApplySpatialFilter() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "temp.csv");
    copyResource("/test-noheader.csv", new File(csvPath.toString()));
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.FilterMBR, "10,100,50,200");
    FileSystem fs = csvPath.getFileSystem(conf);
    long fileLength = fs.getFileStatus(csvPath).getLen();
    FileSplit fsplit = new FileSplit(csvPath, 0, fileLength, new String[0]);
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      conf.set(SpatialInputFormat.InputFormat, "point");
      conf.set(CSVFeatureReader.FieldSeparator, ",");
      reader.initialize(fsplit, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
      int recordCount = 0;
      while (reader.nextKeyValue())
        recordCount++;
      assertEquals(1, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testImmutableObjects() throws IOException, InterruptedException {
    Path csvPath = new Path(scratchPath, "temp.csv");
    copyResource("/test-header.csv", new File(csvPath.toString()));
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.ImmutableObjects, "true");
    FileSystem fs = csvPath.getFileSystem(conf);
    long fileLength = fs.getFileStatus(csvPath).getLen();
    FileSplit fsplit = new FileSplit(csvPath, 0, fileLength, new String[0]);
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      conf.setBoolean(CSVFeatureReader.SkipHeader, true);
      conf.set(SpatialInputFormat.InputFormat, "point");
      conf.set(CSVFeatureReader.FieldSeparator, ",");
      conf.setBoolean(CSVFeatureReader.SkipHeader, true);
      reader.initialize(fsplit, new TaskAttemptContextImpl(conf, new TaskAttemptID()));
      int recordCount = 0;
      List<IFeature> allFeatues = new ArrayList<>();
      while (reader.nextKeyValue()) {
        recordCount++;
        for (IFeature oldFeature : allFeatues) {
          assertFalse("Cannot reuse objects", reader.getCurrentValue() == oldFeature);
        }
        allFeatues.add(reader.getCurrentValue());
      }
      assertEquals(2, recordCount);
    } finally {
      reader.close();
    }
  }
}