/*
 * Copyright 2019 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class BinaryInputFormatTest extends SparkTest {
  public void testCreateSplits() throws IOException {
    Path in1 = makeFileCopy("/test-geometries.csv");
    Path in2 = makeFileCopy("/test-noheader.csv");

    UserOptions opts = new UserOptions("iformat[0]:wkt(1)", "iformat[1]:point", "separator[1]:,");
    Job job = Job.getInstance(opts);
    BinaryInputFormat.setInputPaths(job, in1, in2);

    BinaryInputFormat inputFormat = new BinaryInputFormat();
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(1, splits.size());
    CompositeSpatialFileSplit split = (CompositeSpatialFileSplit) splits.get(0);
    assertEquals(38+32, split.getLength());
  }

  public void testInputsWithSimilarInputFormats() throws IOException, InterruptedException {
    Path in1 = makeFileCopy("/test-header.csv");
    Path in2 = makeFileCopy("/test-noheader.csv");

    UserOptions opts = new UserOptions("iformat:point", "separator:,", "-skipheader[0]");
    Job job = Job.getInstance(opts);
    BinaryInputFormat.setInputPaths(job, in1, in2);

    BinaryInputFormat inputFormat = new BinaryInputFormat();
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(1, splits.size());
    CompositeSpatialFileSplit split = (CompositeSpatialFileSplit) splits.get(0);
    TaskAttemptContextImpl fakeContext = new TaskAttemptContextImpl(opts, new TaskAttemptID());
    RecordReader<Envelope[], Iterable<? extends IFeature>[]> reader =
        inputFormat.createRecordReader(split, fakeContext);
    reader.initialize(split, fakeContext);
    try {
      int count = 0;
      while (reader.nextKeyValue()) {
        count++;
        Envelope[] mbbs = reader.getCurrentKey();
        assertEquals(2, mbbs.length);
        assertEquals(Double.NEGATIVE_INFINITY, mbbs[0].minCoord[0]);
        assertEquals(Double.POSITIVE_INFINITY, mbbs[0].maxCoord[1]);

        int count1 = 0;
        for (IFeature f : reader.getCurrentValue()[0]) {
          count1++;
          assertEquals(GeometryType.POINT, f.getGeometry().getType());
        }
        assertEquals(2, count1);

        int count2 = 0;
        for (IFeature f : reader.getCurrentValue()[1]) {
          count2++;
          assertEquals(GeometryType.POINT, f.getGeometry().getType());
        }
        assertEquals(2, count2);
      }
      assertEquals(1, count);
    } finally {
      reader.close();
    }
  }

  public void testReadRecords() throws IOException, InterruptedException {
    Path in1 = makeFileCopy("/test-geometries.csv");
    Path in2 = makeFileCopy("/test-noheader.csv");

    UserOptions opts = new UserOptions("iformat[0]:wkt(1)", "iformat[1]:point", "separator[1]:,");
    Job job = Job.getInstance(opts);
    BinaryInputFormat.setInputPaths(job, in1, in2);

    BinaryInputFormat inputFormat = new BinaryInputFormat();
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(1, splits.size());
    CompositeSpatialFileSplit split = (CompositeSpatialFileSplit) splits.get(0);

    TaskAttemptContextImpl fakeContext = new TaskAttemptContextImpl(opts, new TaskAttemptID());
    RecordReader<Envelope[], Iterable<? extends IFeature>[]> reader =
        inputFormat.createRecordReader(split, fakeContext);
    reader.initialize(split, fakeContext);
    try {
      int count = 0;
      while (reader.nextKeyValue()) {
        count++;
        Envelope[] mbbs = reader.getCurrentKey();
        assertEquals(2, mbbs.length);
        assertEquals(Double.NEGATIVE_INFINITY, mbbs[0].minCoord[0]);
        assertEquals(Double.POSITIVE_INFINITY, mbbs[0].maxCoord[1]);

        int count1 = 0;
        for (IFeature f : reader.getCurrentValue()[0]) {
          count1++;
          assertEquals(GeometryType.POINT, f.getGeometry().getType());
        }
        assertEquals(2, count1);

        int count2 = 0;
        for (IFeature f : reader.getCurrentValue()[1]) {
          count2++;
          assertEquals(GeometryType.POINT, f.getGeometry().getType());
        }
        assertEquals(2, count2);
      }
      assertEquals(1, count);
    } finally {
      reader.close();
    }
  }

  public void testReadTwoDirs() throws IOException, InterruptedException {
    Path in1 = new Path(scratchPath, "in1");
    copyResource("/test-geometries.csv", new File(new Path(in1, "part-1").toString()));
    copyResource("/test-geometries.csv", new File(new Path(in1, "part-2").toString()));
    copyResource("/test-geometries.csv", new File(new Path(in1, "part-3").toString()));
    Path in2 = new Path(scratchPath, "in2");
    copyResource("/test-noheader.csv", new File(new Path(in2, "part-1").toString()));
    copyResource("/test-noheader.csv", new File(new Path(in2, "part-2").toString()));

    UserOptions opts = new UserOptions("iformat[0]:wkt(1)", "iformat[1]:point", "separator[1]:,");
    Job job = Job.getInstance(opts);
    BinaryInputFormat.setInputPaths(job, in1, in2);

    BinaryInputFormat inputFormat = new BinaryInputFormat();
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(6, splits.size());
    for (InputSplit split : splits) {
      TaskAttemptContextImpl fakeContext = new TaskAttemptContextImpl(opts, new TaskAttemptID());
      RecordReader<Envelope[], Iterable<? extends IFeature>[]> reader =
          inputFormat.createRecordReader(split, fakeContext);

      try {
        reader.initialize(split, fakeContext);
        int count = 0;
        while (reader.nextKeyValue()) {
          count++;
          Envelope[] mbbs = reader.getCurrentKey();
          assertEquals(2, mbbs.length);
          assertEquals(Double.NEGATIVE_INFINITY, mbbs[0].minCoord[0]);
          assertEquals(Double.POSITIVE_INFINITY, mbbs[0].maxCoord[1]);

          int count1 = 0;
          for (IFeature f : reader.getCurrentValue()[0]) {
            count1++;
            assertEquals(GeometryType.POINT, f.getGeometry().getType());
          }
          assertEquals(2, count1);

          int count2 = 0;
          for (IFeature f : reader.getCurrentValue()[1]) {
            count2++;
            assertEquals(GeometryType.POINT, f.getGeometry().getType());
          }
          assertEquals(2, count2);
        }
        assertEquals(1, count);
      } finally {
        reader.close();
      }
    }
  }

  public void testReadIndexedDirsWithFilter() throws IOException {
    Path in1 = makeDirCopy("/test_index");
    Path in2 = makeFileCopy("/test-noheader.csv");

    UserOptions opts = new UserOptions("iformat[0]:wkt(1)", "iformat[1]:point", "separator[1]:,");
    opts.set(SpatialInputFormat.FilterMBR, "0,0,2,2");
    Job job = Job.getInstance(opts);
    BinaryInputFormat.setInputPaths(job, in1, in2);

    BinaryInputFormat inputFormat = new BinaryInputFormat();
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(1, splits.size());
    CompositeSpatialFileSplit split = (CompositeSpatialFileSplit) splits.get(0);
    assertEquals("part-00.csv", ((SpatialFileSplit) split.get(0)).getPath().getName());
  }

  public void testJoinFilter() throws IOException {
    Path in1 = new Path(scratchPath, "in1");
    copyDirectoryFromResources("/test_index", new File(in1.toString()));
    Path in2 = new Path(scratchPath, "in2");
    copyDirectoryFromResources("/test_index", new File(in2.toString()));

    UserOptions opts = new UserOptions("iformat[0]:wkt(1)", "iformat[1]:point", "separator[1]:,");
    opts.setClass(SpatialInputFormat.BlockFilterClass, JoinFilter.class, BlockFilter.class);

    Job job = Job.getInstance(opts);
    BinaryInputFormat.setInputPaths(job, in1, in2);

    BinaryInputFormat inputFormat = new BinaryInputFormat();
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(2, splits.size());
    CompositeSpatialFileSplit split = (CompositeSpatialFileSplit) splits.get(0);
    assertEquals(split.get(0).getPath().getName(), split.get(1).getPath().getName());
    split = (CompositeSpatialFileSplit) splits.get(1);
    assertEquals(split.get(0).getPath().getName(), split.get(1).getPath().getName());
  }
}