/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.util.OperationException;
import edu.ucr.cs.bdlab.util.UserOptions;
import junit.framework.TestCase;

public class SpatialOutputFormatTest extends TestCase {

  public void testSetOutputFormatShouldThrowAnExceptionForWrongFormats() {
    // A valid input should not raise an exception
    boolean errorRaised = false;
    try {
      UserOptions opts = new UserOptions();
      SpatialOutputFormat.setOutputFormat(opts, "point(1,2)");
    } catch (OperationException e) {
      errorRaised = true;
    }
    assertFalse("No error should be raised", errorRaised);

    // An invalid input should raise an exception
    errorRaised = false;
    try {
      UserOptions opts = new UserOptions();
      SpatialOutputFormat.setOutputFormat(opts, "errrr");
    } catch (OperationException e) {
      errorRaised = true;
    }
    assertTrue("An error should be raised", errorRaised);

    // A correction for slight typos should be reported in the error message
    errorRaised = false;
    try {
      UserOptions opts = new UserOptions();
      SpatialOutputFormat.setOutputFormat(opts, "piont");
    } catch (OperationException e) {
      errorRaised = true;
      assertTrue("The error message should suggest a correction", e.getMessage().contains("point"));
    }
    assertTrue("An error should be raised", errorRaised);

    // Suggest a correction when extra parameters are added
    errorRaised = false;
    try {
      UserOptions opts = new UserOptions();
      SpatialOutputFormat.setOutputFormat(opts, "piont(1,2)");
    } catch (OperationException e) {
      errorRaised = true;
      assertTrue("The error message should suggest a correction", e.getMessage().contains("point"));
    }
    assertTrue("An error should be raised", errorRaised);
  }
}