/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.cg;

import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.SpatialReference;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.util.EsriConverter;
import edu.ucr.cs.bdlab.indexing.RRStarTree;
import edu.ucr.cs.bdlab.indexing.RTreeGuttman;
import edu.ucr.cs.bdlab.util.IntArray;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.util.DoubleArray;
import org.apache.hadoop.util.IndexedSortable;
import org.apache.hadoop.util.QuickSort;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A utility class that contains implementations of spatial join algorithms.
 */
public class SpatialJoinAlgorithms {
  /**Logger for this class*/
  private static final Log LOG = LogFactory.getLog(SpatialJoinAlgorithms.class);

  /**Enumerated type for the distributed spatial join algorithms*/
  public enum ESJDistributedAlgorithm {BNLJ, PBSM, SJMR, DJ}

  /**Enumerated type for single-machine spatial join algorithms*/
  public enum ESJSequentialAlgorithm {Planesweep, RTree}

  /**Enumerated type for the spatial join predicate*/
  public enum ESJPredicate {Intersects, MBRIntersects, Contains}

  public interface SJResult {
    void accept(int i, int j, double[] refPoint);
  }

  /**
   * Computes the spatial join between two sets of rectangles and reports all overlapping pairs of indexes.
   *
   * @param minCoord1 an array of coordinates for the lower corners of the first set of rectangles
   * @param maxCoord1 an array of coordinates for the upper corners of the first set of rectangles
   * @param minCoord2 an array of coordinates for the lower corners of the second set of rectangles
   * @param maxCoord2 an array of coordinates for the upper corners of the second set of rectangles
   * @param results the output is reported by calling this function with the indexes of the two rectangles that
   *                overlap. If {@code null}, results are not reported; only the total count is.
   * @return the total number of results found.
   */
  public static int planeSweepRectangles(double[][] minCoord1, double[][] maxCoord1,
                                         double[][] minCoord2, double[][] maxCoord2,
                                         SJResult results) {
    assert minCoord1.length == maxCoord1.length;
    assert minCoord1.length == minCoord2.length;
    assert minCoord1.length == maxCoord2.length;
    int count = 0;
    int numDimensions = minCoord1.length;
    int n1 = minCoord1[0].length;
    int n2 = minCoord2[0].length;
    LOG.debug(String.format("Running the plane sweep join algorithm between lists sizes of %d and %d", n1, n2));
    // Generate indexes to report the answer
    int[] indexes1 = new int[n1];
    int[] indexes2 = new int[n2];
    for (int $i = 0; $i < n1; $i++)
      indexes1[$i] = $i;
    for (int $i = 0; $i < n2; $i++)
      indexes2[$i] = $i;
    // Sort by the first coordinate and keep track of the array index for each entry for reporting
    IndexedSortable sortable1 = new IndexedSortable() {
      @Override
      public int compare(int i, int j) {
        return (int) Math.signum(minCoord1[0][i] - minCoord1[0][j]);
      }

      @Override
      public void swap(int i, int j) {
        if (i == j)
          return;
        // Swap indexes
        indexes1[i] ^= indexes1[j];
        indexes1[j] ^= indexes1[i];
        indexes1[i] ^= indexes1[j];
        // Swap coordinates
        for (int $d = 0; $d < numDimensions; $d++) {
          double t = minCoord1[$d][i];
          minCoord1[$d][i] = minCoord1[$d][j];
          minCoord1[$d][j] = t;
          t = maxCoord1[$d][i];
          maxCoord1[$d][i] = maxCoord1[$d][j];
          maxCoord1[$d][j] = t;
        }
      }
    };
    IndexedSortable sortable2 = new IndexedSortable() {
      @Override
      public int compare(int i, int j) {
        return (int) Math.signum(minCoord2[0][i] - minCoord2[0][j]);
      }

      @Override
      public void swap(int i, int j) {
        if (i == j)
          return;
        // Swap indexes
        indexes2[i] ^= indexes2[j];
        indexes2[j] ^= indexes2[i];
        indexes2[i] ^= indexes2[j];
        // Swap coordinates
        for (int $d = 0; $d < numDimensions; $d++) {
          double t = minCoord2[$d][i];
          minCoord2[$d][i] = minCoord2[$d][j];
          minCoord2[$d][j] = t;
          t = maxCoord2[$d][i];
          maxCoord2[$d][i] = maxCoord2[$d][j];
          maxCoord2[$d][j] = t;
        }
      }
    };
    QuickSort sorter = new QuickSort();
    sorter.sort(sortable1, 0, n1);
    sorter.sort(sortable2, 0, n2);

    // Now, run the planesweep algorithm
    int lastReportedProgress = 0;
    long lastReportedProgressTime = System.currentTimeMillis();
    int i = 0, j = 0;
    double[] refPoint = new double[numDimensions];
    while (i < n1 && j < n2) {
      if (minCoord1[0][i] < minCoord2[0][j]) {
        // R1[i] is the left-most rectangle. Activate it and compare to all rectangles R2 until passing the right end
        // of R1[i]
        int jj = j;
        while (jj < n2 && minCoord2[0][jj] < maxCoord1[0][i]) {
          // Compare the two rectangles R1[i] and R2[jj] and report if needed
          if (rectanglesOverlap(minCoord1, maxCoord1, i, minCoord2, maxCoord2, jj)) {
            // Found an overlap
            count++;
            if (results != null) {
              for (int d = 0; d < numDimensions; d++)
                refPoint[d] = Math.max(minCoord1[d][i], minCoord2[d][jj]);
              results.accept(indexes1[i], indexes2[jj], refPoint);
            }
          }
          jj++;
        }
        // Skip until the first record that might produce a result from i
        do {
          i++;
        } while (i < n1 && maxCoord1[0][i] < minCoord2[0][j]);
      } else {
        // R2[j] is the left-most rectangle. Activate it and compare to all rectangles of R1 until passing the right
        // end of R2[j]
        int ii = i;
        while (ii < n1 && minCoord1[0][ii] < maxCoord2[0][j]) {
          // Compare the two rectangles R1[ii] and R2[j] and report if needed
          if (rectanglesOverlap(minCoord1, maxCoord1, ii, minCoord2, maxCoord2, j)) {
            // Found an overlap
            count++;
            if (results != null) {
              for (int d = 0; d < numDimensions; d++)
                refPoint[d] = Math.max(minCoord1[d][ii], minCoord2[d][j]);
              results.accept(indexes1[ii], indexes2[j], refPoint);
            }
          }
          ii++;
        }
        // Skip until the first record that might produce a result from j
        do {
          j++;
        } while (j < n2 && maxCoord2[0][j] < minCoord1[0][i]);
      }
      if (System.currentTimeMillis() - lastReportedProgressTime > 60000) {
        // Report the result every 60 seconds if there is at least 1% change
        int progress = (int) (((double) i * j) / ((double) n1 * n2) * 100.0);
        if (progress != lastReportedProgress) {
          lastReportedProgress = progress;
          LOG.info(String.format("Spatial join progress %d%% (i=%d/%d, j=%d/%d)", lastReportedProgress, i, n1, j, n2));
        }
        lastReportedProgressTime = System.currentTimeMillis();
      }
    }
    return count;
  }

  /**
   * Test if two rectangles overlap R1[i] and R2[j].
   * @param minCoord1
   * @param maxCoord1
   * @param i
   * @param minCoord2
   * @param maxCoord2
   * @param j
   * @return
   */
  static boolean rectanglesOverlap(double[][] minCoord1, double[][] maxCoord1, int i,
                                   double[][] minCoord2, double[][] maxCoord2, int j) {
    for (int $d = 0; $d < minCoord1.length; $d++)
      if (minCoord1[$d][i] >= maxCoord2[$d][j] || minCoord2[$d][j] >= maxCoord1[$d][i])
        return false;
    return true;
  }

  /**
   * Runs a spatial join of the given lists of geometries through a plan-sweep filter-refine approach.
   * @param r
   * @param s
   * @param results
   * @return
   */
  public static int spatialJoinIntersectsPlaneSweep(List<IGeometry> r, List<IGeometry> s, SJResult results) {
    if (r.size() == 0 || s.size() == 0)
      return 0;
    assert r.get(0).getCoordinateDimension() == 2 : "R should be 2D geometries";
    assert s.get(0).getCoordinateDimension() == 2 : "S should be 2D geometries";
    double[][] coordsR = getMBRs(r);
    double[][] coordsS = getMBRs(s);
    return planeSweepRectangles(new double[][]{coordsR[0], coordsR[1]}, new double[][]{coordsR[2], coordsR[3]},
        new double[][]{coordsS[0], coordsS[1]}, new double[][]{coordsS[2], coordsS[3]}, (i, j, refPoint) -> {
          // Run the refine step
          if (r.get(i).intersects(s.get(j)))
            results.accept(i, j, refPoint);
        });
  }


  /**
   * Runs a spatial join of the given lists of features through a plan-sweep filter-refine approach.
   * @param r
   * @param s
   * @param results
   * @return
   */
  public static int spatialJoinIntersectsPlaneSweepFeatures(List<IFeature> r, List<IFeature> s, SJResult results) {
    if (r.size() == 0 || s.size() == 0)
      return 0;
    assert r.get(0).getGeometry().getCoordinateDimension() == 2 : "R should be 2D geometries";
    assert s.get(0).getGeometry().getCoordinateDimension() == 2 : "S should be 2D geometries";
    double[][] coordsR = getMBRs2(r);
    double[][] coordsS = getMBRs2(s);
    return planeSweepRectangles(new double[][]{coordsR[0], coordsR[1]}, new double[][]{coordsR[2], coordsR[3]},
        new double[][]{coordsS[0], coordsS[1]}, new double[][]{coordsS[2], coordsS[3]}, (i, j, refPoint) -> {
          // Run the refine step
          if (results != null && r.get(i).getGeometry().intersects(s.get(j).getGeometry()))
            results.accept(i, j, refPoint);
        });
  }

  /**
   * Computes the minimum bounding rectangles of all given geometries as primitive arrays.
   * The returned 2D array stores the minimum and maximum coordinate along each dimension.
   * @param geometries
   * @return
   */
  static double[][] getMBRs(List<IGeometry> geometries) {
    int numDimensions = geometries.get(0).getCoordinateDimension();
    double[][] coords = new double[numDimensions*2][geometries.size()];
    Envelope mbr = new Envelope(numDimensions);
    for (int iGeom = 0; iGeom < geometries.size(); iGeom++) {
      geometries.get(iGeom).envelope(mbr);
      for (int d = 0; d < numDimensions; d++) {
        coords[d][iGeom] = mbr.minCoord[d];
        coords[d + numDimensions][iGeom] = mbr.maxCoord[d];
      }
    }
    return coords;
  }

  /**
   * Computes the minimum bounding rectangles of all given geometries as primitive arrays.
   * The returned 2D array stores the minimum and maximum coordinate along each dimension.
   * @param geometries
   * @return
   */
  static double[][] getMBRs2(List<IFeature> geometries) {
    int numDimensions = geometries.get(0).getGeometry().getCoordinateDimension();
    double[][] coords = new double[numDimensions*2][geometries.size()];
    Envelope mbr = new Envelope(numDimensions);
    for (int iGeom = 0; iGeom < geometries.size(); iGeom++) {
      geometries.get(iGeom).getGeometry().envelope(mbr);
      for (int d = 0; d < numDimensions; d++) {
        coords[d][iGeom] = mbr.minCoord[d];
        coords[d + numDimensions][iGeom] = mbr.maxCoord[d];
      }
    }
    return coords;
  }

  /**
   * Computes the overlap join between the two given lists of geometries. Each geometry is simplified using the given
   * threshold. Any geometry that contains more than the threshold points will be simplified by partitioning it
   * into smaller geometries.
   * @param r
   * @param s
   * @param threshold
   * @param results
   * @return
   */
  public static int spatialJoinIntersectsWithSimplification(List<IGeometry> r, List<IGeometry> s, int threshold,
                                                            SJResult results) {
    // A set of reported results to run duplicate elimination
    Set<Long> reportedPairs = new HashSet<>();
    // Run quad split for both inputs
    List<IGeometry> simplifiedR = new ArrayList<>();
    // Map each part to its main
    IntArray mappingR = new IntArray();
    for (int iGeom = 0; iGeom < r.size(); iGeom++) {
      List<IGeometry> parts = quadSplit(r.get(iGeom), threshold);
      simplifiedR.addAll(parts);
      for (int i = 0; i < parts.size(); i++)
        mappingR.add(iGeom);
    }
    // Simplify the second dataset (s)
    List<IGeometry> simplifiedS = new ArrayList<>();
    IntArray mappingS = new IntArray();
    for (int iGeom = 0; iGeom < s.size(); iGeom++) {
      List<IGeometry> parts = quadSplit(s.get(iGeom), threshold);
      simplifiedS.addAll(parts);
      for (int i = 0; i < parts.size(); i++)
        mappingS.add(iGeom);
    }
    // Find overlaps between the simplified lists
    final Envelope mbr1 = new Envelope(2);
    final Envelope mbr2 = new Envelope(2);
    spatialJoinIntersectsPlaneSweep(simplifiedR, simplifiedS, (i, j, refPoint) -> {
      // Retrieve the indexes of the original geometries
      i = mappingR.get(i);
      j = mappingS.get(j);
      long pair = (i << 32) | j;
      if (!reportedPairs.contains(pair)) {
        // First time to find this pair, report it and add it to the list of reported pairs
        reportedPairs.add(pair);
        if (results != null) {
          // Compute the correct reference point based on the original polygons
          r.get(i).envelope(mbr1);
          s.get(j).envelope(mbr2);
          refPoint[0] = Math.max(mbr1.minCoord[0], mbr2.minCoord[0]);
          refPoint[1] = Math.max(mbr1.minCoord[1], mbr2.minCoord[1]);
          results.accept(i, j, refPoint);
        }
      }
    });

    return reportedPairs.size();
  }

  /**
   * Computes the overlap join between the two given lists of geometries. Each geometry is simplified using the given
   * threshold. Any geometry that contains more than the threshold points will be simplified by partitioning it
   * into smaller geometries.
   * @param r
   * @param s
   * @param threshold
   * @param results
   * @return
   */
  public static int spatialJoinIntersectsWithSimplification2(List<IFeature> r, List<IFeature> s, int threshold,
                                                            SJResult results) {
    // A set of reported results to run duplicate elimination
    Set<Long> reportedPairs = new HashSet<>();
    // Run quad split for both inputs
    List<IGeometry> simplifiedR = new ArrayList<>();
    // Map each part to its main
    IntArray mappingR = new IntArray();
    for (int iGeom = 0; iGeom < r.size(); iGeom++) {
      List<IGeometry> parts = quadSplit(r.get(iGeom).getGeometry(), threshold);
      simplifiedR.addAll(parts);
      for (int i = 0; i < parts.size(); i++)
        mappingR.add(iGeom);
    }
    // Simplify the second dataset (s)
    List<IGeometry> simplifiedS = new ArrayList<>();
    IntArray mappingS = new IntArray();
    for (int iGeom = 0; iGeom < s.size(); iGeom++) {
      List<IGeometry> parts = quadSplit(s.get(iGeom).getGeometry(), threshold);
      simplifiedS.addAll(parts);
      for (int i = 0; i < parts.size(); i++)
        mappingS.add(iGeom);
    }
    // Find overlaps between the simplified lists
    final Envelope mbr1 = new Envelope(2);
    final Envelope mbr2 = new Envelope(2);
    spatialJoinIntersectsPlaneSweep(simplifiedR, simplifiedS, (i, j, refPoint) -> {
      // Retrieve the indexes of the original geometries
      i = mappingR.get(i);
      j = mappingS.get(j);
      long pair = (i << 32) | j;
      if (!reportedPairs.contains(pair)) {
        // First time to find this pair, report it and add it to the list of reported pairs
        reportedPairs.add(pair);
        if (results != null) {
          // Compute the correct reference point based on the original polygons
          r.get(i).getGeometry().envelope(mbr1);
          s.get(j).getGeometry().envelope(mbr2);
          refPoint[0] = Math.max(mbr1.minCoord[0], mbr2.minCoord[0]);
          refPoint[1] = Math.max(mbr1.minCoord[1], mbr2.minCoord[1]);
          results.accept(i, j, refPoint);
        }
      }
    });

    return reportedPairs.size();
  }

  /**
   * Simplifies a polygon by recursively splitting it into quadrants until each part has no more than {@code threshold}
   * points.
   * @param geometry
   * @param threshold
   * @return
   */
  public static List<IGeometry> quadSplit(IGeometry geometry, int threshold) {
    assert geometry.getCoordinateDimension() == 2 : "This function only works with 2D geometries";
    Geometry esriGeometry = EsriConverter.createEsriGeometry(geometry);
    List<Geometry> parts = new ArrayList<>();
    int numGeomsToCheck = 1;
    parts.add(esriGeometry);
    com.esri.core.geometry.Envelope mbr = new com.esri.core.geometry.Envelope();
    com.esri.core.geometry.Envelope quadrant = new com.esri.core.geometry.Envelope();
    // Convert the threshold to an estimated memory size for simplicity to use with Esri API
    threshold *= 8 * 2;
    SpatialReference spatialReference = SpatialReference.create(4326);
    while (numGeomsToCheck > 0) {
      Geometry geomToCheck = parts.remove(0);
      numGeomsToCheck--;
      if (geomToCheck.estimateMemorySize() <= threshold) {
        // Already simple. Add to results
        parts.add(geomToCheck);
      } else {
        // A complex geometry, split into four
        geomToCheck.queryEnvelope(mbr);
        // First quadrant
        mbr.queryEnvelope(quadrant);
        quadrant.setXMax(mbr.getCenterX());
        quadrant.setYMax(mbr.getCenterY());
        parts.add(GeometryEngine.intersect(geomToCheck, quadrant, spatialReference));
        // Second quadrant
        mbr.queryEnvelope(quadrant);
        quadrant.setXMin(mbr.getCenterX());
        quadrant.setYMax(mbr.getCenterY());
        parts.add(GeometryEngine.intersect(geomToCheck, quadrant, spatialReference));
        // Third quadrant
        mbr.queryEnvelope(quadrant);
        quadrant.setXMin(mbr.getCenterX());
        quadrant.setYMin(mbr.getCenterY());
        parts.add(GeometryEngine.intersect(geomToCheck, quadrant, spatialReference));
        // Fourth quadrant
        mbr.queryEnvelope(quadrant);
        quadrant.setXMax(mbr.getCenterX());
        quadrant.setYMin(mbr.getCenterY());
        parts.add(GeometryEngine.intersect(geomToCheck, quadrant, spatialReference));
        numGeomsToCheck += 4;
      }
    }
    // Convert all parts back to lite geometry
    List<IGeometry> results = new ArrayList<>();
    for (int i = 0; i < parts.size(); i++) {
      results.add(EsriConverter.fromEsriGeometry(parts.get(i), null));
    }
    return results;
  }

  /**
   * Runs a spatial join by building two R-trees and joining them
   * @param r
   * @param s
   * @param results
   * @return number of results found
   */
  public static int spatialJoinQuadSplitIndex(List<IFeature> r, List<IFeature> s, int threshold, SJResult results) {
    // A set of reported results to run duplicate elimination
    Set<Long> reportedPairs = new HashSet<>();
    // Run quad split for both inputs
    List<IGeometry> simplifiedR = new ArrayList<>();
    // Map each part to its main
    IntArray mappingR = new IntArray();
    for (int iGeom = 0; iGeom < r.size(); iGeom++) {
      List<IGeometry> parts = quadSplit(r.get(iGeom).getGeometry(), threshold);
      simplifiedR.addAll(parts);
      for (int i = 0; i < parts.size(); i++)
        mappingR.add(iGeom);
    }
    // Simplify the second dataset (s)
    List<IGeometry> simplifiedS = new ArrayList<>();
    IntArray mappingS = new IntArray();
    for (int iGeom = 0; iGeom < s.size(); iGeom++) {
      List<IGeometry> parts = quadSplit(s.get(iGeom).getGeometry(), threshold);
      simplifiedS.addAll(parts);
      for (int i = 0; i < parts.size(); i++)
        mappingS.add(iGeom);
    }
    // Find overlaps between the simplified lists
    final Envelope mbr1 = new Envelope(2);
    final Envelope mbr2 = new Envelope(2);
    spatialJoinIntersectsIndex(simplifiedR, simplifiedS, (i, j, refPoint) -> {
      // Retrieve the indexes of the original geometries
      i = mappingR.get(i);
      j = mappingS.get(j);
      long pair = (i << 32) | j;
      if (!reportedPairs.contains(pair)) {
        // First time to find this pair, report it and add it to the list of reported pairs
        reportedPairs.add(pair);
        if (results != null) {
          // Compute the correct reference point based on the original polygons
          r.get(i).getGeometry().envelope(mbr1);
          s.get(j).getGeometry().envelope(mbr2);
          refPoint[0] = Math.max(mbr1.minCoord[0], mbr2.minCoord[0]);
          refPoint[1] = Math.max(mbr1.minCoord[1], mbr2.minCoord[1]);
          results.accept(i, j, refPoint);
        }
      }
    });

    return reportedPairs.size();
  }

  public static int spatialJoinIntersectsIndex(List<IGeometry> r, List<IGeometry> s, SJResult results) {
    if (r.size() == 0 || s.size() == 0)
      return 0;
    assert r.get(0).getCoordinateDimension() == 2 : "R should be 2D geometries";
    assert s.get(0).getCoordinateDimension() == 2 : "S should be 2D geometries";
    double[][] coordsR = getMBRs(r);
    double[][] coordsS = getMBRs(s);
    RTreeGuttman rtree1 = new RRStarTree(10, 40);
    rtree1.initializeFromRects(coordsR[0], coordsR[1], coordsR[2], coordsR[3]);
    RTreeGuttman rtree2 = new RRStarTree(10, 40);
    rtree2.initializeFromRects(coordsS[0], coordsS[1], coordsS[2], coordsS[3]);
    double[] refPoint = new double[2];
    return RTreeGuttman.spatialJoin(rtree1, rtree2, (i,j) -> {
      // Compute the reference point
      if (r.get(i).intersects(s.get(j))) {
        refPoint[0] = Math.max(coordsR[0][i], coordsS[0][j]);
        refPoint[1] = Math.max(coordsR[1][i], coordsS[1][j]);
        results.accept(i, j, refPoint);
      }
    });
  }
  public static int spatialJoinIntersectsIndexFeatures(List<IFeature> r, List<IFeature> s, SJResult results) {
    if (r.size() == 0 || s.size() == 0)
      return 0;
    assert r.get(0).getGeometry().getCoordinateDimension() == 2 : "R should be 2D geometries";
    assert s.get(0).getGeometry().getCoordinateDimension() == 2 : "S should be 2D geometries";
    long t1 = System.nanoTime();
    double[][] coordsR = getMBRs2(r);
    double[][] coordsS = getMBRs2(s);
    RTreeGuttman rtree1 = new RRStarTree(10, 40);
    rtree1.initializeFromRects(coordsR[0], coordsR[1], coordsR[2], coordsR[3]);
    RTreeGuttman rtree2 = new RRStarTree(10, 40);
    rtree2.initializeFromRects(coordsS[0], coordsS[1], coordsS[2], coordsS[3]);
    long t2 = System.nanoTime();
    double[] refPoint = new double[2];
    int resultSize = RTreeGuttman.spatialJoin(rtree1, rtree2, (i, j) -> {
      // Compute the reference point
      refPoint[0] = Math.max(coordsR[0][i], coordsS[0][j]);
      refPoint[1] = Math.max(coordsR[1][i], coordsS[1][j]);
      results.accept(i, j, refPoint);
    });
    long t3 = System.nanoTime();
    LOG.info(String.format("Built two R-trees of sizes %d and %d in %f seconds and joined in %f seconds to produce %d results",
        rtree1.numOfDataEntries(), rtree2.numOfDataEntries(), (t2-t1)*1E-9, (t3-t2)*1E-9, resultSize));
    return resultSize;
  }

}
