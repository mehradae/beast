/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations

import java.io.IOException

import edu.ucr.cs.bdlab.geolite.{Envelope, IFeature, Point}
import edu.ucr.cs.bdlab.io.{FeatureWriter, SpatialInputFormat, SpatialOutputFormat}
import edu.ucr.cs.bdlab.stsynopses.{AbstractHistogram, EulerHistogram2D, UniformHistogram}
import edu.ucr.cs.bdlab.util.{CounterOutputStream, OperationMetadata, OperationParam, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.spark.SparkContext
import org.apache.spark.api.java.{JavaPairRDD, JavaRDD}
import org.apache.spark.rdd.RDD

import scala.annotation.varargs

/**
  * Computes the histogram of an input file.
  */
@OperationMetadata(
  shortName =  "histogram",
  description = "Computes a uniform histogram for a geometry file",
  inputArity = "1",
  outputArity = "0",
  inheritParams = Array(classOf[SpatialInputFormat], classOf[SpatialOutputFormat])
)
object Histogram extends CLIOperation {
  @transient lazy val logger: Log = LogFactory.getLog(getClass)

  /**
    * Number of buckets in the histogram
    */
  @OperationParam(
    description = "Total number of buckets in the histogram",
    defaultValue = "1000"
  )
  val NumBuckets = "numbuckets"

  @OperationParam(
    description = "Type of histogram {simple, euler}",
    defaultValue = "simple"
  )
  val HistogramType = "histogramtype"

  @OperationParam(
    description = "The value to compute in the histogram {count, size}",
    defaultValue = "count"
  )
  val HistogramValue = "histogramvalue"

  /**
    * Compute the grid dimension from the given sequence of numbers. If the given sequence contains a single number,
    * it is treated as a total number of partitions and the number of partitions along each dimension is computed
    * using the function [[UniformHistogram#computeNumPartitions(Envelope, int)]]
    * @param mbr the MBR of the input space. Used to compute square-like cells
    * @param numPartitions the desired number of partitions. Either a single number of a sequence of dimensions
    * @return an array of number of partitions along each axis
    */
  protected def computeGridDimensions(mbr: Envelope, numPartitions: Int*): Array[Int] = {
    if (numPartitions.size < mbr.getCoordinateDimension) {
      // Treat it as number of buckets and compute the number of partitions correctly
      val numBuckets: Int = numPartitions.product
      UniformHistogram.computeNumPartitions(mbr, numBuckets)
    } else {
      numPartitions.toArray
    }
  }

  @varargs def computeSimpleHistogramJ(dataPoints: JavaPairRDD[Point, java.lang.Long], mbr: Envelope,
                                       numPartitions: Int*): UniformHistogram = {
    computeSimpleHistogram(dataPoints.rdd.map(kv => (kv._1, kv._2)), mbr, numPartitions:_*)
  }

  /**
    * Compute a simple histogram where each cell (bucket) accumulates the values of the points in this cell
    * @param dataPoints pairs of points and values to accumulate
    * @param mbr the MBR of the input space
    * @param numPartitions the total number of partitions to create along each dimension
    * @return
    */
  @varargs def computeSimpleHistogram(dataPoints : RDD[(Point, Long)], mbr : Envelope,
                                      numPartitions : Int*): UniformHistogram = {
    val partialHistograms = dataPoints.mapPartitions(iPoints => {
      val partialHistogram = new UniformHistogram(mbr, computeGridDimensions(mbr, numPartitions:_*):_*)
      while (iPoints.hasNext) {
        val iPoint = iPoints.next
        partialHistogram.addPoint(iPoint._1.coords, iPoint._2)
      }
      List(partialHistogram).iterator
    })
    partialHistograms.reduce((h1, h2) => h1.mergeAligned(h2))
  }

  @varargs def computeSimpleSizeHistogramJ(features: JavaRDD[_ <: IFeature], mbr : Envelope,
                                           numPartitions: Int*): AbstractHistogram =
    computeSimpleSizeHistogram(features.rdd, mbr, numPartitions:_*)

  /**
    * Computes a simple uniform histogram where each feature is mapped to its centroid and the accumulated values are
    * the approximate memory storage of a features [[IFeature#getStorageSize]]
    * @param features the features to compute the histogram for
    * @param mbr the minimum bounding rectangle of the features. Used to compute the histogram grid coordinates
    * @param numPartitions either a single value that indicates the total number of buckets or an array that represents
    *                      the grid dimension along each axis, i.e., number of columns, rows, ... etc.
    * @return
    */
  @varargs def computeSimpleSizeHistogram(features: RDD[_ <: IFeature], mbr: Envelope,
                                          numPartitions: Int*): UniformHistogram = {
    val nonEmptyFeatures = features.filter(!_.getGeometry.isEmpty)
    val dataPoints = nonEmptyFeatures.map( f => {
      val p = new Point(f.getGeometry.getCoordinateDimension)
      (f.getGeometry.centroid(p), f.getStorageSize.toLong)
    })
    computeSimpleHistogram(dataPoints, mbr, numPartitions:_*)
  }

  /**
    * Compute a uniform histogram that counts the number of features with a centroid in each grid cell from Java
    * @param features the features to use for computation. Only the centroid of each geometry is used.
    * @param mbr the minimum bounding rectangle of the input
    * @param numPartitions either a single value that indicates the total number of buckets or an array that represents
    *                      the grid dimension along each axis, i.e., number of columns, rows, ... etc.
    * @return the computed histogram
    */
  @varargs def computeSimpleCountHistogram(features: JavaRDD[_ <: IFeature], mbr: Envelope, numPartitions: Int*): UniformHistogram =
    computeSimpleCountHistogram(features.rdd, mbr, numPartitions:_*)

  /**
    * Similar to [[computeSimpleCountHistogram()]] but computes the summary first
    *
    * @param features the features to use for computation. Only the centroid of each geometry is used.
    * @param numPartitions total number of partitions (bins) in the histogram
    * @return
    */
  @varargs def computeSimpleCountHistogramJ(features: JavaRDD[_ <: IFeature], numPartitions: Int*): UniformHistogram =
    computeSimpleCountHistogram(features, mbr=GeometricSummary.computeForFeatures(features), numPartitions:_*)

  /**
    * Compute a uniform histogram that counts the number of features with a centroid in each grid cell
    * @param features the features to use for computation. Only the centroid of each geometry is used.
    * @param mbr the minimum bounding rectangle of the input
    * @param numPartitions either a single value that indicates the total number of buckets or an array that represents
    *                      the grid dimension along each axis, i.e., number of columns, rows, ... etc.
    * @return the computed histogram
    */
  @varargs def computeSimpleCountHistogram(features: RDD[_ <: IFeature], mbr: Envelope,
                                           numPartitions: Int*): UniformHistogram = {
    val nonEmptyFeatures = features.filter(!_.getGeometry.isEmpty)
    val dataPoints = nonEmptyFeatures.map( f => {
      val p = new Point(f.getGeometry.getCoordinateDimension)
      (f.getGeometry.centroid(p), 1L)
    })
    computeSimpleHistogram(dataPoints, mbr, numPartitions:_*)
  }

  @varargs def computeSimpleWriteSizeHistogramJ(opts: UserOptions, features: JavaRDD[_ <: IFeature], mbr: Envelope,
                                                numPartitions: Int*): AbstractHistogram =
    computeSimpleWriteSizeHistogram(opts, features.rdd, mbr, numPartitions:_*)

  /**
    * Compute a uniform simple histogram which calculates the total write size based on the configure output format.
    * @param opts user options used to initialize the feature writer
    * @param features the features to compute the histogram for
    * @param mbr the minimum bounding rectangle of the input that is used to compute the histogram grid boundaries
    * @param numPartitions the dimensions of the histogram grid as the number of partitions along each axis
    * @return
    */
  @varargs def computeSimpleWriteSizeHistogram(opts: UserOptions, features: RDD[_ <: IFeature], mbr: Envelope,
                                               numPartitions: Int*): UniformHistogram = {
    val writerClass = SpatialOutputFormat.getConfiguredFeatureWriterClass(opts)
    // Filter out empty features
    val nonEmptyFeatures = features.filter(!_.getGeometry.isEmpty)
    val partialHistograms = nonEmptyFeatures.mapPartitions(iFeatures => {
      val partialHistogram = new UniformHistogram(mbr, computeGridDimensions(mbr, numPartitions:_*):_*)
      val fakeWriter: FeatureWriter = writerClass.newInstance
      fakeWriter.initialize(new CounterOutputStream, opts)
      val point = new Point(mbr.getCoordinateDimension)
      while (iFeatures.hasNext) {
        val f = iFeatures.next
        f.getGeometry.centroid(point)
        partialHistogram.addPoint(point.coords, fakeWriter.estimateSize(f))
      }
      Option(partialHistogram).iterator
    })
    partialHistograms.reduce((h1, h2) => h1.mergeAligned(h2))
  }

  /**
    * Compute the Euler histogram of the given set of envelopes with values. The resulting histogram contains four
    * values per cell (in two dimensions). Currently, this method only works for two dimensions
    * @param dataRecords the data points contains pairs of envelopes and values
    * @param mbr the MBR of the input space
    * @param numPartitions the number of partitions to create in the histogram
    */
  @varargs def computeEulerHistogram(dataRecords : RDD[(Envelope, Long)], mbr : Envelope,
                                     numPartitions : Int*): EulerHistogram2D = {
    val gridDimensions = computeGridDimensions(mbr, numPartitions:_*)
    val partialHistograms = dataRecords.mapPartitions(iRecords => {
      val partialHistogram = new EulerHistogram2D(mbr, gridDimensions(0), gridDimensions(1))
      while (iRecords.hasNext) {
        val records = iRecords.next
        // Make sure the MBR fits within the boundaries
        records._1.shrink(mbr)
        if (!records._1.isEmpty)
          partialHistogram.addEnvelope(records._1.minCoord(0), records._1.minCoord(1),
            records._1.maxCoord(1), records._1.maxCoord(1), records._2)
      }
      List(partialHistogram).iterator
    })
    partialHistograms.reduce((h1, h2) => h1.mergeAligned(h2))
  }

  @varargs def computeEulerSizeHistogram(features: JavaRDD[_ <: IFeature], mbr: Envelope,
                                         numPartitions: Int*): EulerHistogram2D =
    computeEulerSizeHistogram(features.rdd, mbr, numPartitions:_*)

  @varargs def computeEulerSizeHistogram(features: RDD[_ <: IFeature], mbr: Envelope,
                                         numPartitions: Int*): EulerHistogram2D = {
    val numDimensions = mbr.getCoordinateDimension
    val nonEmptyFeatures = features.filter(!_.getGeometry.isEmpty)
    val envelopeSizes = nonEmptyFeatures.map(f => {
      val envelope = f.getGeometry.envelope(new Envelope(numDimensions))
      (envelope, f.getStorageSize.toLong)
    })
    computeEulerHistogram(envelopeSizes, mbr, numPartitions:_*)
  }

  @varargs def computeEulerCountHistogram(features: RDD[_ <: IFeature], mbr: Envelope,
                                          numPartitions: Int*): EulerHistogram2D = {
    val numDimensions = mbr.getCoordinateDimension
    val nonEmptyFeatures = features.filter(!_.getGeometry.isEmpty)
    val envelopeSizes = nonEmptyFeatures.map(f => (f.getGeometry.envelope(new Envelope(numDimensions)), 1L))
    computeEulerHistogram(envelopeSizes, mbr, numPartitions:_*)
  }

  @throws(classOf[IOException])
  override def run(opts: UserOptions, sc: SparkContext): AbstractHistogram = {
    // Extract user parameters
    def histogramType = Symbol(opts.get(HistogramType, "simple").toLowerCase)
    def histogramValue = Symbol(opts.get(HistogramValue, "count").toLowerCase)
    val numBuckets = opts.getInt(NumBuckets, 1000)
    val features = SpatialReader.readInput(sc, opts)

    // Compute the MBR of the input to be able to compute the boundaries and dimensions of the grid
    val mbr = GeometricSummary.computeForFeatures(features)

    // Compute number of partitions along each dimension
    val numPartitions = UniformHistogram.computeNumPartitions(mbr, numBuckets)

    (histogramType, histogramValue) match {
      case ('simple, 'count) => computeSimpleCountHistogram(features, mbr, numPartitions:_*)
      case ('simple, 'size) => computeSimpleSizeHistogram(features, mbr, numPartitions:_*)
      case ('simple, 'writesize) => computeSimpleWriteSizeHistogram(opts, features, mbr, numPartitions:_*)
      case ('euler, 'count) => computeEulerCountHistogram(features, mbr, numPartitions:_*)
      case ('euler, 'size) => computeEulerSizeHistogram(features, mbr, numPartitions:_*)
    }
  }
}
