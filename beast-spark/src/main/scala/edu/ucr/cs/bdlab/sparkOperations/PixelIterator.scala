package edu.ucr.cs.bdlab.sparkOperations

import java.awt.geom.Point2D
import java.io.File

import edu.ucr.cs.bdlab.raptor.GeoTiffReader
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path

class PixelIterator(iterator: Iterator[(Long, (Long, Int, Int, Int))], broadCastrasterList: Array[String]) extends Iterator[(Long,Float)]{

  var X1: Int = 2
  var X2: Int = 1
  var Y: Int = -1
  var PolygonIndex : Long = -1
  var TileID = -1
  var FileID = -1
  var raster: GeoTiffReader = null
  override def hasNext: Boolean = ((X2 >= X1) || iterator.hasNext)

  override def next(): (Long,Float) = {

    if((X1 == 2) && (X2 == 1)){
      val x = iterator.next()
      X1 = x._2._3
      X2 = x._2._4
      Y = x._2._2
      PolygonIndex = x._2._1
      TileID = (x._1 & 0xffffffff).toInt
      val temp = FileID
      FileID = (x._1 >> 32).toInt

      if(temp!=FileID){

        if(raster!=null)
          raster.close()

        val conf_file = new Configuration()
        val geoTiffFile =new Path (broadCastrasterList(FileID))
        raster = new GeoTiffReader()
        raster.initialize(geoTiffFile.getFileSystem(conf_file), geoTiffFile, 0)
      }
    }

    val pixelValues = new Array[Float](raster.getNumComponents)
    raster.getPixelValueAsFloat(X1, Y , pixelValues)
    X1=X1+1
    while(pixelValues(0) == raster.getFillValue.toFloat && X2 >= X1 )
    {
        raster.getPixelValueAsFloat(X1,Y,pixelValues)
        X1 = X1 +1
    }
    val point = new Point2D.Double()
    raster.gridToModel(X1-1,Y,point)

    if(X1>X2 && !iterator.hasNext)
      raster.close()
    if(X1>X2 && iterator.hasNext)
    {
      X1 = 2
      X2 = 1
    }


   (PolygonIndex, pixelValues(0))

  }
}
