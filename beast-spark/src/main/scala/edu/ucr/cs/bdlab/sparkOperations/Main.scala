/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations

import edu.ucr.cs.bdlab.sparkOperations.Histogram.getClass
import edu.ucr.cs.bdlab.util.{OperationUtil, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.spark.{SparkConf, SparkContext}


/**
  * A main class that runs all supported operations from the command line
  */
object Main {
  @transient lazy val logger: Log = LogFactory.getLog(getClass)

  def main(args: Array[String]): Unit = {
    // Get the operation to run
    if (args.length == 0)
      OperationUtil.printUsageAndExit()
    val operation = OperationUtil.getOperation(args(0))

    // Invoke the run method
    // Parse command line arguments
    val userOptions: UserOptions = OperationUtil.createAndCheckCLO(operation, args)

    // Create the Spark context
    val conf = new SparkConf
    conf.setAppName("BEAST/" + operation.metadata.shortName)

    // Set Spark master to local if not already set
    if (!conf.contains("spark.master"))
      conf.setMaster("local[*]")
    logger.info(s"Using master '${conf.get("spark.master")}'")

    val opInstance: CLIOperation =
      try {
        // 1- Test the operation as a Scala operation
        // See: https://stackoverflow.com/questions/1913092/getting-object-instance-by-string-name-in-scala
        val opClass = Class.forName(operation.operationClass.getName)
        opClass.getField("MODULE$").get(opClass).asInstanceOf[CLIOperation]
      } catch {
        // 2- Fall back to Java operation
        case _other: Exception => operation.operationClass.asSubclass(classOf[CLIOperation]).newInstance
      }
    // Initialize the spark context
    val sc = new SparkContext(conf)
    val t1 = System.nanoTime
    try {
      opInstance.setup(userOptions)
      opInstance.run(userOptions, sc)
      val t2 = System.nanoTime
      logger.info(f"The operation ${operation.metadata.shortName} finished in ${(t2 - t1) * 1E-9}%f seconds")
    } catch {
      case _other: Exception =>
        val t2 = System.nanoTime
        logger.info(f"The operation ${operation.metadata.shortName} failed after ${(t2 - t1) * 1E-9}%f seconds")
        throw _other
    } finally {
      sc.stop
    }
  }
}
