/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations

import java.io.Serializable

import edu.ucr.cs.bdlab.geolite.{EmptyGeometry, Envelope, IFeature, Point}
import edu.ucr.cs.bdlab.io.{CSVFeature, CSVFeatureReader, SpatialInputFormat}
import edu.ucr.cs.bdlab.util.UserOptions
import edu.ucr.cs.bdlab.wktparser.WKTParser
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.io.Text
import org.apache.spark.SparkContext
import org.apache.spark.api.java.{JavaRDD, JavaSparkContext}
import org.apache.spark.rdd.RDD

/**
  * Reads and parses input files that contain spatial features into RDDs
  */
object SpatialReader {
  @transient lazy val logger: Log = LogFactory.getLog(getClass)

  /**
    * A function that parses WKT-encoded CSV files
    * @param fieldSeparator the single character that separates fields in the same line
    * @param gCol the 0-based index of the column that contains the WKT geometry
    */
  class RDDWKTParser(fieldSeparator: Char, gCol: Int, quoteCharacters: String = CSVFeatureReader.DefaultQuoteCharacters) extends Function[String, IFeature] with Serializable {

    /**WKT parser to parse WKT-encoded geometries*/
    @transient var wktParser: WKTParser = _

    /**A temporary Text line for extracting fields and WKT values*/
    @transient var line: Text = _

    override def apply(s: String): IFeature = {
      if (line == null) line = new Text
      if (wktParser == null) wktParser = new WKTParser
      line.set(s.getBytes)
      val feature = new CSVFeature
      val wkt = CSVFeature.deleteAttribute(line, fieldSeparator, gCol, quoteCharacters)
      if (wkt == null) {
        logger.warn(s"Could not find the field #$gCol with the separator '$fieldSeparator' in the line '$line'")
        feature.setGeometry(EmptyGeometry.instance)
      } else {
        feature.setGeometry(wktParser.parse(wkt, null))
      }
      feature.setFieldSeparator(fieldSeparator.toByte)
      feature.setFieldValues(line.getBytes, 0, line.getLength)
      feature
    }
  }

  def parseWKT(lines: JavaRDD[String], gCol: Int, fieldSeparator: Char): JavaRDD[IFeature] =
    parseWKT(lines.rdd, gCol, fieldSeparator)

  /**
    * Parse a text file that contains WKT-encoded geometries
    *
    * @param lines an RDD of text as one record per line
    * @param gCol the index of the field that contains the WKT geometry
    * @param fieldSeparator the field (column) separator, e.g., tab (\t) or comma (,)
    * @return a parse RDD where each line is converted to a feature
    */
  def parseWKT(lines: RDD[String], gCol: Int, fieldSeparator: Char) : RDD[IFeature] =
    lines.map(new RDDWKTParser(fieldSeparator, gCol))

  /**
    * A class that parses CSV lines with point attributes
    * @param fieldSeparator the single character that separates fields in the same line
    * @param colIndexes the indices of the columns that contain the coordinates of the point
    */
  class RDDPointParser(fieldSeparator: Char, private var colIndexes: Array[Int],
                       quoteCharacters: String = CSVFeatureReader.DefaultQuoteCharacters) extends Function[String, IFeature] with Serializable {
    /**
      * Adjust the positions of the columns so that they can be extracted in order
      */
    if (colIndexes != null)
    for (i <- 0 until colIndexes.size) {
      if (colIndexes(i) != -1) {
        for (j <- i + 1 until colIndexes.size) {
          if (colIndexes(j) > colIndexes(i))
            colIndexes(j) = colIndexes(j) - 1
        }
      }
    }

    /**A temporary line to use for parsing. It avoids recreating a Text object for each record.*/
    @transient private var line : Text = _

    override def apply(s: String): IFeature = {
      if (line == null) line = new Text
      line.set(s)
      val p = new Point(colIndexes.size)
      for (iCol <- 0 until colIndexes.size) {
        if (colIndexes(iCol) != -1) {
          p.coords(iCol) = CSVFeature.deleteAttribute(line, fieldSeparator, colIndexes(iCol), quoteCharacters).toDouble
        }
      }
      val feature = new CSVFeature(p)
      feature.setFieldSeparator(fieldSeparator.toByte)
      feature.setFieldValues(line.getBytes, 0, line.getLength)
      feature
    }
  }

  /**Java shortcut*/
  def parsePointsXY(textFile: JavaRDD[String], xCol: Int, yCol: Int, fieldSeparator: Char): JavaRDD[IFeature] =
    JavaRDD.fromRDD(parsePointsXY(textFile.rdd, xCol, yCol, fieldSeparator))

  /**
    * Parses a CSV file with a custom separator that contains two-dimension points. This is a Spark transformation.
    *
    * @param textFile       a text file loaded as one line per record
    * @param xCol           the index of the column that contains the x-coordinate (0-based)
    * @param yCol           the index of the column that contains the y-coordinate (0-based)
    * @param fieldSeparator the field separator
    * @return a new RDD that transforms that given text file into { @link IFeature}s with points as geometries.
    */
  def parsePointsXY(textFile: RDD[String], xCol: Int, yCol: Int, fieldSeparator: Char): RDD[IFeature] =
    textFile.map(new RDDPointParser(fieldSeparator, Array(xCol, yCol)))

  /**Java shortcut*/
  def parsePointsXYZ(textFile: JavaRDD[String], xCol: Int, yCol: Int, zCol: Int,
                      fieldSeparator: Char): JavaRDD[IFeature] =
    JavaRDD.fromRDD(parsePointsXYZ(textFile.rdd, xCol, yCol, zCol, fieldSeparator))

  /**
    * A transformation that transforms a text file (CSV) into three-dimensional points.
    *
    * @param textFile       the text file to parse as a single line per record
    * @param xCol           the index of the column that contains the x-coordinate (0-based)
    * @param yCol           the index of the column that contains the y-coordinate (0-based)
    * @param zCol           the index of the column that contains the z-coordinate (0-based)
    * @param fieldSeparator the field separator
    * @return the transformed RDD that contains the features with points as geometries
    */
  def parsePointsXYZ(textFile: RDD[String], xCol: Int, yCol: Int, zCol: Int, fieldSeparator: Char): RDD[IFeature] =
    textFile.map(new RDDPointParser(fieldSeparator, Array(xCol, yCol, zCol)))

  /**Java shortcut*/
  def parsePointsXYM(textFile: JavaRDD[String], xCol: Int, yCol: Int, mCol: Int, fieldSeparator: Char): JavaRDD[IFeature] =
    JavaRDD.fromRDD(parsePointsXYM(textFile.rdd, xCol, yCol, mCol, fieldSeparator))

  /**
    * A transformation that transforms a text file (CSV) into two-dimensional points with measure (m) values.
    *
    * @param textFile       the text file to parse as a single line per record
    * @param xCol           the index of the column that contains the x-coordinate (0-based)
    * @param yCol           the index of the column that contains the y-coordinate (0-based)
    * @param mCol           the index of the column that contains the measure value (0-based)
    * @param fieldSeparator the field separator
    * @return the transformed RDD that contains the features with points as geometries
    */
  def parsePointsXYM(textFile: RDD[String], xCol: Int, yCol: Int, mCol: Int, fieldSeparator: Char): RDD[IFeature] =
    parsePointsXYZM(textFile, xCol, yCol, -1, mCol, fieldSeparator)

  /**
    * Parses an input text that contains the attributes x, y, z, and m. If zCol or mCol are -1 they are ignored.
    *
    * @param textFile       the text file to parse as a single line per record
    * @param xCol           the index of the column that contains the x-coordinate (0-based)
    * @param yCol           the index of the column that contains the y-coordinate (0-based)
    * @param zCol           the index of the column that contains the z-coordinate (0-based)
    * @param mCol           the index of the column that contains the measure value (0-based)
    * @param fieldSeparator the field separator
    * @return the transformed RDD that contains the features with points as geometries
    */
  def parsePointsXYZM(textFile: RDD[String], xCol: Int, yCol: Int, zCol: Int, mCol: Int, fieldSeparator: Char): RDD[IFeature] =
    textFile.map(new RDDPointParser(fieldSeparator, Array(xCol, yCol, zCol, mCol)))

  /**Java shortcut*/
  def readInput(sc: JavaSparkContext, opts: UserOptions, inputIndex: Int) : JavaRDD[IFeature] =
    JavaRDD.fromRDD(readInput(sc.sc, opts, inputIndex))

  /**
    * Loads an input file using using the input format identified by the given fileIndex
    *
    * @param sc  the context to use for loading the file
    * @param opts      the user-provided options that contain the input format details.
    * @param inputIndex the index of the file to use its input format (0-based)
    * @return an RDD that contains the loaded features
    */
  def readInput(sc: SparkContext, opts: UserOptions, inputIndex: Int = 0) : RDD[IFeature] = {
    // Create a new Hadoop configuration with all the default parameters (e.g., default file system)
    val conf = opts.loadIntoHadoopConfiguration(sc.hadoopConfiguration, inputIndex)
    // Spark is not ready to use mutable objects with all its operations. It is safer to user immutable objects
    conf.setBoolean(SpatialInputFormat.ImmutableObjects, true)
    sc.newAPIHadoopFile(opts.getInputs()(inputIndex), classOf[SpatialInputFormat], classOf[Envelope], classOf[IFeature], conf).values
  }

  /**Java shortcut*/
  def readInput(sc: JavaSparkContext, opts: UserOptions) : JavaRDD[IFeature] = readInput(sc, opts, 0)

  /**Java shortcut*/
  def readInput(sc: JavaSparkContext, opts: UserOptions, filename: String, iFormat: String): JavaRDD[IFeature] =
    JavaRDD.fromRDD(readInput(sc.sc, opts, filename, iFormat))

  /**
    * Loads an input file with the given input format.
    *
    * @param sc  the context to use for loading the file
    * @param opts     the user-provided options that contain the input format details.
    * @param filename the filename (or path) to load
    * @param iFormat  use this input format to load the file and ignore the input format in the given user options.
    * @return an RDD that contains the loaded features
    */
  def readInput(sc: SparkContext, opts: UserOptions, filename: String, iFormat: String) : RDD[IFeature] = {
    // Create a new Hadoop configuration with all the default parameters (e.g., default file system)
    val conf = opts.loadIntoHadoopConfiguration(sc.hadoopConfiguration)
    conf.set(SpatialInputFormat.InputFormat, iFormat)
    // Spark is not ready to use mutable objects with all its operations. It is safer to user immutable objects
    conf.setBoolean(SpatialInputFormat.ImmutableObjects, true)
    sc.newAPIHadoopFile(filename, classOf[SpatialInputFormat], classOf[Envelope], classOf[IFeature], conf).values

  }
}
