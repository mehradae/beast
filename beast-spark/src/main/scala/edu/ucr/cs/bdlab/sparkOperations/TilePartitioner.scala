package edu.ucr.cs.bdlab.sparkOperations

import org.apache.spark.Partitioner

class TilePartitioner(partitions: Int) extends Partitioner {

  override def numPartitions: Int = partitions

  override def getPartition(key: Any): Int = {
    key.asInstanceOf[Int] % numPartitions
  }
}
