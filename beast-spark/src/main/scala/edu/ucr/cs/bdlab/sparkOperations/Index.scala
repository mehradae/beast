/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations
import java.io.IOException
import java.lang.annotation.Annotation

import edu.ucr.cs.bdlab.geolite.{Envelope, IFeature, Point}
import edu.ucr.cs.bdlab.indexing.{CellPartitioner, IndexOutputFormat, IndexerParams, PartitionInfo, SpatialPartitioner}
import edu.ucr.cs.bdlab.io.{SpatialInputFormat, SpatialOutputFormat}
import edu.ucr.cs.bdlab.stsynopses.{AbstractHistogram, Summary, UniformHistogram}
import edu.ucr.cs.bdlab.util.{IntArray, OperationMetadata, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.util.StringUtils
import org.apache.spark.SparkContext
import org.apache.spark.api.java.{JavaPairRDD, JavaRDD}
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel

/**
  * Builds an index over a set of features that can be either stored to disk or kept as an RDD.
  */
@OperationMetadata(
  shortName =  "index",
  description = "Builds a distributed spatial index",
  inputArity = "+",
  outputArity = "1",
  inheritParams = Array(classOf[SpatialInputFormat], classOf[SpatialOutputFormat], classOf[IndexerParams])
)
object Index extends CLIOperation {
  @transient lazy val logger: Log = LogFactory.getLog(getClass)

  /**
    * Create a spatial partitioner from Java.
    * @param features the RDD of features to initialize the partitioner on
    * @param partitionerClass the class of the partitioner
    * @param disjoint whether a disjoint partitioner is needed or not. Not all partitioners support the disjoint option.
    * @param pType the criterion to decide the number of partitions. Allowed values are {fixed, count, size}
    * @param pSize the size associated with the pType attribute.
    * @param opts additional user options that are used to initialize the partitioner
    * @return an initialized partitioner
    */
  def createPartitioner(features : JavaRDD[IFeature],
                        partitionerClass : Class[_ <: SpatialPartitioner],
                        disjoint : Boolean,
                        pType: String,
                        pSize: Long,
                        opts : UserOptions
                        ) : SpatialPartitioner =
    createPartitioner(features.rdd, partitionerClass, disjoint, Symbol(pType.toLowerCase()), pSize, opts)

  /**
    * Create a spatial partitioner from Java.
    * @param features the RDD of features to initialize the partitioner on
    * @param partitionerClass the class of the partitioner
    * @param disjoint whether a disjoint partitioner is needed or not. Not all partitioners support the disjoint option.
    * @param pCriterion the way to compute the number of partitions to create {Fixed(n), Count(c), or Size(s)}
    * @param opts additional user options that are used to initialize the partitioner
    * @return an initialized partitioner
    */
  def createPartitionerJ(features: JavaRDD[IFeature],
                         partitionerClass: Class[_ <: SpatialPartitioner],
                         disjoint: Boolean,
                         pCriterion: String,
                         opts: UserOptions
                        ): SpatialPartitioner = {
    val partitionerInfo = parsePartitioningCriterion(pCriterion)
    createPartitioner(features.rdd, partitionerClass, disjoint, partitionerInfo._1, partitionerInfo._2, opts)
  }

  /**
    * Constructs a spatial partitioner for the given features. Returns an instance of the spatial partitioner class
    * that is given which is initialized based on the given features.
    * @param features the features to create the partitioner on
    * @param partitionerClass the class of the partitioner to construct
    * @param disjoint whether the partitioner should be disjoint or not
    * @param partitionCriterion the criterion that determines the number of partitions {fixed, count, size}
    * @param pValue the value associated with the partitioning criterion
    * @param opts the user options
    * @return a constructed spatial partitioner
    */
  def createPartitioner(features: RDD[IFeature],
                        partitionerClass: Class[_ <: SpatialPartitioner],
                        disjoint: Boolean,
                        partitionCriterion: Symbol,
                        pValue: Long,
                        opts: UserOptions
                       ): SpatialPartitioner = {
    var synopsisSize = opts.getLongBytes(IndexerParams.SynopsisSize, 10 * 1024 * 1024)
    val sc = features.context

    // Retain only non-empty geometries. Empty geometries are not taken into account for creating the partitioner
    val nonEmptyFeatures = features.filter(f => !f.getGeometry.isEmpty)

    val t1 = System.nanoTime()

    var sampleCoordinates: Array[Array[Double]] = null
    var histogram: AbstractHistogram = null
    // Retrieve the construct method to determine the required parameters
    val constructMethod = partitionerClass.getMethod("construct", classOf[Summary],
      classOf[Array[Array[Double]]], classOf[AbstractHistogram])
    val parameterAnnotations:Array[Array[Annotation]] = constructMethod.getParameterAnnotations
    // Determine whether the sample or the histogram (or both) are needed to construct the partitioner
    val sampleNeeded = parameterAnnotations(1).exists(p => p.isInstanceOf[SpatialPartitioner.Required] ||
      p.isInstanceOf[SpatialPartitioner.Preferred])
    val histogramNeeded = parameterAnnotations(2).exists(p => p.isInstanceOf[SpatialPartitioner.Required]) ||
      (opts.getBoolean(IndexerParams.BalancedPartitioning, false) &&
        parameterAnnotations(2).exists(p => p.isInstanceOf[SpatialPartitioner.Preferred]))
    // If both sample and histogram are required, reduce the size of the synopsis size to accommodate both
    if (sampleNeeded && histogramNeeded)
      synopsisSize /= 2
    // The summary is always computed as it is needed to compute either the sample or histogram
    val summary: Summary = if ((partitionCriterion == 'fixed || partitionCriterion == 'size) && histogramNeeded)
      GeometricSummary.computeForFeaturesWithOutputSize(nonEmptyFeatures, opts)
    else
      GeometricSummary.computeForFeatures(nonEmptyFeatures)

    val pCriterion = partitionCriterion match {
      case 'fixed => SpatialPartitioner.PartitionCriterion.FIXED
      case 'count => SpatialPartitioner.PartitionCriterion.COUNT
      case 'size => SpatialPartitioner.PartitionCriterion.SIZE
      case other => throw new RuntimeException(s"Unknown partitioning criterion $other")
    }
    val numBuckets:Int = IndexerParams.computeNumberOfPartitions(pCriterion, pValue, summary)

    var spatialPartitioner: SpatialPartitioner = null
    if (numBuckets == 1) {
      logger.info("Input too small. Creating a cell partitioner with one cell")
      // No need to get a sample or histogram if only one partition will be created
      // Create a cell partitioner that contains one cell that represents the entire input
      spatialPartitioner = new CellPartitioner(new PartitionInfo(summary, 0))
    } else {
      spatialPartitioner = partitionerClass.newInstance
      spatialPartitioner.setup(opts, disjoint, pCriterion, pValue)
      val pMetadata = spatialPartitioner.getMetadata
      if (disjoint && !pMetadata.disjointSupported)
        throw new RuntimeException("Partitioner " + partitionerClass + " does not support disjoint partitioning")

      val numDimensions = summary.getCoordinateDimension
      // Check if a sample is required for this constructor
      if (sampleNeeded) {
        // Compute the sample
        sc.setJobGroup("Sample", "Draw a sample from the input")
        // Calculate sample size based on how many coordinates can fit in the allotted memory (synopsisSize)
        val sampleSize: Int = (synopsisSize / (8 * numDimensions)).toInt
        // Compute the sampleRatio to be able to use the sample transformation
        val sampleRatio: Double = Math.min(sampleSize.toDouble / summary.numFeatures, 1.0)
        // Now, read the sample, convert each feature to a point, and read the maximum number of points that we can take
        val samplePoints: Array[Point] = nonEmptyFeatures.sample(withReplacement = false, sampleRatio)
          .map(f => f.getGeometry.centroid(new Point(numDimensions)))
          .take(sampleSize)
        // Convert the points to a primitive nD array to conserve memory
        sampleCoordinates = Array.ofDim[Double](numDimensions, samplePoints.length)
        for (i <- samplePoints.indices; d <- 0 until numDimensions)
          sampleCoordinates(d)(i) = samplePoints(i).coords(d)
        logger.info(s"Using a sample of ${samplePoints.length} points for partitioner creation")
      }
      // Check if a histogram is required for this constructor
      if (histogramNeeded) {
        // Compute the histogram
        val numBuckets = (synopsisSize / 8).toInt
        val numPartitions = UniformHistogram.computeNumPartitions(summary, numBuckets)
        logger.info(s"Computing a histogram of dimensions ${numPartitions.mkString("x")}")

        sc.setJobGroup("Histogram", "Computing a histogram for the partitioner")
        histogram = Histogram.computeSimpleWriteSizeHistogram(opts, nonEmptyFeatures, summary, numPartitions: _*)
      }
      val t2 = System.nanoTime
      // Construct the partitioner
      spatialPartitioner.construct(summary, sampleCoordinates, histogram)
      val t3 = System.nanoTime
      logger.info(f"Synopses created in ${(t2-t1)*1E-9}%f seconds and partitioner '${partitionerClass.getSimpleName}' " +
        f"constructed in ${(t3-t2)*1E-9}%f seconds")
    }


    features.unpersist()
    spatialPartitioner
  }

  /**
    * Partition features using an already initialized [[SpatialPartitioner]] from Java
    * @param features the set of features to partition
    * @param partitioner an already initialized partitioner
    * @return a JavaPairRDD where the key represents the partition number and the value is the feature.
    */
  def partitionFeatures(features: JavaRDD[IFeature], partitioner: SpatialPartitioner): JavaPairRDD[Integer, IFeature] = {
    val pairs: RDD[(Integer, IFeature)] = _partitionFeatures(features.rdd, partitioner).map(kv => (kv._1, kv._2))
    JavaPairRDD.fromRDD(pairs.partitionBy(new SparkSpatialPartitioner(partitioner)))
  }

  /**
    * Partitions the given features using an already initialized [[SpatialPartitioner]].
    *
    * @param features the features to partition
    * @param spatialPartitioner the spatial partitioner to partition the features with
    * @return an RDD of (partition number, IFeature)
    */
  def partitionFeatures(features : RDD[IFeature], spatialPartitioner : SpatialPartitioner) : RDD[(Int, IFeature)] = {
    val partitionIDFeaturePairs = _partitionFeatures(features, spatialPartitioner)
    // Enforce the partitioner to shuffle records by partition ID
    partitionIDFeaturePairs.partitionBy(new SparkSpatialPartitioner(spatialPartitioner))
  }

  private def _partitionFeatures(features: RDD[IFeature], spatialPartitioner: SpatialPartitioner) = {
    val mbr = new Envelope(spatialPartitioner.getCoordinateDimension)
    if (!spatialPartitioner.isDisjoint) {
      // Non disjoint partitioners are easy as each feature is assigned to exactly one partition
      features.map(f => (spatialPartitioner.overlapPartition(f.getGeometry.envelope(mbr)), f))
    } else {
      // Disjoint partitioners need us to create a list of partition IDs for each record
      features.flatMap(f => {
        val matchedPartitions = new IntArray
        f.getGeometry.envelope(mbr)
        spatialPartitioner.overlapPartitions(mbr, matchedPartitions)
        val resultingPairs = Array.ofDim[(Int, IFeature)](matchedPartitions.size())
        for (i <- 0 until matchedPartitions.size())
          resultingPairs(i) = (matchedPartitions.get(i), f)
        resultingPairs
      })
    }
  }

  /**Java shortcut*/
  def partitionFeatures(features: JavaRDD[IFeature], partitionerClass: Class[_ <: SpatialPartitioner],
                        opts: UserOptions): JavaPairRDD[Integer, IFeature] = {
    val disjoint = opts.getBoolean(IndexerParams.DisjointIndex, false)
    val criterionThreshold = opts.get(IndexerParams.PartitionCriterionThreshold, "Size(128m)")
    val pInfo: (Symbol, Long) = parsePartitioningCriterion(criterionThreshold)
    val spatialPartitioner = createPartitioner(features, partitionerClass, disjoint, pInfo._1, pInfo._2, opts)
    partitionFeatures(features, spatialPartitioner)
  }

  /**
    * Partitions the given features using a partitioner of the given type. This method first initializes the partitioner
    * and then uses this initialized partitioner to partition the data.
    *
    * @param features         the RDD of features to partition
    * @param partitionerClass the partitioner class to use for partitioning
    * @param opts             any user options to use while creating the partitioner
    */
  def partitionFeatures(features: RDD[IFeature], partitionerClass: Class[_ <: SpatialPartitioner],
                        opts: UserOptions): RDD[(Int, IFeature)] = {
    val disjoint = opts.getBoolean(IndexerParams.DisjointIndex, false)
    val criterionThreshold = opts.get(IndexerParams.PartitionCriterionThreshold, "Size(128m)")
    val pInfo: (Symbol, Long) = parsePartitioningCriterion(criterionThreshold)
    val spatialPartitioner = createPartitioner(features, partitionerClass, disjoint, pInfo._1, pInfo._2, opts)
    partitionFeatures(features, spatialPartitioner)
  }

  /**
    * Parses a user-friendly partitioning criterion string into a tuple of (type, value)
    * @param criterionThreshold a user friendly partitioning criterion in the form of Fixed(n), Count(c), or Size(s)
    * @return a tuple where the first entry is one of the symbols {fixed, count, size} and the second entry is the value.
    */
  def parsePartitioningCriterion(criterionThreshold: String): (Symbol, Long) = {
    val pCriterionRegexp = raw"(fixed|count|size)+\((\w+)\)".r
    criterionThreshold.toLowerCase match {
      case pCriterionRegexp(method, value) => (Symbol(method), StringUtils.TraditionalBinaryPrefix.string2long(value))
    }
  }

  def saveIndex(partitionedFeatures: JavaPairRDD[Integer, IFeature], path: String, opts: UserOptions) : Unit = {
    val rdd: RDD[(Integer, IFeature)] = partitionedFeatures.rdd
    if (rdd.partitioner.isEmpty)
      throw new RuntimeException("Cannot save non-partitioned features")
    if (! rdd.partitioner.get.isInstanceOf[SparkSpatialPartitioner])
      throw new RuntimeException("Can only save features that are spatially partitioner")
    val spatialPartitioner = partitionedFeatures.partitioner.get.asInstanceOf[SparkSpatialPartitioner].spatialPartitioner
    val hadoopConf = opts.loadIntoHadoopConfiguration(new Configuration)
    IndexerParams.savePartitionerToHadoopConfiguration(hadoopConf, spatialPartitioner)
    partitionedFeatures.saveAsNewAPIHadoopFile(path, classOf[Any], classOf[IFeature], classOf[IndexOutputFormat], hadoopConf)
  }

  /**
    * Save a partitioner dataset as a global index file to disk
    *
    * @param partitionedFeatures features that are already partitioned using a spatial partitioner
    * @param path path to the output file to be written
    * @param opts any additional user options
    */
  def saveIndex(partitionedFeatures: RDD[(Int, IFeature)], path: String, opts: UserOptions) : Unit = {
    if (partitionedFeatures.partitioner.isEmpty)
      throw new RuntimeException("Cannot save non-partitioned features")
    if (! partitionedFeatures.partitioner.get.isInstanceOf[SparkSpatialPartitioner])
      throw new RuntimeException("Can only save features that are spatially partitioner")
    val spatialPartitioner = partitionedFeatures.partitioner.get.asInstanceOf[SparkSpatialPartitioner].spatialPartitioner
    val hadoopConf = opts.loadIntoHadoopConfiguration(new Configuration)
    IndexerParams.savePartitionerToHadoopConfiguration(hadoopConf, spatialPartitioner)
    partitionedFeatures.saveAsNewAPIHadoopFile(path, classOf[Any], classOf[IFeature], classOf[IndexOutputFormat], hadoopConf)
  }

  @throws(classOf[IOException])
  override def run(opts: UserOptions, sc: SparkContext): Any = {
    // Extract index parameters from the command line arguments
    val gIndex = opts.get(IndexerParams.GlobalIndex)
    val partitionerClass = IndexerParams.partitioners.get(gIndex)

    // Start processing the input to build the index
    // Read the input features
    val features = SpatialReader.readInput(sc, opts)
    // Partition the input records using the created partitioner
    val partitionedFeatures = partitionFeatures(features, partitionerClass, opts)
    // Save the index to disk
    saveIndex(partitionedFeatures, opts.getOutput, opts)
  }
}
