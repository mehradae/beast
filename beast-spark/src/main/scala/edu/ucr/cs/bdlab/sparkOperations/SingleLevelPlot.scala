/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations

import edu.ucr.cs.bdlab.davinci.{Canvas, CommonVisualizationHelper, Plotter, SingleLevelPlotHelper}
import edu.ucr.cs.bdlab.geolite.IFeature
import edu.ucr.cs.bdlab.io.{CSVEnvelopeEncoder, SpatialInputFormat, SpatialOutputFormat}
import edu.ucr.cs.bdlab.stsynopses.Summary
import edu.ucr.cs.bdlab.util.{OperationMetadata, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel

@OperationMetadata(
  shortName =  "splot",
  description = "Plots the input file as a single image",
  inputArity = "1",
  outputArity = "1",
  inheritParams = Array(classOf[SpatialInputFormat], classOf[SpatialOutputFormat],
    classOf[CommonVisualizationHelper], classOf[SingleLevelPlotHelper])
)
object SingleLevelPlot extends CLIOperation {
  @transient private val logger: Log = LogFactory.getLog(getClass)

  /**
    * Run the main function using the given user command-line options and spark context
    *
    * @param opts user options for configuring the operation
    * @param sc   the Spark context used to run the operation
    * @return an optional result of this operation
    */
  override def run(opts: UserOptions, sc: SparkContext): Any = {
    val features: RDD[IFeature] = SpatialReader.readInput(sc, opts).persist(StorageLevel.MEMORY_AND_DISK_SER)
    def plotterName: String = opts.get(CommonVisualizationHelper.PlotterName)
    val imageWidth: Int = opts.getInt(SingleLevelPlotHelper.ImageWidth, 1000)
    val imageHeight: Int = opts.getInt(SingleLevelPlotHelper.ImageHeight, 1000)
    val summary: Summary = GeometricSummary.computeForFeatures(features)
    opts.set("mbr", CSVEnvelopeEncoder.defaultEncoder.apply(summary))
    val partialCanvases: RDD[Canvas] = features.mapPartitions(iFeatureIterator => {
      val plotter: Plotter = Plotter.getConfiguredPlotter(plotterName, opts)
      val canvas: Canvas = plotter.createCanvas(imageWidth, imageHeight, summary)
      for (feature <- iFeatureIterator)
        plotter.plot(canvas, feature)
      Option(canvas).iterator
    })
    // Merge the partial canvases into one final canvas
    val plotter: Plotter = Plotter.getConfiguredPlotter(plotterName, opts)
    val finalCanvas: Canvas = partialCanvases.reduce((c1, c2) => plotter.merge(c1, c2))
    // Write the final canvas to the output
    val outPath: Path = new Path(opts.getOutput)
    val outFileSystem: FileSystem = outPath.getFileSystem(opts)
    val outStream = outFileSystem.create(outPath)
    val vFlip: Boolean = opts.getBoolean(CommonVisualizationHelper.VerticalFlip, true)
    plotter.writeImage(finalCanvas, outStream, vFlip)
    outStream.close()
    outFileSystem.close()
  }
}
