package edu.ucr.cs.bdlab.sparkOperations

import java.io.{File, FileFilter}
import java.util
import java.util.Comparator
import javassist.bytecode.analysis.Util

import com.esri.core.geometry.{Geometry, GeometryEngine, SpatialReference}
import edu.ucr.cs.bdlab.geolite.util.EsriConverter
import edu.ucr.cs.bdlab.geolite.IGeometry
import edu.ucr.cs.bdlab.raptor.{GeoTiffReader, Intersections}
import edu.ucr.cs.bdlab.util.{OperationMetadata, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.sql.types._
import org.apache.spark.{HashPartitioner, RangePartitioner, SparkContext}

import scala.collection.mutable

@OperationMetadata(
  shortName =  "raptor",
  description = "Computes RaptorZS",
  inputArity = "2",
  outputArity = "1"
)
object RaptorZonal extends CLIOperation {

  @transient lazy val logger:Log = LogFactory.getLog(getClass)

  def run(opts: UserOptions, sc: SparkContext): Any = {
    sc.hadoopConfiguration.setInt("mapred.min.split.size",8388608)
    sc.hadoopConfiguration.setInt("mapred.max.split.size",8388608)
    //sc.defaultParallelism
    //check the input size

    val t0 = System.nanoTime()

    val rasterFiles = new Path(opts.getInput(1))
    var broadCastrasterList:Array[String] = null
    if(rasterFiles.getFileSystem(opts).isDirectory(rasterFiles))
    {
      broadCastrasterList = new File(opts.getInput(1)).listFiles(new FileFilter {
        override def accept(pathname: File): Boolean = pathname.getName.toLowerCase().endsWith(".tif")
      }).map(x=>x.getPath.toString)
      /*broadCastrasterList = rasterFiles.getFileSystem(opts).listStatus(rasterFiles,new PathFilter {
        override def accept(pathname: Path): Boolean = pathname.getName.toLowerCase().endsWith(".tif")
      }).map(x=>x.getPath.toString)*/
    }
    else
    {
      broadCastrasterList = new Array[String](1)
      broadCastrasterList(0) = (opts.getInput(1))
    }

    val features = SpatialReader.readInput(sc, opts, opts.getInput(0), "shapefile")
    //val features = SpatialReader.readInput(sc, opts, opts.getInput(0), "WKT(1)")
    //val features = SpatialReader.readInput(sc,opts,opts.getInput(0),"Point(1,2)")

    val geometries = features.map(x=>((x.getAttributeValue("fid").toString.toLong),x.getGeometry))
    //val geometries = features.flatMap(x=>new geomIterator(Integer.parseInt(x.getAttributeValue("id").toString),quadSplit(x.getGeometry,100)))
    //val geometries = features.map(x=>((x.getAttributeValue(0).toString.toLong),x.getGeometry))

    //System.out.println(geometries.count())

    //val numgeometriespermachine = opts.getInput(2).toInt

    //val min_num_partitions = 144 //geometries.count()/numgeometriespermachine

    //val tn = System.nanoTime()
    /*if(geometries.getNumPartitions < min_num_partitions)
      geometries = geometries.repartition(min_num_partitions)
    //geometries.map(x=>x._1).
*/

    //new JobClient(new JobConf()).getClusterStatus().getMaxReduceTasks()

    /*.repartition(576)*/

    val intersectionsRDD = geometries.repartition(3040).mapPartitions{geom: Iterator[(Long,IGeometry)]=>{
      val intersectionsList = new util.ArrayList[Intersections]()
      val rasterList = new util.ArrayList[Int]()
      val IDs = new util.ArrayList[java.lang.Long]()
      val geoms = geom.map(x=> {
        IDs.add(x._1)
        x._2
      }).toArray
        for(i<- 0 until broadCastrasterList.length){
          val conf_file = new Configuration()
          val raster = new GeoTiffReader()
          val geoTiffFile = new Path (broadCastrasterList(i))
          try{
            raster.initialize(geoTiffFile.getFileSystem(conf_file), geoTiffFile, 0)
            val intersections = new Intersections
            intersections.compute(IDs,geoms,raster)
            if(intersections.getNumIntersections!=0)
            {
              intersectionsList.add(intersections)
              rasterList.add(i)
            }
          }
          finally {
            raster.close()
          }

      }


      val intersectionsIterator = new IntersectionsIterator(rasterList,intersectionsList)
      intersectionsIterator
    }}

    //System.out.println("################################ " + intersectionsRDD.count())
    //val ti = System.nanoTime()


    //number of cores - 90%-95%

    val output = intersectionsRDD.repartitionAndSortWithinPartitions(new HashPartitioner(intersectionsRDD.getNumPartitions))//(new RangePartitioner(144,intersectionsRDD))


    //System.out.println("################################ " + output.count())
    //val ts = System.nanoTime()



    val pixeloutput = output.mapPartitions{x:Iterator[(Long, (Long, Int, Int, Int))]=>{
      new PixelIterator(x,broadCastrasterList)
    }}

    //System.out.println("################################ " + pixeloutput.count())
    //val tp = System.nanoTime()

    def agg= (accu: Float, v:Float) => {
      Math.min(accu,v) //(accu + v,Math.min(accu,v),Math.max(accu,v))
    }

    def agg1= (accu: (Float,Int), v:(Float,Int)) => {
      ((accu._1 + v._1),(accu._2+v._2))
    }

    def median(inputList: List[Float]): Float = {
      val count = inputList.size
      if (count % 2 == 0) {
        val l = count / 2 - 1
        val r = l + 1
        (inputList(l) + inputList(r)).toFloat / 2
      } else
        inputList(count / 2).toFloat
    }
    def mode(inputList: List[Float]): (Float,Float,Float,Float,Float,Int,Float) = {
      var mode = new mutable.HashMap[Float,Int]()
      var max = Float.NegativeInfinity
      var min = Float.PositiveInfinity
      var sum = (0).toFloat
      for(j<- 0 until inputList.size)
        {
          val value = inputList(j)
          if(value > max)
            max = value
          if(value < min)
            min = value
          sum+=value

          if(mode.contains(value))
            {
              val x = mode.get(value).get
              mode.getOrElseUpdate(value,x+1)
            }
          else{
            mode.put(value,1)
          }
        }


      val count = inputList.size
      var median = Float.NegativeInfinity
      if (count % 2 == 0) {
        val l = count / 2 - 1
        val r = l + 1
        median = (inputList(l) + inputList(r)).toFloat / 2
      } else
        median = inputList(count / 2).toFloat

      (max,min,median,sum,mutable.ListMap(mode.toSeq.sortWith(_._2 > _._2):_*).head._1,count,(sum/count.toFloat))
    }

    //val sqlContext = new SQLContext(sc)
    //val spark = sqlContext.sparkSession
      /*val schema = StructType(
      List(
        StructField("PolygonID", LongType, true),
        StructField("Value", FloatType, true)
      )
    )*/

    val pixeloutput_filtered = pixeloutput.filter(x=>x._2!=(-9999.0))
    //val pixelrdd = pixeloutput_filtered.map(x=>Row(x._1,x._2))

    val setRDD = pixeloutput_filtered.groupByKey()
    val sortedListRDD = setRDD.mapValues(_.toList.sorted)

    // Output DataFrame of id and median
    sortedListRDD.map(m => {(m._1, mode(m._2)) }).join(geometries).coalesce(1).saveAsTextFile(opts.getOutput)
    //val pixeldf = spark.createDataFrame(pixelrdd,schema)
   // val pixeldf_filtered = pixeldf.filter("Value!=(-9999.0)")
    //pixeldf.registerTempTable("df")

    //val finaloutput = sqlContext.sql("SELECT PolygonID, SUM(Value), MIN(Value), MAX(Value), COUNT(Value) AS value FROM df GROUP BY PolygonID")
    //finaloutput.coalesce(1).write.format("com.databricks.spark.csv").save(opts.getOutput)


    def reg1= (accu:(Double,Double,Double,Double,Double), v:(Double,Double,Double,Double,Double)) => {
      (accu._1+v._1, accu._2 + v._2,accu._3+v._3, accu._4 + v._4, accu._5 + v._5)
    }

   //val finaloutput = /*pixeloutput.filter(x=>x._2!=(-9999.0)).groupBy(x=>x._1) */pixeloutput.reduceByKey(agg)//,136)
    //val reg = finaloutput.map(x => (x._2._1.x,x._2._1.y,(x._2._1.x*x._2._1.y),(x._2._1.x*x._2._1.x),(x._2._1.y*x._2._1.y)))
    //val result = reg.reduce(reg1)
    //System.out.println(result._1 + "," + result._2 + "," + result._3 + "," + result._4 + "," + result._5)

    //System.out.println(finaloutput.count())
    //System.out.println(finaloutput.map(x=>x._2._2).max())
    //finaloutput.saveAsTextFile(opts.getOutput)

    val t1 = System.nanoTime()
    //System.out.println("Geometry quadsplit: " + (tn-t0)/1E9)
    //System.out.println("Intersection Time: " + (ti-tn)/1E9)
    //System.out.println("shuffle time: "+(ts-ti)/1E9)
    //System.out.println("pixel output time: "+ (tp-ts)/1E9)
    //System.out.println("Aggregation Time: " + (t1-tp)/1E9)

    System.out.println("Total Time:" + (t1-t0)/1E9)

  }

  def quadSplit(geometry:IGeometry,theta :Int ) : util.ArrayList[IGeometry] = {

    var threshold = theta
    assert (geometry.getCoordinateDimension == 2,  "This function only works with 2D geometries")
    val esriGeometry = EsriConverter.createEsriGeometry(geometry)
    val parts = new util.ArrayList[Geometry]()
    var numGeomsToCheck:Int = 1
    parts.add(esriGeometry)
    val mbr = new com.esri.core.geometry.Envelope()
    val quadrant = new com.esri.core.geometry.Envelope()
    // Convert the threshold to an estimated memory size for simplicity to use with Esri API
    threshold = threshold * 8 * 2
    val spatialReference = SpatialReference.create(4326)
    while (numGeomsToCheck > 0) {
      val geomToCheck = parts.remove(0)
      numGeomsToCheck = numGeomsToCheck -1
      if (geomToCheck.estimateMemorySize() <= threshold) {
        // Already simple. Add to results
        parts.add(geomToCheck)
      } else {
        // A complex geometry, split into four
        geomToCheck.queryEnvelope(mbr)
        // First quadrant
        mbr.queryEnvelope(quadrant)
        quadrant.setXMax(mbr.getCenterX)
        quadrant.setYMax(mbr.getCenterY)
        parts.add(GeometryEngine.intersect(geomToCheck, quadrant, spatialReference))
        // Second quadrant
        mbr.queryEnvelope(quadrant)
        quadrant.setXMin(mbr.getCenterX)
        quadrant.setYMax(mbr.getCenterY)
        parts.add(GeometryEngine.intersect(geomToCheck, quadrant, spatialReference))
        // Third quadrant
        mbr.queryEnvelope(quadrant)
        quadrant.setXMin(mbr.getCenterX)
        quadrant.setYMin(mbr.getCenterY)
        parts.add(GeometryEngine.intersect(geomToCheck, quadrant, spatialReference))
        // Fourth quadrant
        mbr.queryEnvelope(quadrant)
        quadrant.setXMax(mbr.getCenterX)
        quadrant.setYMin(mbr.getCenterY)
        parts.add(GeometryEngine.intersect(geomToCheck, quadrant, spatialReference))
        numGeomsToCheck += 4
      }
    }
    // Convert all parts back to lite geometry
    val results = new util.ArrayList[IGeometry]()
    for (i <- 0  until parts.size()) {
      results.add(EsriConverter.fromEsriGeometry(parts.get(i), null))
    }
   results
  }

}
