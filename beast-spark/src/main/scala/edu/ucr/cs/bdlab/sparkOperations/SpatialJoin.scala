/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations

import java.io.IOException

import edu.ucr.cs.bdlab.cg.SpatialJoinAlgorithms
import edu.ucr.cs.bdlab.cg.SpatialJoinAlgorithms.{ESJDistributedAlgorithm, ESJPredicate, SJResult}
import edu.ucr.cs.bdlab.geolite.{Envelope, IFeature}
import edu.ucr.cs.bdlab.io.{BinaryInputFormat, BlockFilter, JoinFilter, SpatialInputFormat, SpatialOutputFormat}
import edu.ucr.cs.bdlab.util.{OperationMetadata, OperationParam, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.conf.Configuration
import org.apache.spark.SparkContext
import org.apache.spark.api.java.{JavaPairRDD, JavaRDD}
import org.apache.spark.rdd.RDD

import scala.collection.mutable.ListBuffer
import collection.JavaConverters._


@OperationMetadata(shortName = "sj",
  description = "Computes spatial join that finds all overlapping features from two files.",
  inputArity = "2",
  outputArity = "?",
  inheritParams = Array(classOf[SpatialInputFormat], classOf[SpatialOutputFormat]))
object SpatialJoin extends CLIOperation {
  @transient lazy val logger: Log = LogFactory.getLog(getClass)

  @OperationParam(description = "The spatial join algorithm to use {'bnlj', 'dj'}.", defaultValue = "bnlj")
  val SpatialJoinMethod = "method"

  @OperationParam(description = "The spatial predicate to use in the join. Supported predicates are {intersects, contains}", defaultValue = "intersects")
  val SpatialJoinPredicate = "predicate"

  @OperationParam(description = "Overwrite the output file if it exists", defaultValue = "true")
  val OverwriteOutput = "overwrite"

  @throws(classOf[IOException])
  override def run(opts: UserOptions, sc : SparkContext): Unit = {
    val joinResults = opts.getEnumIgnoreCase(SpatialJoinMethod, ESJDistributedAlgorithm.DJ) match {
      case ESJDistributedAlgorithm.BNLJ =>
        val joinPredicate = opts.getEnumIgnoreCase(SpatialJoinPredicate, ESJPredicate.Intersects)
        val f1rdd = SpatialReader.readInput(sc, opts, inputIndex = 0)
        val f2rdd = SpatialReader.readInput(sc, opts, inputIndex = 1)
        spatialJoinBNLJ(f1rdd, f2rdd, joinPredicate)
      case ESJDistributedAlgorithm.DJ =>
        spatialJoinDJ(sc, opts)
      case _other => throw new RuntimeException(s"Unrecognized spatial join method ${_other}. Please specify one of {'bnlj', 'dj'}")
    }

    joinResults.map(f1f2 => f1f2._1.toString + f1f2._2.toString).saveAsTextFile(opts.getOutput)
  }

  /**Java shortcut*/
  def spatialJoinBNLJ(r1: JavaRDD[IFeature], r2: JavaRDD[IFeature],
                      joinPredicate: ESJPredicate): JavaPairRDD[IFeature, IFeature] =
    JavaPairRDD.fromRDD(spatialJoinBNLJ(r1.rdd, r2.rdd, joinPredicate))

  /**
    * Runs a spatial join between the two given RDDs using the block-nested-loop join algorithm.
    *
    * @param r1 the first set of features
    * @param r2 the second set of features
    * @param joinPredicate the predicate that joins a feature from r1 with a feature in r2
    * @return
    */
  def spatialJoinBNLJ(r1: RDD[IFeature], r2: RDD[IFeature], joinPredicate: ESJPredicate) : RDD[(IFeature, IFeature)] = {
    // Convert the two RDD to arrays
    val f1 = r1.glom()
    val f2 = r2.glom()

    // Combine them using the Cartesian product (as in block nested loop)
    val f1f2 = f1.cartesian(f2)

    // For each pair of blocks, run the spatial join algorithm
    f1f2.flatMap(p1p2 => {
      // Extract the two arrays of features
      val p1: Array[IFeature] = p1p2._1
      val p2: Array[IFeature] = p1p2._2

      // Duplicate avoidance MBR is set to infinity (include all space) in the BNLJ algorithm
      val dupAvoidanceMBR = new Envelope(2)
      dupAvoidanceMBR.setInfinite()
      spatialJoinIntersectsPlaneSweepFeatures(p1, p2, dupAvoidanceMBR, joinPredicate)
    })
  }

  /**
    * Return the MBRs of the given list of features
    * @param features
    * @return a tuple with four lists of doubles that represent mix, miny, maxx, maxy
    */
  def getMBRs(features: Array[_ <: IFeature]): (Array[Double], Array[Double], Array[Double], Array[Double]) = {
    assert(features(0).getGeometry.getCoordinateDimension == 2, "Can only work with two dimensions")
    val minx = Array.ofDim[Double](features.length)
    val miny = Array.ofDim[Double](features.length)
    val maxx = Array.ofDim[Double](features.length)
    val maxy = Array.ofDim[Double](features.length)
    val mbr: Envelope = new Envelope(2)
    for (i <- features.indices) {
      features(i).getGeometry.envelope(mbr)
      minx(i) = mbr.minCoord(0)
      miny(i) = mbr.minCoord(1)
      maxx(i) = mbr.maxCoord(0)
      maxy(i) = mbr.maxCoord(1)
    }
    (minx, miny, maxx, maxy)
  }

  def spatialJoinIntersectsPlaneSweepFeatures(r: Array[_ <: IFeature], s: Array[_ <: IFeature],
                                              dupAvoidanceMBR: Envelope, joinPredicate: ESJPredicate)
      :TraversableOnce[(IFeature, IFeature)] = {
    val results = ListBuffer[(IFeature, IFeature)]()
    if (r.isEmpty || s.isEmpty)
      return results
    // (minx, miny, maxx, maxy)
    val rmbrs: (Array[Double], Array[Double], Array[Double], Array[Double]) = getMBRs(r)
    val smbrs: (Array[Double], Array[Double], Array[Double], Array[Double]) = getMBRs(s)

    val refine: (Int, Int) => Boolean = joinPredicate match {
      case ESJPredicate.Contains => (i1: Int, i2: Int) => r(i1).getGeometry.contains(s(i2).getGeometry)
      case ESJPredicate.Intersects => (i1: Int, i2: Int) => r(i1).getGeometry.intersects(s(i2).getGeometry)
      // For MBR intersects, no refine step is needed. Write the results directly to the output
      case ESJPredicate.MBRIntersects => (i1: Int, i2: Int) => true
    }

    val duplicateAvoidance: (Array[Double]) => Boolean = (point) => {
      dupAvoidanceMBR.containsPoint(point)
    }

    SpatialJoinAlgorithms.planeSweepRectangles(Array(rmbrs._1, rmbrs._2),
      Array(rmbrs._3, rmbrs._4), Array(smbrs._1, smbrs._2), Array(smbrs._3, smbrs._4),
      new SJResult {
        override def accept(i: Int, j: Int, refPoint: Array[Double]): Unit = {
          if (duplicateAvoidance(refPoint) && refine(i,j))
            results.append( (r(i), s(j)) )
        }
      }
    )
    results
  }

  /**
    * Runs a distributed join algorithm between two (indexed) inputs
    *
    * @param sc
    * @param opts
    * @return
    */
  def spatialJoinDJ(sc: SparkContext, opts: UserOptions) : RDD[(IFeature, IFeature)] = {
    val inpaths: String = opts.getInput(0) + "," + opts.getInput(1)
    val conf: Configuration = opts.loadIntoHadoopConfiguration(new Configuration())
    conf.setBoolean(SpatialInputFormat.ImmutableObjects, true)

    conf.setClass(SpatialInputFormat.BlockFilterClass, classOf[JoinFilter], classOf[BlockFilter])
    val joinedInput: RDD[(Array[Envelope], Array[java.lang.Iterable[_ <: IFeature]])] = sc.newAPIHadoopFile(inpaths,
      classOf[BinaryInputFormat], classOf[Array[Envelope]], classOf[Array[java.lang.Iterable[_ <: IFeature]]], conf)
    val joinPredicate: ESJPredicate = opts.getEnumIgnoreCase(SpatialJoinPredicate, ESJPredicate.Intersects)
    joinedInput.flatMap(pairs => {
      val mbrs: Array[Envelope] = pairs._1

      val featureLists: Array[java.lang.Iterable[_ <: IFeature]] = pairs._2
      val numInputs = mbrs.length
      assert(numInputs == 2)

      // Store all features
      var theResult : TraversableOnce[(IFeature, IFeature)] = List()
      // For some reason, featureList.asScala.toArray returns an array of the right size but all nulls.
      val inputs: Array[Array[_ <: IFeature]] = featureLists.map(featureList => featureList.asScala.toList.toArray)
      if (inputs(0).nonEmpty && inputs(1).nonEmpty) {
        val intersectionArea: Envelope = mbrs(0)
        intersectionArea.shrink(mbrs(1))

        theResult = spatialJoinIntersectsPlaneSweepFeatures(inputs(0), inputs(1), intersectionArea, joinPredicate)
      }
      theResult
    })
  }
}
