/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations
import java.io.IOException

import edu.ucr.cs.bdlab.davinci.MultilevelPyramidPlotHelper.TileClass
import edu.ucr.cs.bdlab.davinci._
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D
import edu.ucr.cs.bdlab.geolite.{Envelope, Feature, GeometryType, IFeature, IGeometry, ILineString, IPolygon}
import edu.ucr.cs.bdlab.io.{CSVEnvelopeEncoder, MercatorProjector, SpatialInputFormat, SpatialOutputFormat}
import edu.ucr.cs.bdlab.stsynopses.{AbstractHistogram, Prefix2DHistogram, PrefixEulerHistogram2D}
import edu.ucr.cs.bdlab.util._
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.api.java.JavaRDD
import org.apache.spark.rdd.RDD
import org.apache.spark.{HashPartitioner, SparkContext}

import scala.collection.{JavaConverters, mutable}
import scala.collection.mutable.ArrayBuffer

/**
  * Creates a multilevel visualization using the flat partitioning algorithm. The input is split in a non-spatial way
  * into splits. Each split is visualized as a full pyramid which contains all the tiles but the tiles are not final
  * because they do not cover all the data. Finally, the partial tiles are merged to produce final tile images which
  * are then written to the output.
  */
@OperationMetadata(
  shortName =  "mplot",
  description = "Plots the input file as a multilevel pyramid image",
  inputArity = "1",
  outputArity = "1",
  inheritParams = Array(classOf[SpatialInputFormat], classOf[SpatialOutputFormat],
    classOf[CommonVisualizationHelper], classOf[MultilevelPyramidPlotHelper])
)
object MultilevelPlot extends CLIOperation {
  @transient private val logger: Log = LogFactory.getLog(getClass)

  /** Java shortcut */
  def plotFeatures(features: JavaRDD[_ <: IFeature], minLevel: Int, maxLevel: Int, plotterClass: Class[_ <: Plotter],
                   inputPath: String, outputPath: String, opts: UserOptions): Unit =
    plotFeatures(features.rdd, minLevel to maxLevel, plotterClass, inputPath, outputPath, opts)

  /**
    * Plots the given features and writes the plot results to the output in a pyramid structure
    * @param features the set of features to plot
    * @param levels the range of levels to plot in the pyramid
    * @param plotterClass the plotter class used to generate the tiles
    * @param inputPath the input path. Used to add a link back to this path when the output is partial.
    * @param outputPath the output path where the tiles will be written.
    * @param opts user options for initializing the plotter and controlling the behavior of the operation
    */
  def plotFeatures(features: RDD[_<:IFeature], levels: Range, plotterClass: Class[_ <: Plotter],
                   inputPath: String, outputPath: String, opts: UserOptions): Unit = {
    // Extract plot parameters
    val mbr = new Envelope(2)
    val maxHistogramSize = opts.getLongBytes(MultilevelPyramidPlotHelper.MaximumHistogramSize, 128 * 1024 * 1024)
    val htype = Symbol(opts.get(MultilevelPyramidPlotHelper.HistogramType, "simple").toLowerCase)
    val binSize = htype match {
      case 'simple => 8
      case 'euler => 32
    }
    val gridDimension = MultilevelPyramidPlotHelper.computeHistogramDimension(maxHistogramSize, levels.max, binSize)
    logger.info(s"Creating a histogram with dimensions $gridDimension x $gridDimension")

    var featuresToPlot: RDD[_<: IFeature] = features
    // Check if the Web Mercator option is enabled
    if (opts.getBoolean(CommonVisualizationHelper.UseMercatorProjection, false)) {
      // Web mercator is enabled
      // Apply mercator projection on all features
      featuresToPlot = features.map(f => {
        MercatorProjector.worldToMercator(f.getGeometry)
        f
      })
      // Disable vertical flip when Mercator projection is in use
      opts.setBoolean(CommonVisualizationHelper.VerticalFlip, false)
      // Enforce an MBR that covers the entire world in Mercator projection
      mbr.minCoord(0) = 0.0
      mbr.minCoord(1) = 0.0
      mbr.maxCoord(0) = 256.0
      mbr.maxCoord(1) = 256.0
    } else {
      // No web mercator. Compute the MBR of the input
      // Compute the MBR to know the bounds of the input region
      logger.info("Computing geometric summary")
      val summary = GeometricSummary.computeForFeatures(features)
      mbr.set(summary.minCoord, summary.maxCoord)
      // Expand the MBR to keep the ratio if needed
      if (opts.getBoolean(CommonVisualizationHelper.KeepRatio, true)) {
        // To keep the aspect ratio, expand the MBR to make it a square
        if (mbr.getSideLength(0) > mbr.getSideLength(1)) {
          val diff = mbr.getSideLength(0) - mbr.getSideLength(1)
          mbr.minCoord(1) -= diff / 2.0
          mbr.maxCoord(1) += diff / 2.0
        } else if (mbr.getSideLength(1) > mbr.getSideLength(0)) {
          val diff = mbr.getSideLength(1) - mbr.getSideLength(0)
          mbr.minCoord(0) -= diff / 2.0
          mbr.maxCoord(0) += diff / 2.0
        }
      }
    }
    opts.set("mbr", CSVEnvelopeEncoder.defaultEncoder.apply(mbr))

    plotFeatures(featuresToPlot, mbr, levels, htype, gridDimension, plotterClass, inputPath, outputPath, opts)
  }

  /** Java shortcut */
  @throws(classOf[IOException])
  def plotGeometries(geoms: JavaRDD[_ <: IGeometry], minLevel: Int, maxLevel: Int, outPath: String,
                     opts: UserOptions): Unit =
    plotGeometries(geoms.rdd, minLevel to maxLevel, outPath, opts)

  /**
    * Plots the given set of geometries and writes the generated image to the output directory. This function uses the
    * [[edu.ucr.cs.bdlab.davinci.GeometricPlotter]] to plot the geometry of the shapes and if data tiles are written
    * to the output, it will use CSV file format by default.
    * @param geoms the set of geometries to plot
    * @param levels the range of levels to generate
    * @param outPath the path to the output directory
    * @param opts additional options to customize the behavior of the plot
    */
  @throws(classOf[IOException])
  def plotGeometries(geoms: RDD[_ <: IGeometry], levels: Range, outPath: String, opts: UserOptions): Unit = {
    // Convert geometries to features to be able to use the standard function
    val features: RDD[_ <: IFeature] = geoms.map(g => new Feature(g))

    // Set a default output format if not set
    if (opts.get(SpatialInputFormat.InputFormat) == null && opts.get(SpatialOutputFormat.OutputFormat) == null)
      SpatialOutputFormat.setOutputFormat(opts, "wkt")

    opts.set(MultilevelPyramidPlotHelper.NumLevels, s"${levels.min}..${levels.max}")

    plotFeatures(features, levels, classOf[GeometricPlotter], null, outPath, opts)
  }


  /**
    * Plots the given set of features using multilevel plot
    * @param features the set of features to plot
    * @param mbr the minimum bounding rectangle of the input area that needs to be plotter (i.e., the top tile)
    * @param levels the range of levels to generate
    * @param htype either &quot;simple&quot; or &quot;euler&quot; (case insensitive)
    * @param gridDimension the size length of the histogram to build based on the alotted memory space
    * @param plotterClass the class of the plotter to use for plotting
    * @param inputPath the path where the input features were read from (it is supposed to be an indexed input)
    * @param outputPath the path to write the output
    * @param opts user options
    * @throws IOException if an error happened during plot
    */
  def plotFeatures(features: RDD[_ <: IFeature], mbr: Envelope, levels: Range, htype: Symbol, gridDimension: Int,
                   plotterClass: Class[_ <: Plotter], inputPath: String, outputPath: String, opts: UserOptions): Unit = {
    val threshold: Long = opts.getLongBytes(MultilevelPyramidPlotHelper.DataTileThreshold, 10 * 1024 * 1024)
    val writeDataTiles: Boolean = opts.getBoolean(MultilevelPyramidPlotHelper.IncludeDataTiles, true)
    val tileWidth: Int = opts.getInt(MultilevelPyramidPlotHelper.TileWidth, 256)
    val tileHeight: Int = opts.getInt(MultilevelPyramidPlotHelper.TileHeight, 256)
    val minTileSizeForFlatPartitioning: Long = opts.getLongBytes(MultilevelPyramidPlotHelper.MinSizeFlatPartitioning,
      25 * 1024 * 1024)
    logger.info(s"Computing a histogram of dimension $gridDimension x $gridDimension")

    // Compute the uniform histogram and convert it to prefix sum for efficient sum of rectangles
    val h: AbstractHistogram = if (threshold == 0) null else
    htype match {
      case 'simple =>
        new Prefix2DHistogram(Histogram.computeSimpleSizeHistogram(features, mbr, gridDimension, gridDimension))
      case 'euler =>
        new PrefixEulerHistogram2D(Histogram.computeEulerSizeHistogram(features, mbr, gridDimension, gridDimension))
      case _ =>
        throw new RuntimeException(s"Unsupported histogram type $htype")
    }
    val finalFeatures: RDD[_ <: IFeature] =
    if (opts.getBoolean(CommonVisualizationHelper.FlattenFeatures, false))
      features.mapPartitions(fs => {
        val flattenedFeatures = new ArrayBuffer[IFeature]()
        for (f <- fs) {
          f.getGeometry.getType match {
            case GeometryType.MULTIPOLYGON | GeometryType.POLYGON => {
              val mpoly = f.getGeometry.asInstanceOf[IPolygon]
              for (iRing <- 0 until mpoly.getNumRings) {
                val ring: ILineString = mpoly.getRingN(iRing)
                f.setGeometry(ring)
                flattenedFeatures.append(new Feature(f))
              }
            }
            case _other => flattenedFeatures.append(f)
          }
        }
        flattenedFeatures.iterator
      })
    else
      features

    // Compute the boundaries of the different pyramids that will be used
    val deepestLevelWithLargeTile: Int = if (threshold == 0) levels.max else MultilevelPyramidPlotHelper.findDeepestTileWithSize(h, minTileSizeForFlatPartitioning)
    val deepestLevelWithImageTile: Int = if (threshold == 0) levels.max else MultilevelPyramidPlotHelper.findDeepestTileWithSize(h, threshold)
    val topPart = new SubPyramid(mbr, levels.min, Math.min(6, levels.max))
    val bottomPart = new SubPyramid(mbr, Math.max(7, levels.min), levels.max)

    val sc = features.context
    val plotter: Plotter = Plotter.getConfiguredPlotter(plotterClass, opts)
    val bufferSize: Double = plotter.getBufferSize / tileWidth.toDouble;
    // 1- Create all image tiles in the top part of the pyramid (levels [0, 6]) using flat partitioning
    val partitioner1 = if (h == null)
      new PyramidPartitioner(topPart)
    else new PyramidPartitioner(topPart, h, threshold + 1, Long.MaxValue)
    partitioner1.setBuffer(bufferSize)
    createImageTilesWithFlatPartitioning(features, mbr, plotterClass, tileWidth, tileHeight, partitioner1,
      outputPath+"/imagestop", opts)

    // 2- Create (large) image tiles in the bottom part of the pyramid (levels [0, 6]) using flat partitioning
    bottomPart.setMaximumLevel(Math.min(levels.max, deepestLevelWithLargeTile))
    val partitioner2 = if (h == null)
      new PyramidPartitioner(bottomPart)
      else new PyramidPartitioner(bottomPart, h, minTileSizeForFlatPartitioning, Long.MaxValue)
    partitioner2.setBuffer(bufferSize)
    createImageTilesWithFlatPartitioning(features, mbr, plotterClass, tileWidth, tileHeight, partitioner2,
      outputPath+"/bigimagesbottom", opts)

    // 3- Create (smaller) image tiles in the bottom part using pyramid partitioning
    bottomPart.setMaximumLevel(Math.min(levels.max, deepestLevelWithImageTile))
    val partitioner3 = if (h == null)
      new PyramidPartitioner(bottomPart)
    else new PyramidPartitioner(bottomPart, h, threshold+1, minTileSizeForFlatPartitioning)
    partitioner3.setBuffer(bufferSize)
    partitioner3.setGranularity(opts.getInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1))
    createImageTilesWithPyramidPartitioning(features, mbr, plotterClass, tileWidth, tileHeight, partitioner3,
      outputPath+"/smallimagesbottom", opts)

    // 4- Create data tiles if needed using pyramid partitioning
    if (writeDataTiles) {
      if (threshold > 0) {
        // 2- Create all data tiles using pyramid partitioning
        sc.setJobGroup("Data tiles", "Data tiles using pyramid partitioning")
        logger.info(s"Apply pyramid partitioning to generate all data tiles with size at most $threshold")
        val deepestLevelWithDataTile: Int = deepestLevelWithImageTile + 1
        val fullPyramid = new SubPyramid(mbr, levels.min, Math.min(levels.max, deepestLevelWithDataTile))
        val dataTilePartitioner = if (h == null)
          new PyramidPartitioner(fullPyramid)
        else new PyramidPartitioner(fullPyramid, h, threshold, TileClass.DataTile)
        dataTilePartitioner.setBuffer(bufferSize)
        createDataTilesWithPyramidPartitioning(features, dataTilePartitioner, opts)
      }
    } else {
      // Store the input path in the user configuration so it gets written to _visualization.properties file
      val outPath: Path = new Path(outputPath)
      val inPath: Path = new Path(inputPath)
      opts.set("data", FileUtil.relativize(inPath, outPath).toString)
    }

    // Combine all subdirectories into one directory
    val outPath: Path = new Path(outputPath)
    val outFS: FileSystem = outPath.getFileSystem(opts.loadIntoHadoopConfiguration(null))
    FileUtil.flattenDirectory(outFS, outPath)
    // Write the additional add-on files to allow the generated image to be easily visualized
    if (opts.getBoolean(CommonVisualizationHelper.UseMercatorProjection, false))
      opts.setBoolean(SpatialInputFormat.Mercator, true)
    opts.setClass(CommonVisualizationHelper.PlotterClassName, plotterClass, classOf[Plotter])
    opts.set(MultilevelPyramidPlotHelper.NumLevels, rangeToString(levels))
    MultilevelPyramidPlotHelper.writeVisualizationAddOnFiles(outFS, outPath, opts)
  }


  /**
    * Create data tiles for the given set of features using the pyramid partitioning method. First, each record is
    * assigned to all data tiles that it overlaps with. Then, the records are grouped by the tile ID. Finally, the
    * records in each group (i.e., tile ID) are written to a separate file with the naming convention tile-z-x-y.
    * @param features the set of features
    * @param partitioner the pyramid partitioner that defines which tiles to plot
    * @param opts the user-defined options for the visualization command.
    */
  private def createDataTilesWithPyramidPartitioning(features: RDD[_ <: IFeature], partitioner: PyramidPartitioner,
      opts: UserOptions): Unit = {
    // Create a sub-pyramid that represents the tiles of interest
    val mbr: Envelope = new Envelope(2)
    val partitionedFeatures: RDD[(Long, IFeature)] = features.flatMap(feature => {
      val matchedPartitions: LongArray = new LongArray
      val allMatches = new ArrayBuffer[(Long, IFeature)]
      feature.getGeometry.envelope(mbr)
      partitioner.overlapPartitions(mbr, matchedPartitions)
      for (i <- 0 until matchedPartitions.size)
        allMatches += ((matchedPartitions.get(i), feature))
      allMatches.iterator
    }).repartitionAndSortWithinPartitions(new HashPartitioner(1000))

    // Write the final canvas to the output
    partitionedFeatures.saveAsNewAPIHadoopFile(opts.getOutput + "/data", classOf[java.lang.Long], classOf[IFeature],
      classOf[PyramidOutputFormat], opts.loadIntoHadoopConfiguration(null))
  }

  /**
    * Use flat partitioning to plot all tiles in the given range of zoom levels. First, all the partitions are scanned
    * and a set of partial tiles are created based on the data in each partition. Then, these partial tiles are reduced
    * by tile ID so that all partial images for each tile are merged into one final tile. Finally, the final tiles
    * are written to the output using the naming convention tile-z-x-y
    * @param features the set of features to visualize as an RDD
    * @param featuresMBR the MBR of the features
    * @param plotterClass the class to use as a plotter
    * @param tileWidth the width of each tile in pixels
    * @param tileHeight the height of each tile in pixels
    * @param partitioner the pyramid partitioner that selects the tiles to plot using flat partitioning
    * @param opts any other used-defined options
    */
  private def createImageTilesWithFlatPartitioning(
    features: RDD[_ <: IFeature], featuresMBR: Envelope, plotterClass: Class[_ <: Plotter],
    tileWidth: Int, tileHeight: Int, partitioner: PyramidPartitioner, output: String, opts: UserOptions): Unit = {
    // Skip if the pyramid is empty
    if (partitioner.isEmpty)
      return
    // Scan all the features and plot all image tiles
    val pyramidTiles: RDD[(Long, Canvas)] = features.mapPartitions(iFeatures => {
      val tiles = new mutable.HashMap[Long, Canvas]
      val plotter: Plotter = Plotter.getConfiguredPlotter(plotterClass, opts)
      createTiles(iFeatures, featuresMBR, partitioner, tileWidth, tileHeight, plotter, tiles)
      tiles.iterator
    })

    // Merge partial images tiles into final image tiles
    val plotter: Plotter = Plotter.getConfiguredPlotter(plotterClass, opts)
    val finalTiles: RDD[(Long, Canvas)] = pyramidTiles.reduceByKey((c1, c2) => {
      plotter.merge(c1, c2)
      c1
    })

    // Write the final canvas to the output
    val conf = new Configuration()
    conf.setClass(CommonVisualizationHelper.PlotterClassName, plotterClass, classOf[Plotter])
    finalTiles.saveAsNewAPIHadoopFile(output, classOf[java.lang.Long], classOf[Canvas], classOf[PyramidOutputFormat],
      opts.loadIntoHadoopConfiguration(conf))
  }

  case class CanvasModified(canvas: Canvas, var modified: Boolean = false)

  /**
    * Creates and returns all the tiles in the given sub pyramid that contains
    * the given set of shapes.
    * @param features The features to be plotted
    * @param featuresMBR the minimum bounding rectangle of the features
    * @param partitioner the pyramid partitioner that defines the tiles to geneate
    * @param tileWidth Width of each tile in pixels
    * @param tileHeight Height of each tile in pixels
    * @param plotter The plotter used to create canvases
    * @param tiles (output) The set of tiles that have been created already. It could be
    *              empty which indicates no tiles created yet.
    */
  def createTiles(features: Iterator[_ <: IFeature], featuresMBR: Envelope, partitioner: PyramidPartitioner,
    tileWidth: Int, tileHeight: Int, plotter: Plotter, tiles: mutable.HashMap[Long, Canvas]): Unit = {
    // Create some objects to reuse for all features
    val shapeMBR = new Envelope(2)
    val tileMBR = new Envelope(2)
    val overlappingTiles = new LongArray
    val tileIndex: TileIndex = new TileIndex
    val tempTiles: mutable.HashMap[Long, CanvasModified] = new mutable.HashMap

    for (feature <- features) {
      feature.getGeometry.envelope(shapeMBR)

      partitioner.overlapPartitions(shapeMBR, overlappingTiles)
      for (i <- 0 until overlappingTiles.size) {
        val tileID: Long = overlappingTiles.get(i)
        val c: CanvasModified = tempTiles.getOrElseUpdate(tileID, {
          // First time to encounter this tile, create the corresponding canvas
          TileIndex.decode(tileID, tileIndex)
          TileIndex.getMBR(featuresMBR, tileIndex.z, tileIndex.x, tileIndex.y, tileMBR)
          new CanvasModified(plotter.createCanvas(tileWidth, tileHeight, tileMBR))
        })
        c.modified = plotter.plot(c.canvas, feature) || c.modified
      }
    }
    tempTiles.foreach(tid_cm => {
      if (tid_cm._2.modified)
        tiles.put(tid_cm._1, tid_cm._2.canvas)
    })
  }

  /**
    * Use pyramid partitioning to plot all the images in the given range of zoom levels. First, this method scans all
    * the features and assigns each feature to all image tiles that it overlaps with in the given range of zoom levels.
    * Then, the records are grouped by tile ID and the records in each group (i.e., tile ID) are plotted. Finally,
    * the created image tiles are written to the output using the naming convention tile-z-x-y.
    * This method should be used when the size of each image tile is small enough so that all records in a tile
    * can be grouped together in memory before plotting.
    * @param features the set of features to plot as RDD
    * @param featuresMBR the MBR of the features
    * @param plotterClass the class of the plotter to use
    * @param tileWidth the width of each tile in pixels
    * @param tileHeight the height of each tile in pixels
    * @param partitioner the pyramid partitioner that chooses which tiles to plot
    * @param outputPath the output path to write the result
    * @param opts user-defined options
    */
  private def createImageTilesWithPyramidPartitioning(features: RDD[_ <: IFeature], featuresMBR: Envelope,
                                                      plotterClass: Class[_ <: Plotter], tileWidth: Int,
                                                      tileHeight: Int, partitioner: PyramidPartitioner,
                                                      outputPath: String, opts: UserOptions): Unit = {
    if (partitioner.isEmpty)
      return
    // Assign each feature to all the overlapping tiles and group by tile ID
    val featuresAssignedToTiles: RDD[(Long, IFeature)] = features.flatMap(feature => {
      val mbr = new Envelope(2)
      val matchedPartitions: LongArray = new LongArray()
      val allMatches: ArrayBuffer[(Long, IFeature)] = new ArrayBuffer[(Long, IFeature)]
      feature.getGeometry.envelope(mbr)
      partitioner.overlapPartitions(mbr, matchedPartitions)
      for (i <- 0 until matchedPartitions.size()) {
        val pid: Long = matchedPartitions.get(i)
        allMatches += ((pid, feature))
      }
      allMatches
    })

    val plotter: Plotter = Plotter.getConfiguredPlotter(plotterClass, opts)
    val finalCanvases: RDD[(Long, Canvas)] = featuresAssignedToTiles.groupByKey.flatMap(tidFeatures => {
      val tileIndex: TileIndex = TileIndex.decode(tidFeatures._1, null)
      val granularity: Int = partitioner.getGranularity
      val numLevelsToGenerate: Int = (partitioner.getMaxLevel - tileIndex.z) % granularity + 1
      val pyramid: SubPyramid = new SubPyramid(featuresMBR, tileIndex.z, tileIndex.z + numLevelsToGenerate - 1,
        tileIndex.x << (numLevelsToGenerate - 1), tileIndex.y << (numLevelsToGenerate - 1),
        (tileIndex.x + 1) << (numLevelsToGenerate - 1), (tileIndex.y + 1) << (numLevelsToGenerate - 1))
      val subPartitioner: PyramidPartitioner = new PyramidPartitioner(partitioner, pyramid)

      val tiles = new mutable.HashMap[Long, Canvas]

      createTiles(tidFeatures._2.toIterator, featuresMBR, subPartitioner, tileWidth, tileHeight, plotter, tiles)
      tiles.iterator
    })
    // Write the final canvas to the output
    val conf: Configuration = new Configuration
    conf.setClass(CommonVisualizationHelper.PlotterClassName, plotterClass, classOf[Plotter])
    finalCanvases.saveAsNewAPIHadoopFile(outputPath, classOf[java.lang.Long], classOf[Canvas],
      classOf[PyramidOutputFormat], opts.loadIntoHadoopConfiguration(conf))
  }

  /**
    * A helper function parses a string into range. The string is in the following forms:
    * - 8: Produces the range [0, 8) exclusive of 8, i.e., [0,7]
    * - 3..6' Produces the range [3, 6], inclusive of both 3 and 4
    * - 4...7: Produces the range [4, 7), exclusive of the 7, i.e., [4, 6]
    * @param str the string to parse
    * @return the created range
    */
  def parseRange(str: String): Range = {
    val oneNumber = raw"(\d+)".r
    val inclusiveRange = raw"(\d+)..(\d+)".r
    val exclusiveRange = raw"(\d+)...(\d+)".r
    str match {
      case oneNumber(number) => 0 until number.toInt
      case inclusiveRange(start, end) => start.toInt to end.toInt
      case exclusiveRange(start, end) => start.toInt until end.toInt - 1
      case _ => throw new RuntimeException(s"Unrecognized range format $str. start..end is a supported format")
    }
  }

  def rangeToString(rng: Range): String = if (rng.start == 0) (rng.end+1).toString else s"${rng.start}..${rng.end}"

  /**
    * Run the main function using the given user command-line options and spark context
    *
    * @param opts user options for the operation
    * @param sc the spark context to use
    * @return
    */
  @throws(classOf[IOException])
  override def run(opts: UserOptions, sc: SparkContext): Any = {
    val features: RDD[_ <: IFeature] = SpatialReader.readInput(sc, opts)
    val levels: Range = parseRange(opts.get(MultilevelPyramidPlotHelper.NumLevels, "7"))
    val plotterClass: Class[_ <: Plotter] = Plotter.getPlotterClass(opts.get(CommonVisualizationHelper.PlotterName))
    plotFeatures(features, levels, plotterClass, opts.getInput, opts.getOutput, opts)
  }
}
