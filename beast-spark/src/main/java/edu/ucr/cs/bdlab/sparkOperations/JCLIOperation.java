/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;

/**
  * An interface for a command-line operation that runs from Java
  */
public interface JCLIOperation extends CLIOperation {

  @Override
  default void setup(UserOptions opts) {}

  default Object run(UserOptions opts, SparkContext sc) throws IOException {
    return run(opts, JavaSparkContext.fromSparkContext(sc));
  }

  Object run(UserOptions opts, JavaSparkContext sc) throws IOException;
}
