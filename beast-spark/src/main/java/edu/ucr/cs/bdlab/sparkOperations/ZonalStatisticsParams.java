package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.util.OperationParam;

public final class ZonalStatisticsParams {
  @OperationParam(
      description = "The zonal statistics algorithm {scanline, masking, qsplit, naive}",
      defaultValue = "scanline")
  public static final String ClipperMethod = "method";

  @OperationParam(description = "The statistics function to compute {aggregates}", defaultValue = "aggregates")
  public static final String AggregateFunction = "collector";

  @OperationParam(description = "The name or the index of the raster layer to read from the raster file", defaultValue = "0")
  public static final String RasterLayer = "layer";

  @OperationParam(description = "The format of the raster file {hdf, geotiff}", defaultValue = "hdf")
  public static final String RasterFormat = "rformat";
}
