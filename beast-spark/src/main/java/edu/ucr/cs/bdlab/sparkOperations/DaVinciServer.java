/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import edu.ucr.cs.bdlab.davinci.CommonVisualizationHelper;
import edu.ucr.cs.bdlab.davinci.MultilevelPyramidPlotHelper;
import edu.ucr.cs.bdlab.davinci.TileIndex;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.indexing.MasterFileReader;
import edu.ucr.cs.bdlab.io.*;
import edu.ucr.cs.bdlab.util.CounterOutputStream;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * A web handler that handles visualization methods
 */
public class DaVinciServer extends WebHandler {
  private static final Log LOG = LogFactory.getLog(DaVinciServer.class);

  @OperationParam(
      description = "Enable/Disable server-side caching of generated tiles",
      defaultValue = "true"
  )
  public static final String ServerCache = "cache";

  @OperationParam(
      description = "Path to the directory that contains the plots and data",
      defaultValue = "."
  )
  public static final String DataDirectory = "datadir";

  // The next two configurations allow running the server on data that reside in AWS
  @OperationParam(description = "AWS Access Key ID")
  public static final String AWS_ACCESS_KEY_ID = "fs.s3a.access.key";

  @OperationParam(description = "AWS Access Key ID")
  public static final String AWS_SECRET_ACCESS_KEY = "fs.s3a.secret.key";

  /**User options given at the application startup*/
  protected UserOptions opts;

  /**The file system that contains the data dir*/
  protected FileSystem dataFileSystem;

  /**The Spark context of the underlying application*/
  protected JavaSparkContext sc;

  /**
   * A server-side cache that caches copies of generated tiles to avoid regenerating the same tile for multiple users.
   */
  private Cache<String,byte[]> cache;

  /**Path to the data directory*/
  protected Path datadir;

  @Override
  public void setup(SparkContext sc, UserOptions opts) {
    super.setup(sc, opts);
    this.opts = opts;
    this.sc = new JavaSparkContext(sc);
    if (opts.getBoolean(ServerCache, true)) {
      cache = CacheBuilder.newBuilder()
          .maximumSize(100000) // the cache size is 100,000 tiles
          .expireAfterAccess(30, TimeUnit.DAYS)
          .build();
    }
    this.datadir = new Path(opts.get(DataDirectory, "."));
    try {
      this.dataFileSystem = this.datadir.getFileSystem(opts.loadIntoHadoopConfiguration(sc.hadoopConfiguration()));
    } catch (IOException e) {
      throw new RuntimeException(
              String.format("Cannot retrieve the file system of the data directory '%s'", this.datadir), e);
    }
  }

  /**
   * Returns metadata of the given dataset to create the layer
   * @param datasetID the path to the visualization index
   * @throws IOException
   */
  @WebMethod(url = "/dynamic/metadata.cgi/{datasetID}")
  public boolean handleMetadataRequest(String target,
                                       HttpServletRequest request,
                                       HttpServletResponse response,
                                       String datasetID) throws IOException {
    Path propertiesFile = new Path(new Path(datadir, datasetID), "_visualization.properties");
    UserOptions opts = UserOptions.loadFromTextFile(dataFileSystem, propertiesFile);
    // Write the response as JSON
    response.setStatus(HttpServletResponse.SC_OK);
    response.setContentType("application/json");
    JsonGenerator jsonGenerator = new JsonFactory().createGenerator(response.getOutputStream());
    jsonGenerator.writeStartObject();
    jsonGenerator.writeNumberField("width", opts.getInt(MultilevelPyramidPlotHelper.TileWidth, 256));
    jsonGenerator.writeNumberField("height", opts.getInt(MultilevelPyramidPlotHelper.TileHeight, 256));
    jsonGenerator.writeBooleanField("mercator", opts.getBoolean(CommonVisualizationHelper.UseMercatorProjection, false));
    String levels = opts.get(MultilevelPyramidPlotHelper.NumLevels, "7");
    String[] parts = levels.split("\\.\\.");
    int minLevel, maxLevel;
    if (parts.length == 2) {
      minLevel = Integer.parseInt(parts[0]);
      maxLevel = Integer.parseInt(parts[1]);
    } else {
      minLevel = 0;
      maxLevel = Integer.parseInt(parts[0]) - 1;
    }
    jsonGenerator.writeFieldName("levels");
    jsonGenerator.writeStartArray();
    jsonGenerator.writeNumber(minLevel);
    jsonGenerator.writeNumber(maxLevel);
    jsonGenerator.writeEndArray();
    jsonGenerator.writeEndObject();
    jsonGenerator.close();
    return true;
  }

  @WebMethod(url = "/dynamic/visualize.cgi/{datasetID}")
  public boolean handleVisualizeIndex(String target,
                                      HttpServletRequest request,
                                      HttpServletResponse response,
                                      String datasetID) throws IOException {
    return handleStaticResource(new Path(new Path(datadir, datasetID), "index.html").toString(), request, response);
  }

  /**
   * Returns an image tile that is either materialized on disk, cached in memory, or generated on the fly.
   * @param target
   * @param request
   * @param response
   * @param datasetID
   * @param z
   * @param x
   * @param y
   * @return
   * @throws IOException
   */
  @WebMethod(url = "/dynamic/visualize.cgi/{datasetID}/tile-{z}-{x}-{y}\\.(.+)")
  public boolean getTile(String target,
                         HttpServletRequest request,
                         HttpServletResponse response,
                         String datasetID,
                         int z, int x, int y) throws IOException, InterruptedException {
    long startTime = System.nanoTime();
    byte[] tileData = null;
    String howHandled;
    String tilePath = target.substring(23); // Remove prefix "/dynamic/visualize.cgi/" (23 bytes)
    Path resourcePath = new Path(datadir, tilePath);
    if (this.dataFileSystem.isFile(resourcePath)) {
      howHandled = "static-file";
      handleStaticResource(resourcePath.toString(), request, response);
    } else {
      if (cache != null && (tileData = cache.getIfPresent(tilePath)) != null) {
        // A cached version is found on the server cache. Use it
        howHandled = "server-cached";
      } else {
        // Either no cache is configured or the tile does not exist in the cache
        // Generate the tile or inform the client to use their cached version (if applicable)
        long tileID = TileIndex.encode(z, x, y);

        // Retrieve path to the AID visualization index
        Path pathToAIDIndex = new Path(datadir, datasetID);
        // Create the output stream where the image will be written
        // We need to create the intermediate output stream for two reasons:
        // 1- To be able to set the response headers based on whether the cached version was newer or not.
        // 2- To be able to add it to the cache if a cache is configured
        ByteArrayOutputStream interimOutput = new ByteArrayOutputStream();
        // Extract the timestamp of a possible cached version on the browser to avoid recreating the image when possible
        LongWritable requesterCachedTimestamp = new LongWritable();
        if (request.getHeader("If-Modified-Since") != null)
          requesterCachedTimestamp.set(request.getDateHeader("If-Modified-Since"));
        else
          requesterCachedTimestamp.set(0); // Oldest timestamp ensures sending a new version
        // Call the function that plots the tile from the raw data
        boolean tileGenerated = MultilevelPyramidPlotHelper.plotTile(dataFileSystem, pathToAIDIndex, tileID, interimOutput,
            requesterCachedTimestamp);
        interimOutput.close();
        if (!tileGenerated) {
          // Tile was skipped as it was not modified since the browser cached it
          howHandled = "skipped-client-cached";
          response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
        } else {
          howHandled = "generated on-the-fly";
          tileData = interimOutput.toByteArray();//change interimOutput to byte type which can be used as the value in cache
          // First time returning this tile or a newer version is returned, add to the cache
          cache.put(tilePath,tileData);
        }
      }

      if (tileData != null) {
        // Either a server-side cached version is returned or a tile was generated on the fly
        // TODO: Change the content type depending on the image format (e.g., SVG)
        response.setContentType("image/png");
        response.setStatus(HttpServletResponse.SC_OK);
        OutputStream finalOutput = response.getOutputStream();
        finalOutput.write(tileData);
        finalOutput.close();
      }
    }
    long endTime = System.nanoTime();
    LOG.info(String.format("Requested tile '%s' processed in %f seconds (%s)",
        tilePath, (endTime - startTime) * 1E-9, howHandled));
    return true;
  }


  /**
   * Returns the contents of the master file of an index in GeoJSON format
   * @param datasetID path to the AID index
   * @throws IOException if an error happens while handling the request
   */
  @WebMethod(url = "/dynamic/index.cgi/{datasetID}")
  public boolean handleIndex(String target, HttpServletRequest request, HttpServletResponse response,
                           String datasetID) throws IOException, InterruptedException {
    // Retrieve the URL of the data
    Path indexPath = new Path(datadir, datasetID);
    Path masterFilePath = SpatialInputFormat.getMasterFilePath(dataFileSystem, indexPath);

    if (masterFilePath != null) {
      GeoJSONFeatureWriter writer = new GeoJSONFeatureWriter();
      MasterFileReader reader = new MasterFileReader();
      try {
        long masterFileModificationDate = dataFileSystem.getFileStatus(masterFilePath).getModificationTime();
        reader.initialize(masterFilePath, opts);

        // Set the response header
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/vnd.geo+json");
        response.addDateHeader("Last-Modified", masterFileModificationDate);
        response.addDateHeader("Expires", masterFileModificationDate + OneDay);

        // Prepare the writer
        writer.initialize(response.getOutputStream(), opts);

        final Envelope worldMercatorMBR = new Envelope(2,
            -180.0, -CommonVisualizationHelper.WebMercatorMaxLatitude,
            +180.0, +CommonVisualizationHelper.WebMercatorMaxLatitude);

        // Start writing the partitions
        while (reader.nextKeyValue()) {
          // Ensure that the partition MBR does not go beyond the map boundaries of OSM
          reader.getCurrentValue().shrink(worldMercatorMBR);
          writer.write(reader.getCurrentKey(), reader.getCurrentValue());
        }
      } finally {
        if (reader != null)
          reader.close();
        if (writer != null)
          writer.close(null);
      }
    }
    return true;
  }


  /**
   * Download a subset of the dataset given by its visualization ID.
   * @param target
   * @param request
   * @param response
   * @param datasetID
   * @param format
   * @return
   * @throws IOException
   * @throws InterruptedException
   */
  @WebMethod(url = "/dynamic/download.cgi/{datasetID}\\.{format}", async = true)
  public boolean handleDownload(String target, HttpServletRequest request, HttpServletResponse response,
                             String datasetID, String format)
      throws IOException, InterruptedException, IllegalAccessException, InstantiationException {
    // Retrieve the URL of the data
    Path propertiesFile = new Path(new Path(datadir, datasetID), "_visualization.properties");
    UserOptions opts;
    Path dataPath;
    if (dataFileSystem.exists(propertiesFile)) {
      opts = UserOptions.loadFromTextFile(dataFileSystem, propertiesFile);
      dataPath = new Path(new Path(datadir, datasetID), opts.get("data"));
    } else {
      // Check if the path is for an index
      dataPath = new Path(datadir, datasetID);
      Path masterFile = SpatialInputFormat.getMasterFilePath(dataFileSystem, dataPath);
      if (masterFile == null) {
        reportMissingFile(target, response);
        return true;
      }
      opts = new UserOptions(this.opts);
    }
    // Disable the mercator option because we do not want to convert data to Mercator when downloaded
    opts.setBoolean(SpatialInputFormat.Mercator, false);

    // Initialize the writer and set it to write to the servlet
    Class<? extends FeatureWriter> writerClass;
    if (format.equals("csv")) {
      writerClass = CSVFeatureWriter.class;
      response.setContentType("text/csv");
      if (request.getParameter("point") != null) {
        // Write point data as x,y
        opts.set(SpatialOutputFormat.OutputFormat, "point");
      } else {
        // Write any geometry attribute as WKT-encoded text
        opts.set(SpatialOutputFormat.OutputFormat, "wkt");
      }
      opts.set(CSVFeatureWriter.FieldSeparator, "\t");
      opts.setBoolean(CSVFeatureWriter.WriteHeader, true);
    } else if (format.equals("geojson")) {
      writerClass = GeoJSONFeatureWriter.class;
      response.setContentType("application/geo+json");
    } else if(format.equals("kmz")) {
      writerClass = KMLFeatureWriter.class;
      response.setContentType("application/vnd.google-earth.kmz");
    } else if (format.equals("shp")) {
      writerClass = ShapefileFeatureWriter.class;
      response.setContentType("application/zip");
    } else {
      BeastServer.reportError(response, String.format("Unrecognized format '%s'", format), null);
      return false;
    }
    FeatureWriter featureWriter = writerClass.newInstance();
    response.setStatus(HttpServletResponse.SC_OK);
    CounterOutputStream counterOut;
    String datasetName = datasetID.replace("_plot", "").replaceAll("/", "_");
    String downloadFilename;
    // Only when writing shapefiles, this temporary path stores the written shapefiles
    int partNumber = 0;
    Path tmpDir = null;
    ZipOutputStream zipOut = null;
    ShapefileFeatureWriter shpWriter = null;
    if(writerClass.equals(CSVFeatureWriter.class) || writerClass.equals(GeoJSONFeatureWriter.class)){
      // CSV and GeoJSON files are compressed using gzip
      response.setHeader("HTTP_CONTENT_ENCODING", "gzip");
      downloadFilename = String.format("%s.%s.gz", datasetName, format);
      counterOut = new CounterOutputStream(new GZIPOutputStream(response.getOutputStream()));
      featureWriter.initialize(counterOut, opts);
    } else if (writerClass.equals(KMLFeatureWriter.class)) {
      // KMZ files are compressed using ZIP
      downloadFilename = String.format("%s.kmz", datasetName);

      zipOut = new ZipOutputStream(response.getOutputStream());
      ZipEntry zipEntry = new ZipEntry(String.format("%s.kml",datasetName));
      zipOut.putNextEntry(zipEntry);
      counterOut = new CounterOutputStream(zipOut);
      featureWriter.initialize(counterOut, opts);
    } else if (writerClass.equals(ShapefileFeatureWriter.class)) {
      // Shape file is written in multipart using a temporary location
      tmpDir = new Path(Files.createTempDirectory("shapefiles").toString());
      // Create a writer that creates a shapefile in the temporary directory
      shpWriter = (ShapefileFeatureWriter) featureWriter;
      featureWriter.initialize(new Path(tmpDir, String.format("part-%05d.shp", partNumber++)), conf);
      // Create a ZIP file that writes directly to the response
      counterOut = new CounterOutputStream(response.getOutputStream());
      zipOut = new ZipOutputStream(counterOut);
      downloadFilename = String.format("%s.zip", datasetName);
    } else {
      throw new RuntimeException("Writer not handled"+ writerClass);
    }
    response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", downloadFilename));

    // Now, read all input splits one-by-one and write their contents to the writer
    String mbrString = request.getParameter("mbr");
    Envelope[] mbrs = null;
    if (mbrString != null) {
      Envelope mbr = CSVEnvelopeDecoder.instance.apply(mbrString, null);
      if (mbr.getSideLength(0) > 360.0) {
        mbr.minCoord[0] = -180.0;
        mbr.maxCoord[0] = +180.0;
      } else {
        // Adjust the minimum and maximum longitude to be in the range [-180.0, +180.0]
        mbr.minCoord[0] = mbr.minCoord[0] - 360.0 * Math.floor((mbr.minCoord[0] + 180.0) / 360.0);
        mbr.maxCoord[0] = mbr.maxCoord[0] - 360.0 * Math.floor((mbr.maxCoord[0] + 180.0) / 360.0);
      }
      if (mbr.minCoord[0] > mbr.maxCoord[0]) {
        // The MBR crosses the international day line, split it into two MBRs
        mbrs = new Envelope[2];
        mbrs[0] = new Envelope(2, mbr.minCoord[0], mbr.minCoord[1], +180.0, mbr.maxCoord[1]);
        mbrs[1] = new Envelope(2, -180.0, mbr.minCoord[1], mbr.maxCoord[0], mbr.maxCoord[1]);
      } else {
        // A simple MBR that does not cross the line.
        mbrs = new Envelope[]{mbr};
      }
    }

    int iMBR = 0;
    final long PartitionSize = 256L * 1024 * 1024; // 256 MB
    do {
      if (mbrs != null)
        opts.set(SpatialInputFormat.FilterMBR, CSVEnvelopeEncoder.defaultEncoder.apply(mbrs[iMBR]));
      // Create a fake job to retrieve the splits that overlap this MBR
      Job fakeJob = Job.getInstance(opts);
      SpatialInputFormat.setInputPaths(fakeJob, dataPath);
      SpatialInputFormat inputFormat = new SpatialInputFormat();
      List<InputSplit> splits = inputFormat.getSplits(fakeJob);

      for (InputSplit split : splits) {
        FeatureReader reader = inputFormat.createAndInitRecordReader(split, opts);
        for (IFeature f : reader) {
          if (shpWriter != null && shpWriter.getShapefileSize() >= PartitionSize) {
            assert writerClass.equals(ShapefileFeatureWriter.class);
            // Close the current writer to commit the shapefile
            featureWriter.close(null);
            copyFilesToZIP(dataFileSystem, tmpDir, featureWriter, zipOut);
            // Reinitialize the shapefile to the next part
            featureWriter.initialize(new Path(tmpDir, String.format("part-%05d.shp", partNumber++)), conf);
          }
          featureWriter.write(null, f);
        }
        reader.close();
      }

    } while (mbrs != null && ++iMBR < mbrs.length);
    featureWriter.close(null);
    if (writerClass.equals(ShapefileFeatureWriter.class)) {
      // For shapefiles, do the commit part
      copyFilesToZIP(dataFileSystem, tmpDir, featureWriter, zipOut);
      zipOut.close();
    }
    long numWrittenBytes = counterOut.getCount();
    LOG.info(String.format("Request '%s' resulted in %d bytes", target, numWrittenBytes));
    return true;
  }

  /**
   * Copy all the files in the given directory to the ZIP file in the same name
   * @param fileSystem
   * @param shapefilePath
   * @param featureWriter
   * @param zipOut
   * @throws IOException
   * @throws InterruptedException
   */
  private void copyFilesToZIP(FileSystem fileSystem, Path shapefilePath, FeatureWriter featureWriter,
                              ZipOutputStream zipOut) throws IOException, InterruptedException {
    // Copy all resulting files (typically three) to the ZIP file
    FileStatus[] files = fileSystem.listStatus(shapefilePath, SpatialInputFormat.HiddenFileFilter);
    for (FileStatus fileStatus : files) {
      ZipEntry zipEntry = new ZipEntry(fileStatus.getPath().getName());
      zipOut.putNextEntry(zipEntry);
      FSDataInputStream tempIn = fileSystem.open(fileStatus.getPath());
      IOUtils.copyBytes(tempIn, zipOut, 16 * 1024);
      tempIn.close();
      fileSystem.delete(fileStatus.getPath(), false);
    }
  }
}
