package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.indexing.SpatialPartitioner;
import edu.ucr.cs.bdlab.util.WritableExternalizable;
import org.apache.spark.Partitioner;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * A wrapper around a {@link edu.ucr.cs.bdlab.indexing.SpatialPartitioner} to make it a Spark partitioner.
 */
public class SparkSpatialPartitioner extends Partitioner implements WritableExternalizable {

  /** The internal partitioner */
  protected SpatialPartitioner spatialPartitioner;

  /** A default constructor for deserialization */
  public SparkSpatialPartitioner() {}

  public SparkSpatialPartitioner(SpatialPartitioner spatialPartitioner) {
    this.spatialPartitioner = spatialPartitioner;
  }

  @Override
  public int numPartitions() {
    return spatialPartitioner.getPartitionCount();
  }

  @Override
  public int getPartition(Object key) {
    // They key is an Integer that points to a partition number and we just return it
    // We do this because this method cannot return multiple partitions which is needed for disjoint partitions
    return (Integer) key;
  }

  /**
   * Returns the contained spatial partitioner.
   * @return
   */
  public SpatialPartitioner getSpatialPartitioner() {
    return spatialPartitioner;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeUTF(spatialPartitioner.getClass().getName());
    spatialPartitioner.write(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    try {
      String partitionerClassName = in.readUTF();
      Class<? extends SpatialPartitioner> partitionerClass = Class.forName(partitionerClassName).asSubclass(SpatialPartitioner.class);
      this.spatialPartitioner = partitionerClass.newInstance();
      this.spatialPartitioner.readFields(in);
    } catch (ClassNotFoundException e) {
      throw new RuntimeException("Partitioner class not found", e);
    } catch (IllegalAccessException e) {
      throw new RuntimeException("Cannot access the default constructor", e);
    } catch (InstantiationException e) {
      throw new RuntimeException("Cannot instantiate the spatial partitioner", e);
    }
  }
}
