/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.indexing.GridPartitioner;
import edu.ucr.cs.bdlab.indexing.IndexerParams;
import edu.ucr.cs.bdlab.indexing.PartitionInfo;
import edu.ucr.cs.bdlab.indexing.RSGrovePartitioner;
import edu.ucr.cs.bdlab.indexing.SpatialPartitioner;
import edu.ucr.cs.bdlab.io.CSVFeature;
import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.IntArray;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import scala.Symbol;
import scala.Tuple2;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class IndexTest extends SparkTest {

  public void testBuildIndex() throws IOException {
    Path inputPath = new Path(scratchPath, "input");
    copyResource("/test.rect", new File(inputPath.toString()));
    Path outputPath = new Path(scratchPath, "output.index");
    UserOptions opts = new UserOptions(new String[] {inputPath.toString(), outputPath.toString()});
    opts.assignAllInputsExceptOneOutput();
    opts.set(SpatialInputFormat.InputFormat, "pointk(4)");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(IndexerParams.PartitionCriterionThreshold, "Fixed(4)");
    opts.set(IndexerParams.GlobalIndex, "rsgrove");
    Index.run(opts, getSC().sc());
    FileSystem fileSystem = outputPath.getFileSystem(new Configuration());
    Path masterFilePath = new Path(outputPath, "_master.rsgrove");
    assertTrue("Master file does not exist", fileSystem.exists(masterFilePath));
    // Test the MBR of the global index
    PartitionInfo[] partitions = PartitionInfo.readPartitions(fileSystem, masterFilePath);
    assertEquals(4, partitions[0].getCoordinateDimension());
    Envelope env = new Envelope(4);
    for (PartitionInfo p : partitions) {
      env.merge(p);
    }
    assertEquals(133.0, env.minCoord[0]);
    assertEquals(954.0, env.maxCoord[0]);
    assertEquals(51.0, env.minCoord[3]);
    assertEquals(1000.0, env.maxCoord[3]);
  }

  public abstract static class AbstractPartitioner implements SpatialPartitioner {

    @Override
    public int overlapPartition(Envelope mbr) {
      return 0;
    }

    @Override
    public void overlapPartitions(Envelope mbr, IntArray matchedPartitions) {

    }

    @Override
    public void getPartitionMBR(int partitionID, Envelope mbr) {
    }

    @Override
    public int getPartitionCount() {
      return 0;
    }

    @Override
    public void write(DataOutput out) {
    }

    @Override
    public void readFields(DataInput in) {
    }

    @Override
    public boolean isDisjoint() {
      return false;
    }

    @Override
    public int getCoordinateDimension() {
      return 0;
    }
  }

  @SpatialPartitioner.Metadata(
      disjointSupported = true,
      extension = "sum",
      description = "test"
  )
  public static class SummaryPartitioner extends AbstractPartitioner {

    private Summary summary;

    @Override
    public void construct(Summary summary, double[][] sample, AbstractHistogram histogram) {
      this.summary = summary;
    }

    @Override
    public Envelope getEnvelope() {
      return null;
    }

  }
  @SpatialPartitioner.Metadata(
      disjointSupported = true,
      extension = "smp",
      description = "test"
  )
  public static class SamplePartitioner extends AbstractPartitioner {

    double[] xs, ys;

    @Override
    public void construct(Summary summary, @Required double[][] sample, AbstractHistogram histogram) {
      this.xs = sample[0];
      this.ys = sample[1];
    }

    @Override
    public Envelope getEnvelope() {
      return null;
    }

  }

  public void testCreatePartitioner() {
    List<IFeature> features = Arrays.asList(
        new CSVFeature(new Point(2, 0.0, 1.0)),
        new CSVFeature(new Point(2, 3.0, 2.0)),
        new CSVFeature(new Point(2, 1.0, 7.0))
    );
    JavaRDD<IFeature> rddFeatures = getSC().parallelize(features);
    UserOptions opts = new UserOptions();
    opts.setInt(IndexerParams.SynopsisSize, 100);
    SpatialPartitioner partitioner = Index.createPartitioner(rddFeatures, SummaryPartitioner.class,
        false, "Fixed", 3, opts);
    assertNotNull(partitioner);
    assertEquals(SummaryPartitioner.class, partitioner.getClass());
    assertNotNull(((SummaryPartitioner)partitioner).summary);

    opts.setInt(IndexerParams.SynopsisSize, 100);
    partitioner = Index.createPartitioner(rddFeatures, SamplePartitioner.class,
        false, "Fixed", 3, opts);
    assertNotNull(partitioner);
    assertEquals(SamplePartitioner.class, partitioner.getClass());
    assertNotNull(((SamplePartitioner)partitioner).xs);
    assertTrue("Sample points should be different",
        ((SamplePartitioner)partitioner).xs[0] != ((SamplePartitioner)partitioner).xs[1]);
  }

  public void testCreatePartitionerWithEmptyGeometries() {
    List<IFeature> features = Arrays.asList(
        new CSVFeature(EmptyGeometry.instance),
        new CSVFeature(new Point(2, 3.0, 2.0)),
        new CSVFeature(new Point(2, 1.0, 7.0))
    );
    JavaRDD<IFeature> rddFeatures = getSC().parallelize(features);
    UserOptions opts = new UserOptions();
    opts.setInt(IndexerParams.SynopsisSize, 100);
    SpatialPartitioner partitioner = Index.createPartitioner(rddFeatures, SummaryPartitioner.class,
        false, "Fixed", 3, opts);
    assertNotNull(partitioner);
    assertEquals(SummaryPartitioner.class, partitioner.getClass());
    assertNotNull(((SummaryPartitioner)partitioner).summary);

    opts.setInt(IndexerParams.SynopsisSize, 100);
    partitioner = Index.createPartitioner(rddFeatures, SamplePartitioner.class,
        false, "Fixed", 3, opts);
    assertNotNull(partitioner);
    assertEquals(SamplePartitioner.class, partitioner.getClass());
    assertNotNull(((SamplePartitioner)partitioner).xs);
    assertTrue("Sample points should be different",
        ((SamplePartitioner)partitioner).xs[0] != ((SamplePartitioner)partitioner).xs[1]);
  }

  public void testPartitionFeatures() {
    List<IFeature> features = Arrays.asList(
        new CSVFeature(new Point(2, 0.0, 1.0), new String[] {"1"}, ',', null),
        new CSVFeature(new Point(2, 2.0, 2.0), new String[] {"2"}, ',', null),
        new CSVFeature(new Point(2, 1.5, 3.0), new String[] {"3"}, ',', null),
        new CSVFeature(new Point(2, 1.0, 1.5), new String[] {"4"}, ',', null)
    );
    JavaRDD<IFeature> featuresRDD = getSC().parallelize(features);
    UserOptions clo = new UserOptions();
    clo.setInt(IndexerParams.SynopsisSize, 10);

    GridPartitioner partitioner = (GridPartitioner) Index.createPartitioner(featuresRDD, GridPartitioner.class,
        true, "Fixed", 4, clo);
    assertEquals(2, partitioner.numPartitions[0]);
    assertEquals(2, partitioner.numPartitions[1]);
    JavaPairRDD<Integer, IFeature> partitionedData = Index.partitionFeatures(featuresRDD, partitioner);
    List<Tuple2<Integer, IFeature>> theData = new ArrayList(partitionedData.collect());
    // Sort by attribute value to ensure a consistent and expected order of the results
    theData.sort((f1, f2) -> f1._2.getAttributeValue(0).toString().compareTo(f2._2.getAttributeValue(0).toString()));
    assertEquals(features.size(), theData.size());
    assertEquals(0, (int) theData.get(0)._1);
    assertEquals(3, (int) theData.get(1)._1);
    assertEquals(3, (int) theData.get(2)._1);
    assertEquals(1, (int) theData.get(3)._1);

    assertEquals(features.get(0), theData.get(0)._2);
    assertEquals(features.get(1), theData.get(1)._2);
    assertEquals(features.get(2), theData.get(2)._2);
    assertEquals(features.get(3), theData.get(3)._2);
  }

  public void testPartitionWithReplication() {
    List<IFeature> features = Arrays.asList(
        new CSVFeature(new Envelope(2, 0.5, 0.5, 1.5, 1.5))
    );
    SpatialPartitioner p = new GridPartitioner(new Envelope(2, 0.0, 0.0, 2.0, 2.0), new int[] {2, 2});
    JavaRDD<IFeature> featuresRDD = getSC().parallelize(features);
    JavaPairRDD<Integer, IFeature> partitionedFeatures = Index.partitionFeatures(featuresRDD, p);
    assertEquals(4, partitionedFeatures.count());
  }

  public void testWriteIndex() throws IOException {
    List<IFeature> features = Arrays.asList(
        new CSVFeature(new Point(2, 0, 1)),
        new CSVFeature(new Point(2, 2, 2)),
        new CSVFeature(new Point(2, 1.5, 3))
    );
    JavaRDD<IFeature> featuresRDD = getSC().parallelize(features);
    UserOptions clo = new UserOptions();
    clo.setInt(IndexerParams.SynopsisSize, 10);

    GridPartitioner partitioner = (GridPartitioner) Index.createPartitioner(featuresRDD, GridPartitioner.class,
        true, "Fixed", 4, clo);
    JavaPairRDD<Integer, IFeature> partitionedData = Index.partitionFeatures(featuresRDD, partitioner);
    Path indexPath = new Path(scratchPath, "temp_index");
    // Set output format properties
    SpatialOutputFormat.setOutputFormat(clo, "point(0,1)");
    clo.set("separator", "\t");
    Index.saveIndex(partitionedData, indexPath.toString(), clo);
    String masterFilePath = new Path(indexPath, "_master.grid").toString();
    assertTrue("Master file not found", new File(masterFilePath).exists());
    String[] masterFileInfo = readFile(masterFilePath);
    long totalCount = 0;
    long totalSize = 0;
    for (int i = 1; i < masterFileInfo.length; i++) {
      long count = Long.parseLong(masterFileInfo[i].split("\t")[2]);
      long size = Long.parseLong(masterFileInfo[i].split("\t")[3]);
      totalCount += count;
      totalSize += size;
    }
    assertEquals(3, totalCount);
  }

  public void testWriteIndexWithPolygons() throws IOException {
    List<String> features = Arrays.asList(
        "POLYGON((0 0, 1 1, 2 0, 0 0))",
        "POLYGON((3 5, 4 2, 2 2, 3 5))"
    );
    JavaRDD<String> textRDD = getSC().parallelize(features);
    JavaRDD<IFeature> featuresRDD = SpatialReader.parseWKT(textRDD, 0, '\t');

    UserOptions clo = new UserOptions();
    clo.setInt(IndexerParams.SynopsisSize, 10);
    GridPartitioner partitioner = (GridPartitioner) Index.createPartitioner(featuresRDD, GridPartitioner.class,
        false, "Fixed", 4, clo);
    JavaPairRDD<Integer, IFeature> partitionedData = Index.partitionFeatures(featuresRDD, partitioner);
    Path indexPath = new Path(scratchPath, "temp_index");
    // Set output format properties
    clo.set(SpatialInputFormat.InputFormat, "wkt(0)");
    SpatialOutputFormat.setOutputFormat(clo, "wkt(0)");
    clo.set("separator", "\t");
    Index.saveIndex(partitionedData, indexPath.toString(), clo);
    String masterFilePath = new Path(indexPath, "_master.grid").toString();
    assertTrue("Master file not found", new File(masterFilePath).exists());
    String[] masterFileInfo = readFile(masterFilePath);
    long totalCount = 0;
    long totalSize = 0;
    for (int i = 1; i < masterFileInfo.length; i++) {
      long count = Long.parseLong(masterFileInfo[i].split("\t")[2]);
      long size = Long.parseLong(masterFileInfo[i].split("\t")[3]);
      totalCount += count;
      totalSize += size;
    }
    assertEquals(2, totalCount);
  }

  public void testWritePartitionedIndex() throws IOException {
    int numPoints = 10000;
    List<IFeature> points = new ArrayList<>();
    Random random = new Random(1);
    for (int i = 0; i < numPoints; i++)
      points.add(new CSVFeature(new Point(2, random.nextDouble(), random.nextDouble())));

    JavaRDD<IFeature> featuresRDD = getSC().parallelize(points, 10);

    UserOptions clo = new UserOptions();
    clo.setInt(IndexerParams.SynopsisSize, 10);
    GridPartitioner partitioner = (GridPartitioner) Index.createPartitioner(featuresRDD, GridPartitioner.class,
        false, "Fixed", 4, clo);
    assertEquals(4, partitioner.getPartitionCount());
    JavaPairRDD<Integer, IFeature> partitionedData = Index.partitionFeatures(featuresRDD, partitioner);
    Path indexPath = new Path(scratchPath, "temp_index");
    SpatialOutputFormat.setOutputFormat(clo, "point(0,1)");
    clo.set(CSVFeatureReader.FieldSeparator, "\t");
    Index.saveIndex(partitionedData, indexPath.toString(), clo);
    File masterFile = new File(new Path(indexPath, "_master.grid").toString());
    assertTrue("Master file not found", masterFile.exists());
    String[] lines = readFile(masterFile.toString());
    assertEquals(5, lines.length);
  }

  public void testWriteIndexWithFourDimensions() throws IOException {
    Path inputFile = new Path(scratchPath, "test.rect");
    copyResource("/test.rect", new File(inputFile.toString()));
    UserOptions opts = new UserOptions(inputFile.toString());
    opts.assignAllInputsAndNoOutputs();
    opts.set(SpatialInputFormat.InputFormat, "pointk(4)");
    SpatialOutputFormat.setOutputFormat(opts, "pointk(4)");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.set(CSVFeatureReader.SkipHeader, "false");
    JavaRDD<IFeature> featuresRDD = SpatialReader.readInput(getSC(), opts);
    opts.setInt(IndexerParams.SynopsisSize, 1000);
    SpatialPartitioner partitioner = Index.createPartitioner(featuresRDD, GridPartitioner.class,
        false, "Fixed", 16, opts);
    JavaPairRDD<Integer, IFeature> partitionedData = Index.partitionFeatures(featuresRDD, partitioner);
    Path indexPath = new Path(scratchPath, "temp_index");
    // Write the index to the output
    Index.saveIndex(partitionedData, indexPath.toString(), opts);
    String masterFilePath = new Path(indexPath, "_master.grid").toString();
    assertTrue("Master file not found", new File(masterFilePath).exists());
    String[] masterFileInfo = readFile(masterFilePath);
    long totalCount = 0;
    long totalSize = 0;
    for (int i = 1; i < masterFileInfo.length; i++) {
      String[] parts = masterFileInfo[i].split("\t");
      assertEquals(13, parts.length);
      long count = Long.parseLong(parts[2]);
      long size = Long.parseLong(parts[3]);
      totalCount += count;
      totalSize += size;
      for (int x = 5; x < parts.length; x++)
        assertTrue("Invalid coordinate!", Double.isFinite(Double.parseDouble(parts[x])));
    }
    assertEquals(14, totalCount);
  }

  public void testParsePartitioningCriterion() {
    Tuple2<Symbol, Object> result = Index.parsePartitioningCriterion("Size(128m)");
    assertEquals("size", result._1.name());
    assertEquals(128L * 1024 * 1024, (long)(Long)result._2);
  }

  public void testWriteRSGroveIndexWithFourDimensions() throws IOException {
    Path inputFile = new Path(scratchPath, "test.rect");
    copyResource("/test.rect", new File(inputFile.toString()));

    UserOptions opts = new UserOptions(inputFile.toString(), "iformat:pointk(4)", "separator:,");
    opts.assignAllInputsAndNoOutputs();
    JavaRDD<IFeature> featuresRDD = SpatialReader.readInput(getSC(), opts);
    opts.setInt(IndexerParams.SynopsisSize, 1000);
    SpatialPartitioner partitioner = Index.createPartitioner(featuresRDD, RSGrovePartitioner.class,
        false, "Fixed", 16, opts);
    JavaPairRDD<Integer, IFeature> partitionedData = Index.partitionFeatures(featuresRDD, partitioner);
    Path indexPath = new Path(scratchPath, "temp_index");
    // Write the index to the output
    Index.saveIndex(partitionedData, indexPath.toString(), opts);
    String masterFilePath = new Path(indexPath, "_master.rsgrove").toString();
    assertTrue("Master file not found", new File(masterFilePath).exists());
    String[] masterFileInfo = readFile(masterFilePath);
    long totalCount = 0;
    long totalSize = 0;
    for (int i = 1; i < masterFileInfo.length; i++) {
      String[] parts = masterFileInfo[i].split("\t");
      assertEquals(13, parts.length);
      long count = Long.parseLong(parts[2]);
      long size = Long.parseLong(parts[3]);
      totalCount += count;
      totalSize += size;
      for (int x = 5; x < parts.length; x++)
        assertTrue("Invalid coordinate!", Double.isFinite(Double.parseDouble(parts[x])));
    }
    assertEquals(14, totalCount);
  }

  
  public void testEndToEndIndexing() throws IOException {
    String[] allPartitioners = {"rsgrove"};
    Path inPath = new Path(scratchPath, "test.points");
    copyResource("/test111.points", new File(inPath.toString()));
    FileSystem fs = scratchPath.getFileSystem(new Configuration());
    for (String partitioner : allPartitioners) {
      Path outPath = new Path(scratchPath, "test.index");
      UserOptions clo = new UserOptions(new String[] {inPath.toString(), outPath.toString()});
      clo.assignAndCheckInputsAndOutputs(new int[] {1,1} ,new int[] {1,1});
      fs.delete(outPath, true);
      clo.set(SpatialInputFormat.InputFormat, "pointk(2)");
      SpatialOutputFormat.setOutputFormat(clo, "pointk(2)");
      clo.set(CSVFeatureReader.FieldSeparator, ",");
      clo.set(IndexerParams.GlobalIndex, partitioner);
      clo.set(IndexerParams.PartitionCriterionThreshold, "Fixed(10)");
      clo.set(RSGrovePartitioner.MMRatio, "0.3");
      Index.run(clo, getSC().sc());

      FileSystem fileSystem = outPath.getFileSystem(new Configuration());
      Path masterFilePath = new Path(outPath, "_master."+partitioner);
      assertTrue(String.format("Master file '%s' does not exist", masterFilePath), fileSystem.exists(masterFilePath));
      // Test the MBR of the global index
      PartitionInfo[] partitions = PartitionInfo.readPartitions(fileSystem, masterFilePath);
      assertEquals(2, partitions[0].getCoordinateDimension());
      Envelope env = new Envelope(2);
      for (PartitionInfo p : partitions)
        env.merge(p);
      assertEquals(-122.4850614, env.minCoord[0]);
      assertEquals(139.63086943, env.maxCoord[0]);
      assertEquals(-36.5311761, env.minCoord[1]);
      assertEquals(61.9750933, env.maxCoord[1]);
    }
  }

  public void testEndToEndWithAllIndexersWithTenPartition() throws IOException {
    Set<String> allPartitioners = IndexerParams.partitioners.keySet();
    Path inPath = new Path(scratchPath, "test.points");
    copyResource("/test111.points", new File(inPath.toString()));

    FileSystem fs = scratchPath.getFileSystem(new Configuration());
    for (String partitioner : allPartitioners) {
      try {
        Path outPath = new Path(scratchPath, "test.index");
        UserOptions opts = new UserOptions(new String[] {inPath.toString(), outPath.toString()});
        opts.assignAndCheckInputsAndOutputs(new int[] {1,1}, new int[] {1,1});
        fs.delete(outPath, true);
        opts.set(SpatialInputFormat.InputFormat, "pointk(2)");
        SpatialOutputFormat.setOutputFormat(opts, "pointk(2)");
        opts.set(CSVFeatureReader.FieldSeparator, ",");
        opts.set(IndexerParams.GlobalIndex, partitioner);
        opts.set(IndexerParams.PartitionCriterionThreshold, "Fixed(10)");
        opts.set(RSGrovePartitioner.MMRatio, "0.3");
        Index.run(opts, getSC().sc());

        FileSystem fileSystem = outPath.getFileSystem(new Configuration());
        Path masterFilePath = SpatialInputFormat.getMasterFilePath(fileSystem, outPath);
        assertTrue(String.format("Master file '%s' does not exist", masterFilePath), fileSystem.exists(masterFilePath));
        // Test the MBR of the global index
        PartitionInfo[] partitions = PartitionInfo.readPartitions(fileSystem, masterFilePath);
        assertEquals(2, partitions[0].getCoordinateDimension());
        Envelope env = new Envelope(2);
        for (PartitionInfo p : partitions)
          env.merge(p);
        assertEquals(-122.4850614, env.minCoord[0]);
        assertEquals(139.63086943, env.maxCoord[0]);
        assertEquals(-36.5311761, env.minCoord[1]);
        assertEquals(61.9750933, env.maxCoord[1]);
      } catch (Exception e) {
        throw new RuntimeException(String.format("Error with the index '%s'", partitioner), e);
      }
    }
  }
}