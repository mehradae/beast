/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.davinci.CommonVisualizationHelper;
import edu.ucr.cs.bdlab.davinci.GeometricPlotter;
import edu.ucr.cs.bdlab.davinci.MultilevelPyramidPlotHelper;
import edu.ucr.cs.bdlab.davinci.Plotter;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaRDD;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

public class MultilevelPlotTest extends SparkTest {

  public void testCreateImageTiles() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.point", inputFile);

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:0", "-no-vflip");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-1-0.png",
        "tile-1-1-1.png",
        "tile-2-0-0.png",
        "tile-2-1-0.png",
        "tile-2-3-1.png",
        "tile-2-2-3.png",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);
  }

  public void testPlotGeometries() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.point", inputFile);
    UserOptions opts = new UserOptions();
    opts.setBoolean(CommonVisualizationHelper.VerticalFlip, false);
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    JavaRDD<IFeature> features = SpatialReader.readInput(getSC(), opts, inputFile.toString(), "point");
    JavaRDD<IGeometry> geoms = features.map(f -> f.getGeometry());
    opts.setInt(MultilevelPyramidPlotHelper.DataTileThreshold, 0);
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    MultilevelPlot.plotGeometries(geoms, 0, 2, outputFile.toString(), opts);

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-1-0.png",
        "tile-1-1-1.png",
        "tile-2-0-0.png",
        "tile-2-1-0.png",
        "tile-2-3-1.png",
        "tile-2-2-3.png",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);
  }

  public void testCreateImageTilesWithPartialHistogram() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.point", inputFile);

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:0", "-no-vflip");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.setLong(MultilevelPyramidPlotHelper.MaximumHistogramSize, 4 * 8);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-1-0.png",
        "tile-1-1-1.png",
        "tile-2-0-0.png",
        "tile-2-1-0.png",
        "tile-2-3-1.png",
        "tile-2-2-3.png",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);
  }

  public void testCreateImageTilesWithFlatAndPyramidPartitioning() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.point", inputFile);

    // k=25 means that tiles with three or more records will be generated using flat partitioning
    // Tiles with two or fewer records will be generated using pyramid partitioning.
    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:0", "-no-vflip", "k:25", "pointsize:0");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-1-0.png",
        "tile-1-1-1.png",
        "tile-2-0-0.png",
        "tile-2-1-0.png",
        "tile-2-2-3.png",
        "tile-2-3-1.png",
    };
    for (String expectedFile : expectedFiles) {
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());
      BufferedImage expectedImage = ImageIO.read(new File(outputFile, expectedFile));
      InputStream in = this.getClass().getResourceAsStream("/viztest_mplot/"+expectedFile);
      BufferedImage actualImage = ImageIO.read(in);
      in.close();
      assertImageEquals(expectedImage, actualImage);
    }

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);
  }


  public void testCreateImageTilesWithHigherGranularity() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.point", inputFile);

    // k=25 means that tiles with three or more records will be generated using flat partitioning
    // Tiles with two or fewer records will be generated using pyramid partitioning.
    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:0", "-no-vflip", "k:25", "pointsize:0");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 2);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-1-0.png",
        "tile-1-1-1.png",
        "tile-2-0-0.png",
        "tile-2-1-0.png",
        "tile-2-2-3.png",
        "tile-2-3-1.png",
    };
    for (String expectedFile : expectedFiles) {
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());
      BufferedImage expectedImage = ImageIO.read(new File(outputFile, expectedFile));
      InputStream in = this.getClass().getResourceAsStream("/viztest_mplot/"+expectedFile);
      BufferedImage actualImage = ImageIO.read(in);
      in.close();
      assertImageEquals(expectedImage, actualImage);
    }

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);
  }

  public void testMercatorProjection() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/test-mercator.points", inputFile);

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:0", "-no-vflip",
        "-keep-ratio", "-mercator");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    MultilevelPlot.run(opts, getSC().sc());

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(21, numFiles);
  }

  public void testMercatorProjectionShouldDisableVFlipByDefault() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/test-mercator2.points", inputFile);

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:0", "-mercator", "-no-data-tiles");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-1-0.png",
        "tile-2-0-0.png",
        "tile-2-1-0.png",
        "tile-2-2-0.png",
        "tile-2-3-0.png",
        "tile-2-0-1.png",
        "tile-2-1-1.png",
        "tile-2-2-1.png",
        "tile-2-3-1.png",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);

    FileSystem fs = new Path(outputFile.getPath()).getFileSystem(opts);
    UserOptions opts2 = UserOptions.loadFromTextFile(fs, new Path(new Path(outputFile.getPath()),
        "_visualization.properties"));
    assertTrue("Mercator should be set", opts2.getBoolean(SpatialInputFormat.Mercator, false));
  }

  public void testMercatorProjectionShouldHandleOutOfBoundsObjects() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/test-mercator3.points", inputFile);

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:0", "-mercator", "pointsize:0");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    MultilevelPlot.run(opts, getSC().sc());

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(3, numFiles);
  }

  public void testKeepRatio() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.rect");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.rect", inputFile);

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:envelopek(2)", "separator:,", "levels:3", "threshold:0", "-no-vflip",
        "-keep-ratio", "pointsize:0");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "envelopek(2)");
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-1-1.png",
        "tile-2-0-1.png",
        "tile-2-3-2.png",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);
  }

  public void testCreateImageAndDataTiles() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.point", inputFile);

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:16", "-no-vflip");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-1-0.csv",
        "tile-1-1-1.csv",
        "tile-2-0-0.csv",
        "tile-2-1-0.csv",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);

    // Make sure that no additional 'part-' files are in the output
    int numPartFiles = outputFile.listFiles(n -> n.getName().startsWith("part-")).length;
    assertEquals(0, numPartFiles);
  }

  public void testCreateDataTilesWithDifferentInputOutputFormats() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.point", inputFile);

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "oformat:wkt(0)", "levels:3", "threshold:16", "-no-vflip");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "wkt(0)");
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-1-0.csv",
        "tile-1-1-1.csv",
        "tile-2-0-0.csv",
        "tile-2-1-0.csv",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    Path configFile = new Path(outputFile.toString(), "_visualization.properties");
    opts = UserOptions.loadFromTextFile(configFile.getFileSystem(opts), configFile);
    assertEquals("wkt(0)", opts.get(SpatialInputFormat.InputFormat));
    assertEquals(",", opts.get(CSVFeatureReader.FieldSeparator));
  }

  public void testSkipDataTilesAndReuseIndex() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.point", inputFile);

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:16", "-no-vflip", "-no-data-tiles");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[] {1, 1}, new int[] {1, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    // Make sure that there are no additional tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);

    // Read the configuration file
    Path confFile = new Path(outputFile.getPath(), "_visualization.properties");
    opts = UserOptions.loadFromTextFile(confFile.getFileSystem(new Configuration()), confFile);
    assertEquals("../in.point", opts.get("data"));
  }

  public void testSkipDataTilesAndReuseIndexWithPlotterClass() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/viztest.point", inputFile);
    UserOptions opts = new UserOptions();
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);

    // Read the features in the input dataset
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    JavaRDD<IFeature> input = SpatialReader.readInput(getSC(), opts, inputFile.getPath(), "point");

    opts.setBoolean(MultilevelPyramidPlotHelper.IncludeDataTiles, false);
    opts.setInt(MultilevelPyramidPlotHelper.DataTileThreshold, 16);
    opts.setBoolean(CommonVisualizationHelper.VerticalFlip, false);
    MultilevelPlot.plotFeatures(input, 0, 2, GeometricPlotter.class, inputFile.toString(), outputFile.toString(), opts);

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    // Make sure that there are no additional tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);

    // Read the configuration file
    Path confFile = new Path(outputFile.getPath(), "_visualization.properties");
    opts = UserOptions.loadFromTextFile(confFile.getFileSystem(new Configuration()), confFile);
    assertEquals("../in.point", opts.get("data"));
    assertEquals(GeometricPlotter.class, opts.getClass(CommonVisualizationHelper.PlotterClassName, null, Plotter.class));
    assertEquals("3", opts.get(MultilevelPyramidPlotHelper.NumLevels));
  }


  public void testCreateImageTilesWithBuffer() throws IOException {
    File inputFile = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");

    PrintWriter infile = new PrintWriter(new FileOutputStream(inputFile));

    try {
      infile.println("0,0");
      infile.println("2,2");
      infile.println("4,4");
    } finally {
      infile.close();
    }

    UserOptions opts = new UserOptions(inputFile.getPath(), outputFile.getPath(),
        "plotter:gplot", "iformat:point", "separator:,", "levels:3", "threshold:0", "-no-vflip", "pointsize:5");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.assignAndCheckInputsAndOutputs(new int[] {1, 1}, new int[] {1, 1});
    MultilevelPlot.run(opts, getSC().sc());

    String[] expectedFiles = {
        "tile-0-0-0.png",
        "tile-1-0-0.png",
        "tile-1-0-1.png",
        "tile-1-1-0.png",
        "tile-1-1-1.png",
        "tile-2-0-0.png",
        "tile-2-1-1.png",
        "tile-2-1-2.png",
        "tile-2-2-1.png",
        "tile-2-2-2.png",
        "tile-2-3-3.png",
    };
    for (String expectedFile : expectedFiles)
      assertTrue(String.format("Expected tile '%s' not found", expectedFile), new File(outputFile, expectedFile).exists());

    // Make sure that there are no extra tiles were created
    int numFiles = outputFile.listFiles(n -> n.getName().matches("tile-\\d+-\\d+-\\d+.*")).length;
    assertEquals(expectedFiles.length, numFiles);
  }
}
