/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;

import java.io.File;
import java.io.IOException;

public class SpatialJoinTest extends SparkTest {
  public void testSimpleSpatialJoin() throws IOException {
    File inputFile1 = new File(scratchPath.toString(), "in.rect");
    File inputFile2 = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/test.rect", inputFile1);
    copyResource("/test2.points", inputFile2);

    UserOptions opts = new UserOptions(inputFile1.getPath(),
        inputFile2.getPath(), "iformat[0]:envelope", "iformat[1]:point(1,2)", "separator:,",
        "-skipheader[1]", "predicate:contains", outputFile.getPath());
    SpatialOutputFormat.setOutputFormat(opts, "wkt");
    opts.assignAllInputsExceptOneOutput();
    SpatialJoin.run(opts, getSC().sc());

    assertFileExists(outputFile.getPath());
    String[] lines = readFilesInDirAsLines(outputFile);
    assertEquals(2, lines.length);
  }

  public void testSpatialJoinMoreGeometries() throws IOException {
    File inputFile1 = new File(scratchPath.toString(), "in1");
    File inputFile2 = new File(scratchPath.toString(), "in2");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/sjoinr.wkt", inputFile1);
    copyResource("/sjoins.wkt", inputFile2);

    UserOptions opts = new UserOptions(inputFile1.getPath(),
        inputFile2.getPath(), "iformat:wkt", "predicate:intersects", outputFile.getPath());
    SpatialOutputFormat.setOutputFormat(opts, "wkt");
    opts.assignAllInputsExceptOneOutput();
    SpatialJoin.run(opts, getSC().sc());

    assertFileExists(outputFile.getPath());
    String[] lines = readFilesInDirAsLines(outputFile);
    assertEquals(5, lines.length);
  }

  public void testSpatialJoinMoreGeometriesContains() throws IOException {
    File inputFile1 = new File(scratchPath.toString(), "in1");
    File inputFile2 = new File(scratchPath.toString(), "in2");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/sjoinr.wkt", inputFile1);
    copyResource("/sjoins.wkt", inputFile2);

    UserOptions opts = new UserOptions(inputFile2.getPath(),
        inputFile1.getPath(), "iformat:wkt", "predicate:contains", outputFile.getPath());
    SpatialOutputFormat.setOutputFormat(opts, "wkt");
    opts.assignAllInputsExceptOneOutput();
    SpatialJoin.run(opts, getSC().sc());

    assertFileExists(outputFile.getPath());
    String[] lines = readFilesInDirAsLines(outputFile);
    assertEquals(2, lines.length);
  }

  public void testBNLJDuplicateAvoidance() throws IOException {
    File inputFile1 = new File(scratchPath.toString(), "in1");
    File inputFile2 = new File(scratchPath.toString(), "in2");
    File outputFile = new File(scratchPath.toString(), "out");
    copyDirectoryFromResources("/sjoinr.grid", inputFile1);
    copyDirectoryFromResources("/sjoins.grid", inputFile2);

    UserOptions opts = new UserOptions(inputFile1.getPath(),
        inputFile2.getPath(), "iformat:wkt", "predicate:intersects", outputFile.getPath());
    SpatialOutputFormat.setOutputFormat(opts, "wkt");
    opts.assignAllInputsExceptOneOutput();
    SpatialJoin.run(opts, getSC().sc());

    assertFileExists(outputFile.getPath());
    String[] lines = readFilesInDirAsLines(outputFile);
    assertEquals(5, lines.length);
  }

  public void testDistributedJoin() throws IOException {
    File inputFile1 = new File(scratchPath.toString(), "in.rect");
    File inputFile2 = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/test.rect", inputFile1);
    copyResource("/test2.points", inputFile2);

    UserOptions opts = new UserOptions(inputFile1.getPath(),
        inputFile2.getPath(), "iformat[0]:envelope", "iformat[1]:point(1,2)", "separator:,",
        "-skipheader[1]", "predicate:contains", "method:dj", outputFile.getPath());
    SpatialOutputFormat.setOutputFormat(opts, "wkt");
    SpatialJoin.run(opts, getSC().sc());

    assertFileExists(outputFile.getPath());
    String[] lines = readFilesInDirAsLines(outputFile);
    assertEquals(2, lines.length);
  }

  public void testDistributedJoinDuplicateAvoidance() throws IOException {
    File inputFile1 = new File(scratchPath.toString(), "in1");
    File inputFile2 = new File(scratchPath.toString(), "in2");
    File outputFile = new File(scratchPath.toString(), "out");
    copyDirectoryFromResources("/sjoinr.grid", inputFile1);
    copyDirectoryFromResources("/sjoins.grid", inputFile2);

    UserOptions opts = new UserOptions(inputFile1.getPath(),
        inputFile2.getPath(), "iformat:wkt", "predicate:intersects", outputFile.getPath(), "method:dj");
    SpatialOutputFormat.setOutputFormat(opts, "wkt");
    opts.assignAllInputsExceptOneOutput();
    SpatialJoin.run(opts, getSC().sc());

    assertFileExists(outputFile.getPath());
    String[] lines = readFilesInDirAsLines(outputFile);
    assertEquals(5, lines.length);
  }

  public void testWithDupAvoidanceAndSorting() {
    IFeature[] r = {
        new Feature(new Envelope(2, 0.0, 1.0, 1.0, 2.0)),
        new Feature(new Envelope(2, 3.0, 1.0, 5.0, 3.0)),
    };
    IFeature[] s = {
        new Feature(new Envelope(2, 2.0, 0.0, 4.0, 2.0)),
    };
  }


}