/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaRDD;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SpatialReaderTest extends SparkTest {

  public void testParsePoints() {
    String[] textFile = {
        "abc,3,4",
        "def,5,6"
    };
    JavaRDD<String> textRDD = getSC().parallelize(Arrays.asList(textFile));
    JavaRDD<IFeature> pointRDD = SpatialReader.parsePointsXY(textRDD, 1, 2, ',');
    assertEquals(2, pointRDD.count());
    List<IFeature> points = SparkTest.collectClone(pointRDD);
    assertEquals(2, points.size());
    assertEquals(2, ((Point)points.get(0).getGeometry()).coords.length);
    assertEquals(3.0, ((Point)points.get(0).getGeometry()).coords[0]);
    assertEquals(4.0, ((Point)points.get(0).getGeometry()).coords[1]);
    assertEquals(5.0, ((Point)points.get(1).getGeometry()).coords[0]);
    assertEquals(6.0, ((Point)points.get(1).getGeometry()).coords[1]);
  }

  public void testParsePointsXYZ() {
    String[] textFile = {
        "abc,3,4,1",
        "def,5,6,2"
    };
    JavaRDD<String> textRDD = getSC().parallelize(Arrays.asList(textFile));
    JavaRDD<IFeature> pointRDD = SpatialReader.parsePointsXYZ(textRDD, 1, 2, 3, ',');
    assertEquals(2, pointRDD.count());
    List<IFeature> points = SparkTest.collectClone(pointRDD);
    assertEquals(2, points.size());
    assertEquals(3, ((Point)points.get(0).getGeometry()).coords.length);
    assertEquals(3.0, ((Point)points.get(0).getGeometry()).coords[0]);
    assertEquals(4.0, ((Point)points.get(0).getGeometry()).coords[1]);
    assertEquals(1.0, ((Point)points.get(0).getGeometry()).coords[2]);
    assertEquals(5.0, ((Point)points.get(1).getGeometry()).coords[0]);
    assertEquals(6.0, ((Point)points.get(1).getGeometry()).coords[1]);
    assertEquals(2.0, ((Point)points.get(1).getGeometry()).coords[2]);
  }

  public void testParsePointsXYM() {
    String[] textFile = {
        "abc,3,4,1",
        "def,5,6,2"
    };
    JavaRDD<String> textRDD = getSC().parallelize(Arrays.asList(textFile));
    JavaRDD<IFeature> pointRDD = SpatialReader.parsePointsXYM(textRDD, 1, 2, 3, ',');
    assertEquals(2, pointRDD.count());
    List<IFeature> points = SparkTest.collectClone(pointRDD);
    assertEquals(2, points.size());
    assertEquals(4, ((Point)points.get(0).getGeometry()).coords.length);
    assertEquals(3.0, ((Point)points.get(0).getGeometry()).coords[0]);
    assertEquals(4.0, ((Point)points.get(0).getGeometry()).coords[1]);
    assertTrue(Double.isNaN(((Point)points.get(0).getGeometry()).coords[2]));
    assertEquals(1.0, ((Point)points.get(0).getGeometry()).coords[3]);
    assertEquals(5.0, ((Point)points.get(1).getGeometry()).coords[0]);
    assertEquals(6.0, ((Point)points.get(1).getGeometry()).coords[1]);
    assertEquals(2.0, ((Point)points.get(1).getGeometry()).coords[3]);
  }

  public void testParseFeatureAttributes() {
    String[] textFile = {
        "abc,3,4",
        "defg,5,6"
    };
    JavaRDD<String> textRDD = getSC().parallelize(Arrays.asList(textFile));
    JavaRDD<IFeature> pointRDD = SpatialReader.parsePointsXY(textRDD, 1, 2, ',');
    List<IFeature> features = SparkTest.collectClone(pointRDD);
    assertEquals("abc", features.get(0).getAttributeValue(0));
    assertEquals("defg", features.get(1).getAttributeValue(0));
  }

  public void testParseFeatureAttributes2() {
    String[] textFile = {
        "abc,3,def,4,ghi",
    };
    JavaRDD<String> textRDD = getSC().parallelize(Arrays.asList(textFile));
    JavaRDD<IFeature> pointRDD = SpatialReader.parsePointsXY(textRDD, 1, 3, ',');
    List<IFeature> features = SparkTest.collectClone(pointRDD);
    assertEquals(3, features.get(0).getNumAttributes());
    assertEquals("abc", features.get(0).getAttributeValue(0));
    assertEquals("def", features.get(0).getAttributeValue(1));
    assertEquals("ghi", features.get(0).getAttributeValue(2));
  }

  public void testParseWKT() {
    String[] textFile = {
        "POINT(3 4)",
        "POINT(5 6)"
    };
    JavaRDD<String> textRDD = getSC().parallelize(Arrays.asList(textFile));
    JavaRDD<IFeature> geometryRDD = SpatialReader.parseWKT(textRDD, 0, '\t');
    assertEquals(2, geometryRDD.count());
    List<IFeature> features = SparkTest.collectClone(geometryRDD);
    assertEquals(2, features.size());
    assertEquals(3.0, ((Point)features.get(0).getGeometry()).coords[0], 1E-5);
    assertEquals(4.0, ((Point)features.get(0).getGeometry()).coords[1], 1E-5);
  }

  public void testParseQuotedWKT() {
    String[] textFile = {
        "abc,'POINT(3 4)',def",
        "abc,\"POINT(5 6)\",def"
    };
    JavaRDD<String> textRDD = getSC().parallelize(Arrays.asList(textFile));
    JavaRDD<IFeature> geometryRDD = SpatialReader.parseWKT(textRDD, 1, ',');
    assertEquals(2, geometryRDD.count());
    List<IFeature> features = SparkTest.collectClone(geometryRDD);
    assertEquals(2, features.size());
    assertEquals(3.0, ((Point)features.get(0).getGeometry()).coords[0], 1E-5);
    assertEquals(4.0, ((Point)features.get(0).getGeometry()).coords[1], 1E-5);
  }

  public void testReadShapefile() throws IOException {
    File inputFile = new File(new Path(scratchPath, "test.zip").toString());
    copyResource("/usa-major-cities.zip", inputFile);

    UserOptions opts = new UserOptions();
    JavaRDD<IFeature> input = SpatialReader.readInput(getSC(), opts, inputFile.toString(), "shapefile");
    assertEquals(120, input.count());
  }

  public void testReadKDimensionalPoints() throws IOException {
    File inputFile = new File(new Path(scratchPath, "test.csv").toString());
    copyResource("/test_points.csv", inputFile);

    // Set the shape to three-dimensional points with the default column indexes, i.e., 0, 1, and 2
    UserOptions clo = new UserOptions();
    clo.set(CSVFeatureReader.SkipHeader, "true");
    clo.set(CSVFeatureReader.FieldSeparator, ",");
    JavaRDD<IFeature> input = SpatialReader.readInput(getSC(), clo, inputFile.toString(), "pointk(3)");
    assertEquals(2, input.count());
    IFeature first = input.first();
    assertEquals(Point.class, first.getGeometry().getClass());
    assertEquals(3, first.getGeometry().getCoordinateDimension());
  }

  public void testReadEnvelopes() throws IOException {
    File inputFile = new File(new Path(scratchPath, "test.csv").toString());
    copyResource("/test.partitions", inputFile);

    // Set the shape to three-dimensional points with the default column indexes, i.e., 0, 1, and 2
    UserOptions clo = new UserOptions();
    clo.set(CSVFeatureReader.SkipHeader, "true");
    clo.set(CSVFeatureReader.FieldSeparator, "\t");
    JavaRDD<IFeature> input = SpatialReader.readInput(getSC(), clo, inputFile.toString(), "envelopek(2,5)");
    assertEquals(44, input.count());
    Envelope firstEnvelope = (Envelope) input.first().getGeometry();
    assertEquals(125.6258494, firstEnvelope.minCoord[0]);
  }

  public void testReadTwoFilesWithDifferentFormats() throws IOException {
    File inputFile1 = new File(new Path(scratchPath, "test1.csv").toString());
    copyResource("/test.rect", inputFile1);
    File inputFile2 = new File(new Path(scratchPath, "test2.csv").toString());
    copyResource("/test111.points", inputFile2);

    UserOptions opts = new UserOptions(inputFile1.toString(), inputFile2.toString(), "iformat[0]:envelope",
        "iformat[1]:point");
    opts.assignAndCheckInputsAndOutputs(new int[] {2,2}, new int[] {0,0});
    opts.set(CSVFeatureReader.FieldSeparator, ",");

    JavaRDD<IFeature> rectangles = SpatialReader.readInput(getSC(), opts, 0);
    JavaRDD<IFeature> points = SpatialReader.readInput(getSC(), opts, 1);
    assertEquals(14, rectangles.count());
    assertEquals(GeometryType.ENVELOPE, rectangles.first().getGeometry().getType());
    assertEquals(new Envelope(2, 913.0, 16.0, 924.0, 51.0), rectangles.first().getGeometry());
    assertEquals(111, points.count());
    assertEquals(new Point(101.7075756, 3.2407152), points.first().getGeometry());
  }

  public void testReadWithGivenFormat() throws IOException {
    File inputFile1 = new File(new Path(scratchPath, "test1.csv").toString());
    copyResource("/test.rect", inputFile1);
    File inputFile2 = new File(new Path(scratchPath, "test2.csv").toString());
    copyResource("/test111.points", inputFile2);

    UserOptions opts = new UserOptions();
    opts.set(CSVFeatureReader.FieldSeparator, ",");

    JavaRDD<IFeature> rectangles = SpatialReader.readInput(getSC(), opts, inputFile1.getPath(), "envelope");
    JavaRDD<IFeature> points = SpatialReader.readInput(getSC(), opts, inputFile2.getPath(), "point");
    assertEquals(14, rectangles.count());
    assertEquals(GeometryType.ENVELOPE, rectangles.first().getGeometry().getType());
    assertEquals(new Envelope(2, 913.0, 16.0, 924.0, 51.0), rectangles.first().getGeometry());
    assertEquals(111, points.count());
    assertEquals(new Point(101.7075756, 3.2407152), points.first().getGeometry());
  }

  public void testReadFileWithEmptyRecordsFromIndex() throws IOException {
    Path indexPath = new Path(scratchPath, "index");
    copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
    Path filePath = new Path(indexPath, "part-00.csv");
    UserOptions opts = new UserOptions();
    opts.set(CSVFeatureReader.FieldSeparator, ";");
    JavaRDD<IFeature> features = SpatialReader.readInput(getSC(), opts, filePath.toString(), "wkt");
    assertEquals(3, features.count());
  }
}
