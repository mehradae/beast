/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaRDD;

import java.io.*;
import java.util.Arrays;

public class GeometricSummaryTest extends SparkTest {

  public static class GeomFeature implements IFeature {

    private IGeometry geometry;

    public GeomFeature() {this.geometry = null;}

    public GeomFeature(IGeometry geom) {
      this.geometry = geom;
    }

    @Override
    public IGeometry getGeometry() {
      return geometry;
    }

    @Override
    public void setGeometry(IGeometry geometry) {
      this.geometry = geometry;
    }

    @Override
    public Object getAttributeValue(int i) {
      return null;
    }

    @Override
    public Object getAttributeValue(String name) {
      return null;
    }

    @Override
    public int getNumAttributes() {
      return 0;
    }

    @Override
    public String getAttributeName(int i) {
      return null;
    }

    @Override
    public int getStorageSize() {
      return geometry.getGeometryStorageSize();
    }

    @Override
    public void write(DataOutput out) throws IOException {
      out.writeInt(geometry.getType().ordinal());
      geometry.write(out);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
      this.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
      GeometryType type = GeometryType.values()[in.readInt()];
      if (geometry == null || geometry.getType() != type)
        geometry = type.createInstance();
      geometry.readFields(in);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
      this.readFields(in);
    }
  }

  public void testComputeWithAccumulator() {
    IFeature[] points = {
        new GeomFeature(new Point(2, 0.0, 0.0)),
        new GeomFeature(new Point(2, 3.0, 1.0)),
        new GeomFeature(new Point(2, 2.0, 10.0)),
    };
    GeometricSummary.SummaryAccumulator accum = GeometricSummary.createSummaryAccumulator(getSC().sc());
    JavaRDD<IFeature> features = getSC().parallelize(Arrays.asList(points));
    features = features.map(f -> {accum.add(f); return f;});
    // Force running the accumulator by calling any action
    features.count();
    Summary mbr = accum.value();
    assertEquals(3, mbr.getRecordCount());
    assertEquals(0.0, mbr.minCoord[0]);
    assertEquals(0.0, mbr.minCoord[1]);
    assertEquals(3.0, mbr.maxCoord[0]);
    assertEquals(10.0, mbr.maxCoord[1]);
    assertEquals(3L, mbr.getNumPoints());
  }

  public void testComputeWithEmptyGeometriesUsingAccumulator() {
    IFeature[] points = {
        new GeomFeature(EmptyGeometry.instance),
        new GeomFeature(new Point(2, 3.0, 1.0)),
        new GeomFeature(new Point(2, 2.0, 10.0)),
    };
    GeometricSummary.SummaryAccumulator accum = GeometricSummary.createSummaryAccumulator(getSC().sc());
    JavaRDD<IFeature> features = getSC().parallelize(Arrays.asList(points));
    features = features.map(f -> {accum.add(f); return f;});
    // Force running the accumulator by calling any action
    features.count();
    Summary mbr = accum.value();
    assertEquals(3, mbr.getRecordCount());
    assertEquals(2.0, mbr.minCoord[0]);
    assertEquals(1.0, mbr.minCoord[1]);
    assertEquals(3.0, mbr.maxCoord[0]);
    assertEquals(10.0, mbr.maxCoord[1]);
  }

  public void testComputeWithEmptyGeometriesUsingRDD() {
    IFeature[] points = {
        new GeomFeature(EmptyGeometry.instance),
        new GeomFeature(new Point(2, 3.0, 1.0)),
        new GeomFeature(new Point(2, 2.0, 10.0)),
    };
    JavaRDD<IFeature> features = getSC().parallelize(Arrays.asList(points));
    Summary mbr = GeometricSummary.computeForFeaturesJ(features);
    assertEquals(3, mbr.getRecordCount());
    assertEquals(2.0, mbr.minCoord[0]);
    assertEquals(1.0, mbr.minCoord[1]);
    assertEquals(3.0, mbr.maxCoord[0]);
    assertEquals(10.0, mbr.maxCoord[1]);
    assertEquals(2L, mbr.getNumPoints());
  }

  public void testComputeWithOutputSize() {
    String[] wkts = {
        "POINT(1.0 2.0)",
        "LINESTRING(1.0 2.0,3.0 4.0,5.0 6.0)",
        "POLYGON((10.0 20.0,15.0 25.0,10.0 30.0,10.0 20.0))",
    };
    int totalSize = 0;
    for (String wkt : wkts)
      totalSize += wkt.length() + 1;
    UserOptions opts = new UserOptions();
    opts.set(SpatialOutputFormat.OutputFormat, "wkt(0)");
    JavaRDD<String> wktRDD = getSC().parallelize(Arrays.asList(wkts));
    JavaRDD<IFeature> features = SpatialReader.parseWKT(wktRDD, 0, '\0');
    Summary summary = GeometricSummary.computeForFeaturesWithOutputSize(features, opts);
    assertEquals(totalSize, summary.size);
  }

  public void testComputeOutputSizeWithDifferentOutputFormat() throws IOException {
    Path inPath = new Path(scratchPath, "test.rect");
    copyResource("/test.rect", new File(inPath.toString()));
    UserOptions opts = new UserOptions(inPath.toString(), "iformat:envelope", "separator:,", "oformat:rtree");
    opts.assignAllInputsAndNoOutputs();
    Summary summary = (Summary) GeometricSummary.run(opts, getSC().sc());
    assertEquals(14, summary.numFeatures);
    assertTrue("Size should be large", summary.size >= 448);
    // Try again with same input/output format to make sure it's different
    opts = new UserOptions(inPath.toString(), "iformat:envelope", "separator:,");
    opts.assignAllInputsAndNoOutputs();
    summary = (Summary) GeometricSummary.run(opts, getSC().sc());
    assertTrue("Size should be small", summary.size < 400);
  }

  public void testComputeWithOutputSizeWithAccumulator() {
    String[] wkts = {
        "POINT(1.0 2.0)",
        "LINESTRING(1.0 2.0,3.0 4.0,5.0 6.0)",
        "POLYGON((10.0 20.0,15.0 25.0,10.0 30.0,10.0 20.0))",
    };
    int totalSize = 0;
    for (String wkt : wkts)
      totalSize += wkt.length() + 1;
    UserOptions opts = new UserOptions();
    opts.set(SpatialOutputFormat.OutputFormat, "wkt(0)");
    GeometricSummary.SummaryAccumulator accum = GeometricSummary.createSummaryAccumulatorWithWriteSize(getSC().sc(), opts);
    JavaRDD<String> wktRDD = getSC().parallelize(Arrays.asList(wkts));
    JavaRDD<IFeature> features = SpatialReader.parseWKT(wktRDD, 0, '\0');
    features = features.map(f -> {accum.add(f); return f;});
    // Force an action
    features.count();
    assertEquals(totalSize, accum.value().size);
  }
}