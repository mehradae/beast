package edu.ucr.cs.bdlab.sparkOperations

import edu.ucr.cs.bdlab.cg.SpatialJoinAlgorithms.ESJPredicate
import edu.ucr.cs.bdlab.geolite.{Envelope, Feature}
import org.scalatest.FunSuite

class SpatialJoinTest2 extends FunSuite {
  test("SpatialJoinDupAvoidance") {
    val r = List(
      new Feature(new Envelope(2, 10.0, 10.0, 10.1, 10.1)),
      new Feature(new Envelope(2, 3.0, 1.0, 5.0, 3.0))
    )
    val s = List(
      new Feature(new Envelope(2, 2.0, 0.0, 4.0, 2.0))
    )
    val results = SpatialJoin.spatialJoinIntersectsPlaneSweepFeatures(r.toArray, s.toArray,
      new Envelope(2, 2.0, 0.0, 5.0, 3.0), ESJPredicate.Intersects)
    assert(results.size == 1)

  }
}
