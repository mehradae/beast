/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.stsynopses;

import edu.ucr.cs.bdlab.geolite.Envelope;
import org.apache.hadoop.io.Writable;

/**
 * An abstract class for a histogram that can retrieve the total value for a range.
 */
public abstract class AbstractHistogram extends Envelope implements Writable {

  /**
   * Computes the sum of all values in the given range of grid cells.
   * @param minPos the position of the lower corner in grid coordinates
   * @param sizes the size (number of cells) along each dimension
   * @return
   */
  abstract public long getValue(int[] minPos, int[] sizes);

  /**
   * Get the number of partitions along the given dimension
   * @param d
   * @return
   */
  abstract public int getNumPartitions(int d);

  /**
   * Returns the total number of bins in the histogram
   * @return
   */
  public int getNumBins() {
    int numBins = 1;
    for (int d = 0; d < getCoordinateDimension(); d++) {
      numBins *= getNumPartitions(d);
    }
    return numBins;
  }

  /**
   * Returns a unique ID for the bin (cell) that contains the given point
   * @param coord
   * @return
   */
  public int getBinID(double[] coord) {
    assert coord.length == getCoordinateDimension();
    int[] position = new int[coord.length];
    for (int d = 0; d < coord.length; d++) {
      position[d] = (int) Math.floor((coord[d] - this.minCoord[d]) * this.getNumPartitions(d) / this.getSideLength(d));
      position[d] = Math.min(position[d], getNumPartitions(d) - 1);
    }
    int d = position.length;
    int pos = 0;
    while (d-- > 0) {
      pos *= getNumPartitions(d);
      pos += position[d];
    }
    return pos;
  }

  /**
   * Returns the value of the given binID
   * @param binID
   * @return
   */
  public abstract long getBinValue(int binID);
}
